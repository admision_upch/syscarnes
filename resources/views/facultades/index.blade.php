<!DOCTYPE html>
<html>
    <head>
        <title>Laravel DataTables</title>
        <!--link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugin/datatables/dataTables.bootstrap.css"-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

        <link href="https://datatables.yajrabox.com/css/app.css" rel="stylesheet">
        <link href="https://datatables.yajrabox.com/css/demo.css" rel="stylesheet">
        <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,800" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://datatables.yajrabox.com/highlight/styles/zenburn.css">


        <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>


        <link rel="stylesheet" href="{{ asset('css/wizard.css') }}">
        <script src="{{ asset('js/wizard.js') }}"></script>

    </head>
    <body>
        <div class="container">
            <table id="facultadessinu" class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Cod.Facultad - Sinu</th>
                        <th>Facultad - Sinu</th>
                        <th>Cod.Facultad - Sunedu</th>
                        <th>Facultad - Sunedu</th>

                        <th>Editar</th>
                    </tr>
                </thead>
            </table>
        </div>

        <script type="text/javascript">
$(document).ready(function () {
    oTable = $('#facultadessinu').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('datatable.facultadessinu') }}",
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'codfacultad_sinu', name: 'codfacultad_sinu'},
            {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu'},
            {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
            {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});


function editar(val) {
    alert('ddd');
    alert(val);

}

        </script>
    </body>
</html>