<div class="row widget-row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat  blue" href="#">
            <div class="visual">
                <i class="fa fa-credit-card"></i>

            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" id="total_carnes_universitarios" data-value="{{ $total_carnes_universitarios }}">{{ $total_carnes_universitarios }}</span>
                </div>
                <div class="desc"> Carnés Universitarios </div>

            </div>
        </a>
    </div>
    <div id="carnes_universitarios_detalle">
        @foreach($carnes_universitarios as $item)
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 bordered">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <small class="font-blue-sharp">{{ $item->facultad }}</small>
                            <span data-counter="counterup" data-value="{{ $item->cantidad_total }}">{{ $item->cantidad_total }}</span>

                        </h3>
                        @if ( $item->cantidad_pregrado )
                        <small>Pregrado:  <span data-counter="counterup" data-value="{{ $item->cantidad_pregrado }}">{{ $item->cantidad_pregrado }}</span></small><br>
                        @endif
                        @if ( $item->cantidad_postgrado )
                        <small>Postgrado: <span data-counter="counterup" data-value="{{ $item->cantidad_postgrado }}">{{ $item->cantidad_postgrado }}</span></small>
                        @endif
                        @if (  $item->cantidad_pregrado ==0 && $item->cantidad_postgrado ==0  )
                        <br>
                        @endif
                        
                        
                        

                    </div>

                </div>
                <div class="progress-info">
                    <div class="progress">

                        <?php
                        if ($total_carnes_universitarios) {
                            $porcentaje = number_format((($item->cantidad_total / $total_carnes_universitarios) * 100), 2, '.', ',');
                        } else {
                            $porcentaje = 0;
                        }
                        ?>

                        <span style="width: {{ $porcentaje }}%;" class="progress-bar progress-bar-success red-haze">
                            <span class="sr-only">{{ $porcentaje }}% Porcentaje</span>
                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"> Porcentaje </div>
                        <div class="status-number"> {{ $porcentaje }}% </div>
                    </div>
                </div>
            </div>
        </div>


        @endforeach
    </div>
</div>

<hr style="background-color: #fff;border-top: 2px dashed #8c8b8b;">

<div class="row widget-row">
    
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a class="dashboard-stat  red" href="#">
            <div class="visual">
                <i class="fa fa-credit-card"></i>

            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" id="total_carnes_universitarios" data-value="{{ $total_fotochecks_solicitados }}"> {{ $total_fotochecks_solicitados }}</span>
                </div>
                <div class="desc"> Fotochecks Solicitados </div>

            </div>
        </a>
    </div> 
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_generados) {
                                        $porcentaje_generado = number_format((($total_fotochecks_generados / $total_fotochecks_solicitados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_generado = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-purple-soft">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_generados }}">{{ $total_fotochecks_generados }}</span>
                                            <small class="font-purple-soft"></small>
                                        </h3>
                                        <small>Fotochecks Generados</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_generado }}%;" class="progress-bar progress-bar-success purple-soft">
                                            <span class="sr-only">{{ $porcentaje_generado }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_generado }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_entregados_facultad) {
                                        $porcentaje_fotochecks_entregados_facultad = number_format((($total_fotochecks_entregados_facultad / $total_fotochecks_solicitados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_fotochecks_entregados_facultad = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_entregados_facultad }}">{{ $total_fotochecks_entregados_facultad }}</span>
                                            <small class="font-blue-sharp"></small>
                                        </h3>
                                        <small>Fotochecks Entregados a Facultad</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_fotochecks_entregados_facultad }}%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">{{ $porcentaje_fotochecks_entregados_facultad }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_fotochecks_entregados_facultad }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_entregados_estudiante) {
                                        $porcentaje_fotochecks_entregados_estudiante = number_format((($total_fotochecks_entregados_estudiante / $total_fotochecks_solicitados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_fotochecks_entregados_estudiante = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_entregados_estudiante }}">{{ $total_fotochecks_entregados_estudiante }}</span>
                                            <small class="font-green-sharp"></small>
                                        </h3>
                                        <small>Fotochecks Entregados a Estudiante</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_fotochecks_entregados_estudiante }}%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">{{ $porcentaje_fotochecks_entregados_estudiante }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_fotochecks_entregados_estudiante }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
    



    
  
    <div id="fotochecks_detalle">
        @foreach($fotochecks as $item)
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 bordered">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <small class="font-blue-sharp">{{ $item->facultad }}</small>

                            <span data-counter="counterup" data-value="{{ $item->solicitados }}">{{ $item->solicitados }}</span>


                        </h3>

                        @if($item->codfacultad==322 || $item->codfacultad==998 )
                        <br>
                        <small>Cantidad:  <span data-counter="counterup" data-value="{{ $item->solicitado_pregrado }}">{{ $item->solicitado_pregrado }}</span></small><br>
                        @else
                        <small>Pregrado:  <span data-counter="counterup" data-value="{{ $item->solicitado_pregrado }}">{{ $item->solicitado_pregrado }}</span></small><br>
                        <small>Postgrado: <span data-counter="counterup" data-value="{{ $item->solicitado_postgrado }}">{{ $item->solicitado_postgrado }}</span></small>
                        @endif

                    </div>
                </div>

                <?php
                if ($total_fotochecks_solicitados) {
                    $porcentaje = number_format((($item->solicitados / $total_fotochecks_solicitados) * 100), 2, '.', ',');
                } else {
                    $porcentaje = 0;
                }
                ?>

                <div class="progress-info">
                    <div class="progress">
                        <span style="width: {{ $porcentaje }}%;" class="progress-bar progress-bar-success red-haze">
                            <span class="sr-only">{{ $porcentaje }}% Porcentaje</span>
                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"> Porcentaje </div>
                        <div class="status-number"> {{ $porcentaje }}% </div>
                    </div>
                </div>

            </div>
        </div>


        @endforeach
    </div>
</div>





<div class="row widget-row">
    
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a class="dashboard-stat  red" href="#">
            <div class="visual">
                <i class="fa fa-copy"></i>

            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"  data-value="{{ $total_fotochecks_solicitados_duplicados }}"> {{ $total_fotochecks_solicitados_duplicados }}</span>
                </div>
                <div class="desc"> Fotochecks Solicitados (Duplicados) </div>

            </div>
        </a>
    </div> 
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_generados_duplicados) {
                                        $porcentaje_generado_duplicados = number_format((($total_fotochecks_generados_duplicados / $total_fotochecks_solicitados_duplicados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_generado_duplicados = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-purple-soft">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_generados_duplicados }}">{{ $total_fotochecks_generados_duplicados }}</span>
                                            <small class="font-purple-soft"></small>
                                        </h3>
                                        <small>Fotochecks Generados <br>(Duplicados)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-copy"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_generado_duplicados }}%;" class="progress-bar progress-bar-success purple-soft">
                                            <span class="sr-only">{{ $porcentaje_generado_duplicados }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_generado_duplicados }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_entregados_facultad_duplicados) {
                                        $porcentaje_fotochecks_entregados_facultad_duplicados = number_format((($total_fotochecks_entregados_facultad_duplicados / $total_fotochecks_solicitados_duplicados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_fotochecks_entregados_facultad_duplicados = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_entregados_facultad_duplicados }}">{{ $total_fotochecks_entregados_facultad_duplicados }}</span>
                                            <small class="font-blue-sharp"></small>
                                        </h3>
                                        <small>Fotochecks Entregados a Facultad <br>(Duplicados)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-copy"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_fotochecks_entregados_facultad_duplicados }}%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">{{ $porcentaje_fotochecks_entregados_facultad_duplicados }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_fotochecks_entregados_facultad_duplicados }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                   <?php
                                    if ($total_fotochecks_entregados_estudiante_duplicados) {
                                        $porcentaje_fotochecks_entregados_estudiante_duplicados = number_format((($total_fotochecks_entregados_estudiante_duplicados / $total_fotochecks_solicitados_duplicados) * 100), 2, '.', ',');
                                    } else {
                                        $porcentaje_fotochecks_entregados_estudiante_duplicados = 0;
                                    }
                                    ?>
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="{{ $total_fotochecks_entregados_estudiante_duplicados }}">{{ $total_fotochecks_entregados_estudiante_duplicados }}</span>
                                            <small class="font-green-sharp"></small>
                                        </h3>
                                        <small>Fotochecks Entregados a Estudiante <br>(Duplicados)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-copy"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{ $porcentaje_fotochecks_entregados_estudiante_duplicados }}%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">{{ $porcentaje_fotochecks_entregados_estudiante_duplicados }}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Progreso </div>
                                        <div class="status-number"> {{ $porcentaje_fotochecks_entregados_estudiante_duplicados }}% </div>
                                    </div>
                                </div>
                            </div>
        </div>
        </div>
    