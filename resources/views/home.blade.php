@extends ('layouts.index')         




@section('content')   
<div class="m-heading-1 border-green m-bordered">
    <h2 > Bienvenidos al <img style="margin:10px 10px 0;" width="130" height="120" src="{{ asset('/img/logo/sedi.png') }}" alt="logo" class="logo-default" /> Sistema de Emisión de Documentos de Identificación </h2>
</div>
<div class="m-heading-1 border-green m-bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase">
                @yield('title2')
            </span>
        </div>
        <div class="tools"> </div>
    </div>
    <div class="portlet-body">

        <!-- BEGIN PAGE BASE CONTENT -->


        <div class="row">
            <div class="form-group ">

                <div class="col-md-4">
                    <div id="reportrange" class="btn default">
                        <i class="fa fa-calendar"></i> &nbsp;
                        <span> </span>
                        <b class="fa fa-angle-down"></b>
                    </div>
                </div>
            </div>

        </div>
        <br>

        <div id="home_detail">
           @include('home_detail', ['carnes_universitarios' => $carnes_universitarios,'fotochecks'=>$fotochecks,'total_carnes_universitarios'=>$total_carnes_universitarios,'total_fotochecks_entregados'=>$total_fotochecks_entregados])
        </div>
        <!-- END PAGE BASE CONTENT -->

    </div>

</div>

@endsection
@section('js')
<script type="text/javascript">

    $(document).ready(function () {
        var anio = (new Date).getFullYear();
        $('#reportrange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            //minDate: '01/01/2012',
            //maxDate: '12/31/2014',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Hoy': [moment(), moment()],
                'Primer Semestre': ['01/01/' + anio, '07/31/' + anio],
                'Segundo Semestre': ['08/01/' + anio, '12/31/' + anio],
                'Año Actual': ['01/01/' + anio, moment().subtract('days', 0)],
                'Año Anterior': ['01/01/' + (anio - 1), '12/31/' + (anio - 1)],
                'Ultimos 7 días': [moment().subtract('days', 6), moment()],
                'Ultimos 30 días': [moment().subtract('days', 30), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Ultimo mes': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            language: 'es',
            locale: {
                applyLabel: 'Buscar',
                cancelLabel: 'Cancelar',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Indicar Fechas',
                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        },
                function (start, end) {
                    //alert(start.format('DD/MM/YYYY'));
                    //
                    $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                    cambiarFechas(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
                }
        );

    });
    function cambiarFechas(fecha_inicio, fecha_fin) {
        //var anio = $("#anio").val();
        $.post("{{ route('home.detalle.carnes.universitarios')}}",
                {
                    fecha_inicio: fecha_inicio,
                    fecha_fin: fecha_fin,
                    "_token": "{{ csrf_token() }}"
                }).done(function (data) {
            $("#home_detail").html(data);
        }).fail(function (data) {
            $("#home_detail").html(data);
        });
    }

</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{ asset('/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>




@endsection


