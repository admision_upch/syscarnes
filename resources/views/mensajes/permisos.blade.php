@extends ('layouts.index')    
@section('css')
@yield('css2')
@endsection 
@section('content')   
     <span class="caption-subject bold uppercase">
                <div class="note note-danger">
                    <span class="label label-danger">PERMISO DENEGADO!</span>
                    <span class="bold">Usted no tiene permisos para ingresar a esta parte del sistema</span> 
                </div>
            </span>
@section('js')
@yield('js2')
@endsection
@endsection


