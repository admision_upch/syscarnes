@extends ('layouts.index')    
@section('css')
@yield('css2')
@endsection 
@section('content')   
     <span class="caption-subject bold uppercase">
                <div class="note note-danger">
                    <span class="label label-danger">REGISTRO NO ENCONTRADO!</span>
                    <span class="bold">El registro buscado no ha sido encontrado en nuestras bases de datos.</span> 
                </div>
            </span>
@section('js')
@yield('js2')
@endsection
@endsection


