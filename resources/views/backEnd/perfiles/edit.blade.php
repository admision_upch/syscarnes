@extends('backLayout.app')
@section('title2')
<h1>Editar Perfil</h1>
@endsection
@section('content2')
    <hr/>
{!! Form::model($perfile, [
'method' => 'PATCH',
'url' => ['perfiles', $perfile->id],
'class' => 'form-horizontal'
]) !!}

<div class="form-group {{ $errors->has('sistema_id') ? 'has-error' : ''}}">
    {!! Form::label('sistema_id', 'Sistema Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectSistemas !!}
        {!! $errors->first('sistema_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>




@include('componentes.submit_reset_form_update')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection