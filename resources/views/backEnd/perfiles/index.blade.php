@extends('backLayout.app')
@section('title2')
<h1>Perfiles <a href="{{ url('perfiles/create') }}" class="btn btn-primary pull-right btn-sm">Agregar Nuevo Perfil</a></h1>
@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblperfiles">
        <thead>
            <tr>
                <th>Sistema</th><th>ID</th><th>Perfil</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($perfiles as $item)
            <tr>
         
                <td>{{ $item->sistema->name }}</td>
                       <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>
                    <a href="{{ url('perfiles/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Actualizar</a> 
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['perfiles', $item->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                    <a href="{{ url('perfiles/' . $item->id . '/accesos') }}" class="btn btn-primary btn-xs">Accesos</a> 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tblperfiles').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection