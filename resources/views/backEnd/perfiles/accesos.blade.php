@extends('backLayout.app')
@section('css2')
<link href="{{ asset('/assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('title2')
<h1>Editar Opciones</h1>
@endsection
@section('content2')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">{{$perfil->name}} ( {{$perfil->sistema->name}} )</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="jstree_accesos" class="tree-demo"> </div>
    </div>
    <br>
    <hr>
    <button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>
</div>

<hr/>
@endsection
@section('js2')
<script src="{{ asset('/assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js') }}" type="text/javascript"></script>

<script type="text/javascript">

            $(function () {

            $('#jstree_accesos').jstree({
            'core': {
            'data': {
            "url": "{{ route('accesos_tree', ['id' => $perfil->id]) }}",
                "dataType": "json",
                    'data': function (node) {
                    return {'id': node.id};
                    }
            },
                    'check_callback': true,
                    'themes': {
                    'responsive': false
                    }
            },   
                    'force_text': true,
                    'plugins': ['checkbox']
            })
                    .on('changed.jstree', function (e, data) {
                      //   alert('nodo padre:'+data.node.parent);
               //  alert( data.selected.join(':'));
                     $.post("{{ route('accesos_tree_crud_change_node')}}",
                    {
                           padre:data.node.parent,
                           id: data.selected.join(':'),
                           perfil_id: {{ $perfil->id}},
                            "_token": "{{ csrf_token() }}"
                    }).done(function (d) {
                      //alert(d.id);
                    })
                     .fail(function (d) {
                      
                      });

                    
                    });
            });

</script>

@endsection


