@extends ('layouts.modal_formulario')    


@section('title2')
<h1>Editar Opción</h1>
@endsection

@section('content')
<hr/>
<div class="portlet-body form">
    {!! Form::model($opcione, [
    'method' => 'PUT',
    'id'=>'formulario',
    'name'=>'formulario',

    'class' => 'form-horizontal'
    ]) !!}
    <input type="hidden" class="form-control" value="{{ $opcione->id}}" id="id" name="id" >

    <div class="form-body">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name', 'Nombre: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('enlace') ? 'has-error' : ''}}">
            {!! Form::label('enlace', 'Enlace: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('enlace', null, ['class' => 'form-control']) !!}
                {!! $errors->first('enlace', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('visiblemenu') ? 'has-error' : ''}}">
            {!! Form::label('visiblemenu', 'Visible Menu: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('visiblemenu', null, ['class' => 'form-control']) !!}
                {!! $errors->first('visiblemenu', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('destino') ? 'has-error' : ''}}">
            {!! Form::label('destino', 'Destino: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('destino', null, ['class' => 'form-control']) !!}
                {!! $errors->first('destino', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('nivel') ? 'has-error' : ''}}">
            {!! Form::label('nivel', 'Nivel: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('nivel', null, ['class' => 'form-control']) !!}
                {!! $errors->first('nivel', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('tipoenlace') ? 'has-error' : ''}}">
            {!! Form::label('tipoenlace', 'Tipoenlace: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('tipoenlace', null, ['class' => 'form-control']) !!}
                {!! $errors->first('tipoenlace', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('padre') ? 'has-error' : ''}}">
            {!! Form::label('padre', 'Padre: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::number('padre', null, ['class' => 'form-control','readonly'=>'readonly' ]) !!}
                {!! $errors->first('padre', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('orden') ? 'has-error' : ''}}">
            {!! Form::label('orden', 'Orden: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::number('orden', null, ['class' => 'form-control']) !!}
                {!! $errors->first('orden', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('imagen') ? 'has-error' : ''}}">
            {!! Form::label('imagen', 'Imagen: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('imagen', null, ['class' => 'form-control']) !!}
                {!! $errors->first('imagen', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>
    <div class="form-actions right">
        <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
        <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
    </div>

    {!! Form::close() !!}
</div>
@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection



@section('js')
<script type="text/javascript">
    $(document).ready(function () {

        $("#btn_enviar").click(function () {
            var url = "{{ route('opciones.update') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "PUT",
                url: url,
                data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    parent.$('#jstree_opciones').jstree(true).refresh();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>




@endsection