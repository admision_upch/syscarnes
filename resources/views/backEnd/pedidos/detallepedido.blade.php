@extends('backLayout.app')
@section('title2')
<h1>Detalle de Pedido - Carnés Universitarios </h1>    
@endsection
@section('content2')
<hr>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Pedido: {{ $pedido->nombre }}</span>
        </div>
    </div>
    
    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_5_2" data-toggle="tab"> Solicitud de Carnés </a>
                </li>
                <li>
                    <a href="#tab_5_3" data-toggle="tab"> Match SINU - SUNEDU </a>
                </li>
                 @if ( $permisos->permission_update ==true )
                <li>
                    <a href="#tab_5_4" data-toggle="tab"> Pedido Masivo </a>
                </li>
                @endif
                <li>
                    <a href="#tab_5_5" data-toggle="tab"> Entrega de Carnés al Estudiante </a>
                </li>
                
          
                <li>
                    <a href="#tab_5_6" data-toggle="tab"> Resumen del Pedido</a>
                </li>
                        
          
           
                <li>
                    <a href="#tab_5_7" data-toggle="tab"> Solicitudes Anteriores SUNEDU </a>
                </li>
            </ul>
            
            <div class="tab-content">
                <div  class="tab-pane active" id="tab_5_2">
                    <div class="portlet-body">
                        <div class="col-md-12 ">
                            <form method="POST"  role="form" id="formulario" name="formulario"  accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly="">

                                <div class="form-group {{ $errors->has('facultad_id') ? 'has-error' : ''}}">
                                    {!! Form::label('facultad_id', 'Facultad: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectFacultades !!}
                                        {!! $errors->first('programa_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('programa_id') ? 'has-error' : ''}}">
                                    {!! Form::label('programa_id', 'Programa: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectProgramas !!}
                                        {!! $errors->first('programa_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <button type="submit" id="btn_buscar" name="btn_buscar" class="btn blue">Buscar</button>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>
                        <hr>

                        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantes">
                            <thead>
                                <tr>
                                    <!--th></th-->
                                    <th>Pedido</th>
                                    <th>Cod.Estudiante</th>
                                    <th>Tipo Documento</th>
                                    <th>N° Documento</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Nombre</th>
                                    <th>Facultad Sinu</th>
                                    <th>Facultad Sunedu</th>
                                    <th>Cod.Prog.Sinu</th>
                                    <th>Programa Sinu</th>
                                    <th>Programa Sunedu</th>
                                    <th>Año Ingreso</th>
                                    <th>Periodo Ingreso</th>
                                    <th>Solicitar Carné</th>
                                    <th>Modalidad</th>
                                    <th>foto</th>
                                    <th>Cod Programa Sinu</th>
                                </tr>
                            </thead>
                        </table>

                        
                            @if ( $permisos->permission_update ==true )
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="portlet light portlet-fit ">
                                    <div class="portlet-body">
                                        @if ( $pedidocerrado_estado ==1 or $pedido->estado=='2' )
                                        <a id="downloadExcelConfirmados"  class="btn btn-primary" href="{{Route('downloadExcelConfirmados',['pedido_id' => $pedido->id])}}"><i class="glyphicon glyphicon-download-alt"> </i> Descargar Reporte de Carnés Confirmados</a> 
                                        @else
                                        <a id="cerrarPedido" class="btn btn-danger" onclick="cerrarPedido()"><i class="glyphicon glyphicon-close"> </i>CERRAR PEDIDO DE CARNÉS UNIVERSITARIOS</a>
                                        @endif
                                        <div class="mt-element-list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                         @endif
                        
                        
                        
                    </div>
                </div>


                <div class="tab-pane" id="tab_5_3">
                    <div class="portlet-body">
                        <ul class="nav nav-pills">
                            <li class="active">
                                <a href="#tab_3_1" data-toggle="tab"> Facultades SINU - SUNEDU </a>
                            </li>
                            <li>
                                <a href="#tab_3_2" data-toggle="tab"> Programas SINU - SUNEDU </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab_3_1">
                                <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="facultadessinu">
                                    <thead>
                                        <tr>
                                            
                                            <th>Cod. Facultad Sinu</th>
                                            <th>Abrv.Facultad Sinu</th>
                                            <th>Facultad Sinu</th>
                                            <th>Modalidad</th>
                                            <th>Formacion</th>
                                            <th>Cod. Facultad Sunedu</th>
                                            <th>Facultad Sunedu</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="tab_3_2">
                                <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="programassinu">
                                    <thead>
                                        <tr>
                                            
                                            <th>Cod Facultad Sinu</th>
                                            <th>Facultad Sinu</th>
                                            <th>Modalidad</th>
                                            <th>Formacion</th>
                                            <th>Codigo<br> Programa Sinu</th>
                                            <th>Programa Sinu</th>
                                            <th>Codigo <br>Programa Sunedu</th>
                                            <th>Programa Sunedu</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                  @if ( $permisos->permission_update ==true )
               <div  class="tab-pane" id="tab_5_4">
                    <div class="portlet-body">
                
                        <div class="alert alert-info"><strong>Información!</strong> Para solicitar los carnés de forma masiva, por favor seleccionar el programa y hacer click en <strong>Procesar Solicitud Masiva</strong>. Finalizado el procesamiento, ir a la pestaña <strong>Estudiantes</strong> para verificar. </div>
                        
                        
                            <form method="POST"  role="form" id="formulario2" name="formulario2"  accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly="">

                                <div class="form-group {{ $errors->has('facultad_id2') ? 'has-error' : ''}}">
                                    {!! Form::label('facultad_id2', 'Facultad: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectFacultades2 !!}
                                        {!! $errors->first('facultad_id2', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('programa_id2') ? 'has-error' : ''}}">
                                    {!! Form::label('programa_id2', 'Programa: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectProgramas2 !!}
                                        {!! $errors->first('programa_id2', '<p class="help-block">:message</p>') !!}
                                    </div>
                                   
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                    </div>
                                     <button type="submit" id="btn_procesar_solicitudmasiva" name="btn_procesar_solicitudmasiva" class="btn blue">Procesar Solicitud Masiva</button>
                                </div>
                            </form>
                     

                  
            
                        
                        
                    </div>
                </div>

                  @endif
                
               <div  class="tab-pane" id="tab_5_5">
                    <div class="portlet-body">

                        <div class="alert alert-info"><strong>Información!</strong> En esta pestaña usted puede registrar la entrega de carnés a los estudiantes.</div>

                        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantesentregacarne">
                            <thead>
                                <tr>
                                    <!--th></th-->
                                    <th>Pedido</th>
                                    <th>Cod.Estudiante</th>
                                    <th>Tipo Documento</th>
                                    <th>N° Documento</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Nombre</th>
                                    <th>Facultad Sinu</th>
                                    <th>Facultad Sunedu</th>
                                    <th>Cod.Prog.Sinu</th>
                                    <th>Programa Sinu</th>
                                    <th>Programa Sunedu</th>
                                    <th>Año Ingreso</th>
                                    <th>Periodo Ingreso</th>
                                    <th>Se entregó carné</th>
                                    <th>Modalidad</th>
                                    <th>foto</th>
                                    <th>Cod Programa Sinu</th>
                                </tr>
                            </thead>
                        </table>
                     
             
                        
                        
                        
                    </div>
                </div>
 
                
                
             
               <div  class="tab-pane" id="tab_5_6">
                    <div class="portlet-body">

       

                        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="resumenpedido">
                            <thead>
                                <tr>
                                    <!--th></th-->
                                    <th>Facultad</th>
                                    <th>N° Documento</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Cantidad</th>
                                   
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="4" style="text-align:right">Total:</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                     
             
                        
                        
                        
                    </div>
                </div>
              
                
                           <div  class="tab-pane" id="tab_5_7">
                    <div class="portlet-body">

                        <div class="alert alert-info"><strong>Información!</strong> En esta pestaña usted puede visualizar los pedidos anteriores de carnés universitarios.</div>



                    <div class="table ">
                        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="solicitudesSunedu">
                            <thead>

                                <tr>
                                    <th>ID</th>
                                    <th>Nro. Solicitud</th>
                                    <th>Total Estudiantes</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div  id="divEstudiantesSunedu">


                    </div>
                     
             
                        
                        
                        
                    </div>
                </div>
 
                
            </div>
        </div>
    </div>
    <hr>
    <button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>
</div>

<div class="row">
    <div class="mt-element-ribbon bg-grey-steel">
        <div class="ribbon ribbon-color-default uppercase">Emails Seleccionados</div>
        <p class="ribbon-content" id="emailscopiados"></p>
    </div>
</div>
@endsection

@section('js2')
<script type="text/javascript">
  
    $(document).ready(function () {
        
    /*INICIO: Buscar*/    
    $("#btn_buscar").click(function () {
        var codprograma = ($("#programa_id").val());
        var facultad = $('#facultad_id option:selected').text();
        var val_buscar = '';
        
        if (codprograma != 'all'){
            val_buscar = codprograma;
            oTableEstudiantes.search(val_buscar).draw();
            return false;
        }

        if (facultad != 'Todos'){
            val_buscar = facultad;
            oTableEstudiantes.search(val_buscar).draw();
            return false;
        }
        
        oTableEstudiantes.search(val_buscar).draw();
    });
    /*FIN: Buscar*/    
    
    /*INICIO: Mostrar Programas en Select al Cambiar Facultad*/   
    $('#facultad_id').change(function () {
        var facultad_id = $("#facultad_id").val();
        
        $.post("{{ route('programas_select')}}",
        {
            facultad_id: facultad_id,
            pedido_id: {{ $pedido->id}},
            "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $('#programa_id').html(data);
        }).fail(function (d) {
            alert('Error');
        });
    });
    /*FIN: Mostrar Programas en Select al Cambiar Facultad*/   
    
    /*INICIO: Procesar Solicitud Masiva*/    
    $("#btn_procesar_solicitudmasiva").click(function () {
        var codprograma = ($("#programa_id2").val());
        $('#btn_procesar_solicitudmasiva').attr('disabled', 'disabled');
        loading(true);        
        $.post("{{ route('pedidos.procesar.solicitudmasiva')}}",
        {
                programa_cod: codprograma,
                pedido_id: {{$pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false); 
            $('#btn_procesar_solicitudmasiva').removeAttr('disabled');
            swal(data.message, null, data.status);
            //oTableEstudiantes.ajax.reload();
        }).fail(function (data) {
           // alert('error x1');
              location.reload();
        });
    });
    /*FIN: Procesar Solicitud Masiva*/  
    
    /*INICIO: Mostrar Programas en Select al Cambiar Facultad*/   
    $('#facultad_id2').change(function () {
        var facultad_id = $("#facultad_id2").val();
        
        $.post("{{ route('programas_select')}}",
        {
            facultad_id: facultad_id,
            pedido_id: {{ $pedido->id}},
            "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $('#programa_id2').html(data);
        }).fail(function (d) {
            alert('Error');
        });
    });
    /*FIN: Mostrar Programas en Select al Cambiar Facultad*/   

    var codprograma = ($("#programa_id").val());
    
    /*INICIO: Cargar Tabla Estudiantes*/   
         oTableEstudiantes = $('#estudiantes').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#estudiantes').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.estudiantes')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            codprograma:  codprograma,
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#estudiantes').show();
                            if (type == "error") {
                                oTableEstudiantes.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableEstudiantes.ajax.reload();
                }
            },
            "order": [[9, "asc"]],
            "columns": [
                        {data: 'pedido_id', name: 'pedido_id', visible: false},
                        {data: 'codalumno', name: 'codalumno', visible: false},
                        {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                        {data: 'nrodocumento', name: 'nrodocumento'},
                        {data: 'apepaterno', name: 'apepaterno'},
                        {data: 'apematerno', name: 'apematerno'},
                        {data: 'nombre', name: 'nombre'},
                        {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu', visible: false},
                        {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                        {data: 'codprograma_sinu', name: 'codprograma_sinu', visible:false},
                        {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                        {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu', visible: true},
                        {data: 'ano_ing', name: 'ano_ing', visible: false},
                        {data: 'per_ing', name: 'per_ing', visible: false},
                        {data: 'estado', name: 'estado', "className": "dt-center"},
                        {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                        {data: 'foto', name: 'foto', "className": "dt-center"},
                        {data: 'codprograma_sinu', name: 'codprograma_sinu', visible: false},
            ]
        });
    /*FIN: Cargar Tabla Estudiantes*/
    

    /*INICIO: Cargar Tabla Facultades SINU*/   
         oTableFacultadesSINU = $('#facultadessinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#facultadessinu').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.facultadessinu')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#facultadessinu').show();
                            if (type == "error") {
                                oTableFacultadesSINU.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableFacultadesSINU.ajax.reload();
                }
            },
            "order": [[1, "desc"]],
            "columns": [
                        
                        {data: 'codfacultad', name: 'codfacultad', visible: true},
                        {data: 'abreviaturafacultad', name: 'abreviaturafacultad'},
                        {data: 'nomfacultad', name: 'nomfacultad'},
                        {data: 'modalidad_desc', name: 'modalidad_desc'},
                        {data: 'formacion', name: 'formacion'},
                        {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
                        {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, visible: false}
            ]
        });
    /*FIN: Cargar Tabla Facultades SINU*/   
    
    /*INICIO: Cargar Tabla Programas SINU*/   
         oTableProgramasSINU = $('#programassinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#programassinu').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.programassinu')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#programassinu').show();
                            if (type == "error") {
                                oTableProgramasSINU.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableProgramasSINU.ajax.reload();
                }
            },
            "order": [[2, "desc"]],
            "columns": [
                        
                        {data: 'codfacultad', name: 'codfacultad', visible: false},
                        {data: 'nomfacultad', name: 'nomfacultad', visible: true},
                        {data: 'modalidad_desc', name: 'modalidad_desc', visible: true},
                        {data: 'formacion', name: 'formacion', visible: true},
                        {data: 'codprograma', name: 'codprograma'},
                        {data: 'nomprograma', name: 'nomprograma'},
                        {data: 'codprograma_sunedu', name: 'codprograma_sunedu', visible: true},
                        {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                        {data: 'action', name: 'action', orderable: false, searchable: false, visible: false}
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;
                api.column(2, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group color md-bg-blue-500" style="font-weight:bold;"  ><td colspan="8">' + group + '</td></tr>'
                                );
                        last = group;
                    }
                });
            }
        });
    /*FIN: Cargar Tabla Programas SINU*/   
    
    
    
        /*INICIO: Cargar Tabla Estudiantes Entrega Carne*/   
         oTableEstudiantesEntregaCarne = $('#estudiantesentregacarne').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#estudiantesentregacarne').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.estudiantes.entregacarne')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#estudiantesentregacarne').show();
                            if (type == "error") {
                                oTableEstudiantesEntregaCarne.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableEstudiantesEntregaCarne.ajax.reload();
                }
            },
            "order": [[9, "asc"]],
            "columns": [
                        {data: 'pedido_id', name: 'pedido_id', visible: false},
                        {data: 'codalumno', name: 'codalumno', visible: false},
                        {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                        {data: 'nrodocumento', name: 'nrodocumento'},
                        {data: 'apepaterno', name: 'apepaterno'},
                        {data: 'apematerno', name: 'apematerno'},
                        {data: 'nombre', name: 'nombre'},
                        {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu', visible: false},
                        {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                        {data: 'codprograma_sinu', name: 'codprograma_sinu', visible:false},
                        {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                        {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu', visible: true},
                        {data: 'ano_ing', name: 'ano_ing', visible: false},
                        {data: 'per_ing', name: 'per_ing', visible: false},
                        {data: 'estado_entregado', name: 'estado_entregado', "className": "dt-center"},
                        {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                        {data: 'foto', name: 'foto', "className": "dt-center"},
                        {data: 'codprograma_sinu', name: 'codprograma_sinu', visible: false},
            ]
        });
    /*FIN: Cargar Tabla Estudiantes Entrega Carne*/
    
    
       /*INICIO: Cargar Tabla Resumen Pedido*/   
         oTableResumenPedido = $('#resumenpedido').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#resumenpedido').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.resumenpedido')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#resumenpedido').show();
                            if (type == "error") {
                                oTableResumenPedido.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                           oTableResumenPedido.ajax.reload();
                }
            },
            "order": [[3, "asc"]],
            "columns": [
                        
                        {data: 'facultad', name: 'facultad', visible: true},
                        {data: 'username', name: 'username', visible: true},
                        {data: 'usuario', name: 'usuario'},
                        {data: 'estado', name: 'estado'},
                        {data: 'cantidad', name: 'cantidad', className: "dt-right"}

            ],
            
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                pageTotal
            );
        }
        });
    /*FIN: Cargar Tabla Resumen Pedido*/  
    
    
    
    
    
    
        /*INICIO: Cargar Tabla Solicitudes SUNEDU*/
    oTableSolicitudesSunedu = $('#solicitudesSunedu').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": true,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#solicitudesSunedu').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.solicitudes.sunedu.visualiza')}}",
                    "type": "GET",
                    "complete": function (json, type) {
                    $('#solicitudesSunedu').show();
                        if (json.sesion==0 ) {
                              swal(json.message, null, json.status);
                        }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                      oTableSolicitudesSunedu.ajax.reload();
                    }
            },
            "order": [[1, "asc"]],
            "columns": [

            {data: 'id', name: 'id', visible:false},
            {data: 'nrosolicitud', name: 'nrosolicitud'},
            {data: 'totalestudiantes', name: 'totalestudiantes'},
            {data: 'action', name: 'action'}

            ]
    });
    /*FIN: Cargar Tabla Solicitudes SUNEDU*/
    
    
    
    
 });



    function verEstudiantesSunedu(id) {
        $.post("{{ route('pedidos.open.paso02.ver.estudiantessunedu')}}",
        {
                solicitud_id: id,
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             $("#divEstudiantesSunedu").html(data);
        }).fail(function (data) {
             $("#divEstudiantesSunedu").html(data);
        });
    }

    /*INICIO: Cerrar Pedido*/
    function cerrarPedido(){
        $.post("{{ route('cerrarpedido')}}",
        {
                pedido_id: {{ $pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
                location.reload();
        }).fail(function (d) {
                alert('Error');
        });
    }
    /*FIN: Cerrar Pedido*/

    /*INICIO: Agregar Email*/
    function agregarEmail(email){
        $("#emailscopiados").append(email + ', ');
    }
    /*INICIO: Agregar Email*/


    /*INICIO: Cambiar Estado*/
    function cambiarEstado(id){
        var estado = '0';
        
        if ($('#' + id).prop('checked')) {
            estado = '1';
        }

        $.post("{{ route('cambiarestado')}}",
        {
            id: id,
            pedido_id: {{ $pedido->id}},
            estado: estado,
            "_token": "{{ csrf_token() }}"
        }).done(function (d) {
            $("#nameestado" + id).html(d.nameestado);
            $("#validadopor" + id).css("display", 'none');
        }).fail(function () {
            alert('error');
        });
    }
    /*FIN: Cambiar Estado*/
    
    
    /*INICIO: Cambiar Estado Entregado*/
    function cambiarEstadoEntregado(id){
        var estado = '0';
        if ($('#' + id).prop('checked')) {
            estado = '1';
        }
        $.post("{{ route('cambiarestado.entregado')}}",
        {
            id: id,
            pedido_id: {{ $pedido->id}},
            estado: estado,
            "_token": "{{ csrf_token() }}"
        }).done(function (d) {
            $("#nameestadoentregado" + id).html(d.nameestado);
            $("#validadoporentregado" + id).css("display", 'none');
        }).fail(function () {
            alert('error');
        });
    }
    /*FIN: Cambiar Estado Entregado*/

</script>  


@endsection