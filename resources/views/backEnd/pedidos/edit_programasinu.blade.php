@extends ('layouts.modal_formulario')    
@section('content')   


<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase">Programa SINU - Editar</span>
        </div>
    </div>
    <div class="portlet-body form">
        <div id="respuesta"  ></div>
        <form role="form"  method="POST" id="formularioModal" name="formularioModal" >
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
             <input type="hidden" class="form-control" value="{{ $programaSinu->codprograma}}" id="codprograma" name="codprograma"  >
                 <input type="hidden" class="form-control" value="{{ $programaSinu->codfacultad}}" id="codfacultad" name="codfacultad" >
                     <input type="hidden" class="form-control" value="{{ $programaSinu->pedido_id}}" id="pedido_id" name="pedido_id"  >
            <div class="form-body">



                <div class="form-group">
                    <label>Cod.Programa SINU</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="{{ $programaSinu->codprograma}}" readonly="">
                    </div>
                </div>

                <div class="form-group">
                    <label>Programa SINU</label>

                    <input type="text" class="form-control" placeholder="{{ $programaSinu->nomprograma}}" readonly="">

                </div>

                <div class="form-group">
                    <label>Facultad SUNEDU</label>

                    <input type="text" class="form-control" placeholder="{{ $facultadSunedu->nomfacultad}}" readonly="">

                </div>


                {!! $selectProgramaSunedu !!}

            </div>
            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->




@endsection


@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $("#btn_enviar").click(function () {

              var url = "{{ route('pedidos.programa.update') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "PUT",
                url: url,
                data: $("#formularioModal").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
                    parent.oTableProgramasSINU.ajax.reload();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>




@endsection