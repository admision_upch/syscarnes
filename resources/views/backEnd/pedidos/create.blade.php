@extends('backLayout.app')
@section('title2')
<h1>Crear Nuevo Pedido</h1>
@endsection

@section('content2')
<hr/>
{!! Form::open(['url' => 'pedidos',  'class' => 'form-horizontal']) !!}

<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre: ', ['class' => 'col-sm-3 control-label', 'data-required'=>'1']) !!}
    <div class="col-sm-6">
        {!! Form::text('nombre', null, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('fec_inicio') ? 'has-error' : ''}}">
    {!! Form::label('fec_inicio', 'Fecha Inicio: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('fec_inicio', null, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('fec_inicio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('abreviatura') ? 'has-error' : ''}}">
    {!! Form::label('abreviatura', 'Abreviatura: ', ['class' => 'col-sm-3 control-label', 'required'=>'required']) !!}
    <div class="col-sm-6">
        {!! Form::text('abreviatura', null, ['class' => 'form-control']) !!}
        {!! $errors->first('abreviatura', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('version') ? 'has-error' : ''}}">
    {!! Form::label('version', 'Version: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('version', null, ['class' => 'form-control']) !!}
        {!! $errors->first('version', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@include('componentes.submit_reset_form_create')

{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
@endsection

