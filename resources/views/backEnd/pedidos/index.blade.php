@extends('backLayout.app')
@section('title2')




<h1>@if( $permisos->permission_create==true)Monitoreo de @endif Pedidos - Carnés Universitarios</h1>    



@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblpedidos">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha Importacion Estudiantes</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Fecha Cierre</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedidos as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->nombre }}</td>
                <td>{{ $item->fec_importacion_estudiantes }}</td>
                <td>{{ $item->fec_inicio }}</td>
                <td>{{ $item->fec_fin }}</td>
                <td>{{ $item->fec_cierre }}</td>
                <td>{{ $item->estado }}</td>
                <td>
                    <a href="{{ url('pedidos/'.$item->id) }}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>Ver Detalle</a>
               
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div  id="detallePedido">


</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {

        $('#tblpedidos').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: true
                },
            ],
            order: [[0, "desc"]],
        });
    });

</script>
@endsection