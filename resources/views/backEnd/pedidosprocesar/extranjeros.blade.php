@extends('backLayout.app')
@section('title2')




<h1>Estudiantes Extranjeros </h1>    



@endsection

@section('content2')
<hr/>
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Pedido: {{ $pedido->nombre }}</span>
        </div>

    </div>
<hr/>



<form class="form-inline" role="form" method="POST" id="formuploadajax" name="formuploadajax" accept-charset="UTF-8" enctype="multipart/form-data" >
    <label><i>Subir el archivo que ha descargado de INTRANET( Opción: Valida Usuarios), para actualizar los número de documentos originales de los estudiantes extranjeros: </i></label>
    <div class="form-group">
           <input type="hidden" value="{{$pedido->id}}"  id="pedido_id" name="pedido_id" />
        <input type="file" class="btn btn-xs btn-primary"  id="file_archivo" name="file_archivo"  />
    </div>
    <button type="button" onclick="cargarArchivo();" class="btn btn-xs btn-primary"><i class="fa fa-upload"></i> Cargar Archivo</button>

</form>
<hr>

<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblextranjeros">
        <thead>
            <tr>
                <th>ID</th>
                <th>Ape. Paterno</th>
                <th>Ape. Materno</th>
                <th>Nombres</th>
                <th>Nro Documento SINU</th>
                <th>Nro. Documento Original</th>
                <th>Acción</th>

            </tr>
        </thead>

    </table>
</div>

<a id="downloadExtranjeros"  class="btn btn-primary" href="{{Route('download.extranjeros',['pedido_id' => $pedido->id])}}"><i class="glyphicon glyphicon-download-alt"> </i> Descargar Estudiantes Extranjeros</a> 

<hr>
<button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>

@endsection

@section('js2')
<script type="text/javascript">

    $(document).ready(function () {
        oTable = $('#tblextranjeros').DataTable({

            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,

            "ajax": "{{ route('datatable.extranjeros', ['pedido_id' => $pedido->id ]) }}",
            "columns": [

                {data: 'id', name: 'id', visible: false},
                {data: 'apepat', name: 'apepat'},
                {data: 'apemat', name: 'apemat'},
                {data: 'nombre', name: 'nombre'},
                {data: 'nrodocumento', name: 'nrodocumento'},
                {data: 'nrodocumento_original', name: 'nrodocumento_original'},
                {data: 'action', name: 'action'}

            ]
        });
    });



    function cargarArchivo() {

        loading(true);
        var formData = new FormData(document.getElementById("formuploadajax"));
        formData.append("_token", "{{ csrf_token() }}");
        $.ajax({
            url: "{{ route('pedidosprocesar.subirarchivo.extranjeros')}}",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            loading(false);
            swal(data.message, null, data.status);
            oTable.ajax.reload();
        }).error(function (data) {
            location.reload();
        });
    }
</script>
@endsection