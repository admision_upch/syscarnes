@extends('backLayout.app')
@section('title2')




<h1>Procesamiento de Pedidos - Carnés Universitarios </h1>    



@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblpedidos">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha Importacion Estudiantes</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Fecha Procesado</th>
                <th>Fecha Cierre</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
        </thead>

    </table>
</div>

<div  id="divArchivosGenerados">
 

</div>

@endsection

@section('js2')
<script type="text/javascript">

    $(document).ready(function () {
        oTable = $('#tblpedidos').DataTable({

            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.pedidos') }}",
            "order": [[0, "desc"]],
            "columns": [
                {data: 'id', name: 'id', visible: true},
                {data: 'nombre', name: 'nombre'},
                {data: 'fec_importacion_estudiantes', name: 'fec_importacion_estudiantes', visible:false},
                {data: 'fec_inicio', name: 'fec_inicio'},
                {data: 'fec_fin', name: 'fec_fin'},
                {data: 'fec_procesado', name: 'fec_procesado'},
                {data: 'fec_cierre', name: 'fec_cierre'},
                {data: 'estado', name: 'estado'},
                {data: 'action', name: 'action'}

            ]
        });
    });
    function procesar(pedido_id) {
        loading(true);      
        $.post("{{ route('procesarpedido')}}",
        {
                    pedido_id: pedido_id,
                    "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             loading(false);      
            swal(data.message, null, data.status);
            oTable.ajax.reload();
            verArchivosGenerados(pedido_id);

        }).fail(function (d) {
                    alert('Error');
        });
    }
    
    function verArchivosGenerados(pedido_id){
         loading(true);      
        $.post("{{ route('procesarpedido.verarchivosgenerados')}}",
        {
            pedido_id: pedido_id,
            "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             loading(false);      
            $("#divArchivosGenerados").html(data);
        }).fail(function (d) {
                alert('Error');
        });
    }
    
</script>
@endsection