@extends ('layouts.modal_formulario')    


@section('title2')
<h1>Editar Opción</h1>
@endsection

@section('content')
<hr/>
<div class="portlet-body form">
    {!! Form::model($estudiante_pedido, [
    'method' => 'PUT',
    'id'=>'formularioModal',
    'name'=>'formularioModal',

    'class' => 'form-horizontal'
    ]) !!}
    <input type="hidden" class="form-control" value="{{ $estudiante_pedido->id}}" id="id" name="id" >

    <div class="form-body">
        <div class="form-group {{ $errors->has('nrodocumento') ? 'has-error' : ''}}">
            {!! Form::label('nrodocumento', 'Nro. Documento: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('nrodocumento', null, ['class' => 'form-control']) !!}
                {!! $errors->first('nrodocumento', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('nrodocumento_original') ? 'has-error' : ''}}">
            {!! Form::label('nrodocumento_original', 'Nro. Documento Original: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('nrodocumento_original', null, ['class' => 'form-control']) !!}
                {!! $errors->first('nrodocumento_original', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
  

    </div>
    <div class="form-actions right">
        <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
        <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
    </div>

    {!! Form::close() !!}
</div>
@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection



@section('js')
<script type="text/javascript">
    $(document).ready(function () {

       $("#btn_enviar").click(function () {
            var url = "{{ route('extranjeros.update') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "PUT",
                url: url,
                data: $("#formularioModal").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    location.reload();
                    //parent.$('#jstree_opciones').jstree(true).refresh();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>




@endsection