@extends('backLayout.app')
@section('title2')

<h1> Entrega de fotochecks</h1> 



<div class="alert alert-info"><strong>Información!</strong> <p style="font-weight: normal;" class="caption-subject  lowercase">Puede utilizar una lectora de código de barras, caso contrario ingrese el n° de documento en la caja de texto. </p></div>



@endsection
@section('content2')
<hr>
<div class="portlet light bordered">
    <div class="portlet-title" style="text-align: center;">
        <form method="POST"  role="form" class="form-horizontal" id="formulario" name="formulario"  accept-charset="UTF-8" >
                <div class="form-group">
                    <label class="col-sm-4 control-label">N° Documento:</label>
                    <div class="col-sm-2">
                        <input type="text"   id="nrodocumento" name="nrodocumento" value="" autocomplete="off"  />
                    </div>
                </div>
       
            <input type="submit" value="Enviar" id="addRow" style="display:none;" >
        </form>
    </div>

    <div class="portlet-body">
        <div class="tabbable-custom ">
            <div class="tab-content">
                <div class="portlet-body">
                    <div class="col-md-12 ">
                        <form method="POST"  role="form" id="formulario" name="formulario"  accept-charset="UTF-8" class="form-horizontal">
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <br>
                    <hr>
                    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantes">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th style="width:20px;" >N° Documento</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Nombre</th>
                                <th>Facultad</th>
                                <th>Programa</th>
                                <th>Vencimiento</th>
                                <th>Estado</th> 
                                <th>Acciones</th> 
                            </tr>
                        </thead>
                        <tbody id="body">
           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>
</div>

<div class="row">
    <div class="mt-element-ribbon bg-grey-steel">
        <div class="ribbon ribbon-color-default uppercase">Emails Seleccionados</div>
        <p class="ribbon-content" id="emailscopiados"></p>
    </div>
</div>
@endsection

@section('js2')
<script type="text/javascript">

    $(document).ready(function () {
    nrodocumento.focus();
    //  var t = $('#estudiantes').DataTable();

    var t = $('#estudiantes').DataTable({
    columnDefs: [{
    targets: [0],
            visible: true,
            searchable: true
    }
    ],
            order: [[0, "desc"]],
    });
    var counter = 0;
    $('#addRow').on('click', function () {
    var val_nrodocumento = $("#nrodocumento").val();
        
    $.post("{{ route('fotocheckentregar.ver.estado')}}",
    {
             nrodocumento: val_nrodocumento,
            "_token": "{{ csrf_token() }}"
    }).done(function (data) {
    loading(false);

    if (data.error != 1){
   
    var data_retorno = data.data;
    for (var i in data_retorno) {

var estado='--';
var estado2='';
if (data_retorno[i].estado==2 || data_retorno[i].estado==3){
    estado2='<input type="button" class="btn btn-warning" style="color:#000;" onclick="entregarFotocheck('+data_retorno[i].id+');" value="Entregar"  />';
}
if (data_retorno[i].devolucion==1){
    estado='Fotocheck Devuelto';
}else{
    if (data_retorno[i].estado==3 || data_retorno[i].estado==4){
    estado='<input type="button" onclick="devolverFotocheckEdit('+data_retorno[i].id+')" class="btn btn-danger" value="devolver" />';
    
}
}
    t.row.add([
            counter + 1,
            data_retorno[i].nrodocumento,
            data_retorno[i].apepaterno,
            data_retorno[i].apematerno,
            data_retorno[i].nombre,
            data_retorno[i].nomfacultad,
            data_retorno[i].nomprograma,
            data_retorno[i].fec_vencimiento,
            data_retorno[i].estado_nombre,
            estado2+estado
    ]).draw(false);
    }
    counter++;
    document.getElementById("nrodocumento").value = "";
    nrodocumento.focus();
    }else{
         swal(data.message, null, data.status);
    }

    }).fail(function (d) {
    alert('Error');
    });
    });
    // Automatically add a first row of data
    //$('#addRow').click();
    });
    function entregarFotocheck(id) {
        loading(true);      
        $.post("{{ route('fotocheckentregar.entregar')}}",
        {
           id: id,
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
           var finalizar = function(){
             location.reload();
            };
            setTimeout(finalizar, 2000);

        }).fail(function (d) {
            alert('Error');
        });
    }
    
    
    function devolverFotocheckEdit(id) {
         $('#ajax_modal').modal('toggle');
          $('#ajax_modal').find(".modal-body").load( 'fotocheckentregar/devolver/edit/'+id );
   }

</script>  


@endsection