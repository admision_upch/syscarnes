@extends ('layouts.modal_formulario')    
@section('content')   


<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase">Devolver Fotocheck</span>
        </div>
    </div>
    <div class="portlet-body form">
        <div id="respuesta"  ></div>
        
        
        @if ($EstudianteFotocheck)
        
        
        
        <form role="form" id="form_sample_3" method="POST"  name="form_sample_3" >
            
            <input type="hidden" name="id" value="{{$EstudianteFotocheck->id}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-body">
                <div class="alert alert-danger">
                    <strong>Información!</strong>
                    Devolución del fotocheck de la persona <strong>{{ $EstudianteFotocheck->apepaterno }} {{ $EstudianteFotocheck->apematerno }} {{ $EstudianteFotocheck->nombre }} </strong>
                    con N° documento <strong>{{ $EstudianteFotocheck->nrodocumento }}.</strong> <br>Para realizar la devolución del fotocheck es obligario que ingrese el motivo de la devolución:
                    </div>
                <div class="form-group">
                    <label>Motivo de devolución:</label>
                    <div>
                        <textarea name="motivo_devolucion" id="motivo_devolucion" class="form-control"  ></textarea>
                    </div>
                </div>
                  <div id="mensajes"></div>
            </div>
            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="button" class="btn blue" id="btn_enviar">Devolver</button>
            </div>
        </form>
        
        
        @else
        
        <div class="alert alert-danger">
                    <strong>Error!</strong>
                    Este registro ya se ha devuelto.
                    </div>
         @endif
        
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->




@endsection


@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $("#btn_enviar").click(function () {

            var url = "{{ route('fotocheckentregar.devolver.update') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_sample_3").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    
             
                   // $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
                       if (data.error != 1){
                           $('#ajax_modal').modal('toggle');
                           swal(data.message, null, data.status);
                            var finalizar = function(){
                            location.reload();
                           };
                           setTimeout(finalizar, 2000);     
                       }else{
                           $("#mensajes").html(data.message);
                           
                       }
                    
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>



@endsection