@extends('backLayout.app')
@section('title')
Edit Users_perfile
@stop

@section('content')

    <h1>Edit Users_perfile</h1>
    <hr/>

    {!! Form::model($users_perfile, [
        'method' => 'PATCH',
        'url' => ['users_perfiles', $users_perfile->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('perfil_id') ? 'has-error' : ''}}">
                {!! Form::label('perfil_id', 'Perfil Id: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('perfil_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('perfil_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
                {!! Form::label('user_id', 'User Id: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('user_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection