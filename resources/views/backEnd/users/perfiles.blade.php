@extends('backLayout.app')
@section('title2')
<h1>Asignación de Perfiles</h1>
@endsection

@section('content2')
<hr/>


<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">{{$user->name}} ( {{$user->username}})</span>
        </div>

    </div>
    <div class="portlet-body">
        <div class="col-md-8 ">
            <form method="POST" role="form" id="formulario" name="formulario"  accept-charset="UTF-8" class="form-inline">

                {{ csrf_field() }}

                <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ $user->id}}" readonly="">

                <div class="form-group {{ $errors->has('sistema_id') ? 'has-error' : ''}}">
                    {!! Form::label('sistema_id', 'Sistema: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! $selectSistemas !!}
                        {!! $errors->first('sistema_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('perfil_id') ? 'has-error' : ''}}">
                    {!! Form::label('perfil_id', 'Perfil: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! $selectPerfiles !!}
                        {!! $errors->first('perfil_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-6">
                        <button type="submit" id="btn_enviar" name="btn_enviar" class="btn blue">Agregar</button>

                    </div>
                </div>
            </form>
        </div>

        <div class="table ">
            <table class="table table-bordered table-striped table-hover" id="tblperfiles">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Sistema</th>
                        <th>Perfil</th>
                        <th>Actions</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
    <hr>





    <!--div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase"></span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="col-md-8 ">
      
        </div>


    </div-->

</div>

<hr>
<button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>

@endsection
@section('js2')
<script type="text/javascript">
    $(document).ready(function () {


        oTable = $('#tblperfiles').DataTable({

            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.perfilesasignados', ['user_id' => $user->id]) }}",
            "columns": [
                {data: 'id', name: 'id', visible: false},
                {data: 'sistema', name: 'sistema'},
                {data: 'perfil', name: 'perfil'},
                {data: 'action', name: 'action'}

            ]
        });

        $('#sistema_id').change(function () {
            var sistema_id = $("#sistema_id").val();
            $.post("{{ route('perfiles_select')}}",
                    {
                        id: sistema_id,
                        "_token": "{{ csrf_token() }}"
                    }).done(function (data) {
                $('#perfil_id').html(data);
            })
                    .fail(function (d) {
                        alert('Error');
                    });
        });



        $("#btn_enviar").click(function () {


            var url = "{{ route('asignarperfil')}}";// El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    swal(data.message, null, data.status);
                    $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
                    oTable.ajax.reload();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });

    });

    function eliminar(userperfil_id) {
        $.post("{{ route('asignarperfil_delete')}}",
                {
                    id: userperfil_id,
                    "_token": "{{ csrf_token() }}"
                }).done(function (data) {
                    
            swal(data.message, null, data.status);
            $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
            oTable.ajax.reload();
            
        })
                .fail(function (d) {
                    alert('Error');
                });

    }
</script>
@endsection