@extends('backLayout.app')
@section('title2')
<h1>Usuarios <a href="{{ url('users/create') }}" class="btn btn-primary pull-right btn-sm">Agregar Nuevo Usuario</a></h1>
@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblusers">
        <thead>
            <tr>
                <th>ID</th><th>Name</th><th>Username</th><th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td><a href="{{ url('users', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->username }}</td>
                <td>
                    <a href="{{ url('users/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['users', $item->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                    <a href="{{ url('users/' . $item->id . '/perfiles') }}" class="btn btn-primary btn-xs">Perfiles</a> 
                    <a href="{{ url('users/' . $item->id . '/programas') }}" class="btn btn-primary btn-xs">Programas</a> 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('js2')
<script type="text/javascript">
    $(document).ready(function () {

        $('#tblusers').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection