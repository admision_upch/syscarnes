@extends('backLayout.app')
@section('title2')
<h1>Crear Nueva Facultad</h1>
@endsection

@section('content2')
<hr/>
{!! Form::open(['url' => 'facultades', 'class' => 'form-horizontal']) !!}

<div class="form-group {{ $errors->has('id_dependencia') ? 'has-error' : ''}}">
    {!! Form::label('id_dependencia', 'Id Dependencia: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('id_dependencia', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('id_dependencia', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cod_dependencia') ? 'has-error' : ''}}">
    {!! Form::label('cod_dependencia', 'Cod Dependencia: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('cod_dependencia', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('cod_dependencia', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('modalidad_id') ? 'has-error' : ''}}">
    {!! Form::label('modalidad_id', 'Modalidad Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectModalidades !!}
        {!! $errors->first('modalidad_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('formacion_id') ? 'has-error' : ''}}">
    {!! Form::label('formacion_id', 'Formacion Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectFormaciones !!}
        {!! $errors->first('formacion_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_create')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
@endsection