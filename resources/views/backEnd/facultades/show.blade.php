@extends('backLayout.app')
@section('title')
Facultade
@stop

@section('content')

    <h1>Facultade</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Id Dependencia</th><th>Cod Dependencia</th><th>Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $facultade->id }}</td> <td> {{ $facultade->id_dependencia }} </td><td> {{ $facultade->cod_dependencia }} </td><td> {{ $facultade->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection