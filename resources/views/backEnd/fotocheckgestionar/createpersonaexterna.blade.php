@extends ('layouts.modal_formulario')    
@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-dark sbold uppercase"> <i class="fa fa-user-plus"></i>  Agregar Nueva Persona</span>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" id="form_sample_3" method="POST" class="form-horizontal"  name="form_sample_3" accept-charset="UTF-8" enctype="multipart/form-data" >
            
                   <input type="hidden"  value="{{$pedido_id}}" id="pedido_id" name="pedido_id"  />
            
            <div class="form-body">
                <div class="form-group {{ $errors->has('tipodocumento') ? 'has-error' : ''}}">
                    {!! Form::label('tipodocumento', 'Tipo Documento: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! $selectTipodocumento !!}
                        {!! $errors->first('tipodocumento', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('nrodocumento') ? 'has-error' : ''}}">
                    {!! Form::label('nrodocumento', 'N° Documento: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('nrodocumento', null, ['class' => 'form-control','maxlength'=>'15']) !!}
                        {!! $errors->first('nrodocumento', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('apepaterno') ? 'has-error' : ''}}">
                    {!! Form::label('apepaterno', 'Apellido Paterno: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('apepaterno', null, ['class' => 'form-control','maxlength'=>'120']) !!}
                        {!! $errors->first('apepaterno', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('apematerno') ? 'has-error' : ''}}">
                    {!! Form::label('apepaterno', 'Apellido Materno: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('apematerno', null, ['class' => 'form-control','maxlength'=>'120']) !!}
                        {!! $errors->first('apematerno', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
                    {!! Form::label('nombre', 'Nombre: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('nombre', null, ['class' => 'form-control','maxlength'=>'120']) !!}
                        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('codfacultad_sinu') ? 'has-error' : ''}}">
                    {!! Form::label('codfacultad_sinu', 'Dirección: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! $selectFacultades !!}
                        {!! $errors->first('codfacultad_sinu', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('nomprograma') ? 'has-error' : ''}}">
                    {!! Form::label('nomprograma', 'Programa / Proyecto: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('nomprograma', null, ['class' => 'form-control','maxlength'=>'120']) !!}
                        {!! $errors->first('nomprograma', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('fec_vencimiento') ? 'has-error' : ''}}">
                    {!! Form::label('fec_vencimiento', 'Caduca: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                <input type="text" class="form-control" id="fec_vencimiento" name="fec_vencimiento" value="" readonly name="datepicker">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('file_foto') ? 'has-error' : ''}}">
                    {!! Form::label('file_foto', 'Foto: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        <input name="file_foto" type="file" id="file_foto" required='required'>

                        {!! $errors->first('file_foto', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <br>
                <div id="mensajes"></div>


            </div>
            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="button"  class="btn blue" onclick="btn_enviar()">Guardar</button>
            </div>

        </form>
    </div>
    @if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif

</div>
@endsection



@section('js')
<script type="text/javascript">

    function btn_enviar() {

        loading(true);
        var formData = new FormData(document.getElementById("form_sample_3"));
        formData.append("_token", "{{ csrf_token() }}");

        $.ajax({
            url: "{{ route('fotocheckgestionarpedido.store.personaexterna')}}",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
                .done(function (data) {
                    
                    loading(false);
                    if (data.validacion == 1) {
                        $("#mensajes").html(data.message);
                    } else {
                         $('#ajax_modal').modal('toggle');
                        swal(data.message, null, data.status);
                    }
                    //oTableSolicitudesSunedu.ajax.reload();
                }).error(function (data) {
            // location.reload();
        });
    }

</script>




@endsection