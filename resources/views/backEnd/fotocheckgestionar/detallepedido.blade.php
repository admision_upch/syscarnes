@extends('backLayout.app')
@section('title2')
 @if ( $pedido->codestado ==0 )
 
<h1>Detalle de Pedido - Fotocheck @if ($pedido->duplicado==1) (Duplicados) @else <a href="{{ Route('fotocheckgestionarpedido.create.personaexterna', ['pedido_id'=>$pedido->id] ) }}" class="btn btn-primary pull-right btn-sm"  data-target="#ajax_modal" data-toggle="modal"  ><i class="fa fa-user-plus"></i> Agregar Nueva Persona</a>  @endif</h1> 

@else

<h1>Detalle de Pedido - Fotocheck @if ($pedido->duplicado==1) (Duplicados) @endif</h1> 
@endif
                                    

 @if ( $pedido->codestado =='0'  )
 <div class="alert alert-info"><strong>Información!</strong> <p style="font-weight: normal;" class="caption-subject  lowercase">Por favor, seleccione los carnés que va a solicitar, haciendo click sobre el check en la columna <strong>Solicitar Fotocheck</strong></p>
  <p style="font-weight: normal;" class="caption-subject  lowercase">La <strong> FECHA DE CADUCIDAD</strong> es aproximada, por favor verifiquela.</p>
 </div>

@endif

@endsection
@section('content2')
<hr>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Pedido: Código N° {{ $pedido->id }} - Estado: {{ $pedido->estado }}</span>
        </div>
    </div>

    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_5_1" data-toggle="tab"> Solicitud de Fotochecks </a>
                </li>

@if ($pedido->duplicado!=1)
                <li>
                    <a href="#tab_5_2" data-toggle="tab"> Pedido Masivo </a>
                </li>
@endif

            </ul>

            <div class="tab-content">
                <div  class="tab-pane active" id="tab_5_1">
                    <div class="portlet-body">
                        <div class="col-md-12 ">
                            <form method="POST"  role="form" id="formulario" name="formulario"  accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly="">

                                <div class="form-group {{ $errors->has('facultad_id') ? 'has-error' : ''}}">
                                    {!! Form::label('facultad_id', 'Facultad: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectFacultades !!}
                                        {!! $errors->first('programa_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('programa_id') ? 'has-error' : ''}}">
                                    {!! Form::label('programa_id', 'Programa: ', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! $selectProgramas !!}
                                        {!! $errors->first('programa_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <button type="submit" id="btn_buscar" name="btn_buscar" class="btn blue">Buscar</button>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>
                        <hr>

                        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantes">
                            <thead>
                                <tr>
                                    <!--th></th-->
                                    <th>Cod.Estudiante</th>
                                    <th>Tipo Documento</th>
                                    <th>N° Documento</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Nombre</th>
                                    <th>Facultad</th>
                                    <th>Cod.Prog</th>
                                    <th>Programa</th>
                                    <th>Fec.Caducidad</th>
                                    <th>Año Ingreso</th>
                                    <th>Modalidad</th>

                                    @if ( $pedido->estado ==0 )

                                    <th>Solicitar Fotocheck</th>
                                    @else

                                    <th>Estado</th> 
                                    @endif
                                    <th>foto</th>
                                    <th>Abreviatura Facultad</th>
                                    <th>Ultima Matricula</th>
                                </tr>
                            </thead>
                        </table>



                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet light portlet-fit ">
                                    <div class="portlet-body">
                                        @if ( $pedido->codestado =='0'  )
                                        <a id="cerrarPedido" class="btn btn-danger btn-lg" onclick="cerrarPedido()"><i class="glyphicon glyphicon-close"> </i>SOLICITAR PEDIDO DE FOTOCHECKS</a>

                                        @else
                                        @if ( $permisos->permission_create ==true )
                                         @if ( $pedido->codestado ==1 )
                                         <div  id="divArchivosGenerados">


                                         </div>
                                         
                                        <br>
                                        
                                        <a id="imprimirpedido" onclick="imprimirpedido();"   class="btn btn-primary btn-lg" href="#"><i class="glyphicon glyphicon-print"> </i> Generar archivos para impresión</a> 
                                        
                                             @endif
                                             
                                            @if ( $pedido->codestado ==2 )

                                           <br>
                              
                                           <div class="col-md-12 ">
                                           <form class="form-inline" role="form">
                                                <div class="form-group">
                                                    <label class="sr-only" for="nrodocumento_recepciona">N° Documento Recepciona</label>
                                                    <input style="width:410px;" class="form-control input-lg col-lg-3" type="text" id="nrodocumento_recepciona" name="nrodocumento_recepciona" placeholder="N° doc. de persona que recepciona el pedido"  />
                                                </div>

                                               <button type="button" onclick="cambiar_estado_entregado();"    class="btn btn-primary btn-lg" ><i class="fa fa-hand-o-right"> </i> Entregar a Facultad/Escuela</button> 

                                            </form>
                                          </div>
                                           @endif
                                           
                                                                                      
                                            @if ( $pedido->codestado ==3 )

                                           <br>
                              
                                           <div class="col-md-12 ">
                                               <a class="btn btn-primary btn-lg" target="_blank" href="{{ asset( Storage::url('fotochecks/pedidos/pedido_' . $pedido->id.'/pedido_'.$pedido->id.'_entrega.pdf'))}}"><i class="fa fa-print"></i> Reporte de Entrega</a>
                                          </div>
                                           @endif
                                             
                                        
                                        @endif

                                        @endif
                                        <div class="mt-element-list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                



                    </div>
                </div>

@if ($pedido->duplicado!=1)
             


                <div  class="tab-pane" id="tab_5_2">
                    <div class="portlet-body">

                        <div class="alert alert-info"><strong>Información!</strong> Para solicitar los fotochecks de forma masiva, por favor seleccionar el programa y hacer click en <strong>Procesar Solicitud Masiva</strong>. Finalizado el procesamiento, ir a la pestaña <strong>Solicitud de Fotochecks</strong> para verificar. </div>


                        <form method="POST"  role="form" id="formulario2" name="formulario2"  accept-charset="UTF-8" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly="">

                            <div class="form-group {{ $errors->has('facultad_id2') ? 'has-error' : ''}}">
                                {!! Form::label('facultad_id2', 'Facultad: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-6">
                                    {!! $selectFacultades2 !!}
                                    {!! $errors->first('facultad_id2', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('programa_id2') ? 'has-error' : ''}}">
                                {!! Form::label('programa_id2', 'Programa: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-6">
                                    {!! $selectProgramas2 !!}
                                    {!! $errors->first('programa_id2', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha Vencimiento: </label>
                                <div class="col-sm-6">
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" id="fec_vencimiento" name="fec_vencimiento"  readonly name="datepicker">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Reemplazar Fecha de Vencimiento: </label>
                                <div class="col-sm-6">
                                    
                                        <input type="checkbox" id="chk_fec_vencimiento_reemplazo" name="chk_fec_vencimiento_reemplazo" /> 
                                        <span>(Seleccionar si desea reemplazar la fecha de vencimiento asignada al estudiante)</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-6">
                                </div>
                                <button type="submit" id="btn_procesar_solicitudmasiva" name="btn_procesar_solicitudmasiva" class="btn blue">Procesar Solicitud Masiva</button>
                            </div>
                        </form>






                    </div>
                </div>


@endif

            </div>
        </div>
    </div>
    <hr>
    <button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>
</div>

<div class="row">
    <div class="mt-element-ribbon bg-grey-steel">
        <div class="ribbon ribbon-color-default uppercase">Emails Seleccionados</div>
        <p class="ribbon-content" id="emailscopiados"></p>
    </div>
</div>
@endsection

@section('js2')
<script type="text/javascript">
    

    
    $(document).ready(function () {
        verArchivosGenerados();

    //    $('.datepicker').datepicker();
    /*INICIO: Buscar*/
    $("#btn_buscar").click(function () {
    var codprograma = ($("#programa_id").val());
    var facultad = $('#facultad_id option:selected').text();
    var val_buscar = '';
    if (codprograma != 'all'){
    val_buscar = codprograma;
    oTableEstudiantes.search(val_buscar).draw();
    return false;
    }

    if (facultad != 'Todos'){
    val_buscar = facultad;
    oTableEstudiantes.search(val_buscar).draw();
    return false;
    }

    oTableEstudiantes.search(val_buscar).draw();
    });
    /*FIN: Buscar*/

    /*INICIO: Mostrar Programas en Select al Cambiar Facultad*/
    $('#facultad_id').change(function () {
    var facultad_id = $("#facultad_id").val();
    $.post("{{ route('fotocheck.programas_select')}}",
    {
    facultad_id: facultad_id,
            "_token": "{{ csrf_token() }}"
    }).done(function (data) {
    $('#programa_id').html(data);
    }).fail(function (d) {
    alert('Error');
    });
    });
    /*FIN: Mostrar Programas en Select al Cambiar Facultad*/

    /*INICIO: Procesar Solicitud Masiva*/
    $("#btn_procesar_solicitudmasiva").click(function () {
        
      var estado = '0';
      if ($('#chk_fec_vencimiento_reemplazo').prop('checked')) {
        estado = '1';
      }    
    var codprograma = ($("#programa_id2").val());
    var fec_vencimiento = $("#fec_vencimiento").val();
    $('#btn_procesar_solicitudmasiva').attr('disabled', 'disabled');
    loading(true);
    $.post("{{ route('fotocheckgestionarpedido.procesar.solicitudmasiva')}}",
    {
    programa_cod: codprograma,
            pedido_id: {{$pedido->id}},
            fec_vencimiento: fec_vencimiento,
            chk_fec_vencimiento_reemplazo: estado,
            "_token": "{{ csrf_token() }}"
    }).done(function (data) {
    loading(false);
    $('#btn_procesar_solicitudmasiva').removeAttr('disabled');
    swal(data.message, null, data.status);
    oTableEstudiantes.ajax.reload();
    }).fail(function (data) {
    // alert('error x1');
    location.reload();
    });
    });
    /*FIN: Procesar Solicitud Masiva*/

    /*INICIO: Mostrar Programas en Select al Cambiar Facultad*/
    $('#facultad_id2').change(function () {
    var facultad_id = $("#facultad_id2").val();
    $.post("{{ route('fotocheck.programas_select')}}",
    {
    facultad_id: facultad_id,
            pedido_id: {{ $pedido->id}},
            "_token": "{{ csrf_token() }}"
    }).done(function (data) {
    $('#programa_id2').html(data);
    }).fail(function (d) {
    alert('Error');
    });
    });
    /*FIN: Mostrar Programas en Select al Cambiar Facultad*/

    var codprograma = ($("#programa_id").val());
    /*INICIO: Cargar Tabla Estudiantes*/
    oTableEstudiantes = $('#estudiantes').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#estudiantes').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.estudiantes.fotocheck')}}",
                    "type": "POST",
                    "data" : {
                    pedido_id: {{ $pedido->id}},
                            codprograma:  codprograma,
                            "_token": "{{ csrf_token() }}"
                    },
                    "complete": function (json, type) {
                    $('#estudiantes').show();
                    if (type == "error") {
                    //   oTableEstudiantes.ajax.reload();
                    }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                    //  oTableEstudiantes.ajax.reload();
                    }
            },
            "order": [[3, "asc"]],
            "columns": [

            {data: 'codalumno', name: 'codalumno', visible: false},
            {data: 'tipodocumento', name: 'tipodocumento', visible: false},
            {data: 'nrodocumento', name: 'nrodocumento'},
            {data: 'apepaterno', name: 'apepaterno'},
            {data: 'apematerno', name: 'apematerno'},
            {data: 'nombre', name: 'nombre'},
            {data: 'nomfacultad', name: 'nomfacultad', visible: false},
            {data: 'codprograma', name: 'codprograma', visible:false},
            {data: 'nomprograma', name: 'nomprograma'},
            {data: 'fec_vencimiento', name: 'fec_vencimiento'},
            {data: 'ano_ing', name: 'ano_ing', visible: false},
            {data: 'codmodalidad', name: 'codmodalidad', visible: false},
            {data: 'estado', name: 'estado', "className": "dt-center"},
            {data: 'foto', name: 'foto', "className": "dt-center"},
            {data: 'abreviaturafacultad', name: 'abreviaturafacultad', visible: false},
            {data: 'cod_periodo_matricula', name: 'cod_periodo_matricula', visible: false}

            ]
    });
    /*FIN: Cargar Tabla Estudiantes*/







    });
    /*INICIO: Cerrar Pedido*/
    function cerrarPedido(){
    loading(true);
    $.post("{{ route('fotocheckgestionarpedido.solicitar.pedido')}}",
    {
    pedido_id: {{ $pedido->id}},
            "_token": "{{ csrf_token() }}"
    }).done(function (data) {

    loading(false);
    swal(data.message, null, data.status);
    if (data.error!=1){
        var finalizar = function(){
        location.reload()
        };
        setTimeout(finalizar, 2000);
    }
    
    }).fail(function (d) {
    alert('Error');
    });
    }
    /*FIN: Cerrar Pedido*/

    /*INICIO: Agregar Email*/
    function agregarEmail(email){
    $("#emailscopiados").append(email + ', ');
    }
    /*INICIO: Agregar Email*/


    /*INICIO: Cambiar Estado*/
    function cambiarEstado(id){
    var estado = '0';
    if ($('#' + id).prop('checked')) {
    estado = '1';
    }

    $.post("{{ route('fotocheckgestionarpedido.cambiarestado')}}",
    {
    id: id,
            pedido_id: {{ $pedido->id}},
            estado: estado,
            "_token": "{{ csrf_token() }}"
    }).done(function (d) {
        if (d.code==0){
          
             $('#' + id).css("display", 'none');  
        }
            $("#nameestado" + id).html(d.nameestado);
            $("#validadopor" + id).css("display", 'none'); 
    
    }).fail(function () {
    alert('error');
    });
    }
    /*FIN: Cambiar Estado*/




    function imprimirpedido() {
        pedido_id= {{ $pedido->id}};
        loading(true);      
        $.post("{{ route('fotocheckgestionarpedido.imprimir.pedido')}}",
        {
           pedido_id: pedido_id,
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
            verArchivosGenerados();

        }).fail(function (d) {
                    alert('Error');
        });
    }
    
    
     function verArchivosGenerados(){
         
         pedido_id= {{ $pedido->id}};
         loading(true);      
        $.post("{{ route('fotocheckgestionarpedido.verarchivosgenerados')}}",
        {
            pedido_id: pedido_id,
            "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             loading(false);      
            $("#divArchivosGenerados").html(data);
        }).fail(function (d) {
                alert('Error');
        });
    }
    
    
    function cambiar_estado_generado() {
        pedido_id= {{ $pedido->id}};
        loading(true);      
        $.post("{{ route('fotocheckgestionarpedido.cambiar.estado.generado')}}",
        {
           pedido_id: pedido_id,
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
            var finalizar = function(){
            location.reload()
            };
            setTimeout(finalizar, 2000);

        }).fail(function (d) {
            alert('Error');
        });
    }
    
    
    function cambiar_estado_entregado() {
            loading(true);    
            var val_nrodocumento = $("#nrodocumento_recepciona").val();
            //alert(val_nrodocumento);
        $.post("{{ route('fotocheckentregar.imprimir.pedido')}}",
        {
           pedido_id: {{ $pedido->id}},
           nrodocumento_recepciona: val_nrodocumento,
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             loading(false);      
             swal(data.message, null, data.status);
            if (data.error != 1){
                var finalizar = function(){
                location.reload()
                };
                setTimeout(finalizar, 2000);
            }

        }).fail(function (d) {
            alert('Error');
        });
    }
    




</script>  


@endsection