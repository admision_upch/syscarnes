@extends('backLayout.app')
@section('title2')




<h1>Gestionar Pedidos - Fotocheck    <a href="{{ Route('fotocheckgestionarpedido.create', ['duplicado'=>0]) }}"  style="font-weight:bold;" class="btn btn-primary pull-right btn-sm">Solicitar Nuevo Pedido <i>(Regular)</i></a> <a href="{{ Route('fotocheckgestionarpedido.create', ['duplicado'=>1]) }}" style="font-weight:bold;" class="btn btn-info pull-right btn-sm">Solicitar Nuevo Pedido <i>(Duplicados)</i></a>  </h1>    



@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblpedidos">
        <thead>
            <tr>
                <th>ID</th>
                @if ($permisos->permission_create ==true)
                <th>Solicitado Por:</th>
                @endif
                <th>Fecha Solicitado</th>
                <th>Fecha Generado</th>
                <th>Fecha Entregado</th>
                <th>Cantidad</th>
                <td>Estado</td>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedidos as $item)

            
            <tr>
                <td>{{ $item->id }}</td>
                @if ($permisos->permission_create ==true)
                <th>{{ $item->user_create }}</th>
                @endif
                <td>{{ $item->fec_solicita }}</td>
                <td>{{ $item->fec_genera }}</td>
                <td>{{ $item->fec_entrega }}</td>
                <td>{{ $item->cantidad }}</td>
                <td >
                    
                     
                        
                            
                    @if ($item->estado_id==0)
                        <button class="btn btn-xs btn-default bold" style="color:#000;"  >{{ $item->estado }} @if ($item->duplicado==1)    <i>(Duplicado)</i>    @endif</button>
                    @endif
                                
                    @if ($item->estado_id==1)
                        <button class="btn btn-xs btn-warning bold" style="color:#000;"  >{{ $item->estado }} @if ($item->duplicado==1)    <i>(Duplicado)</i>    @endif </button>
                    @endif
                    @if ($item->estado_id==2)
                        <button class="btn btn-xs btn-success bold" style="color:#000;"  >{{ $item->estado }} @if ($item->duplicado==1)    <i>(Duplicado)</i>    @endif </button>
                    @endif
                    @if ($item->estado_id==3)
                    <button class="btn btn-xs btn-primary bold" >{{ $item->estado }} @if ($item->duplicado==1)    <i>(Duplicado)</i>    @endif </button>
                    @endif
            
                
                </td>
                
                <td>
                    <a href="{{ Route('fotocheckgestionarpedido.detalle', ['pedido_id'=>$item->id] ) }}" class="btn btn-xs btn-primary"><i class="icon-settings"></i> Gestionar</a>
                    
                </td>
            </tr>
            
            
            @endforeach
        </tbody>
    </table>
</div>

<div  id="detallePedido">


</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {

        $('#tblpedidos').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: true
                },
            ],
            order: [[0, "desc"]],
        });
    });

</script>
@endsection