

<h3>
<button id="importarEstudiantesSINU"  class="btn btn-primary"  onclick="importarEstudiantesSINU()" ><i class="fa fa-database"></i> Importar Estudiantes SINU </button> <span id="ultimaFechaImportacion" class="badge badge-success badge-roundless"><strong>( Ultima Fecha de Importación:@if ( !empty ($pedido->fec_importacion_estudiantes) ) {{$pedido->fec_importacion_estudiantes->format('d/m/Y H:i:s')}}   @endif )</strong></span>
 </h3>
<div id="mensajes"></div>
<hr/>
<h4>Estudiantes SINU Importados para el {{$pedido->nombre}} </h4>    

<div class="table ">
    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantesSinu">
        <thead>
            <tr>
                <th>Cod.Alumno</th>
                <th>Tipo Documento</th>
                <th>N° Documento</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Nombre</th>
                <th>Facultad</th>
                <th>Programa</th>
                <th>Año Ingreso</th>
                <th>Periodo Ingreso</th>
                <th>Modalidad</th>
                <th>Formacion</th>
                <th>Estado</th>

            </tr>
        </thead>
    </table>
</div>

<div  id="detallePedido">


</div>

<script type="text/javascript">

    $(document).ready(function () {
    /*INICIO: Cargar Tabla Estudiantes*/
     oTableEstudiantesSinu = $('#estudiantesSinu').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#estudiantes').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.estudiantes.sinu')}}",
                    "type": "POST",
                    "data" : {
                    pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                    },
                    "complete": function (json, type) {
                        $('#estudiantes').show();
                    },
                    "error": function(jqXHR, textStatus, ex) {
                        location.reload();
                    }
            },
            "order": [[9, "asc"]],
            "columns": [

            {data: 'codalumno', name: 'codalumno'},
            {data: 'tipodocumento', name: 'tipodocumento'},
            {data: 'nrodocumento', name: 'nrodocumento'},
            {data: 'apepaterno', name: 'apepaterno'},
            {data: 'apematerno', name: 'apematerno'},
            {data: 'nombre', name: 'nombre'},
            {data: 'abreviaturafacultad', name: 'abreviaturafacultad'},
            {data: 'codprograma', name: 'codprograma'},
            {data: 'ano_ing', name: 'ano_ing'},
            {data: 'per_ing', name: 'per_ing'},
            {data: 'modalidad_desc', name: 'modalidad_desc'},
            {data: 'formacion', name: 'formacion'},
            {data: 'estado', name: 'estado', "className": "dt-center"}
            ]
    });
    /*FIN: Cargar Tabla Estudiantes*/

    });
    
    function importarEstudiantesSINU(){
         $('#importarEstudiantesSINU').attr('disabled', 'disabled');
        loading(true);         
        var l = Ladda.create( document.querySelector( '#importarEstudiantesSINU' ) );
        l.start();
        $("#mensajes").html('');
        generarCodigosAlumnos();
        var finalizar = function(){
             $('#importarEstudiantesSINU').removeAttr('disabled');
           
             l.stop();
        };
        setTimeout(finalizar, 1000);

    }
    function generarCodigosAlumnos() {
        $.post("{{ route('pedidosgestionar.open.paso01.generarcodalumnosinu')}}",
        {
                pedido_id: {{$pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#mensajes").append(data.message);
            insertarAlumnosSINU();
        }).fail(function (data) {
            $("#mensajes").append(data.message);
        });
    }
    function insertarAlumnosSINU() {
        $.post("{{ route('pedidosgestionar.open.paso01.insertaralumnossinu')}}",
        {
                pedido_id: {{$pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {

			if (data.info='1'){
				$("#ultimaFechaImportacion").html('<strong>Ultima Fecha de Importación: '+data.messageFechaActualizacion+'</strong>');
			}
            $("#mensajes").append(data.messageHtml);
            $("#mensajes").append(data.messageFinalizadoHtml);
            
            swal(data.messageFinalizado, null, data.status);
            oTableEstudiantesSinu.ajax.reload();
            loading(false); 

        }).fail(function (data) {
            $("#mensajes").append(data.messageHtml);
        });
    }
</script>
