<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            @page {
                size: 325px 202px;
                margin: 0px;
            }
            .bgimg {
                position: fixed;
                width: 325px;
                height: 200px;
                z-index: -999;
                margin: 0px 0px 0px 0px;
            }
            .page-break {
                page-break-after: always;
            }

            .titulo_tarjeta{

                display: block;
                position: absolute;
                top:  8px;
                left: 155px;
                padding: 5px;
                width: 200px;
                height: 10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
            }
            
            .titulo_duplicado{

                display: block;
                position: absolute;
                top:  20px;
                left: 175px;
                padding: 5px;
                width: 200px;
                height: 10px;
                color:#D82600;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
            
            }

            .fotoestudiante {
                display: block;
                position: absolute;
                top:  53px;
                left: 7.5px;

                width: 95px;
                height: 116.5px;
                color:#000;

                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
            }


            .doc {
                display: block;
                position: absolute;
                top:  173px;
                left: 8px;
                width: 95px;
                color:#000;
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
                text-align: center;
                font-weight: bold;

            }

            .titulo_apellidos {
                display: block;
                position: absolute;
                top:  47px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;

                font-family: Arial, Helvetica, sans-serif;
                font-size:9px;
            }

            .apellidos {
                display: block;
                position: absolute;
                top:  57px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:11px;
            }

            .titulo_nombres {
                display: block;
                position: absolute;
                top:  85px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;

                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
            }

            .nombres {
                display: block;
                position: absolute;
                top:  95px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:11px;
            }


            .titulo_facultad {
                display: block;
                position: absolute;
                top:  115px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;

                font-family: Arial, Helvetica, sans-serif;
                font-size:9px;
            }

            .facultad {
                display: block;
                position: absolute;
                top:  125px;
                left: 107px;
                padding: 5px;
                width: 200px;
                height: 10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:9px;
            }


            .titulo_programa {
                display: block;
                position: absolute;
                top:  147px;
                left: 107px;
                padding: 5px;
                width: 100%;
                color:#000;

                font-family: Arial, Helvetica, sans-serif;
                font-size:8px;
            }

            .programa {
                display: block;
                position: absolute;
                top:  157px;
                left: 107px;
                padding: 5px;
                width: 190px;
                height:10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:7px;
            }

            .titulo_fecvencimiento{
                -webkit-transform: rotate(-90deg);
                transform: rotate(-90deg);
                display: block;
                position: absolute;
                top:  60px;
                left: 200px;
                padding: 5px;
                width: 200px;
                height: 10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:11px;
            }

            .fecvencimiento{
                -webkit-transform: rotate(-90deg);
                transform: rotate(-90deg);
                display: block;
                position: absolute;
                top:  15px;
                left: 200px;
                padding: 5px;
                width: 200px;
                height: 10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:11px;
            }


            .mensaje_back{

                display: block;
                position: absolute;
                top:  30px;
                left: 10px;
                padding: 5px;
                width: 280px;
                height: 10px;
                color:#000;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
                font-size:9px;

            }

            .codigobarras{
                display: block;
                position: absolute;
                top:  165px;
                left: 80px;
                width: 160px;
                height: 30px;
                color:#000;
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
                text-align: center;
                font-weight: bold;
            }

            .linea_inferior {
                display: block;
                position: absolute;
                top:  196px;
                width: 110%;
                height: 5px;
                color:#000;
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
                text-align: center;
                font-weight: bold;
            }
            

            
            
        </style>
    </head>
    <body>


        @foreach($estudiantesfotocheck  as $indexKey => $item)
        <div style="width:323px;">

            <img  src="{{ asset('/img/logo/fotocheck2017.jpg') }}" alt="logo" class="bgimg" /> 

            <img  src="{{ asset(Storage::url('/fotochecks/fotos/'.$item['nrodocumento'].'.jpg')) }}" alt="logo" class="fotoestudiante" /> 
            <div class="titulo_tarjeta">TARJETA PERSONAL </div>
            
            @if ($item['duplicado'] == 1) 
            <div class="titulo_duplicado">DUPLICADO </div>
            @endif
             
             @if ($item['tipodocumento'] > 1)
            <div class="doc">COD: {{$item['nrodocumento']}}</div>
            @else
                <div class="doc">DNI: {{$item['nrodocumento']}}</div>
            @endif
            <div class="titulo_apellidos">Apellidos:</div>
            <div class="apellidos">{{$item['apepaterno']}} <br>{{$item['apematerno']}}   </div>
            <div class="titulo_nombres">Nombres:</div>
            <div class="nombres">{{$item['nombre']}}</div>
            
            @if ($item['niv_formacion'] == '' )
          
                <div class="titulo_facultad">Dirección:</div>
            @else
                <div class="titulo_facultad">Facultad/Escuela:</div>
            @endif
            
            
            <div class="facultad">{{$item['nomfacultad']}}</div>
            
            @if ($item['niv_formacion'] == '' )
          
                <div class="titulo_programa">Programa/Proyecto:</div>
            @else
                <div class="titulo_programa">Programa:</div>
            @endif
            
            <div class="programa">{{$item['nomprograma']}}</div>
            <div class="titulo_fecvencimiento">Caduca:</div>
            <div class="fecvencimiento">{{$item['fec_vencimiento']}}</div>

            @if ($item['niv_formacion'] == 4 )
                <div class="linea_inferior" style="background-color: #edb91dfa;"></div>
            @endif
            
            
            @if ($item['niv_formacion'] == 5 || $item['niv_formacion'] == 6 || $item['niv_formacion'] == 7 || $item['niv_formacion'] == 8 )
                <div class="linea_inferior" style="background-color: #ab2c43fa;"></div>
            @endif
            
            
            @if ($item['niv_formacion'] == 9 )
                <div class="linea_inferior" style="background-color: #438a1dfa;"></div>

                
            @endif
            @if ($item['niv_formacion'] == '' )
          
                <div class="linea_inferior" style="background-color: #333;"></div>
                
            @endif
            @if ($item['niv_formacion'] == '2' )
          
                <div class="linea_inferior" style="height: 3px;background-color: #FFF;"></div>
                
            @endif
            
            
        </div>

        <div class="page-break"></div>

        <div style="width:323px;">

            <img  src="{{ asset('/img/logo/fotocheck2017back.jpg') }}" alt="logo" class="bgimg" /> 

            <div class="mensaje_back">La utilización de esta tarjeta permite identificar al estudiante dentro de las instalaciones de la UPCH.<br>
                Su uso es obligatorio.
            </div>

            <img  src="{{ asset(Storage::url('/fotochecks/codigobarras/'.$item['nrodocumento'].'.png')) }}" alt="codigobarras" class="codigobarras" /> 


        </div>

        @if (count($estudiantesfotocheck) > $indexKey+1)
        <div class="page-break"></div>
        @endif



        @endforeach

    </body>
</html>