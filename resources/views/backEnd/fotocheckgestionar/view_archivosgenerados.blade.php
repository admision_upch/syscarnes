

<div class="portlet-body">
    <div class="mt-element-list">
        <div class="mt-list-container list-simple ext-1 group">
            <a class="list-toggle-container" data-toggle="collapse" href="#archivosGenerados" aria-expanded="false">
                <div class="list-toggle done uppercase"> Archivos generados para impresión
                    <span class="badge badge-default pull-right bg-white font-blue-sharp bold">{{ count($listadoArchivosGenerados) }}</span>
                </div>
            </a>
            <div class="panel-collapse collapse in" id="archivosGenerados">
                <ul>

                    @if (count($listadoArchivosGenerados)>0)
                    @foreach($listadoArchivosGenerados as $archivo)


                    <li class="mt-list-item done">
                        <div class="list-icon-container">
                            <i class="icon-check"></i>
                        </div>
                        <div class="list-datetime">  </div>
                        <div class="list-item-content">
                            <h3 class="uppercase">
                                <a target="_blank" href="{{ asset(  $archivo['link'] )}}">  {{$archivo['name']}}</a>
                            </h3>
                        </div>
                    </li>
                    @endforeach
                    @if ( $pedido->estado ==1  )
                    <a  onclick="cambiar_estado_generado()();"   class="btn btn-info btn-lg" href="#"><i class="glyphicon glyphicon-transfer"> </i> Cambiar pedido a estado <strong>GENERADO</strong><em>( Click solo si ha impreso todos los fotochecks )</em></a> 
                    @endif
                    @else

                    <li class="mt-list-item done">
                        <div class="list-icon-container">
                            <i class="icon-check"></i>
                        </div>
                        <div class="list-datetime">  </div>
                        <div class="list-item-content">
                            <h3 class="uppercase">
                                No existen archivos generados,<br> click en el botón <strong>Generar archivos para impresión</strong>
                            </h3>
                        </div>
                    </li>

                    @endif




                </ul>
            </div>

        </div>
    </div>
</div>