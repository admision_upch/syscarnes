@extends ('layouts.modal_formulario')    
@section('content')   


<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase">Fecha Vencimiento Editar</span>
        </div>
    </div>
    <div class="portlet-body form">
        <div id="respuesta"  ></div>
        
        @if ($EstudianteFotocheck)
        
        <form role="form" id="form_sample_3" method="POST"  name="form_sample_3" >
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" class="form-control" value="{{ $EstudianteFotocheck->id}}" id="estudiante_fotocheck_id" name="estudiante_fotocheck_id" >
            <input type="hidden" class="form-control" value="{{ $EstudianteFotocheck->codprograma}}" id="estudiante_fotocheck_codprograma" name="estudiante_fotocheck_codprograma" >

            <div class="form-body">
                
                <div class="form-group">
                    <label>Estudiante:</label>
                    <div>
                        <input type="text" class="form-control" placeholder="{{ $EstudianteFotocheck->apepaterno}} {{ $EstudianteFotocheck->apematerno}}, {{ $EstudianteFotocheck->nombre}}" readonly="">
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Facultad:</label>
                    <div>
                        <input type="text" class="form-control" placeholder="{{ $EstudianteFotocheck->nomfacultad}}" readonly="">
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Programa:</label>
                    <div>
                        <input type="text" class="form-control" placeholder="{{ $EstudianteFotocheck->nomprograma}}" readonly="">
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Fecha Vencimiento</label>
                    <div class="input-group">
                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                            <input type="text" class="form-control" id="estudiante_fotocheck_fec_vencimiento" name="estudiante_fotocheck_fec_vencimiento" value="{{ $EstudianteFotocheck->fec_vencimiento}}" readonly name="datepicker">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                
                
                <div class="alert alert-info"><strong>Información!</strong> Si desea actualizar esta fecha de vencimiento para todos los estudiantes que usted a <strong>solicitado Fotocheck </strong>del programa <strong>{{ $EstudianteFotocheck->nomprograma }}</strong>. Marque la siguiente casilla: <strong><input type="checkbox" id="chk_all_programas" name="chk_all_programas"  /></strong> </div>
            </div>
            
        
            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
            </div>
        </form>
        
        @else
        
        Para cambiar la fecha de vencimiento, primero tiene que hacer click en Solicitar Fotocheck.
        
        @endif
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->




@endsection


@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $("#btn_enviar").click(function () {

            var url = "{{ route('fotocheckgestionarpedido.estudiantefotocheck.update') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "PUT",
                url: url,
                data: $("#form_sample_3").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
                    parent.oTableEstudiantes.ajax.reload();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>



@endsection