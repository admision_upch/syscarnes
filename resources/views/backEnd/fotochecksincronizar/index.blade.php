@extends('backLayout.app')
@section('title2')




<h1>Sincronización de Estudiantes (Fotocheck - SINU)</h1>    



@endsection

@section('content2')
<form role="form" method="POST" class="form-horizontal"   >

    <div class="col-sm-5">
        <a href="#" onclick="sincronizarEstudiantes();" class="btn btn-primary pull-left btn-sm"><i class="fa fa-users"></i> SINCRONIZAR ESTUDIANTES <em>(Este proceso es muy pesado, puede demorar varios minutos)</em></a>
    </div>

    <div class="col-sm-5">
        <a href="#" onclick="sincronizarFotosEstudiantes(null);" class="btn btn-primary pull-left btn-sm"><i class="fa fa-file-image-o"></i> SINCRONIZAR FOTOS ESTUDIANTES  <em>(Este proceso es muy pesado, puede demorar varios minutos)</em></a>
    </div>


</form>




<hr/>
<hr/>


<div class="tab-content">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-users font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Estudiantes UPCH </span>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantes">
                <thead>
                    <tr>
                        <!--th></th-->
                        <th>Cod.Estudiante</th>
                        <th>Tipo Documento</th>
                        <th>N° Documento</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Nombre</th>
                        <th>Facultad</th>
                        <th>Cod.Prog</th>
                        <th>Programa</th>
                        <th>Estado SINU</th>
                        <th>Caduca</th>
                        <th>Año Ingreso</th>
                        <th>Modalidad</th>
                        <th>Sincronizar</th> 
                        <th>foto</th>
                        <th>Abreviatura Facultad</th>
                    </tr>
                </thead>
            </table>

        </div>

    </div>    
</div>
@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {


    var codprograma = ($("#programa_id").val());
    /*INICIO: Cargar Tabla Estudiantes*/
    oTableEstudiantes = $('#estudiantes').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#estudiantes').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('fotochecksincronizar.datatable.estudiantes')}}",
                    "type": "POST",
                    "data" : {
                            "_token": "{{ csrf_token() }}"
                    },
                    "complete": function (json, type) {
                    $('#estudiantes').show();
                    if (type == "error") {
                    //   oTableEstudiantes.ajax.reload();
                    }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                    //  oTableEstudiantes.ajax.reload();
                    }
            },
            "order": [[3, "asc"]],
            "columns": [

            {data: 'codalumno', name: 'codalumno', visible: false},
            {data: 'tipodocumento', name: 'tipodocumento', visible: false},
            {data: 'nrodocumento', name: 'nrodocumento'},
            {data: 'apepaterno', name: 'apepaterno'},
            {data: 'apematerno', name: 'apematerno'},
            {data: 'nombre', name: 'nombre'},
            {data: 'nomfacultad', name: 'nomfacultad', visible: false},
            {data: 'codprograma', name: 'codprograma', visible:false},
            {data: 'nomprograma', name: 'nomprograma'},
            {data: 'estado_sinu', name: 'estado_sinu'},
            {data: 'fec_vencimiento', name: 'fec_vencimiento'},
            {data: 'ano_ing', name: 'ano_ing', visible: false},
            {data: 'codmodalidad', name: 'codmodalidad', visible: false},
            {data: 'estado', name: 'estado', "className": "dt-center"},
            {data: 'foto', name: 'foto', "className": "dt-center"},
            {data: 'abreviaturafacultad', name: 'abreviaturafacultad', visible: false}

            ]
    });
    /*FIN: Cargar Tabla Estudiantes*/


    });
    
    
    
    function sincronizarEstudiantes() {
        $.post("{{ route('fotochecksincronizar.generarcodigossinu')}}",
        {
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#mensajes").append(data.message);
            //insertarAlumnosSINU();
             loading(true);      
        $.post("{{ route('fotochecksincronizar.estudiantes')}}",
        {
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
            oTableEstudiantes.ajax.reload();
        }).fail(function (d) {
             swal('Error al procesar', null, 'warning');
        });
            //
        }).fail(function (data) {
            $("#mensajes").append(data.message);
        });
    }
    
    
   /*  function sincronizarEstudiantes() {
        loading(true);      
        $.post("{{ route('fotochecksincronizar.estudiantes')}}",
        {
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
            oTableEstudiantes.ajax.reload();
        }).fail(function (d) {
             swal('Error al procesar', null, 'warning');
        });
    }*/
    
    
      function sincronizarFotosEstudiantes(nrodocumento) {
        loading(true);      
        $.post("{{ route('fotochecksincronizar.imagenes')}}",
        {
           nrodocumento: nrodocumento,
           "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false);      
            swal(data.message, null, data.status);
            if (data.valor!=0){
                oTableEstudiantes.ajax.reload();    
            }

        }).fail(function (d) {
             swal('Error al procesar', null, 'warning');
        });
    }

</script>
@endsection