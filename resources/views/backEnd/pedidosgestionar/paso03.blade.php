<h3>Cargar Facultades/Programas Sunedu</h3>     
<hr>
<form class="form-inline" role="form" method="POST" id="formuploadajaxPaso03" name="formuploadajaxPaso03" accept-charset="UTF-8" enctype="multipart/form-data" >
    <label><i>Subir el archivo actualizado de la SUNEDU denominado "Archivo SUNEDU" : </i></label>
    <div class="form-group"><input type="file" class="btn btn-xs btn-primary"  id="file_archivosunedu" name="file_archivosunedu"  /></div>
    <button type="button" onclick="cargarArchivoSunedu();" class="btn btn-xs btn-primary"><i class="fa fa-upload"></i> Cargar Archivo</button>


</form>
<hr>


<div class="portlet-body">
    <div class="tabbable-custom ">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_open03_facultades" data-toggle="tab"> <strong>Facultades SUNEDU</strong></a>
            </li>
            <li>
                <a href="#tab_open03_programas" data-toggle="tab"> <strong>Programas SUNEDU </strong></a>
            </li>

        </ul>

        <div class="tab-content">
            <div  class="tab-pane active" id="tab_open03_facultades">
                <div class="table ">
                    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="facultadesSunedu">
                        <thead>
                            <tr>
                                <!--th></th-->
                                <th>Cod. Facultad</th>
                                <th>Facultad</th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>


            <div class="tab-pane" id="tab_open03_programas">
                <div class="table ">
                    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="programasSunedu">
                        <thead>
                            <tr>
                                <th>Cod. Facultad</th>
                                <th>Cod. Programa</th>
                                <th>Programa</th>
                                <th>Abreviatura</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).ready(function () {

        oTableFacultadesSunedu = $('#facultadesSunedu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": true,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": true,
            "preDrawCallback": function (settings) {
                $('#facultadesSunedu').hide();
            },
            'language': {
                "processing": '<b> Cargando...</b>'
            },
            "ajax": {
                "url": "{{ route('datatable.facultades.sunedu')}}",
                "type": "GET",
                "complete": function (json, type) {
                    $('#facultadesSunedu').show();
                    if (type == "error") {
                          location.reload();
                    }
                },
                "error": function (jqXHR, textStatus, ex) {
                     location.reload();
                }
            },
            "order": [[1, "asc"]],
            "columns": [

                {data: 'codfacultad', name: 'codfacultad', visible: true},
                {data: 'nomfacultad', name: 'nomfacultad'}


            ]
        });

        oTableProgramasSunedu = $('#programasSunedu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function (settings) {
                $('#programasSunedu').hide();
            },
            'language': {
                "processing": '<b> Cargando...</b>'
            },
            "ajax": {
                "url": "{{ route('datatable.programas.sunedu')}}",
                "type": "GET",
                "complete": function (json, type) {
                    $('#programasSunedu').show();
                    if (type == "error") {
                          location.reload();
                    }
                },
                "error": function (jqXHR, textStatus, ex) {
                      location.reload();
                }
            },
            "order": [[1, "asc"]],
            "columns": [

                {data: 'codfacultad', name: 'codfacultad', visible: true},
                {data: 'codprograma', name: 'codprograma'},
                {data: 'nomprograma', name: 'nomprograma'},
                {data: 'abreviatura', name: 'abreviatura'}

            ]
        });

    });
    function cargarArchivoSunedu() {

        loading(true);
        var formData = new FormData(document.getElementById("formuploadajaxPaso03"));
        formData.append("_token", "{{ csrf_token() }}");
        $.ajax({
            url: "{{ route('pedidosgestionar.open.paso02.subirarchivo.archivosunedu')}}",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            loading(false);
            swal(data.message, null, data.status);
            oTableFacultadesSunedu.ajax.reload();
            oTableProgramasSunedu.ajax.reload();
        }).error(function (data) {
            location.reload();
        });
    }
</script>
