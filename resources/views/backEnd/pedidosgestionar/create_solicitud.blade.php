@extends ('layouts.modal_formulario')    
@section('content')   


<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase">Crear Solicitud</span>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form"  class="form-inline" method="POST" id="formularioModal" name="formularioModal" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-body">
                <div class="form-group">
                    <label>Nro. Solicitud : </label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="nrosolicitud" name="nrosolicitud" >
                    </div>
                </div>
           </div>
            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->




@endsection


@section('js')
<script type="text/javascript">

    $(document).ready(function () {
        $("#btn_enviar").click(function () {
            var url = "{{ route('pedidosgestionar.open.paso02.store.solicitud') }}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#formularioModal").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                      
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    parent.oTableSolicitudesSunedu.ajax.reload();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>




@endsection