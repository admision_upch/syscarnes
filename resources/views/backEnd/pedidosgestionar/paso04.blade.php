<h3>
<button id="filtrarEstudiantes"  class="btn btn-primary"  onclick="filtrarEstudiantes()" ><i class="fa fa-play-circle"></i> Filtrar Estudiantes, Facultades y Programas </button> <span id="ultimaFechaFiltracion" class="badge badge-success badge-roundless"><strong>( Ultima Fecha de Filtracion: @if ( !empty ($pedido->fec_filtracion_estudiantes) ) {{$pedido->fec_filtracion_estudiantes->format('d/m/Y H:i:s')}} @endif )</strong></span>
 </h3>
<div id="mensajes"></div>
<hr/>

<div class="portlet-body">
    <div class="tabbable-custom ">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_open04_estudiantespedido" data-toggle="tab"> <strong>Estudiantes Pedido</strong></a>
            </li>
            <li>
                <a href="#tab_open04_facultades" data-toggle="tab"> <strong>Facultades SINU - SUNEDU</strong></a>
            </li>
            <li>
                <a href="#tab_open04_programas" data-toggle="tab"> <strong>Programas  SINU - SUNEDU </strong></a>
            </li>

        </ul>

        <div class="tab-content">
            <div  class="tab-pane active" id="tab_open04_estudiantespedido">
                <div class="table ">
                    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantesPedido">
                        <thead>
                            <tr>
                                <th>Cod.Alumno</th>
                                <th>N° Documento</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Nombre</th>
                                <th>Facultad</th>
                                <th>Programa</th>
                                <th>Modalidad</th>
                                <th>Formacion</th>
                                <th>Ubigeo Nac.</th>
                                <th>Foto</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
            <div  class="tab-pane" id="tab_open04_facultades">
                <div class="table ">
                                <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="facultadessinu">
                                    <thead>
                                        <tr>
                                            
                                            <th>Cod. Facultad Sinu</th>
                                            <th>Abrv.Facultad Sinu</th>
                                            <th>Facultad Sinu</th>
                                            <th>Modalidad</th>
                                            <th>Formacion</th>
                                            <th>Cod. Facultad Sunedu</th>
                                            <th>Facultad Sunedu</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                </table>
                </div>
            </div>


            <div class="tab-pane" id="tab_open04_programas">
                <div class="table ">
                                <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="programassinu">
                                    <thead>
                                        <tr>
                                            
                                            <th>Cod Facultad Sinu</th>
                                            <th>Facultad Sinu</th>
                                            <th>Modalidad</th>
                                            <th>Formacion</th>
                                            <th>Codigo<br> Programa Sinu</th>
                                            <th>Programa Sinu</th>
                                            <th>Codigo <br>Programa Sunedu</th>
                                            <th>Programa Sunedu</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                </table>
                </div>
            </div>


        </div>
    </div>
</div>




<script type="text/javascript">

    $(document).ready(function () {
    /*INICIO: Cargar Tabla Estudiantes Pedido*/
     oTableEstudiantesPedido = $('#estudiantesPedido').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#estudiantesPedido').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.estudiantes.pedido')}}",
                    "type": "POST",
                    "data" : {
                    pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                    },
                    "complete": function (json, type) {
                    $('#estudiantesPedido').show();
                    if (type == "error") {
                       location.reload();
                    }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                       location.reload();
                    }
            },
            "order": [[0, "asc"]],
            "columns": [

            {data: 'codalumno', name: 'codalumno'},
            {data: 'nrodocumento', name: 'nrodocumento'},
            {data: 'apepaterno', name: 'apepaterno'},
            {data: 'apematerno', name: 'apematerno'},
            {data: 'nombre', name: 'nombre'},
            {data: 'abreviaturafacultad_sinu', name: 'abreviaturafacultad_sinu'},
            {data: 'codprograma_sinu', name: 'codprograma_sinu'},
            {data: 'modalidad_desc', name: 'modalidad_desc',visible:false},
            {data: 'formacion', name: 'formacion'},
            {data: 'ubigeo_nac', name: 'ubigeo_nac', "className": "dt-center", visible:false},
            {data: 'foto', name: 'foto'}
            
            ]
    });
    /*FIN: Cargar Tabla Estudiantes Pedido*/
    
    
        /*INICIO: Cargar Tabla Facultades SINU*/   
         oTableFacultadesSINU = $('#facultadessinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#facultadessinu').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.facultadessinusunedu')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#facultadessinu').show();
                            if (type == "error") {
                                oTableFacultadesSINU.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableFacultadesSINU.ajax.reload();
                }
            },
            "order": [[1, "desc"]],
            "columns": [
                        
                        {data: 'codfacultad', name: 'codfacultad', visible: true},
                        {data: 'abreviaturafacultad', name: 'abreviaturafacultad'},
                        {data: 'nomfacultad', name: 'nomfacultad'},
                        {data: 'modalidad_desc', name: 'modalidad_desc'},
                        {data: 'formacion', name: 'formacion'},
                        {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
                        {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    /*FIN: Cargar Tabla Facultades SINU*/   
    
    /*INICIO: Cargar Tabla Programas SINU*/   
         oTableProgramasSINU = $('#programassinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
                $('#programassinu').hide();
            },
            'language':{
                "processing": '<b> Cargando...</b>'
            },            
            "ajax": {
                "url" : "{{ route('datatable.programassinusunedu')}}",
                "type": "POST",
                "data" : {
                            pedido_id: {{ $pedido->id}},
                            "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) { 
                            $('#programassinu').show();
                            if (type == "error") {
                                oTableProgramasSINU.ajax.reload();
                            }
                },
                "error": function(jqXHR, textStatus, ex) {
                            oTableProgramasSINU.ajax.reload();
                }
            },
            "order": [[2, "desc"]],
            "columns": [
                        
                        {data: 'codfacultad', name: 'codfacultad', visible: false},
                        {data: 'nomfacultad', name: 'nomfacultad', visible: true},
                        {data: 'modalidad_desc', name: 'modalidad_desc', visible: true},
                        {data: 'formacion', name: 'formacion', visible: true},
                        {data: 'codprograma', name: 'codprograma'},
                        {data: 'nomprograma', name: 'nomprograma'},
                        {data: 'codprograma_sunedu', name: 'codprograma_sunedu', visible: true},
                        {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;
                api.column(2, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group color md-bg-blue-500" style="font-weight:bold;"  ><td colspan="8">' + group + '</td></tr>'
                                );
                        last = group;
                    }
                });
            }
        });
    
    

    });

    function filtrarEstudiantes() {
        $('#filtrarEstudiantes').attr('disabled', 'disabled');
        loading(true);        
        $.post("{{ route('pedidosgestionar.open.paso04.filtrarestudiantespedido')}}",
        {
                pedido_id: {{$pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            loading(false); 
            $('#filtrarEstudiantes').removeAttr('disabled');
            $("#ultimaFechaFiltracion").html('<strong>Ultima Fecha de Filtración: '+data.messageFechaFiltracion+'</strong>');
            swal(data.message, null, data.status);
            oTableEstudiantesPedido.ajax.reload();
            oTableFacultadesSINU.ajax.reload();
            oTableProgramasSINU.ajax.reload();
        }).fail(function (data) {
            location.reload();
        });
    }
</script>
