@extends('backLayout.app')
@section('title2')
<h1>Editar Pedido</h1>
@endsection
@section('content2')
    <hr/>
{!! Form::model($pedido, [
'method' => 'PATCH',
'url' => ['pedidosgestionar/update', $pedido->id],
'class' => 'form-horizontal'
]) !!}

<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre: ', ['class' => 'col-sm-3 control-label', 'data-required'=>'1']) !!}
    <div class="col-sm-6">
        {!! Form::text('nombre', null, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('fec_inicio') ? 'has-error' : ''}}">
    {!! Form::label('fec_inicio', 'Fecha Inicio: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::date('fec_inicio',  $pedido->fec_inicio  , ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('fec_inicio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('fec_fin') ? 'has-error' : ''}}">
    {!! Form::label('fec_fin', 'Fecha Fin: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::date('fec_fin', $pedido->fec_fin, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('fec_fin', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_update')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection