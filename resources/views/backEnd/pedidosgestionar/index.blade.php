@extends('backLayout.app')
@section('title2')




<h1>Gestión de Pedidos - Carnés Universitarios<a href="{{ Route('pedidosgestionar.create') }}" class="btn btn-primary pull-right btn-sm">Agregar Nuevo Pedido</a></h1>    



@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblpedidos">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha Importacion Estudiantes</th>
                <th>Fecha Filtración Estudiantes</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Fecha Cierre</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pedidos as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->nombre }}</td>
                <td>{{ $item->fec_filtracion_estudiantes }}</td>
                <td>{{ $item->fec_importacion_estudiantes }}</td>
                <td>{{ $item->fec_inicio }}</td>
                <td>{{ $item->fec_fin }}</td>
                <td>{{ $item->fec_cierre }}</td>
                <td>{{ $item->estado }}</td>
                <td>
                    <a href="{{ Route('pedidosgestionar.edit', ['id'=>$item->id] ) }}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                    <a href="{{ Route('pedidosgestionar.open', ['id'=>$item->id] ) }}" class="btn btn-xs btn-primary"><i class="icon-settings"></i> Gestionar</a>
               
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div  id="detallePedido">


</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {

        $('#tblpedidos').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: true
                },
            ],
            order: [[0, "desc"]],
        });
    });

</script>
@endsection