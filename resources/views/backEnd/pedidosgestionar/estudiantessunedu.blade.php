



<h4>Estudiantes SUNEDU de la solicitud  N° {{$solicitud->nrosolicitud}} </h4>    

<div class="table ">
    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantesSunedu">
        <thead>
            <tr>
                

                <th>Cod.Alumno</th>
                <th>Carrera</th>
                <th>Tipo Documento</th>
                <th>N° Documento</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Nombre</th>
            
                <th>Estado</th>

            </tr>
        </thead>
    </table>
</div>

<script type="text/javascript">

    $(document).ready(function () {
    /*INICIO: Cargar Tabla Estudiantes*/
     oTableEstudiantesSunedu = $('#estudiantesSunedu').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#estudiantesSunedu').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.estudiantes.sunedu')}}",
                    "type": "POST",
                    "data" : {
                    solicitud_id: {{ $solicitud->id}},
                            "_token": "{{ csrf_token() }}"
                    },
                    "complete": function (json, type) {
                    $('#estudiantesSunedu').show();
                    if (type == "error") {
                    oTableEstudiantesSunedu.ajax.reload();
                    }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                   // oTableEstudiantesSunedu.ajax.reload();
                    }
            },
            "order": [[1, "asc"]],
            "columns": [

            {data: 'codalumno', name: 'codalumno'},
            {data: 'carrera', name: 'tipodocumento'},
            {data: 'tipodocumento', name: 'tipodocumento'},
            {data: 'nrodocumento', name: 'nrodocumento'},
            {data: 'apepaterno', name: 'apepaterno'},
            {data: 'apematerno', name: 'apematerno'},
            {data: 'nombre', name: 'nombre'},
            {data: 'estado', name: 'estado', "className": "dt-center"}
            ]
    });
    /*FIN: Cargar Tabla Estudiantes Sunedu*/

    });
    

</script>
