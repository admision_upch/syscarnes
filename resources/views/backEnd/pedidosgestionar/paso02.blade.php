<h3>Solicitudes SUNEDU <a href="{{ Route('pedidosgestionar.open.paso02.create.solicitud') }}" class="btn btn-primary pull-right btn-sm" data-target="#ajax_modal" data-toggle="modal">Agregar Nueva Solicitud</a></h3>  


<div id="mensajes"></div>
<div class="table ">
    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="solicitudesSunedu">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nro. Solicitud</th>
                <th>Total Estudiantes</th>
                <th>Acción</th>
            </tr>
        </thead>
    </table>
</div>

<div  id="divEstudiantesSunedu">


</div>

<script type="text/javascript">


    $(document).ready(function () {

    /*INICIO: Cargar Tabla Solicitudes SUNEDU*/
    oTableSolicitudesSunedu = $('#solicitudesSunedu').DataTable({
    "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": true,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": true,
            "preDrawCallback": function(settings) {
            $('#solicitudesSunedu').hide();
            },
            'language':{
            "processing": '<b> Cargando...</b>'
            },
            "ajax": {
            "url" : "{{ route('datatable.solicitudes.sunedu')}}",
                    "type": "GET",
                    "complete": function (json, type) {
                    $('#solicitudesSunedu').show();
                        if (json.sesion==0 ) {
                              swal(json.message, null, json.status);
                        }
                    },
                    "error": function(jqXHR, textStatus, ex) {
                      oTableSolicitudesSunedu.ajax.reload();
                    }
            },
            "order": [[1, "asc"]],
            "columns": [

            {data: 'id', name: 'id', visible:false},
            {data: 'nrosolicitud', name: 'nrosolicitud'},
            {data: 'totalestudiantes', name: 'totalestudiantes'},
            {data: 'action', name: 'action'}

            ]
    });
    /*FIN: Cargar Tabla Solicitudes SUNEDU*/



    });
        
    function cargarArchivoEstudiantesSunedu(id){
        $("#divEstudiantesSunedu").html('');
        loading(true);
        var formData = new FormData(document.getElementById("formuploadajax" + id));
        formData.append("_token", "{{ csrf_token() }}");
        formData.append("solicitud_id", id);
        $.ajax({
        url: "{{ route('pedidosgestionar.open.paso02.subirarchivo.estudiantessunedu')}}",
                type: "post",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false
                })
                .done(function(data){
                    loading(false);
                    swal(data.message, null, data.status);
                    oTableSolicitudesSunedu.ajax.reload();
                }).error(function(data){
                    location.reload();
                });
    }
    function deleteSolicitud(id){
         $.post("{{ route('pedidosgestionar.open.paso02.destroy.solicitud')}}",
        {
                solicitud_id: id,
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
                $("#divEstudiantesSunedu").html('');
                swal(data.message, null, data.status);
                oTableSolicitudesSunedu.ajax.reload();
        }).fail(function (data) {
            swal(data.message, null, data.status);
            location.reload();
        });
  }
    function verEstudiantesSunedu(id) {
        $.post("{{ route('pedidosgestionar.open.paso02.ver.estudiantessunedu')}}",
        {
                solicitud_id: id,
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             $("#divEstudiantesSunedu").html(data);
        }).fail(function (data) {
             $("#divEstudiantesSunedu").html(data);
        });
    }
</script>
