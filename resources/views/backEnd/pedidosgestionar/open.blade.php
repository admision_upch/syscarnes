@extends('backLayout.app')
@section('content2')
<h3 > <span class="caption-subject font-green-sharp bold uppercase"><i class="icon-doc font-green-sharp"></i> {{ $pedido->nombre }}</span>    
    <span id="divSelectPedidoEstado"></span>
</h3>    


<div class="portlet light bordered">

    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_open01" data-toggle="tab"> <b>Paso 01:<br> Importar <br>Estudiantes SINU </b></a>
                </li>
                <li>
                    <a href="#tab_open02" data-toggle="tab"> <b>Paso 02:<br>Importar <br>Estudiantes SUNEDU </b></a>
                </li>
                <li>
                    <a href="#tab_open03" data-toggle="tab"> <b>Paso 03:<br>Importar <br>Facultades/Programas SUNEDU </b></a>
                </li>
                
                <li>
                    <a href="#tab_open04" data-toggle="tab"><b>Paso 04:<br>Filtrar <br>Estudiantes, Facultades, Programas SINU</b></a>
                </li>
            </ul>

            <div class="tab-content">
                <div  class="tab-pane active" id="tab_open01">
                   
                </div>


                <div class="tab-pane" id="tab_open02">
                 
                </div>
                
                <div class="tab-pane" id="tab_open03">
                 
                </div>
                <div class="tab-pane" id="tab_open04">
                 
                </div>
            </div>
        </div>
    </div>
    <hr>
    <button type="button" class="btn default" onclick="window.history.back();" > Regresar</button>
</div>


@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {
       
       paso01();
       paso02();
       paso03();
       paso04();
       selectPedidoEstado();
       
    });
    
    function selectPedidoEstado(){
        $.post("{{ route('select.estado.pedido')}}",
        {
                pedido_id: {{ $pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#divSelectPedidoEstado").html(data);
        }).fail(function (d) {
                alert('Error');
        });
    }
   
    function cambiarEstadoPedido(){
        var pedido_estado = $("#pedido_estado").val();
        $.post("{{ route('cambio.estado.pedido')}}",
        {
            pedido_estado: pedido_estado,
            pedido_id: {{ $pedido->id}},
            "_token": "{{ csrf_token() }}"
        }).done(function (data) {
             swal(data.message, null, data.status);
             
             /*if (data.status=='success'){
                setTimeout(location.reload(), 4000);    
             }*/
             selectPedidoEstado();
        }).fail(function (d) {
            location.reload();
        });
   }
    
    function paso01(){
        $.post("{{ route('pedidosgestionar.open.paso01')}}",
        {
                pedido_id: {{ $pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#tab_open01").html(data);
            
        }).fail(function (d) {
                alert('Error');
        });
    }
    
    function paso02(){
        $.post("{{ route('pedidosgestionar.open.paso02')}}",
        {
                pedido_id: {{ $pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#tab_open02").html(data);
            
        }).fail(function (d) {
                alert('Error');
        });
    }
    function paso03(){
        $.post("{{ route('pedidosgestionar.open.paso03')}}",
        {
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#tab_open03").html(data);
            
        }).fail(function (d) {
                alert('Error');
        });
    }
    function paso04(){
        $.post("{{ route('pedidosgestionar.open.paso04')}}",
        {
                 pedido_id: {{ $pedido->id}},
                "_token": "{{ csrf_token() }}"
        }).done(function (data) {
            $("#tab_open04").html(data);
            
        }).fail(function (d) {
                alert('Error');
        });
    }
    
</script>  


@endsection