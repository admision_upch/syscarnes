@extends('layouts.index')

@section('css')
<link href="{{ asset('/assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet bordered">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="{{ $foto }}" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">  {{ $persona->nom_largo }} </div>
                    
                    <div class="profile-usertitle-job"> {{ $persona->tipodocumento }}: {{ $persona->nrodocumento }} </div>
                    
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <!--button type="button" class="btn btn-circle green btn-sm">Follow</button-->
                    <!--button type="button" class="btn btn-circle red btn-sm">Message</button-->
                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a>
                                <i class="fa fa-envelope"></i> {{ $persona->email }} </a>
                        </li>
                        <li class="active">
                            <a>
                                <i class="fa fa-mobile"></i>  {{ $persona->movil }} </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->

        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Carné Universitario</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-hover table-light">
                                    <thead>
                                        <tr class="uppercase">
                                            <th> Pedido </th>
                                            
                                            <th> Facultad </th>
                                            <th> Cod.Prog. </th>
                                            <th> Programa </th>
                                            <th> Modalidad </th>
                                            <th> Formación </th>
                                        </tr>
                                    </thead>

                                    @foreach($carnes_universitarios as $item)
                                    <tr>
                          
                                        <td> {{ $item->pedido }}</td>
                                        
                                        <td> {{ $item->nomfacultad_sinu }}</td>
                                        <td> {{ $item->codprograma_sinu }}</td>
                                        <td> {{ $item->nomprograma_sinu }}</td>
                                        <td> {{ $item->modalidad_desc }}</td>
                                        <td> {{ $item->formacion }}</td>
                                        
                                        
                                    </tr>
                                    @endforeach



                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Fotocheck</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-hover table-light">
                                    <thead>
                                        <tr class="uppercase">
                                            <th> Estado </th>
                                            <th> Cod. Prog. </th>
                                            <th> Programa </th>
                                            <th>Trazabilidad</th>
                                            <th> Devolución </th>
                                            <th> Observaciones </th>
                                        </tr>
                                    </thead>
                              @foreach($fotochecks as $item)
                                    <tr>
                                        

                                        <td>
                                        
                                                 @if ($item->cod_estado==1)
                                                 <span class="label label-sm label-danger"> {{ $item->estado }}</span>    
                                                 @endif
                                                 @if ($item->cod_estado==2)
                                                 <span class="label label-sm label-info"> {{ $item->estado }}</span>    
                                                 @endif
                                                 @if ($item->cod_estado==3)
                                                 <span class="label label-sm label-warning"> {{ $item->estado }}</span>    
                                                 @endif
                                                 @if ($item->cod_estado==4)
                                                 <span class="label label-sm label-success"> {{ $item->estado }}</span>    
                                                 @endif
                                             
                                                
                                        </td>                                        
                                                 
                                        
                                        <td> {{ $item->codprograma }} </td>
                                        <td> {{ $item->nomprograma }} </td>
                                        
                                        <td>
                                  
                                             <table>
                                                 @if ($item->fec_entrega_estudiante)<tr><td><span  class="label label-sm label-success">  Fec. Entrega Estudiante: {{ $item->fec_entrega_estudiante }}</span></td><td> </td></tr> @endif
                                                 @if ($item->fec_entrega_facultad)<tr><td> <span class="label label-sm label-warning"> Fec. Entrega Facultad: {{ $item->fec_entrega_facultad }}</span></td><td> </td></tr>@endif
                                                 @if ($item->fec_genera)<tr><td> <span class="label label-sm label-info"> Fec. Genera: {{ $item->fec_genera }}</span></td><td> </td></tr>@endif
                                                 @if ($item->fec_solicita)<tr><td><span class="label label-sm label-danger"> Fec. Solicita: {{ $item->fec_solicita }}</span></td><td> </td></tr>@endif
                                                </table>
                                        
                                        </td>
                             
                                        <td>
                                             <table>
                                                 @if ($item->fec_devolucion)<tr><td><span  class="label label-sm label-danger">  Fec. Devolución: {{ $item->fec_devolucion }}</span></td><td> </td></tr> @endif
                                                 @if ($item->motivo_devolucion)<tr><td> <span class="label label-sm label-primary"> Motivo: {{ $item->motivo_devolucion }}</span></td><td> </td></tr>@endif
                                             </table>
                                        </td>
                                        @if ($item->duplicado==1)
                                        <td><span  class="label label-sm label-danger">Duplicado</span></td>
                                        @endif
                                        
                                        
                                        
                                       
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>

        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->




@endsection

