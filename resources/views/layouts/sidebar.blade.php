<!-- BEGIN SIDEBAR -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
          <li class="heading">
          <form class="search-form" action="page_general_search_2.html" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm search-input" placeholder="Buscar Persona" name="q" >
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
        </li>
       @foreach ($opciones as $opcion) 
        @if ($opcion->enlace == '#')
            <li class="heading">
            <h3 class="uppercase"> {{$opcion->opcion}} </h3>
        </li>
        @else
           <li class="nav-item  ">
            <a href="{{url($opcion->enlace)}}" class="nav-link nav-toggle">
                <i class="{{$opcion->imagen}}"></i>
                <span class="title"> {{$opcion->opcion}} </span>
            </a>
        </li>
        @endif
       @endforeach
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->


<script>
    jQuery(document).ready(function ($) {
        // Set the Options for "Bloodhound" suggestion engine
        var engine = new Bloodhound({
            remote: {
                url: '/syscarnes/public/index.php/autocomplete?q=%QUERY%',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        $(".search-input").typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        }, {
            source: engine.ttAdapter(),

            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'usersList',

            // the key from the array we want to display (name,id,email,etc...)
            displayKey: 'nom_largo',
            templates: {
                empty: [
                    '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function (data) {
                    return '<a href="/syscarnes/public/index.php/buscarpersona/' + data.tipodocumento + '/' + data.nrodocumento + '"   class="list-group-item">' + data.nom_largo + ' <br> ' + data.nrodocumento + '</a>';
                    
                }
            }
        }).on('typeahead:selected', function (obj, data) {
            window.location.href = "/syscarnes/public/index.php/buscarpersona/" + data.tipodocumento + '/' + data.nrodocumento;
        });
    });


</script>
