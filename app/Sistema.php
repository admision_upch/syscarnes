<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sistema extends Model {

    protected $connection = 'pgsql_sysseguridad_seguridad';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sistemas';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'url', 'abreviatura', 'version','estado'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function opciones() {
        return $this->hasMany('App\Opcion');
    }

    public function perfiles() {
        return $this->hasMany('App\Perfile');
    }

}
