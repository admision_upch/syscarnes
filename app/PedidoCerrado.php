<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoCerrado extends Model
{
  protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pedidos_cerrados';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_update', 'pedido_id', 'estado'];
}
