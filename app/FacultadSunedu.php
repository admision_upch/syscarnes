<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacultadSunedu extends Model
{
     protected $connection = 'pgsql_syscarnes';
     protected $table = 'facultades_sunedu';
     protected $fillable = ['codfacultad', 'nomfacultad'];
}
