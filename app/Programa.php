<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programa extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'programas';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_dependencia', 'modalidad_id', 'formacion_id', 'cod', 'name'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function facultad() {
        return $this->belongsTo('App\Facultad');
        
    }

    public function modalidad() {
        return $this->belongsTo('App\Modalidad');
    }

    public function formacion() {
        return $this->belongsTo('App\Formacion');
    }

}
