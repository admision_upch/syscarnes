<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteSunedu extends Model
{
  protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'estudiantes_sunedu';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['solicitud_id', 'codalumno','carrera','tipodocumento','nrodocumento','apepaterno','apematerno','nombre','estado'];
}
