<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facultad extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facultades';
    

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_dependencia', 'cod_dependencia', 'name', 'modalidad_id', 'formacion_id'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function modalidad() {
        return $this->belongsTo('App\Modalidad');
    }

    public function formacion() {
        return $this->belongsTo('App\Formacion');
    }

    public function programas() {
        return $this->hasMany('App\Programa');
        //return  $this->hasManyThrough('App\Programa', 'id_dependencia', 'modalidad_id', 'formacion_id');
    }

}
