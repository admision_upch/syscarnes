<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteFotocheck extends Model
{
  protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'estudiantes_fotocheck';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['duplicado','ultimo_id_detalle_fotocheck','codalumno','tipodocumento','nrodocumento','apepaterno','apematerno','nombre','estado','user_update','codfacultad_sinu','nomprograma', 'cod_estado_sinu'];
}
