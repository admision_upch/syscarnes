<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use App\Acceso;
use App\Opcion;
use App\User_Perfil;
use Illuminate\Support\Facades\Auth;

class AccesoControlador {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
     
        $user = Auth::user();
        
        if ($user->estado=='2'){
            Session::flash('message', 'No tiene acceso a este sistema');
            return redirect('logout');
        }
        
        $user_perfil = User_Perfil::where([
                    ['perfil_id', '=', Session::get('perfil')],
                    ['user_id', '=', $user->id],
                    ['estado', '=', '1'],
                    ['eliminado', '=', null],
                ])->first();

        if ($user_perfil) {

            $url = explode('/', $request->path());
            $link = '/' . $url[0];
            $opcion = Opcion::where([
                        ['enlace', '=', $link],
                        ['sistema_id', '=', Session::get('sistema')],
                    ])->first();


            if (count($opcion) > 0) {
                $acceso = Acceso::where([
                            ['perfil_id', '=', Session::get('perfil')],
                            ['opcion_id', '=', $opcion->id],
                            ['estado', '=', '1'],
                            ['eliminado', '=', null],
                        ])->first();

                if (count($acceso) > 0) {
                    return $next($request, compact('acceso'));
                } else {
                 return redirect('home');
                         
                }
            } else {
                return redirect('home');
                     
            }
        } else {
            if ($request->ajax()) {
                  return response()->json(['status' => 'warning', 'message' => 'Sesion cerrada, vuelva a logearse.', 'estado'=>'0' ], 200);
            }
            Session::flash('message', 'No tiene acceso a este sistema');
            return redirect('logout');
        }
    }

}
