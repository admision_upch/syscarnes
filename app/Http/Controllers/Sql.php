<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class Sql {

    public function sql_fotochecks($fecha_inicio, $fecha_fin) {

        $fechas_fotochecks = "pfd.fec_solicita BETWEEN CAST ('$fecha_inicio' AS DATE) AND CAST ('$fecha_fin' AS DATE)+'1 day'::interval ";
        
   $fotochecks_facultades = DB::connection('pgsql_syscarnes')->select("select 
                        
			B.facultad,
                        A.codfacultad,
			(coalesce(A.cantidad,0)+B.cantidad) as cantidad_total,
			A.cantidad as cantidad_pregrado,
			B.cantidad as cantidad_postgrado,
			
                        coalesce(A.solicitado_duplicado,0) as solicitado_duplicado_pregrado,
                        coalesce(A.generado_duplicado,0) as generado_duplicado_pregrado,
                        coalesce(A.entregado_facultad_duplicado,0) as entregado_facultad_duplicado_pregrado,
                        coalesce(A.entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado_pregrado,
                        coalesce(B.solicitado_duplicado,0) as solicitado_duplicado_postgrado,
                        coalesce(B.generado_duplicado,0) as generado_duplicado_postgrado,
                        coalesce(B.entregado_facultad_duplicado,0) as entregado_facultad_duplicado_postgrado,
                        coalesce(B.entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado_postgrado,
                        
                        coalesce(A.solicitado,0)+coalesce(B.solicitado,0) as solicitados,
                        coalesce(A.solicitado,0) as solicitado_pregrado,
                        coalesce(A.generado,0) as generado_pregrado,
                        coalesce(A.entregado_facultad,0) as entregado_facultad_pregrado,
                        coalesce(A.entregado_estudiante,0) as entregado_estudiante_pregrado,
                        coalesce(B.solicitado,0) as solicitado_postgrado,
                        coalesce(B.generado,0) as generado_postgrado,
                        coalesce(B.entregado_facultad,0) as entregado_facultad_postgrado,
                        coalesce(B.entregado_estudiante,0) as entregado_estudiante_postgrado,
                        coalesce(A.entregado_facultad,0)+coalesce(A.entregado_estudiante,0) as entregados_facultad_estudiante_pregrado,
                        coalesce(B.entregado_facultad,0)+coalesce(B.entregado_estudiante,0) as entregados_facultad_estudiante_postgrado,
                        coalesce(A.entregado_facultad,0)+coalesce(B.entregado_facultad,0)+coalesce(A.entregado_estudiante,0)+coalesce(B.entregado_estudiante,0) as entregados_facultad_estudiante
                        
   from (


                        select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad, 
                        coalesce(solicitado_duplicado,0) as solicitado_duplicado,
                        coalesce(generado_duplicado,0) as generado_duplicado,
                        coalesce(entregado_facultad_duplicado,0) as entregado_facultad_duplicado,
                        coalesce(entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado,
                        coalesce(solicitado,0) as solicitado,
                        coalesce(generado,0) as generado,
                        coalesce(entregado_facultad,0) as entregado_facultad,
                        coalesce(entregado_estudiante,0) as entregado_estudiante

                        from (
                        
                        select 
                       A.codfacultad, A.abreviaturafacultad as facultad, 
                       sum(case when pf.duplicado=1 and pfd.fec_solicita is not null then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.fec_genera is not null then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.fec_entrega_facultad  is not null then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.fec_entrega_estudiante  is not null then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.fec_solicita is not null then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.fec_genera is not null then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.fec_entrega_facultad  is not null then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.fec_entrega_estudiante  is not null then 1 else 0 end) as entregado_estudiante, 

                        count(nomfacultad) as cantidad
                          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_fotocheck ) A on ef.cod_escuela=A.codfacultad
                          where $fechas_fotochecks
			and (codmodalidad='1' or codmodalidad is null) 

			group by A.codfacultad,A.abreviaturafacultad
			

			) AA
			right join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_fotocheck) BB
			on AA.codfacultad=BB.codfacultad
                         
                          
) A
right join (
                             select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad, 
                        coalesce(solicitado_duplicado,0) as solicitado_duplicado,
                        coalesce(generado_duplicado,0) as generado_duplicado,
                        coalesce(entregado_facultad_duplicado,0) as entregado_facultad_duplicado,
                        coalesce(entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado,
                        coalesce(solicitado,0) as solicitado,
                        coalesce(generado,0) as generado,
                        coalesce(entregado_facultad,0) as entregado_facultad,
                        coalesce(entregado_estudiante,0) as entregado_estudiante

                        from (
                        
                        select 
                       A.codfacultad, A.abreviaturafacultad as facultad, 
                       sum(case when pf.duplicado=1 and pfd.fec_solicita is not null then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.fec_genera is not null then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.fec_entrega_facultad  is not null then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.fec_entrega_estudiante  is not null then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.fec_solicita is not null then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.fec_genera is not null then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.fec_entrega_facultad  is not null then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.fec_entrega_estudiante  is not null then 1 else 0 end) as entregado_estudiante, 

                        count(nomfacultad) as cantidad
                          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_fotocheck ) A on ef.cod_escuela=A.codfacultad
                          where $fechas_fotochecks
			and codmodalidad='2' 

			group by A.codfacultad,A.abreviaturafacultad
			

			) AA
			right join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_fotocheck) BB
			on AA.codfacultad=BB.codfacultad
			
			



			
) B on A.facultad=B.facultad order by 1 desc");
        return $fotochecks_facultades;
    }

    public function sql_fotochecks_v1($fecha_inicio, $fecha_fin) {

        $fechas_fotochecks = "pfd.fec_solicita BETWEEN CAST ('$fecha_inicio' AS DATE) AND CAST ('$fecha_fin' AS DATE)";
        $fotochecks_facultades = DB::connection('pgsql_syscarnes')->select("select 
			B.facultad,
			(coalesce(A.cantidad,0)+B.cantidad) as cantidad_total,
			A.cantidad as cantidad_pregrado,
			B.cantidad as cantidad_postgrado,
			coalesce(A.solicitado_duplicado,0) as solicitado_duplicado_pregrado,
                        coalesce(A.generado_duplicado,0) as generado_duplicado_pregrado,
                        coalesce(A.entregado_facultad_duplicado,0) as entregado_facultad_duplicado_pregrado,
                        coalesce(A.entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado_pregrado,
                        coalesce(A.solicitado,0) as solicitado_pregrado,
                        coalesce(A.generado,0) as generado_pregrado,
                        coalesce(A.entregado_facultad,0) as entregado_facultad_pregrado,
                        coalesce(A.entregado_estudiante,0) as entregado_estudiante_pregrado,
                        coalesce(B.solicitado_duplicado,0) as solicitado_duplicado_postgrado,
                        coalesce(B.generado_duplicado,0) as generado_duplicado_postgrado,
                        coalesce(B.entregado_facultad_duplicado,0) as entregado_facultad_duplicado_postgrado,
                        coalesce(B.entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado_postgrado,
                        coalesce(B.solicitado,0) as solicitado_postgrado,
                        coalesce(B.generado,0) as generado_postgrado,
                        coalesce(B.entregado_facultad,0) as entregado_facultad_postgrado,
                        coalesce(B.entregado_estudiante,0) as entregado_estudiante_postgrado,
                        coalesce(A.entregado_facultad,0)+coalesce(A.entregado_estudiante,0) as entregados_facultad_estudiante_pregrado,
                        coalesce(B.entregado_facultad,0)+coalesce(B.entregado_estudiante,0) as entregados_facultad_estudiante_postgrado,
                        coalesce(A.entregado_facultad,0)+coalesce(B.entregado_facultad,0)+coalesce(A.entregado_estudiante,0)+coalesce(B.entregado_estudiante,0) as entregados_facultad_estudiante
   from (


                        select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad, 
                        coalesce(solicitado_duplicado,0) as solicitado_duplicado,
                        coalesce(generado_duplicado,0) as generado_duplicado,
                        coalesce(entregado_facultad_duplicado,0) as entregado_facultad_duplicado,
                        coalesce(entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado,
                        coalesce(solicitado,0) as solicitado,
                        coalesce(generado,0) as generado,
                        coalesce(entregado_facultad,0) as entregado_facultad,
                        coalesce(entregado_estudiante,0) as entregado_estudiante

                        from (
                        
                        select 
                       A.codfacultad, A.abreviaturafacultad as facultad, 
                       sum(case when pf.duplicado=1 and pfd.estado=1 then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.estado=2 then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=3 then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=4 then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.estado=1 then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.estado=2 then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.estado=3 then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.estado=4 then 1 else 0 end) as entregado_estudiante, 

                        count(nomfacultad) as cantidad
                          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks
			and codmodalidad='1' and 
			codalumno not in ( select 
                        codalumno
			from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks and codmodalidad='1' and cod_escuela=999)

			group by A.codfacultad,A.abreviaturafacultad
			

			) AA
			right join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu) BB
			on AA.codfacultad=BB.codfacultad
			
			union
			  select 
                         '999' as codfacultad,'ETM' as facultad, count(nomfacultad) as cantidad,        sum(case when pf.duplicado=1 and pfd.estado=1 then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.estado=2 then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=3 then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=4 then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.estado=1 then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.estado=2 then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.estado=3 then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.estado=4 then 1 else 0 end) as entregado_estudiante
		          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks and codmodalidad='1' and cod_escuela=999


                          
                          
) A
right join (
                             select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad, 
                        coalesce(solicitado_duplicado,0) as solicitado_duplicado,
                        coalesce(generado_duplicado,0) as generado_duplicado,
                        coalesce(entregado_facultad_duplicado,0) as entregado_facultad_duplicado,
                        coalesce(entregado_estudiante_duplicado,0) as entregado_estudiante_duplicado,
                        coalesce(solicitado,0) as solicitado,
                        coalesce(generado,0) as generado,
                        coalesce(entregado_facultad,0) as entregado_facultad,
                        coalesce(entregado_estudiante,0) as entregado_estudiante

                        from (
                        
                        select 
                       A.codfacultad, A.abreviaturafacultad as facultad, 
                       sum(case when pf.duplicado=1 and pfd.estado=1 then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.estado=2 then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=3 then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=4 then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.estado=1 then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.estado=2 then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.estado=3 then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.estado=4 then 1 else 0 end) as entregado_estudiante, 

                        count(nomfacultad) as cantidad
                          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks
			and codmodalidad='2' and 
			codalumno not in ( select 
                        codalumno
			from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks and codmodalidad='2' and cod_escuela=999)

			group by A.codfacultad,A.abreviaturafacultad
			

			) AA
			right join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu) BB
			on AA.codfacultad=BB.codfacultad
			
			union
			  select 
                         '999' as codfacultad,'ETM' as facultad, count(nomfacultad) as cantidad,        sum(case when pf.duplicado=1 and pfd.estado=1 then 1 else 0 end) as solicitado_duplicado,
                       sum(case when pf.duplicado=1 and pfd.estado=2 then 1 else 0 end) as generado_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=3 then 1 else 0 end) as entregado_facultad_duplicado, 
                       sum(case when pf.duplicado=1 and pfd.estado=4 then 1 else 0 end) as entregado_estudiante_duplicado, 
                       
                       sum(case when  pf.duplicado is null and pfd.estado=1 then 1 else 0 end) as solicitado,
                       sum(case when pf.duplicado is null and pfd.estado=2 then 1 else 0 end) as generado, 
                       sum(case when pf.duplicado is null and pfd.estado=3 then 1 else 0 end) as entregado_facultad, 
                       sum(case when pf.duplicado is null and pfd.estado=4 then 1 else 0 end) as entregado_estudiante
		          from estudiantes_fotocheck ef 
                          inner join pedidos_fotocheck_detalle pfd on ef.id=pfd.estudiante_fotocheck_id
                          inner join pedidos_fotocheck pf on pf.id=pfd.pedido_fotocheck_id
                          inner join (select distinct cast(codfacultad as integer), abreviaturafacultad from facultades_sinu ) A on ef.codfacultad=A.codfacultad
                          where $fechas_fotochecks and codmodalidad='2' and cod_escuela=999



			
) B on A.facultad=B.facultad order by 1 desc");
        return $fotochecks_facultades;
    }

    
    public function sql_carnes_universitarios($fecha_inicio, $fecha_fin) {
        $fecha_carnes_universitarios = "and pedidos.fec_fin BETWEEN CAST ('$fecha_inicio' AS DATE) AND CAST ('$fecha_fin' AS DATE)";


        /*
         * Carnes universitarios incluido ufbi
         *  $carnes_universitarios_facultades = DB::connection('pgsql_syscarnes')->select("			select 'POSTGRADO' AS facultad, count(*) as cantidad 
          from estudiantes_pedido  inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
          where pedidos.estado='2' and estudiantes_pedido.estado='1' and codmodalidad='2' and niv_formacion in ('6','7')
          union

          select
          A.abreviaturafacultad as facultad, count(nomfacultad_sinu) as cantidad
          from
          estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
          inner join (select distinct codfacultad, abreviaturafacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
          where pedidos.estado='2' and  estudiantes_pedido.estado='1'
          and ((codmodalidad='2' and niv_formacion not in ('6','7')) or (codmodalidad='1' and (nivel not in ('1','2')  or nivel is null) ))
          group by A.abreviaturafacultad

          union
          select 'UFBI' as facultad, count(*) as cantidad from estudiantes_pedido
          inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
          where pedidos.estado='2' and estudiantes_pedido.estado='1' and codmodalidad='1' and nivel in ('1','2')
          ORDER BY 2 DESC"); */


        /*  $carnes_universitarios_facultades = DB::connection('pgsql_syscarnes')->select("		select B.facultad, (coalesce(A.cantidad,0)+B.cantidad) as cantidad_total, A.cantidad as cantidad_pregrado,B.cantidad as cantidad_postgrado  from (

          select
          A.abreviaturafacultad as facultad, count(nomfacultad_sinu) as cantidad
          from
          estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
          inner join (select distinct codfacultad, abreviaturafacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
          where pedidos.estado='2' and  estudiantes_pedido.estado='1' and codmodalidad='1'
          group by A.abreviaturafacultad
          ) A
          right join (

          select
          A.abreviaturafacultad as facultad, count(nomfacultad_sinu) as cantidad
          from
          estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
          inner join (select distinct codfacultad, abreviaturafacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
          where pedidos.estado='2' and  estudiantes_pedido.estado='1' and codmodalidad='2'
          group by A.abreviaturafacultad

          ) B on A.facultad=B.facultad order by 2 desc "); */

        $carnes_universitarios = DB::connection('pgsql_syscarnes')->select("select B.facultad, (coalesce(A.cantidad,0)+B.cantidad) as cantidad_total, A.cantidad as cantidad_pregrado,B.cantidad as cantidad_postgrado  from (
                        select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad from (
                        select 
                        codfacultad_sinu as codfacultad, abreviaturafacultad_sinu as facultad, count(nomfacultad_sinu) as cantidad
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                        inner join (select distinct codfacultad, abreviaturafacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='1' and codalumno not in ( select 
                        codalumno
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                       
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='1' and cod_escuela=999)
			group by codfacultad_sinu,abreviaturafacultad_sinu) AA
			right join (select distinct codfacultad, abreviaturafacultad from facultades_sinu) BB
			on AA.codfacultad=BB.codfacultad
			union
			  select 
                         '999' as codfacultad,'ETM' as facultad, count(nomfacultad_sinu) as cantidad
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                       
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='1' and cod_escuela=999
) A
right join (
               select BB.codfacultad, BB.abreviaturafacultad as facultad, coalesce(AA.cantidad,0) as cantidad from (
                        select 
                         codfacultad_sinu as codfacultad, abreviaturafacultad_sinu as facultad, count(nomfacultad_sinu) as cantidad
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                        inner join (select distinct codfacultad, abreviaturafacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='2'  and codalumno not in (  select 
                        codalumno
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                       
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='2' and cod_escuela=999)
			group by codfacultad_sinu,abreviaturafacultad_sinu) AA
			right join (select distinct codfacultad, abreviaturafacultad from facultades_sinu) BB
			on AA.codfacultad=BB.codfacultad
			union
			      select 
                        '999' as codfacultad,'ETM' as facultad, count(nomfacultad_sinu) as cantidad
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                       
                         where pedidos.estado='2' $fecha_carnes_universitarios and  estudiantes_pedido.estado='1' and codmodalidad='2' and cod_escuela=999
			
) B on A.facultad=B.facultad order by 1 desc");
        return $carnes_universitarios;
    }

}
