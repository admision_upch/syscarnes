<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EstudianteFotocheck;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $now = new \DateTime();
        $anio = $now->format('Y');

        $fecha_inicio = $anio . '-01-01';
        $fecha_fin = $anio . '-12-31';

        $sql = New Sql();
        $carnes_universitarios = $sql->sql_carnes_universitarios($fecha_inicio, $fecha_fin);
        $fotochecks = $sql->sql_fotochecks($fecha_inicio, $fecha_fin);


        $total_fotochecks_solicitados = 0;
        $total_fotochecks_generados = 0;
        $total_fotochecks_entregados_facultad = 0;
        $total_fotochecks_entregados_estudiante = 0;
        $total_fotochecks_entregados = 0;
        
        $total_fotochecks_solicitados_duplicados = 0;
        $total_fotochecks_generados_duplicados = 0;
        $total_fotochecks_entregados_facultad_duplicados = 0;
        $total_fotochecks_entregados_estudiante_duplicados = 0;
        
        
        foreach ($fotochecks as $index => $dato) {

            $total_fotochecks_solicitados = $total_fotochecks_solicitados + $dato->solicitado_pregrado + $dato->solicitado_postgrado;
            $total_fotochecks_generados = $total_fotochecks_generados + $dato->generado_pregrado + $dato->generado_postgrado;
            $total_fotochecks_entregados_facultad = $total_fotochecks_entregados_facultad + $dato->entregado_facultad_pregrado + $dato->entregado_facultad_postgrado;
            $total_fotochecks_entregados_estudiante = $total_fotochecks_entregados_estudiante + $dato->entregado_estudiante_pregrado + $dato->entregado_estudiante_postgrado;
            $total_fotochecks_entregados = $total_fotochecks_entregados + $dato->entregado_facultad_pregrado + $dato->entregado_facultad_postgrado + $dato->entregado_estudiante_pregrado + $dato->entregado_estudiante_postgrado;
            
            $total_fotochecks_solicitados_duplicados = $total_fotochecks_solicitados_duplicados + $dato->solicitado_duplicado_pregrado + $dato->solicitado_duplicado_postgrado;
            $total_fotochecks_generados_duplicados = $total_fotochecks_generados_duplicados + $dato->generado_duplicado_pregrado + $dato->generado_duplicado_postgrado;
            $total_fotochecks_entregados_facultad_duplicados = $total_fotochecks_entregados_facultad_duplicados + $dato->entregado_facultad_duplicado_pregrado + $dato->entregado_facultad_duplicado_postgrado;
            $total_fotochecks_entregados_estudiante_duplicados = $total_fotochecks_entregados_estudiante_duplicados + $dato->entregado_estudiante_duplicado_pregrado + $dato->entregado_estudiante_duplicado_postgrado;
        }

        $total_carnes_universitarios = 0;
        foreach ($carnes_universitarios as $index => $dato) {
            $total_carnes_universitarios = $total_carnes_universitarios + $dato->cantidad_total;
        }

        return view('home', compact('carnes_universitarios', 'total_carnes_universitarios', 'fotochecks', 'total_fotochecks_solicitados', 'total_fotochecks_generados', 'total_fotochecks_entregados_facultad', 'total_fotochecks_entregados_estudiante', 'total_fotochecks_entregados','total_fotochecks_solicitados_duplicados','total_fotochecks_generados_duplicados','total_fotochecks_entregados_facultad_duplicados','total_fotochecks_entregados_estudiante_duplicados'));
    }

    public function detalle(Request $request) {

        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;

        /* $now = new \DateTime();
          $anio = $now->format('Y');

          $fecha_inicio = $anio . '-01-01';
          //$fecha_fin=$now->format('Y-m-d');
          $fecha_fin = '2017-12-08';
         */
        $sql = New Sql();
        $carnes_universitarios = $sql->sql_carnes_universitarios($fecha_inicio, $fecha_fin);
        $fotochecks = $sql->sql_fotochecks($fecha_inicio, $fecha_fin);

        $total_fotochecks_solicitados = 0;
        $total_fotochecks_generados = 0;
        $total_fotochecks_entregados_facultad = 0;
        $total_fotochecks_entregados_estudiante = 0;
        $total_fotochecks_entregados = 0;
        
        $total_fotochecks_solicitados_duplicados = 0;
        $total_fotochecks_generados_duplicados = 0;
        $total_fotochecks_entregados_facultad_duplicados = 0;
        $total_fotochecks_entregados_estudiante_duplicados = 0;
       
        foreach ($fotochecks as $index => $dato) {
            $total_fotochecks_solicitados = $total_fotochecks_solicitados + $dato->solicitado_pregrado + $dato->solicitado_postgrado;
            $total_fotochecks_generados = $total_fotochecks_generados + $dato->generado_pregrado + $dato->generado_postgrado;
            $total_fotochecks_entregados_facultad = $total_fotochecks_entregados_facultad + $dato->entregado_facultad_pregrado + $dato->entregado_facultad_postgrado;
            $total_fotochecks_entregados_estudiante = $total_fotochecks_entregados_estudiante + $dato->entregado_estudiante_pregrado + $dato->entregado_estudiante_postgrado;
            $total_fotochecks_entregados = $total_fotochecks_entregados + $dato->entregado_facultad_pregrado + $dato->entregado_facultad_postgrado + $dato->entregado_estudiante_pregrado + $dato->entregado_estudiante_postgrado;
            
            $total_fotochecks_solicitados_duplicados = $total_fotochecks_solicitados_duplicados + $dato->solicitado_duplicado_pregrado + $dato->solicitado_duplicado_postgrado;
            $total_fotochecks_generados_duplicados = $total_fotochecks_generados_duplicados + $dato->generado_duplicado_pregrado + $dato->generado_duplicado_postgrado;
            $total_fotochecks_entregados_facultad_duplicados = $total_fotochecks_entregados_facultad_duplicados + $dato->entregado_facultad_duplicado_pregrado + $dato->entregado_facultad_duplicado_postgrado;
            $total_fotochecks_entregados_estudiante_duplicados = $total_fotochecks_entregados_estudiante_duplicados + $dato->entregado_estudiante_duplicado_pregrado + $dato->entregado_estudiante_duplicado_postgrado;
            
        }

        $total_carnes_universitarios = 0;
        foreach ($carnes_universitarios as $index => $dato) {
            $total_carnes_universitarios = $total_carnes_universitarios + $dato->cantidad_total;
        }

        return view('home_detail', compact('carnes_universitarios', 'total_carnes_universitarios', 'fotochecks', 'total_fotochecks_solicitados', 'total_fotochecks_generados', 'total_fotochecks_entregados_facultad', 'total_fotochecks_entregados_estudiante', 'total_fotochecks_entregados','total_fotochecks_solicitados_duplicados','total_fotochecks_generados_duplicados','total_fotochecks_entregados_facultad_duplicados','total_fotochecks_entregados_estudiante_duplicados'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search() {
        return view('search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request) {
        $valor = strtoupper($request->input('q'));
        if (empty($valor)) {
            $valor = "nulo";
        }
        $data = DB::connection('oracle_prod')->select("select tip_identificacion as  tipodocumento,  num_identificacion as nrodocumento, pri_apellido as apepaterno, seg_apellido as apematerno, nom_largo from bas_tercero where nom_largo ||num_identificacion like upper('%{$valor}%')");
        if (empty($data)) {
            $data = DB::connection('pgsql_syscarnes')->select("select   tipodocumento,  nrodocumento,  apepaterno,  apematerno, apepaterno || ' ' || apematerno || ' ' || nombre as nom_largo from estudiantes_fotocheck where apepaterno || ' ' || apematerno || ' ' || nombre ||nrodocumento like upper('%{$valor}%')");
        }
        return response()->json($data);
    }

}
