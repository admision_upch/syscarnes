<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pedido;
use App\PedidoCerrado;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use App\Estudiante_Pedido;
use Intervention\Image\ImageManagerStatic as Image;
use Chumper\Zipper\Zipper;
use Excel;
use Yajra\Datatables\Datatables;
use Imagick;
use ImagickPixel;

class Pedidos_ProcesarController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);
        $pedidos = DB::connection('pgsql_syscarnes')->select("select case when p.estado='1' and pc.estado='1' then 'Cerrado' when p.estado='2' then 'Cerrado' else 'Abierto' end as estado, p.id,to_char(p.fec_inicio,'dd/MM/YYYY') as fec_inicio, to_char(p.fec_fin,'dd/MM/YYYY') as fec_fin,p.nombre,to_char(p.fec_importacion_estudiantes,'dd/MM/YYYY') as  fec_importacion_estudiantes, to_char(pc.updated_at,'dd/MM/YYYY') as fec_cierre from pedidos p 
            left join pedidos_cerrados pc on p.id=pc.pedido_id and user_update='" . Auth::User()->username . "' where ( p.eliminado is null or p.eliminado='0')    and p.estado='0'        ");
        return view('backEnd.pedidosprocesar.index', compact('pedidos', 'permisos'));
    }

    public function datatablePedidos() {
        $pedidos = DB::connection('pgsql_syscarnes')->select("select p.id,  case when p.estado='1' then 'Abierto' when p.estado='2' then 'Cerrado' when p.estado='0' then 'Creado' when p.estado='3' then 'Prueba' else 'Estado No Asignado' end as estado, p.id,to_char(p.fec_inicio,'dd/MM/YYYY') as fec_inicio, to_char(p.fec_fin,'dd/MM/YYYY') as fec_fin,p.nombre,to_char(p.fec_importacion_estudiantes,'dd/MM/YYYY') as  fec_importacion_estudiantes, to_char(p.fec_procesado,'dd/MM/YYYY') as fec_procesado, to_char(p.fec_cierre,'dd/MM/YYYY') as fec_cierre from pedidos p  where ( p.eliminado is null or p.eliminado='0' ) and p.estado<>'0'  ");

        return Datatables::of($pedidos)
                        ->addColumn('action', function ($data) {
                            $pedido_id = $data->id;
                            $dataExtranjeros = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . "  and docu_tip<>'1'  ");



                            $pedido = Pedido::where([
                                        ['id', '=', $pedido_id],
                                        ['eliminado', '=', null],
                                    ])->first();


                            if ($pedido->estado == '1') {
                                if (count($dataExtranjeros) > 0) {
                                    return '<a href="#" onclick="procesar(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Procesar Pedido</a>'
                                            . '<a href="' . route('extranjeros', $data->id) . '"  class="btn btn-xs btn-danger"><i class="fa fa-edit"></i> Actualizar Nro. Documento Extranjeros</a>'
                                            . '<a href="#" onclick="verArchivosGenerados(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Ver Archivos Generados</a>';
                                } else {
                                    return '<a href="#" onclick="procesar(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Procesar Pedido</a>'
                                            . '<a href="#" onclick="verArchivosGenerados(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Ver Archivos Generados</a>';
                                }
                            }

                            if ($pedido->estado == '2') {

                                return '<a href="#" onclick="verArchivosGenerados(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Ver Archivos Generados</a>';
                            }

                            if ($pedido->estado == '3') {
                                if (count($dataExtranjeros) > 0) {
                                    return '<a href="#" onclick="procesar(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Procesar Pedido</a>'
                                            . '<a href="' . route('extranjeros', $data->id) . '"  class="btn btn-xs btn-danger"><i class="fa fa-edit"></i> Actualizar Nro. Documento Extranjeros</a>'
                                            . '<a href="#" onclick="verArchivosGenerados(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Ver Archivos Generados</a>';
                                } else {
                                    return '<a href="#" onclick="procesar(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Procesar Pedido</a>'
                                            . '<a href="#" onclick="verArchivosGenerados(' . $data->id . ')" class="btn btn-xs btn-primary"><i class="fa fa-play"></i> Ver Archivos Generados</a>';
                                }
                            }
                            /* $pedido = Pedido::where([
                              ['id', '=', $data->pedido_id],
                              ['eliminado', '=', null],
                              ])->first();
                              if ($pedido->estado != '1') {
                              return '<span>Pedido Cerrado</span>';
                              }
                              $pedidocerrado = PedidoCerrado::where([
                              ['pedido_id', '=', $data->pedido_id],
                              ['user_update', '=', Auth::User()->username],
                              ['estado', '=', '1'],
                              ])->first();

                              if (count($pedidocerrado) > 0) {
                              return '<span>Pedido Cerrado</span>';
                              } else {
                              return '<a class="btn btn-xs btn-primary" href="facultadsinu/edit/' . $data->id . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                              } */
                        })
                        ->make(true);
    }

    public function validarPedidos(Request $request) {
        ini_set('max_execution_time', 600);
        $pedido_id = $request->pedido_id;

        try {

            $pedido = Pedido::findOrFail($pedido_id);
            if ($pedido->estado == '2') {
                return response()->json(['status' => 'warning', 'message' => 'No se puede procesar: Este pedido se encuentra cerrado', 'data' => ''], 200);
            }


            $dataExtranjeros = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . "  and docu_tip<>'1' and nrodocumento_original is null    ");

            if (count($dataExtranjeros) > 0) {
                return response()->json(['status' => 'warning', 'message' => 'Debe actualizar el Nro. de Documento de los Extranjeros', 'data' => 'extranjeros'], 200);
            }

            $dataDuplicados = DB::connection('pgsql_syscarnes')->select("select docu_num,count(docu_num) from vst_pedido_exporta_confirmados  where pedido_id=" . $pedido_id . " group by docu_num having count(docu_num)>1 limit 5 ");

            if (count($dataDuplicados) > 0) {
                $dataDuplicadosString = '';
                $i = 1;
                foreach ($dataDuplicados as $value) {
                    if ($i < count($dataDuplicados)) {
                        $dataDuplicadosString .= $value->docu_num . ",";
                    } else {
                        $dataDuplicadosString .= $value->docu_num;
                    }
                    $i++;
                }
                return response()->json(['status' => 'warning', 'message' => 'Los Siguientes Nro. Documentos ' . $dataDuplicadosString . ' tiene mas de un código de estudiante', 'data' => 'duplicados'], 200);
            }

            /* INICIO: Actualizar todos los estudiantes de codmodalidad=1 and user_update=null and estado=null */
            DB::connection('pgsql_syscarnes')->table('estudiantes_pedido')->where('pedido_id', $pedido_id)
                    ->where('codmodalidad', '1')
                    ->where('estado', null)
                    ->update(['estado' => '1', 'user_update' => Auth::User()->username]);
            /* FIN: Actualizar todos los estudiantes de codmodalidad=1 and user_update=null and estado=null */

            $data = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . "     ");
            $data = json_decode(json_encode($data), true);

            //   $carpetaPersona = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/fotosPersona';
            //  if (!file_exists($carpetaPersona)) {
            //     mkdir($carpetaPersona, 0777, true);
            //  }
            // $fotosCarpetaPersona = glob($carpetaPersona . "/*.jpg");
            //  array_map('unlink', $fotosCarpetaPersona);



            $carpetaCarne = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/fotosCarne';
            if (!file_exists($carpetaCarne)) {
                mkdir($carpetaCarne, 0777, true);
            }
            $fotosCarpetaCarne = glob($carpetaCarne . "/*.jpg");
            array_map('unlink', $fotosCarpetaCarne);


            $carpetaReportes = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/reportes';
            if (!file_exists($carpetaReportes)) {
                mkdir($carpetaReportes, 0777, true);
            }
            $reportes = glob($carpetaReportes . "/*.jpg");
            array_map('unlink', $reportes);


            foreach ($data as $index => $dato) {
                $fotoRepositorio = RepositorioFotosPersonas . $dato['docu_num'] . ".jpg";

                // $fotoPersona = $carpetaPersona . "/" . $dato['docu_num'] . ".jpg";

                if ($dato['docu_tip'] <> '1') {
                    $dato['docu_num'] = $dato['nrodocumento_original'];
                }
                $fotoCarne = $carpetaCarne . "/014_" . $dato['cod_est'] . $dato['docu_num'] . ".jpg";

                if (file_exists($fotoRepositorio)) {
                    //copy($fotoRepositorio, $fotoPersona);
                    if (copy($fotoRepositorio, $fotoCarne)) {
                        $imagick = new \Imagick(realpath($fotoCarne));
                        $imagick->stripImage();
                        $imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                        $imagick->setImageResolution(300, 300);
                        $imagick->cropThumbnailImage(240, 288);
                        $imagick->setImageCompression(imagick::COMPRESSION_JPEG);
                        $imagick->setImageCompressionQuality(90);
                        $imagick->writeImage(realpath($fotoCarne));
                    }
                } else {
                    $estudiantesPedido = Estudiante_Pedido::findOrFail($dato['id']);
                    $estudiantesPedido->estado = null;
                    $estudiantesPedido->user_update = null;
                    $estudiantesPedido->notienefoto = '1';
                    $estudiantesPedido->save();
                }
            }

            // $fotosCarpetaPersona = glob($carpetaPersona . "/*.jpg");
            $fotosCarpetaCarne = glob($carpetaCarne . "/*.jpg");


            $zipper = new Zipper();
            //  $zipper->make($carpetaReportes . '/pedido_' . $pedido_id . '_fotosEstudiantes.zip')->add($fotosCarpetaPersona)->close();
            $zipper->make($carpetaReportes . '/pedido_' . $pedido_id . '_fotosEstudiantesFormatoSUNEDU.zip')->add($fotosCarpetaCarne)->close();
            $this->downloadExcelProcesadosLibros($pedido_id, 'xls', $carpetaCarne, $carpetaReportes);
            $archivosXls = glob($carpetaReportes . "/*.xls");
            $this->reporteByUsuarios($pedido_id);
            $this->downloadReporteByFacultades($pedido_id);
            $zipper->make($carpetaReportes . '/pedido_' . $pedido_id . '_reportesFormatoSUNEDU.zip')->add($archivosXls)->close();

            $pedido = Pedido::findOrFail($pedido_id);
            if ($pedido->estado == '1') {
                $now = new \DateTime();
                $fechaActual = $now->format('Y-m-d H:i:s');
                $pedido->fec_cierre = $fechaActual;
                $pedido->estado = '2';
                $pedido->save();
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => 'todo bien'], 200);
    }

    public function downloadExcelProcesadosLibros($pedido_id, $type, $carpetaCarne, $carpetaReportes) {
        $data = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados where  pedido_id=" . $pedido_id . "  ");
        $data = json_decode(json_encode($data), true);
        $dataoriginal = $data;
        $k = 0;

        if (count($data) > 500) {
            $hojas_despues_de_500 = intval(count($data) / 500);
            $hojas = intval(count($data) / 500);
        } else {
            $hojas = 0;
            $hojas_despues_de_500 = 0;
        }

        for ($ii = 1; $ii <= $hojas + 1; ++$ii) {
            $data = $dataoriginal;
            $data = array_slice($data, $k, 500);
            $archivo = Excel::create('Pedido_' . $pedido_id . '_ArchivoExcelFormatoSUNEDU_' . $ii, function($excel) use ($data, $carpetaCarne) {
                        $excel->sheet('Archivo de Carga', function($sheet) use ($data, $carpetaCarne) {
                            $sheet->setStyle(array(
                                'font' => array(
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                )
                            ));
                            $sheet->mergeCells('A1:D1');
                            $sheet->row(1, array(
                                'SUNEDU'
                            ));
                            $sheet->mergeCells('A2:D2');
                            $sheet->row(2, array(
                                'SISTEMA DE EMISIÓN Y EXPEDICIÓN DE CARNÉS UNIVERSITARIOS'
                            ));
                            $sheet->mergeCells('G1:J1');
                            $sheet->cell('G1', function($cell) {
                                $cell->setValue("014");
                            });
                            $sheet->mergeCells('G2:J2');
                            $sheet->cell('G2', function($cell) {
                                $cell->setValue("UNIVERSIDAD PERUANA CAYETANO HEREDIA");
                            });
                            $sheet->cell('E2', function($cell) {
                                $cell->setValue('ARCHIVO DE CARGA');
                            });
                            $sheet->cell('F1', function($cell) {
                                $cell->setValue('Codigo Universidad');
                            });
                            $sheet->cell('F2', function($cell) {
                                $cell->setValue('Universidad');
                            });
                            $sheet->cell('A4:Q4', function($cell) {
                                $cell->setBackground('#1F497D');
                                $cell->setFontColor('#ffffff');
                            });
                            $ultimaFila = count($data) + 4;
                            $sheet->setBorder('A1:Q' . $ultimaFila, 'thin');
                            $i = 0;
                            foreach ($data as $index => $dato) {

                                /* INICIO: SI es extramjero se le coloca su nrodocumento_original */
                                if ($dato['docu_tip'] <> '1') {
                                    $dato['docu_num'] = $dato['nrodocumento_original'];
                                }
                                /* FIN: SI es extramjero se le coloca su nrodocumento_original */

                                $nombre_fichero = $carpetaCarne . "/014_" . $dato['cod_est'] . $dato['docu_num'] . ".jpg";

                                if (file_exists($nombre_fichero)) {
                                    $sheet->row($i + 5, array(
                                        $dato['cod_univ'], $dato['cod_est'], $dato['apepat'], $dato['apemat'], $dato['nombre'], $dato['fec_nac'], $dato['genero'], $dato['ubigeo'], $dato['fac_nom_esc_pos'], $dato['abr_fac_abr_pos'], $dato['esc_carr_esp_pos'], $dato['abr_carr_abr_esp_pos'], $dato['docu_tip'], $dato['docu_num'], $dato['ano_ing'], $dato['per_ing'], $dato['fil']
                                    ));

                                    $i++;
                                }
                            }

                            $sheet->row(4, [
                                'COD_UNIV', 'COD_EST', 'APEPAT', 'APEMAT', 'NOMBRE', 'FEC_NAC', 'GENERO', 'UBIGEO', 'FAC_NOM-ESC_POS', 'ABR_FAC-ABR_POS', 'ESC_CARR-ESP_POS', 'ABR_CARR-ABR_ESP_POS', 'DOCU_TIP', 'DOCU_NUM', 'AÑO_ING', 'PER_ING', 'FIL'
                            ]);

                            $sheet->cells('A5:A' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('J5:J' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('L5:L' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('N5:N' . $ultimaFila, function($cells) {
                                $cells->setAlignment('right');
                            });
                            $sheet->cells('A4:Q4', function($cells) {
                                $cells->setAlignment('center');
                            });
                            $sheet->setColumnFormat(array(
                                'I5:I' . $ultimaFila => '0',
                                'M5:M' . $ultimaFila => '0',
                            ));
                        });
                    })->store($type, $carpetaReportes . '/');

            $k = $k + 500;
        }
        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => ''], 200);
    }

    public function extranjeros($pedido_id) {

        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();


        return view('backEnd.pedidosprocesar.extranjeros', compact('pedido'));
    }

    public function datatableExtranjeros($pedido_id) {
        $extranjeros = DB::connection('pgsql_syscarnes')->select("select id, apepat, apemat, nombre, docu_num as nrodocumento, nrodocumento_original  from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . "  and docu_tip<>'1' ");

        return Datatables::of($extranjeros)
                        ->addColumn('action', function ($data) {
                            return '<a class="btn btn-xs btn-primary" href="' . route('extranjeros.edit', $data->id) . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                        })->make(true);
    }

    public function extranjerosEdit($id) {
        $estudiante_pedido = Estudiante_Pedido::findOrFail($id);
        return view('backEnd.pedidosprocesar.extranjeroedit', compact('estudiante_pedido'));
    }

    public function extranjerosUpdate(Request $request) {
        $estudiante_pedido = Estudiante_Pedido::findOrFail($request->id);
        $estudiante_pedido->nrodocumento_original = $request->nrodocumento_original;
        $estudiante_pedido->save();
        return response()->json(['status' => 'success', 'message' => 'Actualización ejecutada exitosamente', 'id' => ''], 200);
    }

    public function duplicados($pedido_id) {

        $duplicados = DB::connection('pgsql_syscarnes')->select("select docu_num,count(docu_num) from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . " group by docu_num having count(docu_num)>1 ");
        return view('backEnd.pedidosprocesar.extranjeros', compact('duplicados'));
    }

    public function viewArchivosGenerados(Request $request) {
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();


        $carpetaReportes = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/reportes';
        $urlReportes = \Storage::url('pedido_' . $pedido_id . '/procesamiento/reportes');


        $listadoArchivosGenerados = array();

        $archivoFotosEstudiantes = $carpetaReportes . '/pedido_' . $pedido_id . '_fotosEstudiantes.zip';
        if (file_exists($archivoFotosEstudiantes)) {

            $listadoArchivosGenerados1 = array('name' => 'Fotos Estudiantes', 'link' => $urlReportes . '/pedido_' . $pedido_id . '_fotosEstudiantes.zip');
            array_push($listadoArchivosGenerados, $listadoArchivosGenerados1);
        }

        $archivoEstudiantesFormatoSunedu = $carpetaReportes . '/pedido_' . $pedido_id . '_fotosEstudiantesFormatoSUNEDU.zip';
        if (file_exists($archivoEstudiantesFormatoSunedu)) {

            $listadoArchivosGenerados2 = array('name' => 'Fotos Estudiantes Formato Sunedu', 'link' => $urlReportes . '/pedido_' . $pedido_id . '_fotosEstudiantesFormatoSUNEDU.zip');
            array_push($listadoArchivosGenerados, $listadoArchivosGenerados2);
        }

        $archivoFormatoSUNEDU = $carpetaReportes . '/pedido_' . $pedido_id . '_reportesFormatoSUNEDU.zip';
        if (file_exists($archivoFormatoSUNEDU)) {

            $listadoArchivosGenerados3 = array('name' => 'Archivos Excel Formato SUNEDU', 'link' => $urlReportes . '/pedido_' . $pedido_id . '_reportesFormatoSUNEDU.zip');
            array_push($listadoArchivosGenerados, $listadoArchivosGenerados3);
        }

        $archivoReporteXUsuarios = $carpetaReportes . '/pedido_' . $pedido_id . '_Reportes_X_Usuario.xls';
        if (file_exists($archivoReporteXUsuarios)) {

            $listadoArchivosGenerados4 = array('name' => 'Archivo Excel - Reporte x Usuarios', 'link' => $urlReportes . '/pedido_' . $pedido_id . '_Reportes_X_Usuario.xls');
            array_push($listadoArchivosGenerados, $listadoArchivosGenerados4);
        }

        $archivoReporteXFacultades = $carpetaReportes . '/Pedido_' . $pedido_id . '_Reporte_Por_Facultades.xls';
        if (file_exists($archivoReporteXFacultades)) {

            $listadoArchivosGenerados5 = array('name' => 'Archivo Excel - Reporte x Facultades', 'link' => $urlReportes . '/Pedido_' . $pedido_id . '_Reporte_Por_Facultades.xls');
            array_push($listadoArchivosGenerados, $listadoArchivosGenerados5);
        }

        return view('backEnd.pedidosprocesar.view_archivosgenerados', compact('pedido', 'listadoArchivosGenerados'));
    }

    public function reporteByUsuarios($pedido_id) {
        $pedido_id = $pedido_id;
        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();

        $pedido_id = $pedido->id;
        $type = 'xls';

        $carpetaReportes = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/reportes';
        if (!file_exists($carpetaReportes)) {
            mkdir($carpetaReportes, 0777, true);
        }

        $archivo = Excel::create('pedido_' . $pedido_id . '_Reportes_X_Usuario', function($excel) use ($pedido_id) {
                    $dataUsuario = DB::connection('pgsql_syscarnes')->select("SELECT distinct
                                        user_update,
                                        substring(trim(name),0,30) as name,
                                        codfacultad_usuario

                                FROM    estudiantes_pedido_usuarios 
                                WHERE   pedido_id=" . $pedido_id . " and 
                                        estado='1' and
                                        codfacultad_usuario is not null 
                                GROUP BY
                                        user_update,
                                        name,
                                        codfacultad_usuario,
                                        codprograma_sinu,
                                        nomprograma_sinu 
                                order by 3

                                ");

                    $dataUsuario = json_decode(json_encode($dataUsuario), true);


                    $existeUFBIRelacionUsuario = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                                codfacultad_usuario,
                                codprograma_sinu,
                                nomprograma_sinu,
                                count(codprograma_sinu) as cantidad
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                                estado='1' and
                                codfacultad_usuario='UFBI'
                        GROUP BY
                                user_update,
                                name,
                                codfacultad_usuario,
                                codprograma_sinu,
                                nomprograma_sinu order by 3,1,6,5");

                    $existeUFBIRelacionUsuario = json_decode(json_encode($existeUFBIRelacionUsuario), true);


                    foreach ($dataUsuario as $indexUsuario => $datoUsuario) {

                        $data = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                                codfacultad_usuario,
                                codprograma_sinu,
                                nomprograma_sinu,
                                count(codprograma_sinu) as cantidad
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                                estado='1' and
                                user_update ='" . $datoUsuario['user_update'] . "'
                        GROUP BY
                                user_update,
                                name,
                                codfacultad_usuario,
                                codprograma_sinu,
                                nomprograma_sinu order by 3,1,6,5");



                        $data = json_decode(json_encode($data), true);


                        $dataDetalle = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                                codfacultad_usuario,
                                codprograma_sinu,
                                nomprograma_sinu,
                                 codalumno, 
                                nrodocumento, 
                                apepaterno, 
                                apematerno, 
                                nombre
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                                estado='1' and
                                user_update ='" . $datoUsuario['user_update'] . "'
                        
                        order by 5 ");



                        $dataDetalle = json_decode(json_encode($dataDetalle), true);



                        $usuarioName = $datoUsuario['name'];
                        $usuarioUsername = $datoUsuario['user_update'];
                        $usuarioCodfacultadUsuario = $datoUsuario['codfacultad_usuario'];

                        $excel->sheet($datoUsuario['name'], function($sheet) use ($data, $dataDetalle, $usuarioName, $usuarioUsername, $usuarioCodfacultadUsuario, $pedido_id) {

                            $sheet->mergeCells('A1:E1');
                            $ultimaFila = count($data) + 3;
                            $sheet->setBorder('A1:E' . $ultimaFila, 'thin');
                            $sheet->cells('A1:E1', function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('A1', function($cell) use ($data, $usuarioName, $usuarioUsername, $usuarioCodfacultadUsuario, $pedido_id) {
                                $cell->setValue("PEDIDO " . $pedido_id . " -  CARNES UNIVERSITARIOS - " . $usuarioName . " - " . $usuarioUsername . " (" . $usuarioCodfacultadUsuario . ")");
                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->cell('A2:E2', function($cell) {

                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $i = 0;
                            $ii = 1;
                            $total = 0;
                            foreach ($data as $index => $dato) {
                                $sheet->row($i + 3, array(
                                    $ii, $dato['user_update'], $dato['codprograma_sinu'], $dato['nomprograma_sinu'], $dato['cantidad']
                                ));
                                $total = $total + $dato['cantidad'];
                                $i++;
                                $ii++;
                            }




                            $sheet->cell('A' . ($i + 3) . ':E' . ($i + 3), function($cell) {

                                $cell->setAlignment('right');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->row(2, [
                                'N°', 'USUARIO', 'COD.PROG', 'PROGRAMA', 'Cant.'
                            ]);

                            $sheet->row($i + 3, [
                                '', '', '', 'TOTAL:', $total
                            ]);


                            /* Inicio Detalle */
                            $ultimaFilaDetalle = $ultimaFila + 5;
                            $sheet->row(($ultimaFilaDetalle), [
                                'N°', 'Nro.Doc.', 'COD.PROG', 'PROGRAMA', 'Cod.Alumno', 'Ape.Paterno', 'Ape.Maternos', 'Nombres'
                            ]);

                            $iDetalle = 0;
                            $iiDetalle = 1;
                            $totalDetalle = 0;

                            $fila = 0;
                            foreach ($dataDetalle as $indexDetalle => $datoDetalle) {

                                $fila = $ultimaFilaDetalle + $iiDetalle;
                                $sheet->row($fila, array(
                                    $iiDetalle, $datoDetalle['nrodocumento'], $datoDetalle['codprograma_sinu'], $datoDetalle['nomprograma_sinu'], $datoDetalle['codalumno'], $datoDetalle['apepaterno'], $datoDetalle['apematerno'], $datoDetalle['nombre'],
                                ));
                                $iDetalle++;
                                $iiDetalle++;
                            }

                            $sheet->setBorder('A' . $ultimaFilaDetalle . ':H' . $fila, 'thin');
                            $sheet->cells('A' . $ultimaFilaDetalle . ':H' . $ultimaFilaDetalle, function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('B' . ($ultimaFilaDetalle + 1) . ':B' . $fila, function($cell) {

                                $cell->setAlignment('right');
                            });

                            /* Fin Detalle */


                            if ($usuarioCodfacultadUsuario == 'UFBI') {


                                $dataUfbi = DB::connection('pgsql_syscarnes')->select("select 
                                            user_update,
                                            name,
                                            codfacultad_usuario,
                                            codprograma_sinu,
                                            nomprograma_sinu,
                                            count(codprograma_sinu) as cantidad
                                    FROM 
                                            estudiantes_pedido_usuarios 
                                    WHERE 
                                            pedido_id=" . $pedido_id . " and 
                                            estado='1' and 
                                            codfacultad_usuario is null  and
                                            codmodalidad='1'  and
                                            nivel  in (1,2)
                                    GROUP BY 
                                            user_update,
                                            name,
                                            codfacultad_usuario,
                                            codprograma_sinu,
                                            nomprograma_sinu 
                                    ORDER BY
                                            3,1,6,5");

                                $dataUfbi = json_decode(json_encode($dataUfbi), true);


                                $ultimaFila = count($dataUfbi) + 10;
                                $sheet->setBorder('A' . ($i + 6) . ':E' . $ultimaFila, 'thin');

                                $sheet->cell('A' . ($i + 7) . ':E' . ($i + 7), function($cell) {

                                    $cell->setAlignment('center');
                                    $cell->setFontWeight('bold');
                                });

                                $sheet->mergeCells('A' . ($i + 6) . ':E' . ($i + 6));

                                $sheet->cell('A' . ($i + 6), function($cell) {
                                    $cell->setValue("AGREGADOS");
                                    $cell->setAlignment('center');
                                    $cell->setFontWeight('bold');
                                });

                                $sheet->row(($i + 7), [
                                    'N°', 'USUARIO', 'COD.PROG', 'PROGRAMA', 'Cant.'
                                ]);
                                $iii = 1;
                                $totalUfbi = 0;
                                foreach ($dataUfbi as $index => $dato) {
                                    $sheet->row($i + 8, array(
                                        $iii, $dato['user_update'], $dato['codprograma_sinu'], $dato['nomprograma_sinu'], $dato['cantidad']
                                    ));
                                    $totalUfbi = $totalUfbi + $dato['cantidad'];
                                    $i++;
                                    $iii++;
                                }

                                $sheet->cell('A' . ($i + 8) . ':E' . ($i + 8), function($cell) {

                                    $cell->setAlignment('right');
                                    $cell->setFontWeight('bold');
                                });

                                $sheet->row($i + 8, [
                                    '', '', '', 'TOTAL:', $totalUfbi
                                ]);





                                /* Inicio Detalle */


                                $dataDetalle = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                               codfacultad_usuario as facultad,
                                codprograma_sinu,
                                nomprograma_sinu,
                                 codalumno, 
                                nrodocumento, 
                                apepaterno, 
                                apematerno, 
                                nombre
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                              estado='1' and 
                                            codfacultad_usuario is null  and
                                            codmodalidad='1'  and
                                            nivel  in (1,2)
                        
                        order by 5 ");



                                $dataDetalle = json_decode(json_encode($dataDetalle), true);

                                $ultimaFila = count($dataDetalle) + 3;
                                $ultimaFilaDetalle = $ultimaFila + 5;
                                $sheet->row(($ultimaFilaDetalle), [
                                    'N°', 'Nro.Doc.', 'COD.PROG', 'PROGRAMA', 'Cod.Alumno', 'Ape.Paterno', 'Ape.Maternos', 'Nombres'
                                ]);

                                $iDetalle = 0;
                                $iiDetalle = 1;
                                $totalDetalle = 0;

                                $fila = 0;
                                foreach ($dataDetalle as $indexDetalle => $datoDetalle) {

                                    $fila = $ultimaFilaDetalle + $iiDetalle;
                                    $sheet->row($fila, array(
                                        $iiDetalle, $datoDetalle['nrodocumento'], $datoDetalle['codprograma_sinu'], $datoDetalle['nomprograma_sinu'], $datoDetalle['codalumno'], $datoDetalle['apepaterno'], $datoDetalle['apematerno'], $datoDetalle['nombre'],
                                    ));
                                    // $totalDetalle = $totalDetalle + $datoDetalle['cantidad'];
                                    $iDetalle++;
                                    $iiDetalle++;
                                }

                                $sheet->setBorder('A' . $ultimaFilaDetalle . ':H' . $fila, 'thin');
                                $sheet->cells('A' . $ultimaFilaDetalle . ':H' . $ultimaFilaDetalle, function($cells) {
                                    $cells->setBackground('#FFFF00');
                                });

                                $sheet->cell('B' . ($ultimaFilaDetalle + 1) . ':B' . $fila, function($cell) {

                                    $cell->setAlignment('right');
                                });

                                /* Fin Detalle */
                            }
                        });
                    }





                    if (count($existeUFBIRelacionUsuario) == 0) {

                        $data = DB::connection('pgsql_syscarnes')->select("select 
                                            user_update,
                                            name,
                                            'UFBI' as facultad,
                                            codprograma_sinu,
                                            nomprograma_sinu,
                                            count(codprograma_sinu) as cantidad
                                    FROM 
                                            estudiantes_pedido_usuarios 
                                    WHERE 
                                            pedido_id=" . $pedido_id . " and 
                                            estado='1' and 
                                            codfacultad_usuario is null  and
                                            codmodalidad='1'  and
                                            nivel  in (1,2)
                                    GROUP BY 
                                            user_update,
                                            name,
                                            codfacultad_usuario,
                                            codprograma_sinu,
                                            nomprograma_sinu 
                                    ORDER BY
                                            3,1,6,5");
                        $data = json_decode(json_encode($data), true);



                        $excel->sheet("UFBI", function($sheet) use ($data, $pedido_id) {
                            $sheet->mergeCells('A1:E1');
                            $ultimaFila = count($data) + 3;
                            $sheet->setBorder('A1:E' . $ultimaFila, 'thin');
                            $sheet->cells('A1:E1', function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('A1', function($cell) use ($pedido_id) {
                                $cell->setValue("Pedido " . $pedido_id . " - UFBI");
                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->cell('A2:E2', function($cell) {

                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $i = 0;
                            $ii = 1;
                            $total = 0;
                            foreach ($data as $index => $dato) {
                                $sheet->row($i + 3, array(
                                    $ii, $dato['facultad'], $dato['codprograma_sinu'], $dato['nomprograma_sinu'], $dato['cantidad']
                                ));
                                $total = $total + $dato['cantidad'];
                                $i++;
                                $ii++;
                            }

                            $sheet->cell('A' . ($i + 3) . ':E' . ($i + 3), function($cell) {

                                $cell->setAlignment('right');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->row(2, [
                                'N°', 'FACULTAD', 'COD.PROG', 'PROGRAMA', 'Cant.'
                            ]);

                            $sheet->row($i + 3, [
                                '', '', '', 'TOTAL:', $total
                            ]);







                            /* Inicio Detalle */


                            $dataDetalle = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                                 'UFBI' as facultad,
                                codprograma_sinu,
                                nomprograma_sinu,
                                 codalumno, 
                                nrodocumento, 
                                apepaterno, 
                                apematerno, 
                                nombre
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                               estado='1' and 
                                            codfacultad_usuario is null  and
                                            codmodalidad='1'  and
                                            nivel  in (1,2)
                        
                        order by 5 ");



                            $dataDetalle = json_decode(json_encode($dataDetalle), true);

                            $ultimaFila = count($dataDetalle) + 3;
                            $ultimaFilaDetalle = $ultimaFila + 5;
                            $sheet->row(($ultimaFilaDetalle), [
                                'N°', 'Nro.Doc.', 'COD.PROG', 'PROGRAMA', 'Cod.Alumno', 'Ape.Paterno', 'Ape.Maternos', 'Nombres'
                            ]);

                            $iDetalle = 0;
                            $iiDetalle = 1;
                            $totalDetalle = 0;

                            $fila = 0;
                            foreach ($dataDetalle as $indexDetalle => $datoDetalle) {

                                $fila = $ultimaFilaDetalle + $iiDetalle;
                                $sheet->row($fila, array(
                                    $iiDetalle, $datoDetalle['nrodocumento'], $datoDetalle['codprograma_sinu'], $datoDetalle['nomprograma_sinu'], $datoDetalle['codalumno'], $datoDetalle['apepaterno'], $datoDetalle['apematerno'], $datoDetalle['nombre'],
                                ));
                                // $totalDetalle = $totalDetalle + $datoDetalle['cantidad'];
                                $iDetalle++;
                                $iiDetalle++;
                            }

                            $sheet->setBorder('A' . $ultimaFilaDetalle . ':H' . $fila, 'thin');
                            $sheet->cells('A' . $ultimaFilaDetalle . ':H' . $ultimaFilaDetalle, function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('B' . ($ultimaFilaDetalle + 1) . ':B' . $fila, function($cell) {

                                $cell->setAlignment('right');
                            });

                            /* Fin Detalle */
                        });
                    }




                    $data = DB::connection('pgsql_syscarnes')->select("select 
                            user_update,
                            name,
                            abreviaturafacultad_sinu as facultad  ,
                            codprograma_sinu,
                            nomprograma_sinu,
                            count(codprograma_sinu) as cantidad 
                    FROM 
                            estudiantes_pedido_usuarios 
                    WHERE 
                            pedido_id=" . $pedido_id . " and 
                            estado='1' and 
                            codfacultad_usuario is null  and
                            codmodalidad='1'  and
                            nivel  NOT in (1,2)
                    GROUP BY 
                            user_update,
                            name,
                            abreviaturafacultad_sinu,
                            codprograma_sinu,
                            nomprograma_sinu 
                            
                            union
                            
                            select 
                            user_update,
                            name,
                            abreviaturafacultad_sinu as facultad  ,
                            codprograma_sinu,
                            nomprograma_sinu,
                            count(codprograma_sinu) as cantidad 
                    FROM 
                            estudiantes_pedido_usuarios 
                    WHERE 
                            pedido_id=" . $pedido_id . " and 
                            estado='1' and 
                            codfacultad_usuario is null  and
                            codmodalidad<>'1'  
                    GROUP BY 
                            user_update,
                            name,
                            abreviaturafacultad_sinu,
                            codprograma_sinu,
                            nomprograma_sinu 
                            ");
                    $data = json_decode(json_encode($data), true);

                    if (count($data) > 0) {
                        $excel->sheet("Agregados Pregrado", function($sheet) use ($data, $pedido_id) {
                            $sheet->mergeCells('A1:E1');
                            $ultimaFila = count($data) + 3;
                            $sheet->setBorder('A1:E' . $ultimaFila, 'thin');
                            $sheet->cells('A1:E1', function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('A1', function($cell) use ($pedido_id) {
                                $cell->setValue("Pedido " . $pedido_id . " - Agregados Pregrado");
                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->cell('A2:E2', function($cell) {

                                $cell->setAlignment('center');
                                $cell->setFontWeight('bold');
                            });

                            $i = 0;
                            $ii = 1;
                            $total = 0;
                            foreach ($data as $index => $dato) {
                                $sheet->row($i + 3, array(
                                    $ii, $dato['facultad'], $dato['codprograma_sinu'], $dato['nomprograma_sinu'], $dato['cantidad']
                                ));
                                $total = $total + $dato['cantidad'];
                                $i++;
                                $ii++;
                            }

                            $sheet->cell('A' . ($i + 3) . ':E' . ($i + 3), function($cell) {

                                $cell->setAlignment('right');
                                $cell->setFontWeight('bold');
                            });

                            $sheet->row(2, [
                                'N°', 'FACULTAD', 'COD.PROG', 'PROGRAMA', 'Cant.'
                            ]);

                            $sheet->row($i + 3, [
                                '', '', '', 'TOTAL:', $total
                            ]);





                            /* Inicio Detalle */


                            $dataDetalle = DB::connection('pgsql_syscarnes')->select("SELECT 
                                user_update,
                                name,
                                   abreviaturafacultad_sinu as facultad  ,
                                codprograma_sinu,
                                nomprograma_sinu,
                                 codalumno, 
                                nrodocumento, 
                                apepaterno, 
                                apematerno, 
                                nombre
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                              estado='1' and 
                            codfacultad_usuario is null  and
                            codmodalidad='1'  and
                            nivel  NOT in (1,2)
                            
                            union 

                            SELECT 
                                user_update,
                                name,
                                   abreviaturafacultad_sinu as facultad  ,
                                codprograma_sinu,
                                nomprograma_sinu,
                                 codalumno, 
                                nrodocumento, 
                                apepaterno, 
                                apematerno, 
                                nombre
                        FROM    estudiantes_pedido_usuarios 
                        WHERE   pedido_id=" . $pedido_id . " and 
                              estado='1' and 
                            codfacultad_usuario is null  and
                            codmodalidad<>'1'  
                        
                        ");



                            $dataDetalle = json_decode(json_encode($dataDetalle), true);

                            $ultimaFila = count($dataDetalle) + 3;
                            $ultimaFilaDetalle = $ultimaFila + 5;
                            $sheet->row(($ultimaFilaDetalle), [
                                'N°', 'Nro.Doc.', 'COD.PROG', 'PROGRAMA', 'Cod.Alumno', 'Ape.Paterno', 'Ape.Maternos', 'Nombres'
                            ]);

                            $iDetalle = 0;
                            $iiDetalle = 1;
                            $totalDetalle = 0;

                            $fila = 0;
                            foreach ($dataDetalle as $indexDetalle => $datoDetalle) {

                                $fila = $ultimaFilaDetalle + $iiDetalle;
                                $sheet->row($fila, array(
                                    $iiDetalle, $datoDetalle['nrodocumento'], $datoDetalle['codprograma_sinu'], $datoDetalle['nomprograma_sinu'], $datoDetalle['codalumno'], $datoDetalle['apepaterno'], $datoDetalle['apematerno'], $datoDetalle['nombre'],
                                ));
                                // $totalDetalle = $totalDetalle + $datoDetalle['cantidad'];
                                $iDetalle++;
                                $iiDetalle++;
                            }

                            $sheet->setBorder('A' . $ultimaFilaDetalle . ':H' . $fila, 'thin');
                            $sheet->cells('A' . $ultimaFilaDetalle . ':H' . $ultimaFilaDetalle, function($cells) {
                                $cells->setBackground('#FFFF00');
                            });

                            $sheet->cell('B' . ($ultimaFilaDetalle + 1) . ':B' . $fila, function($cell) {

                                $cell->setAlignment('right');
                            });

                            /* Fin Detalle */
                        });
                    }
                })->store($type, $carpetaReportes . '/');



        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => ''], 200);
    }

    public function downloadEstudiantesExtranjeros($pedido_id) {
        $pedido_id = $pedido_id;



        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();

        $pedido_id = $pedido->id;
        $type = 'xls';




        $carpetaReportes = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/reportes';
        if (!file_exists($carpetaReportes)) {
            mkdir($carpetaReportes, 0777, true);
        }


        $extranjeros = DB::connection('pgsql_syscarnes')->select("select id, apepat, apemat, nombre, docu_num as codigo, nrodocumento_original  from vst_pedido_exporta_confirmados where pedido_id=" . $pedido_id . "  and docu_tip<>'1' ");

        $data = json_decode(json_encode($extranjeros), true);


        $archivo = Excel::create('Pedido_' . $pedido_id . '_Estudiantes_Extranjeros', function($excel) use ($data, $pedido) {
                    $excel->sheet('Estudiantes Extranjeros', function($sheet) use ($data, $pedido) {
                        $sheet->setStyle(array(
                            'font' => array(
                                'name' => 'Calibri',
                                'size' => 11
                            )
                        ));
                        $sheet->mergeCells('A1:E1');
                        $sheet->row(1, array(
                            'ESTUDIANTES EXTRANJEROS DEL PEDIDO N°' . $pedido->name
                        ));


                        $ultimaFila = count($data) + 3;
                        $sheet->setBorder('A1:E' . $ultimaFila, 'thin');
                        $i = 0;
                        foreach ($data as $index => $dato) {


                            $sheet->row($i + 4, array(
                                $dato['apepat'], $dato['apemat'], $dato['nombre'], $dato['codigo'], $dato['nrodocumento_original']
                            ));

                            $i++;
                        }

                        $sheet->row(3, [
                            'APEPAT', 'APEMAT', 'NOMBRE', 'CODIGO', 'NÚMERO DOCUMENTO ORIGINAL'
                        ]);
                    });
                })->export('xls');
    }

    public function downloadReporteByFacultades($pedido_id) {


        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();

        $pedido_id = $pedido->id;
        $type = 'xls';




        $carpetaReportes = storage_path('app/public') . '/pedido_' . $pedido_id . '/procesamiento/reportes';
        if (!file_exists($carpetaReportes)) {
            mkdir($carpetaReportes, 0777, true);
        }


        $reporteByFacultades = DB::connection('pgsql_syscarnes')->select("select 'ESCUELA DE POSTGRADO' AS facultad, count(*) as cantidad from estudiantes_pedido where pedido_id=" . $pedido_id . " and estudiantes_pedido.estado='1' and codmodalidad='2' and niv_formacion in ('6','7') 
                        union
                        select 
                        nomfacultad_sinu as facultad, count(nomfacultad_sinu) as cantidad
                          from  
                        estudiantes_pedido inner join pedidos on estudiantes_pedido.pedido_id=pedidos.id
                        inner join (select distinct codfacultad, nomfacultad from facultades_sinu ) A on estudiantes_pedido.codfacultad_sinu=A.codfacultad
                         where 
                         pedido_id=" . $pedido_id . " 
                         and estudiantes_pedido.estado='1'  and ((codmodalidad='2' and niv_formacion not in ('6','7')) or (codmodalidad='1' and (nivel not in ('1','2')  or nivel is null) )) group by nomfacultad_sinu
                        union
                        select 'UNIDAD DE FORMACIÓN BASICA INTEGRAL' as facultad, count(*) as cantidad from estudiantes_pedido where pedido_id=" . $pedido_id . " and estudiantes_pedido.estado='1' and codmodalidad='1' and nivel in ('1','2') 

                        ORDER BY 2 DESC");

        $data = json_decode(json_encode($reporteByFacultades), true);


        $archivo = Excel::create('Pedido_' . $pedido_id . '_Reporte_Por_Facultades', function($excel) use ($data, $pedido) {
                    $excel->sheet('Reporte', function($sheet) use ($data, $pedido) {


                        $sheet->setStyle(array(
                            'font' => array(
                                'name' => 'Calibri',
                                'size' => 11
                            )
                        ));
                        $sheet->mergeCells('A1:B1');
                        $sheet->row(1, array(
                            'REPORTE POR FACULTADES DEL ' . $pedido->nombre
                        ));

                        $sheet->cells('A1:B1', function($cells) {
                            $cells->setBackground('#FFFF00');
                        });

                        $sheet->cell('A1:B3', function($cell) {
                            $cell->setAlignment('center');
                            $cell->setFontWeight('bold');
                        });



                        $ultimaFila = count($data) + 4;
                        $sheet->setBorder('A1:B' . $ultimaFila, 'thin');
                        $i = 0;
                        $total = 0;
                        foreach ($data as $index => $dato) {
                            $sheet->row($i + 4, array(
                                $dato['facultad'], $dato['cantidad']
                            ));
                            $i++;

                            $total = $total + $dato['cantidad'];
                        }
                        $sheet->row(3, [
                            'FACULTAD', 'CANTIDAD'
                        ]);

                        $sheet->row($ultimaFila, [
                            'TOTAL', $total
                        ]);


                        $sheet->cell('A' . $ultimaFila . ':B' . $ultimaFila, function($cell) {
                            $cell->setFontWeight('bold');
                        });

                        $sheet->cell('A' . $ultimaFila, function($cell) {
                            $cell->setAlignment('center');
                        });
                    });
                })->store($type, $carpetaReportes . '/');
    }

    public function subirArchivoEstudiantesExtranjeros(Request $request) {
        try {

            if (empty($request->file('file_archivo'))) {
                return response()->json(['status' => 'warning', 'message' => 'Debe seleccionar un archivo'], 200);
            }

            $pedido_id = $request->pedido_id;

            $file = $request->file('file_archivo');
            $extension = $file->getClientOriginalExtension();
            $cargarArchivo = \Storage::disk('local')->put('pedido_' . $pedido_id . '_ReporteEstudiantesExtranjeros.' . $extension, \File::get($file));
            if ($cargarArchivo) {
                $this->actualizarEstudiantesExtranjeros($pedido_id, $extension);
                /* if ($procesar == true) {
                  return response()->json(['status' => 'success', 'message' => 'El archivo se ha procesado correctamente.'], 200);
                  } else {
                  return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al procesar el archivo.'], 200);
                  } */
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al cargar el archivo.'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'El archivo se ha procesado correctamente.'], 200);
    }

    public function actualizarEstudiantesExtranjeros($pedido_id, $extension) {
        ini_set('max_execution_time', 600);
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {

            $url = storage_path('app') . '/pedido_' . $pedido_id . '_ReporteEstudiantesExtranjeros.' . $extension;

            $returnImportacion = Excel::selectSheets('Validacion de usuarios registr')->load($url, function($reader ) use ($pedido_id) {
                        $reader->get();
                        $reader->each(function($row) use($pedido_id) {
                            if (!empty($row['num._documento'])) {
                                $estudiantePedido = Estudiante_Pedido::where([
                                            ['pedido_id', '=', $pedido_id],
                                            ['nrodocumento', '=', $row['codigo']],
                                        ])->first();
                                $estudiantePedido->nrodocumento_original = $row['num._documento'];
                                $estudiantePedido->save();
                            }
                        });
                    })->get();
            $totalReturnImportacion = count($returnImportacion) - 1;
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return false;
            // return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage()], 500);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return true;
        // return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han actualizado ' . $totalReturnImportacion . ' estudiantes.</div>', 'message' => 'Se han actualizado ' . $totalReturnImportacion . ' estudiantes. ', 'messageFinalizado' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>'], 200);
    }

}
