<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Pedido;
use App\PedidoCerrado;
use App\Estudiante_Pedido;
use App\SolicitudSunedu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use Excel;
use Yajra\Datatables\Datatables;

class PedidosController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);

        if ($permisos->permission_create == true) {
            $estadoSql = "and p.estado <>'0' ";
        } else {
            $estadoSql = "and p.estado in ('1','2') ";
        }

        $pedidos = DB::connection('pgsql_syscarnes')->select("select 
            case
            when p.estado='1' and pc.estado='1' then 'Cerrado'
            when p.estado='3' then 'Prueba'
            when p.estado='2' then 'Cerrado' else 'Abierto' end 
            as estado, p.id,to_char(p.fec_inicio,'dd/MM/YYYY') as fec_inicio,
            to_char(p.fec_fin,'dd/MM/YYYY') as fec_fin,
            p.nombre,to_char(p.fec_importacion_estudiantes,'dd/MM/YYYY') as  fec_importacion_estudiantes,
            to_char(pc.updated_at,'dd/MM/YYYY') as fec_cierre from pedidos p 
            left join pedidos_cerrados pc on p.id=pc.pedido_id and  pc.estado='1' and user_update='" . Auth::User()->username . "' where ( p.eliminado is null or p.eliminado='0')   " . $estadoSql . "      ");


        return view('backEnd.pedidos.index', compact('pedidos', 'permisos'));
    }

    /* FUNCION: Ver detalle del Pedido */

    public function detalle(Request $request, $pedido_id) {
        $permisos = $this->seguridad->validarPermisos($request);
        $pedido = Pedido::findOrFail($pedido_id);
        $programasSession = Session::get('programasString');

        if (empty($programasSession)) {
            $programasSession = "'NO EXISTE'";
        }

        $facultades = DB::connection('pgsql_syscarnes')->select("select distinct codfacultad as cod, nomfacultad as name from facultades_sinu where codfacultad in (
                                                                        select codfacultad from programas_sinu  where  codprograma in  (" . $programasSession . ") )");

        $programas = DB::connection('pgsql_syscarnes')->select("select distinct codprograma as cod, '('||codprograma||') - '||nomprograma as name from programas_sinu where  codprograma in  (" . $programasSession . ")");

        
        $formularios = new FormulariosController();
        $selectFacultades = $formularios->selectSimpleNoLabelDefaultTodos($facultades, 'facultad_id', '0');
        $selectProgramas = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id', '0');
        $selectFacultades2 = $formularios->selectSimpleNoLabelDefaultTodos($facultades, 'facultad_id2', '0');
        $selectProgramas2 = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id2', '0');


        $pedidocerrado = PedidoCerrado::where([
                    ['pedido_id', '=', $pedido_id],
                    ['user_update', '=', Auth::User()->username],
                    ['estado', '=', '1'],
                ])->first();

        if (count($pedidocerrado) > 0) {
            $pedidocerrado_estado = 1;
        } else {
            $pedidocerrado_estado = 0;
        }

        return view('backEnd.pedidos.detallepedido', compact('pedido', 'selectFacultades', 'selectProgramas', 'selectFacultades2', 'selectProgramas2', 'permisos', 'pedidocerrado_estado'));
    }

    /* FUNCION: Cerrar Pedido */

    public function cerrarPedido(Request $request) {
        $pedido_id = $request->pedido_id;

        //dd($pedido_id);
        $pedidocerrado = PedidoCerrado::where([
                    ['pedido_id', '=', $pedido_id],
                    ['user_update', '=', Auth::User()->username],
                    ['estado', '=', '1'],
                ])->first();
        $retorno = 0;
        if (count($pedidocerrado) > 0) {
            $retorno = 1;
        } else {
            $retorno = 2;
            $pedidocerrado = new PedidoCerrado();
            $pedidocerrado->estado = '1';
            $pedidocerrado->pedido_id = $pedido_id;
            $pedidocerrado->user_update = Auth::User()->username;
            $pedidocerrado->save();
        }

        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => $retorno], 200);
    }

    /* FUNCION: Descargar excel de los Pedidos confirmados por cada usuario */

    public function downloadExcelConfirmados($pedido_id) {
        $programas = Session::get('programasString');

        if (empty($programas)) {
            $programas = "'NO EXISTE'";
        }
        $data = DB::connection('pgsql_syscarnes')->select("select pedido_id ,codalumno ,nrodocumento,nombre ,apepaterno ,apematerno ,abreviaturafacultad_sinu as facultad,codprograma_sinu|| ' - ' ||nomprograma_sinu as programa, codfacultad_sunedu,  codprograma_sunedu, nomprograma_sunedu from vst_estudiantes_pedido where pedido_id='" . $pedido_id . "'  and  codprograma_sinu in (" . $programas . ") and estado='1' and user_update='" . Auth::User()->username . "' ");
        $user = Auth::User()->username;
        $data = json_decode(json_encode($data), true);
        Excel::create('ReporteCarnesUniversitariosConfirmados_Pedido_' . $pedido_id . '_usuario_' . Auth::User()->username, function($excel) use($data, $pedido_id, $user) {

            $excel->sheet('Listado', function($sheet) use($data, $pedido_id, $user) {

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Calibri',
                        'size' => 11,
                    )
                ));
                $sheet->mergeCells('A1:K1');
                $sheet->row(1, array(
                    'REPORTE DE PEDIDO DE CARNÉS UNIVERSITARIOS N° ' . $pedido_id
                ));

                $sheet->cells('A1:K1', function($cells) {
                    $cells->setAlignment('center');
                });

                $i = 0;
                foreach ($data as $index => $dato) {
                    $sheet->row($i + 3, array(
                        $dato['pedido_id'], $dato['codalumno'], $dato['nrodocumento'], $dato['nombre'], $dato['apepaterno'], $dato['apematerno'], $dato['facultad'], $dato['programa'], $dato['codfacultad_sunedu'], $dato['nomprograma_sunedu'], $user
                    ));

                    $i++;
                }

                $sheet->row(2, [
                    'PEDIDO', 'COD.ALUMNO', 'NRO. DOCUMENTO', ' NOMBRE ', ' APE.PATERNO ', ' APE.MATERNO ', 'FACULTAD', ' PROGRAMA SINU ', 'FAC.SUNEDU', 'PROGRAMA SUNEDU', 'VALIDADO POR:'
                ]);


                $sheet->cells('A2:K2', function($cells) {
                    $cells->setAlignment('center');
                });


                $ultimaFila = count($data) + 2;
                $sheet->cells('A3:A' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('B3:B' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('C3:C' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('D3:D' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('E3:E' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('F3:F' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('G3:G' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('H3:H' . $ultimaFila, function($cells) {
                    $cells->setAlignment('left');
                });

                $sheet->cells('I3:I' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('J3:J' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->cells('K3:K' . $ultimaFila, function($cells) {
                    $cells->setAlignment('center');
                });

                //$sheet->setBorder('A1', 'thin');
                $sheet->setOrientation('landscape');


                $sheet->setBorder('A2:K' . $ultimaFila, 'thin');
            });
        })->export('xls');

        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => ''], 200);
    }

    public function procesarSolicitudMasiva(Request $request) {
        ini_set('max_execution_time', 600);
        try {
            $pedido_id = $request->pedido_id;
            $codprograma_sinu = $request->programa_cod;

            $pedido = Pedido::where([
                        ['id', '=', $pedido_id],
                        ['eliminado', '=', null],
                    ])->first();


            if ($pedido->estado == '2') {
                return response()->json(['status' => 'warning', 'message' => 'El pedido de encuentra cerrado'], 200);
            }

            $pedidocerrado = PedidoCerrado::where([
                        ['pedido_id', '=', $pedido_id],
                        ['user_update', '=', Auth::User()->username],
                        ['estado', '=', '1'],
                    ])->first();

            if (count($pedidocerrado) > 0) {
                return response()->json(['status' => 'warning', 'message' => 'El pedido de encuentra cerrado'], 200);
            }



            if ($codprograma_sinu == 'all') {
                return response()->json(['status' => 'warning', 'message' => 'Debe de elegir un programa'], 200);
            }
            $estudiantesPedido = Estudiante_Pedido::where([
                        ['pedido_id', '=', $pedido_id],
                        ['estado', '=', null],
                        ['codprograma_sinu', '=', $codprograma_sinu]
                    ])->get();


            $totalRegistros = count($estudiantesPedido);
            $data = json_decode(json_encode($estudiantesPedido), true);
            $carpeta = storage_path('app/public') . '/pedido_' . $pedido_id . '/gestion/fotospedido';
            $i = 0;
            foreach ($data as $index => $dato) {
                $fotoCarpeta = $carpeta . "/" . $dato['nrodocumento'] . ".jpg";
                if (file_exists($fotoCarpeta)) {
                    $estudiantePedido = Estudiante_Pedido::findOrFail($dato['id']);
                    $estudiantePedido->estado = '1';
                    $estudiantePedido->user_update = Auth::User()->username;
                    $estudiantePedido->save();

                    $i++;
                }
            }
        } catch (Exception $e) {
            return response()->json(['status' => 'success', 'message' => 'Error al ejecutar procedimiento de filtración'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Se solicitaron ' . $i . ' de ' . $totalRegistros . ' carnés universitarios para el programa ' . $codprograma_sinu], 200);
    }

    public function datatableResumenPedido(Request $request) {


        $pedido_id = $request->pedido_id;




        $data = DB::connection('pgsql_syscarnes')->select("select codfacultad_usuario as facultad, username , name as usuario ,count(ep.user_update) as cantidad,case when pc.user_update is null then 'Abierto' else 'Cerrado' end estado
                                                                from 
                                                                estudiantes_pedido_usuarios ep 
                                                                left join pedidos_cerrados pc
                                                                on ep.user_update=pc.user_update and ep.pedido_id=pc.pedido_id and pc.estado='1'
                                                                where ep.pedido_id=" . $pedido_id . "  and ep.estado='1'  group by codfacultad_usuario, username, name ,pc.user_update
                                                                order by 4
                                                                ");


        return Datatables::of($data)
                        ->editColumn('estado', function ($data) {
                            if ($data->estado == 'Abierto') {
                                return '<span class="btn btn-danger" >' . $data->estado . '</span>';
                            } else {
                                return '<span class="btn btn-success" >' . $data->estado . '</span>';
                            }
                        })->rawColumns(['estado'])
                        ->make(true);

        /* FIN: Verificar si el USUARIO ha CERRADO el PEDIDO */
    }

    public function cambiarEstadoPedidoUsuario(Request $request) {

        /* INICIO: Obtener Parametros */
        $user_update = $request->user_update;
        $pedido_id = $request->pedido_id;
        /* FIN: Obtener Parametros */
        $Estudiante_Pedido = Estudiante_Pedido::findOrFail($id);
        /* INICIO: Verificar si el estado enviado al hacer check es SI */
        if ($estado == '1') {
            /* INICIO: Verificar si al hacer check ya otra persona ha validado al estudiante */
            if (Auth::User()->username != $Estudiante_Pedido->user_update_entregado && $Estudiante_Pedido->estado_entregado == '1') {
                return response()->json(['status' => 'success', 'message' => 'Registrado Validado por otro usuario', 'data' => 'Todo bien', 'nameestado' => 'No puede validar, registro validado por otro usuario'], 200);
            }
            /* FIN: Verificar si al hacer check ya otra persona ha validado al estudiante */

            $Estudiante_Pedido->estado_entregado = '1';
            $NameEstado = 'SI';
            $Estudiante_Pedido->user_update_entregado = Auth::User()->username;
        } else {

            $Estudiante_Pedido->estado_entregado = null;
            $NameEstado = 'NO';
            $Estudiante_Pedido->user_update_entregado = null;
        }
        /* FIN: Verificar si el estado enviado al hacer check es SI */

        $Estudiante_Pedido->save();

        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => 'Datos guardados exitosamente', 'nameestado' => $NameEstado], 200);
    }

    public function datatableSolicitudesSuneduVisualiza() {
        $solicitudes = DB::connection('pgsql_syscarnes')->select("select ss.id,  ss.nrosolicitud, count(es.solicitud_id) as totalestudiantes from solicitudes_sunedu ss left join 
                                                                    estudiantes_sunedu es on ss.id=es.solicitud_id where ss.estado='1'  group by ss.id,ss.nrosolicitud order by  ss.nrosolicitud    ");
        return Datatables::of($solicitudes)
                        ->addColumn('action', function ($data) {
                            return '<form class="form-inline" role="form" method="POST" id="formuploadajax' . $data->id . '" name="formuploadajax' . $data->id . '" accept-charset="UTF-8" enctype="multipart/form-data" >
                                        <div  class="form-group"><button type="button" class="btn btn-xs btn-primary " onclick="verEstudiantesSunedu(' . $data->id . ');" >Ver Estudiantes</button></div>
                                   </form>';
                        })
                        ->make(true);
    }

    public function verEstudiantesSunedu(Request $request) {

        $solicitud_id = $request->solicitud_id;
        $solicitud = SolicitudSunedu::findOrFail($solicitud_id);
        return view('backEnd.pedidos.estudiantessunedu', compact('solicitud'));
    }

}
