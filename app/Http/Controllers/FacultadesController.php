<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacultadSinu;
use App\FacultadSunedu;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\componentes\FormulariosController;
use Illuminate\Support\Facades\Session;
use App\PedidoCerrado;
use App\Pedido;
use Illuminate\Support\Facades\Auth;

class FacultadesController extends Controller {

    public function datatableFacultadesSinuSunedu(Request $request) {
        $pedido_id = $request->pedido_id;
        $facultadesSinuSunedu = DB::connection('pgsql_syscarnes')->select("select * from facultades_sinu where codfacultad in ( select codfacultad_sinu from estudiantes_pedido where pedido_id=" . $pedido_id . " )");

        return Datatables::of($facultadesSinuSunedu)
                        ->addColumn('action', function ($data)  {
                            return '<a class="btn btn-xs btn-primary" href="facultadsinu/edit/' . $data->id . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                        })
                        ->make(true);
    }

    public function datatableFacultadesSinu(Request $request) {
        $pedido_id = $request->pedido_id;
        $programasSession = Session::get('programasString');

        if (empty($programasSession)) {
            $programasSession = "'NO EXISTE'";
        }


       // $facultadesSinu = DB::connection('pgsql_syscarnes')->select("select * from facultades_sinu where  codfacultad in (select distinct codfacultad from programas_sinu  where codprograma in  (" . $programasSession . ") ) ");
         $facultadesSinu = DB::connection('pgsql_syscarnes')->select("select * from facultades_sinu where  codfacultad in ( select distinct  codfacultad_sinu from estudiantes_pedido where pedido_id=" . $pedido_id . " and codprograma_sinu in  (" . $programasSession . ") )");

         

        return Datatables::of($facultadesSinu)
                        ->addColumn('action', function ($data) use ($pedido_id) {

                            $pedido = Pedido::where([
                                        ['id', '=', $pedido_id],
                                        ['eliminado', '=', null],
                                    ])->first();

                            if ($pedido->estado == '2') {
                                return '<span>Pedido Cerrado</span>';
                            }

                            $pedidocerrado = PedidoCerrado::where([
                                        ['pedido_id', '=', $pedido_id],
                                        ['user_update', '=', Auth::User()->username],
                                        ['estado', '=', '1'],
                                    ])->first();

                            if (count($pedidocerrado) > 0) {
                                return '<span>Pedido Cerrado</span>';
                            } else {
                                return '<a class="btn btn-xs btn-primary" href="facultadsinu/edit/' . $data->id . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            }
                        })
                        ->make(true);
    }

    public function editFacultadSinu($id) {
        $facultadSinu = FacultadSinu::findOrFail($id);
        $facultadSunedu = FacultadSunedu::where([])->get(array('codfacultad as cod', 'nomfacultad as name'));
        $codfacultad_sunedu = $facultadSinu->codfacultad_sunedu;

        $formularios = new FormulariosController();
        $selectFacultadSunedu = $formularios->selectSimple($facultadSunedu, 'Facultad SUNEDU', 'select_facultadsunedu', $codfacultad_sunedu);
        return view('backEnd.pedidos.edit_facultadsinu', compact('facultadSinu', 'selectFacultadSunedu'));
    }

    public function updateFacultadSinu(Request $request) {


        $facultad_id = $request->id;
        $codfacultad_sunedu = $request->select_facultadsunedu;
        $facultadSinu = FacultadSinu::findOrFail($facultad_id);

        if (!$facultadSinu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra una Facultad con ese código.'])], 404);
        }

        $facultadSunedu = FacultadSunedu::find($codfacultad_sunedu);


        $facultadSunedu = FacultadSunedu::where([
                    ['codfacultad', '=', $codfacultad_sunedu]
                ])->first();


        if (count($facultadSunedu) < 1) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'No existen facultades para relacionar'])], 422);
        }


        $codFacultadSunedu = $codfacultad_sunedu;
        $nomFacultadSunedu = $facultadSunedu->nomfacultad;

        if (!$codFacultadSunedu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }


        $facultadSinu->codfacultad_sunedu = $codFacultadSunedu;
        $facultadSinu->nomfacultad_sunedu = $nomFacultadSunedu;

        $facultadSinu->save();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => $facultadSinu], 200);
    }

    public function datatableFacultadesSunedu() {
        $facultadesSunedu = FacultadSunedu::all();
        return Datatables::of($facultadesSunedu)->make(true);
    }

}
