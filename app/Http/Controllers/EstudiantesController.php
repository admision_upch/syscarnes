<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Estudiante_Pedido;
use App\ProgramaSinu;
use App\FacultadSinu;
use App\FacultadSunedu;
use App\ProgramaSunedu;
use App\EstudianteSunedu;
use App\PedidoFotocheck;
use App\EstudianteFotocheck;
use App\Http\Controllers\componentes\FormulariosController;
use App\PedidoCerrado;
use App\Pedido;
use App\Http\Controllers\Auth\PermisosController;
use Illuminate\Support\Facades\Storage;
use Carbon;
use Imagick;
use ImagickPixel;

class EstudiantesController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /* INICIO: DATATABLE para listar lso estudiantes en la vista detallepdido.blade.php */

    public function datatableEstudiantes(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);
        /* INICIO: Obtener Parametros */
        $pedido_id = $request->pedido_id;
        $codprograma = $request->codprograma;
        /* FIN: Obtener Parametros */

        if ($codprograma == 'all') {
            $programas = Session::get('programasArray');
        } else {
            $programas = array($codprograma);
        }
        if (empty($programas)) {
            $programas = array('NO EXISTE');
        }

        if (Session::get('perfil') == 14) {
            $EstudiantesPedido = DB::connection('pgsql_syscarnes')->table('vst_estudiantes_pedido')
                    ->select('vst_estudiantes_pedido.id', 'vst_estudiantes_pedido.pedido_id as pedido_id', 'vst_estudiantes_pedido.codalumno as codalumno', 'vst_estudiantes_pedido.tipodocumento', 'vst_estudiantes_pedido.nrodocumento', 'vst_estudiantes_pedido.apepaterno', 'vst_estudiantes_pedido.apematerno', 'vst_estudiantes_pedido.nombre', 'vst_estudiantes_pedido.nomfacultad_sinu', 'vst_estudiantes_pedido.nomfacultad_sunedu', 'vst_estudiantes_pedido.nomprograma_sinu', 'vst_estudiantes_pedido.nomprograma_sunedu', 'vst_estudiantes_pedido.ano_ing', 'vst_estudiantes_pedido.per_ing', 'vst_estudiantes_pedido.estado', 'vst_estudiantes_pedido.codmodalidad', 'vst_estudiantes_pedido.foto', 'vst_estudiantes_pedido.codprograma_sinu', 'vst_estudiantes_pedido.user_update', 'vst_estudiantes_pedido.email', 'vst_estudiantes_pedido.codprograma_sunedu', 'vst_estudiantes_pedido.codfacultad_sinu')
                    ->where('vst_estudiantes_pedido.pedido_id', $pedido_id)
                    ->whereIn('vst_estudiantes_pedido.codprograma_sinu', $programas)
                    ->whereIn('nivel', array(1, 2))
                    ->get();
        } else {
            $EstudiantesPedido = DB::connection('pgsql_syscarnes')->table('vst_estudiantes_pedido')
                    ->select('vst_estudiantes_pedido.id', 'vst_estudiantes_pedido.pedido_id as pedido_id', 'vst_estudiantes_pedido.codalumno as codalumno', 'vst_estudiantes_pedido.tipodocumento', 'vst_estudiantes_pedido.nrodocumento', 'vst_estudiantes_pedido.apepaterno', 'vst_estudiantes_pedido.apematerno', 'vst_estudiantes_pedido.nombre', 'vst_estudiantes_pedido.nomfacultad_sinu', 'vst_estudiantes_pedido.nomfacultad_sunedu', 'vst_estudiantes_pedido.nomprograma_sinu', 'vst_estudiantes_pedido.nomprograma_sunedu', 'vst_estudiantes_pedido.ano_ing', 'vst_estudiantes_pedido.per_ing', 'vst_estudiantes_pedido.estado', 'vst_estudiantes_pedido.codmodalidad', 'vst_estudiantes_pedido.foto', 'vst_estudiantes_pedido.codprograma_sinu', 'vst_estudiantes_pedido.user_update', 'vst_estudiantes_pedido.email', 'vst_estudiantes_pedido.codprograma_sunedu', 'vst_estudiantes_pedido.codfacultad_sinu')
                    ->where('vst_estudiantes_pedido.pedido_id', $pedido_id)
                    ->whereIn('vst_estudiantes_pedido.codprograma_sinu', $programas)
                    ->get();
        }


        /* INICIO: Verificar si el PEDIDO esta cerrado */
        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();

        $pedidocerrado = PedidoCerrado::where([
                    ['pedido_id', '=', $pedido_id],
                    ['user_update', '=', Auth::User()->username],
                    ['estado', '=', '1'],
                ])->first();

        $carpeta = storage_path('app/public') . '/pedido_' . $pedido_id . '/gestion/fotospedido';
        return Datatables::of($EstudiantesPedido, $request)
                        ->editColumn('nrodocumento', function ($data) use ($pedido, $pedidocerrado, $carpeta, $permisos ) {
                            $pedido_id = $pedido->id;
                            $pedido = Pedido::where([
                                        ['id', '=', $pedido_id],
                                        ['eliminado', '=', null],
                                    ])->first();


                            if ($pedido->estado == '2') {
                                return $data->nrodocumento;
                            }

                            $pedidocerrado = PedidoCerrado::where([
                                        ['pedido_id', '=', $pedido_id],
                                        ['user_update', '=', Auth::User()->username],
                                        ['estado', '=', '1'],
                                    ])->first();

                            if (count($pedidocerrado) > 0) {
                                return $data->nrodocumento;
                            } else {
                                if (!$data->codprograma_sunedu) {
                                    return $data->nrodocumento . ' <br><br><a class="btn btn-xs btn-success" href="estudiantes/programasunedu/edit/' . $data->id . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                                } else {
                                    return $data->nrodocumento . ' <br><br><a class="btn btn-xs btn-primary" href="estudiantes/programasunedu/edit/' . $data->id . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                                }
                            }
                        })
                        ->editColumn('nomprograma_sinu', function ($data) {
                            return $data->codprograma_sinu . '<br>' . $data->nomprograma_sinu;
                        })
                        ->editColumn('foto', function ($data) use ($pedido, $pedidocerrado, $carpeta, $permisos ) {
                            $pedido_id = $pedido->id;
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');
                            if (file_exists($carpeta)) {
                                $url = Storage::url('pedido_' . $pedido_id . '/gestion/fotospedido/' . $data->nrodocumento . '.jpg?' . rand(1, 1000));
                                return '<img src="' . asset($url) . '" width="120" height="144" >';
                            } else {
                                return '<img src="' . asset($fotoNoEncontrada) . '" width="120" height="144" ><br> <a onclick="agregarEmail(\'' . $data->email . '\')" style="font-size:11px;">Seleccionar email</a>';
                            }
                        })
                        ->editColumn('estado', function ($data) use ($pedido, $pedidocerrado, $carpeta, $permisos) {
                            $pedido_id = $pedido->id;
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';

                            if ($permisos->permission_update == true) {

                                if ($pedido->estado == '2') {


                                    if ($data->estado == '1') {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestado' . $data->id . '">SI</span><br><span id="validadopor' . $data->id . '" style="font-size:10px;">Validado por usuario:' . $data->user_update . '</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    } else {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestado' . $data->id . '">NO</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    }
                                }

                                if (count($pedidocerrado) > 0) {

                                    if ($data->estado == '1') {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestado' . $data->id . '">SI</span><br><span id="validadopor' . $data->id . '" style="font-size:10px;">Validado por usuario:' . $data->user_update . '</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    } else {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestado' . $data->id . '">NO</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    }
                                } else {

                                    if ($data->estado == '1') {

                                        if (Auth::User()->username == $data->user_update) {
                                            $valor = '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstado(' . $data->id . ')" checked />';
                                        } else {
                                            $valor = '';
                                        }

                                        if (file_exists($carpeta)) {
                                            return $valor . '<span class="bold" id="nameestado' . $data->id . '">SI</span><br><span id="validadopor' . $data->id . '" style="font-size:10px;">Validado por usuario:' . $data->user_update . '</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    } else {
                                        if (file_exists($carpeta)) {

                                            if ($data->codprograma_sunedu != '') {
                                                return '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstado(' . $data->id . ')" /> <span class="bold" id="nameestado' . $data->id . '">NO</span>';
                                            } else {
                                                return 'Realizar match SINU-SUNEDU';
                                            }
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    }
                                }
                            } else {

                                if ($data->estado == '1') {
                                    if (file_exists($carpeta)) {
                                        return '<span class="bold" id="nameestado' . $data->id . '">SI</span><br><span id="validadopor' . $data->id . '" style="font-size:10px;">Validado por usuario:' . $data->user_update . '</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                } else {
                                    if (file_exists($carpeta)) {
                                        return '<span class="bold" id="nameestado' . $data->id . '">NO</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                }
                            }
                        })
                        ->rawColumns(['foto', 'estado', 'nrodocumento', 'nomprograma_sinu'])
                        ->make(true);

        /* FIN: Verificar si el PEDIDO esta cerrado */
    }

    /* FIN: DATATABLE para listar lso estudiantes en la vista detallepdido.blade.php */

    /* INICIO: DATATABLE para listar lso estudiantes en la vista detallepdido.blade.php */

    public function datatableEstudiantesSinu(Request $request) {

        /* INICIO: Obtener Parametros */
        $pedido_id = $request->pedido_id;
        /* FIN: Obtener Parametros */


        $EstudiantesSinu = DB::connection('pgsql_syscarnes')->table('estudiantes_sinu')
                ->select('estudiantes_sinu.id', 'estudiantes_sinu.codalumno', 'estudiantes_sinu.tipodocumento', 'estudiantes_sinu.nrodocumento', 'estudiantes_sinu.apepaterno', 'estudiantes_sinu.apematerno', 'estudiantes_sinu.nombre', 'estudiantes_sinu.codfacultad', 'estudiantes_sinu.nomfacultad', 'estudiantes_sinu.abreviaturafacultad', 'estudiantes_sinu.codprograma', 'estudiantes_sinu.nomprograma', 'estudiantes_sinu.id_alum_programa', 'estudiantes_sinu.ano_ing', 'estudiantes_sinu.per_ing', 'estudiantes_sinu.estado', 'estudiantes_sinu.codmodalidad', 'estudiantes_sinu.modalidad_desc', 'estudiantes_sinu.niv_formacion', 'estudiantes_sinu.formacion', 'estudiantes_sinu.ano_ing_completo', 'estudiantes_sinu.nivel', 'estudiantes_sinu.email', 'estudiantes_sinu.genero', 'estudiantes_sinu.fec_nac', 'estudiantes_sinu.departamento', 'estudiantes_sinu.distrito'
                )
                ->where('estudiantes_sinu.pedido_id', $pedido_id)
                ->get();


        return Datatables::of($EstudiantesSinu, $request)->make(true);

        /* FIN: Verificar si el USUARIO ha CERRADO el PEDIDO */
    }

    /* FIN: DATATABLE para listar lso estudiantes en la vista detallepdido.blade.php */


    /* INICIO: DATATABLE para listar lso estudiantes sunedu en la vista detallepdido.blade.php */

    public function datatableEstudiantesSunedu(Request $request) {
        $solicitud_id = $request->solicitud_id;
        $estudianteSunedu = EstudianteSunedu::where([
                    ['solicitud_id', '=', $solicitud_id],
        ]);
        return Datatables::of($estudianteSunedu, $request)->make(true);
    }

    /* FIN: DATATABLE para listar lso estudiantes sunedu en la vista detallepdido.blade.php */

    public function datatableEstudiantesPedido(Request $request) {
        $pedido_id = $request->pedido_id;
        $EstudiantesPedido = Estudiante_Pedido::where([
                    ['pedido_id', '=', $pedido_id]
        ]);

        $carpeta = storage_path('app/public') . '/pedido_' . $pedido_id . '/gestion/fotospedido';
        return Datatables::of($EstudiantesPedido, $request)
                        ->editColumn('foto', function ($data) use ($pedido_id, $carpeta) {
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');
                            if (file_exists($carpeta)) {
                                $url = Storage::url('pedido_' . $pedido_id . '/gestion/fotospedido/' . $data->nrodocumento . '.jpg');
                                return '<img src="' . asset($url) . '" width="120" height="144" >';
                            } else {
                                return '<img src="' . asset($fotoNoEncontrada) . '" width="120" height="144" >';
                            }
                        })
                        ->rawColumns(['foto'])
                        ->make(true);
    }

    /* Funcion que permite cambiar validar el pedido de carné de un estudiante haciendo check (SI Y NO) */

    public function cambiarEstado(Request $request) {

        /* INICIO: Obtener Parametros */
        $id = $request->id;
        $estado = $request->estado;
        /* FIN: Obtener Parametros */

        /* INICIO: Verificar si usuario ha cerrado el Pedido */
        $pedidocerrado = PedidoCerrado::where([
                    ['pedido_id', '=', $request->pedido_id],
                    ['user_update', '=', Auth::User()->username],
                    ['estado', '=', '1'],
                ])->first();

        if (count($pedidocerrado) > 0) {
            return response()->json(['status' => 'danger', 'message' => 'No puede cambiar el estado, el pedido ya se encuentra cerrado', 'data' => 'Error', 'nameestado' => 'Error: Pedido ya cerrado'], 200);
        }
        /* FIN: Verificar si usuario ha cerrado el Pedido */


        $Estudiante_Pedido = Estudiante_Pedido::findOrFail($id);



        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');
        /* INICIO: Verificar si el estado enviado al hacer check es SI */
        if ($estado == '1') {
            /* INICIO: Verificar si al hacer check ya otra persona ha validado al estudiante */
            if (Auth::User()->username != $Estudiante_Pedido->user_update && $Estudiante_Pedido->estado == '1') {
                return response()->json(['status' => 'success', 'message' => 'Registrado Validado por otro usuario', 'data' => 'Todo bien', 'nameestado' => 'No puede validar, registro validado por otro usuario'], 200);
            }
            /* FIN: Verificar si al hacer check ya otra persona ha validado al estudiante */

            $Estudiante_Pedido->estado = '1';
            $NameEstado = 'SI';

            $Estudiante_Pedido_Duplicado = Estudiante_Pedido::where([
                        ['pedido_id', '=', $Estudiante_Pedido->pedido_id],
                        ['nrodocumento', '=', $Estudiante_Pedido->nrodocumento],
                        ['estado', '=', '1'],
                    ])->first();

            if (count($Estudiante_Pedido_Duplicado) > 0) {
                $Estudiante_Pedido->estado = null;
                return response()->json(['status' => 'success', 'message' => 'El carné del estudiante ya fue solicitado con otro programa.', 'nameestado' => 'El carné del estudiante ya fue solicitado con otro programa.'], 200);
            }

            $Estudiante_Pedido->user_update = Auth::User()->username;
            $Estudiante_Pedido->fecha_solicitado = $fechaActual;
        } else {

            $Estudiante_Pedido->estado = null;
            $NameEstado = 'NO';
            $Estudiante_Pedido->user_update = null;
            $Estudiante_Pedido->fecha_solicitado = null;
        }
        /* FIN: Verificar si el estado enviado al hacer check es SI */





        $Estudiante_Pedido->save();

        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => 'Datos guardados exitosamente', 'nameestado' => $NameEstado], 200);
    }

    public function editProgramaSuneduEstudiantes($estudiante_pedido_id) {
        $Estudiante_Pedido = Estudiante_Pedido::findOrFail($estudiante_pedido_id);

        $facultadSunedu = FacultadSunedu::where([
                    ['codfacultad', '=', $Estudiante_Pedido->codfacultad_sunedu]
                ])->first();
        if (!$facultadSunedu) {
            $mensaje = "Por favor primero relacionar Facultad SINU con Facultad SUNEDU";
            $tipoalerta = "alert-warning";
            return view('layouts.mensaje', compact('mensaje', 'tipoalerta'));
        }

        $programaSunedu = DB::connection('pgsql_syscarnes')->select("select codprograma as cod, nomprograma as name from programas_sunedu where codfacultad in ('" . $Estudiante_Pedido->codfacultad_sunedu . "') ");

        $formularios = new FormulariosController();
        $selectProgramaSunedu = $formularios->selectSimple($programaSunedu, 'Programa SUNEDU', 'select_programasunedu', $Estudiante_Pedido->codprograma_sunedu);

        return view('backEnd.pedidos.edit_programasinu_estudiantes', compact('Estudiante_Pedido', 'selectProgramaSunedu', 'facultadSunedu'));
    }

    public function updateProgramaSuneduEstudiantes(Request $request) {

        $estudiante_pedido_id = $request->estudiante_pedido_id;
        $Estudiante_Pedido = Estudiante_Pedido::findOrFail($estudiante_pedido_id);

        /* if (!$programaSinu) {
          return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra una Programa con ese código.'])], 404);
          } */

        $programaSunedu = ProgramaSunedu::where([
                    ['codprograma', '=', $request->select_programasunedu],
                ])->first();


        if (!$programaSunedu->codprograma) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }

        $Estudiante_Pedido->codprograma_sunedu = $programaSunedu->codprograma;


        $Estudiante_Pedido->save();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => ''], 200);
    }

    public function datatableEstudiantesEntregaCarne(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);
        /* INICIO: Obtener Parametros */
        $pedido_id = $request->pedido_id;
        /* FIN: Obtener Parametros */

        /* INICIO: Verificar si el PEDIDO esta cerrado */
        $pedido = Pedido::where([
                    ['id', '=', $pedido_id],
                    ['eliminado', '=', null],
                ])->first();



        if ($pedido->estado == '1' || $pedido->estado == '0') {
            $EstudiantesPedido = DB::connection('pgsql_syscarnes')->table('vst_estudiantes_pedido')
                    ->select('vst_estudiantes_pedido.id', 'vst_estudiantes_pedido.pedido_id as pedido_id', 'vst_estudiantes_pedido.codalumno as codalumno', 'vst_estudiantes_pedido.tipodocumento', 'vst_estudiantes_pedido.nrodocumento', 'vst_estudiantes_pedido.apepaterno', 'vst_estudiantes_pedido.apematerno', 'vst_estudiantes_pedido.nombre', 'vst_estudiantes_pedido.nomfacultad_sinu', 'vst_estudiantes_pedido.nomfacultad_sunedu', 'vst_estudiantes_pedido.nomprograma_sinu', 'vst_estudiantes_pedido.nomprograma_sunedu', 'vst_estudiantes_pedido.ano_ing', 'vst_estudiantes_pedido.per_ing', 'vst_estudiantes_pedido.estado', 'vst_estudiantes_pedido.codmodalidad', 'vst_estudiantes_pedido.foto', 'vst_estudiantes_pedido.codprograma_sinu', 'vst_estudiantes_pedido.user_update', 'vst_estudiantes_pedido.email', 'vst_estudiantes_pedido.codprograma_sunedu', 'vst_estudiantes_pedido.codfacultad_sinu', 'vst_estudiantes_pedido.estado_entregado', 'vst_estudiantes_pedido.user_update_entregado')
                    ->where('vst_estudiantes_pedido.pedido_id', 0)
                    ->get();
        } else {
            $programas = Session::get('programasArray');
            if (Session::get('perfil') == 14) {
                $EstudiantesPedido = DB::connection('pgsql_syscarnes')->table('vst_estudiantes_pedido')
                        ->select('vst_estudiantes_pedido.id', 'vst_estudiantes_pedido.pedido_id as pedido_id', 'vst_estudiantes_pedido.codalumno as codalumno', 'vst_estudiantes_pedido.tipodocumento', 'vst_estudiantes_pedido.nrodocumento', 'vst_estudiantes_pedido.apepaterno', 'vst_estudiantes_pedido.apematerno', 'vst_estudiantes_pedido.nombre', 'vst_estudiantes_pedido.nomfacultad_sinu', 'vst_estudiantes_pedido.nomfacultad_sunedu', 'vst_estudiantes_pedido.nomprograma_sinu', 'vst_estudiantes_pedido.nomprograma_sunedu', 'vst_estudiantes_pedido.ano_ing', 'vst_estudiantes_pedido.per_ing', 'vst_estudiantes_pedido.estado', 'vst_estudiantes_pedido.codmodalidad', 'vst_estudiantes_pedido.foto', 'vst_estudiantes_pedido.codprograma_sinu', 'vst_estudiantes_pedido.user_update', 'vst_estudiantes_pedido.email', 'vst_estudiantes_pedido.codprograma_sunedu', 'vst_estudiantes_pedido.codfacultad_sinu', 'vst_estudiantes_pedido.estado_entregado', 'vst_estudiantes_pedido.user_update_entregado')
                        ->where('vst_estudiantes_pedido.pedido_id', $pedido_id)
                        ->where('vst_estudiantes_pedido.estado', '1')
                        ->whereIn('vst_estudiantes_pedido.codprograma_sinu', $programas)
                        ->whereIn('nivel', array(1, 2))
                        ->get();
            } else {
                $EstudiantesPedido = DB::connection('pgsql_syscarnes')->table('vst_estudiantes_pedido')
                        ->select('vst_estudiantes_pedido.id', 'vst_estudiantes_pedido.pedido_id as pedido_id', 'vst_estudiantes_pedido.codalumno as codalumno', 'vst_estudiantes_pedido.tipodocumento', 'vst_estudiantes_pedido.nrodocumento', 'vst_estudiantes_pedido.apepaterno', 'vst_estudiantes_pedido.apematerno', 'vst_estudiantes_pedido.nombre', 'vst_estudiantes_pedido.nomfacultad_sinu', 'vst_estudiantes_pedido.nomfacultad_sunedu', 'vst_estudiantes_pedido.nomprograma_sinu', 'vst_estudiantes_pedido.nomprograma_sunedu', 'vst_estudiantes_pedido.ano_ing', 'vst_estudiantes_pedido.per_ing', 'vst_estudiantes_pedido.estado', 'vst_estudiantes_pedido.codmodalidad', 'vst_estudiantes_pedido.foto', 'vst_estudiantes_pedido.codprograma_sinu', 'vst_estudiantes_pedido.user_update', 'vst_estudiantes_pedido.email', 'vst_estudiantes_pedido.codprograma_sunedu', 'vst_estudiantes_pedido.codfacultad_sinu', 'vst_estudiantes_pedido.estado_entregado', 'vst_estudiantes_pedido.user_update_entregado')
                        ->where('vst_estudiantes_pedido.pedido_id', $pedido_id)
                        ->where('vst_estudiantes_pedido.estado', '1')
                        ->whereIn('vst_estudiantes_pedido.codprograma_sinu', $programas)
                        ->get();
            }
        }





        $carpeta = storage_path('app/public') . '/pedido_' . $pedido_id . '/gestion/fotospedido';
        return Datatables::of($EstudiantesPedido, $request)
                        ->editColumn('nomprograma_sinu', function ($data) {
                            return $data->codprograma_sinu . '<br>' . $data->nomprograma_sinu;
                        })
                        ->editColumn('foto', function ($data) use ($pedido, $carpeta ) {
                            $pedido_id = $pedido->id;
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');
                            if (file_exists($carpeta)) {
                                $url = Storage::url('pedido_' . $pedido_id . '/gestion/fotospedido/' . $data->nrodocumento . '.jpg');
                                return '<img src="' . asset($url) . '" width="120" height="144" >';
                            } else {
                                return '<img src="' . asset($fotoNoEncontrada) . '" width="120" height="144" ><br> <a onclick="agregarEmail(\'' . $data->email . '\')" style="font-size:11px;">Seleccionar email</a>';
                            }
                        })
                        ->editColumn('estado_entregado', function ($data) use ($pedido, $carpeta, $permisos) {
                            $pedido_id = $pedido->id;
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';

                            if ($permisos->permission_update == true) {

                                if ($pedido->estado == '1' || $pedido->estado == '0') {
                                    if ($data->estado_entregado == '1') {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestadoentregado' . $data->id . '">SI</span><br><span id="validadoporentregado' . $data->id . '" style="font-size:10px;">Entregado por usuario:' . $data->user_update_entregado . '</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    } else {
                                        if (file_exists($carpeta)) {
                                            return '<span class="bold" id="nameestadoentregado' . $data->id . '">NO</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    }
                                } else {
                                    if ($data->estado_entregado == '1') {

                                        if (Auth::User()->username == $data->user_update_entregado) {
                                            $valor = '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstadoEntregado(' . $data->id . ')" checked />';
                                        } else {
                                            $valor = '';
                                        }

                                        if (file_exists($carpeta)) {
                                            return $valor . '<span class="bold" id="nameestadoentregado' . $data->id . '">SI</span><br><span id="validadoporentregado' . $data->id . '" style="font-size:10px;">Entregado por usuario:' . $data->user_update_entregado . '</span>';
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    } else {
                                        if (file_exists($carpeta)) {

                                            if ($data->codprograma_sunedu != '') {
                                                return '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstadoEntregado(' . $data->id . ')" /> <span class="bold" id="nameestadoentregado' . $data->id . '">NO</span>';
                                            } else {
                                                return 'Realizar match SINU-SUNEDU';
                                            }
                                        } else {
                                            return 'No tiene foto';
                                        }
                                    }
                                }
                            } else {

                                if ($data->estado_entregado == '1') {
                                    if (file_exists($carpeta)) {
                                        return '<span class="bold" id="nameestadoentregado' . $data->id . '">SI</span><br><span id="validadoporentregado' . $data->id . '" style="font-size:10px;">Entregado por usuario:' . $data->user_update_entregado . '</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                } else {
                                    if (file_exists($carpeta)) {
                                        return '<span class="bold" id="nameestadoentregado' . $data->id . '">NO</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                }
                            }
                        })
                        ->rawColumns(['foto', 'estado_entregado', 'nrodocumento', 'nomprograma_sinu'])
                        ->make(true);

        /* FIN: Verificar si el PEDIDO esta cerrado */
    }

    public function cambiarEstadoEntregado(Request $request) {

        /* INICIO: Obtener Parametros */
        $id = $request->id;
        $estado = $request->estado;
        /* FIN: Obtener Parametros */
        $Estudiante_Pedido = Estudiante_Pedido::findOrFail($id);
        /* INICIO: Verificar si el estado enviado al hacer check es SI */
        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');

        if ($estado == '1') {
            /* INICIO: Verificar si al hacer check ya otra persona ha validado al estudiante */
            if (Auth::User()->username != $Estudiante_Pedido->user_update_entregado && $Estudiante_Pedido->estado_entregado == '1') {
                return response()->json(['status' => 'success', 'message' => 'Registrado Validado por otro usuario', 'data' => 'Todo bien', 'nameestado' => 'No puede validar, registro validado por otro usuario'], 200);
            }
            /* FIN: Verificar si al hacer check ya otra persona ha validado al estudiante */

            $Estudiante_Pedido->estado_entregado = '1';
            $NameEstado = 'SI';
            $Estudiante_Pedido->user_update_entregado = Auth::User()->username;
            $Estudiante_Pedido->fecha_entregado = $fechaActual;
        } else {

            $Estudiante_Pedido->estado_entregado = null;
            $NameEstado = 'NO';
            $Estudiante_Pedido->user_update_entregado = null;
            $Estudiante_Pedido->fecha_entregado = null;
        }
        /* FIN: Verificar si el estado enviado al hacer check es SI */

        $Estudiante_Pedido->save();

        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => 'Datos guardados exitosamente', 'nameestado' => $NameEstado], 200);
    }

    /*     * **************************INICIO FOTOCHECK*************************************************************** */

    public function datatableEstudiantesFotocheck(Request $request) {

        $permisos = $this->seguridad->validarPermisos($request);
        /* INICIO: Obtener Parametros */
        $codprograma = $request->codprograma;

        $pedido_id = $request->pedido_id;
        /* FIN: Obtener Parametros */
        $now = new \DateTime();
        $anio = $now->format('Y');

        $pedido = PedidoFotocheck::findOrFail($pedido_id);
        if ($codprograma == 'all') {
            $programas = Session::get('programasArray');
        } else {
            $programas = array($codprograma);
        }
        if (empty($programas)) {
            $programas = array('NO EXISTE');
        }

        array_push($programas, "DURIN", "TROPI", 'VRACAD');

        if ($pedido->duplicado == 1) {
            if ($pedido->estado == '0') {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.cod_periodo_matricula', 'estudiantes_fotocheck.nivel', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                        ->whereIn(DB::raw('upper(estudiantes_fotocheck.codprograma) '), $programas)
                        ->whereIn(DB::raw('substring(trim(estudiantes_fotocheck.cod_periodo_matricula),0,5)'), array($anio,null))
                        ->where('estudiantes_fotocheck.cod_estado_sinu', 1)
                        ->where('estudiantes_fotocheck.nivel', '<>', null)
                        ->where('estudiantes_fotocheck.estado', 4)
                        ->orWhere('estudiantes_fotocheck.duplicado', 1)
                        ->where('estudiantes_fotocheck.user_update', Auth::User()->username)
                        ->get();
            } else {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.cod_periodo_matricula', 'estudiantes_fotocheck.nivel', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                        ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                        ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                        ->join('generica', 'pedidos_fotocheck_detalle.estado', '=', 'generica.cod')
                        //->whereIn(DB::raw('substring(trim(estudiantes_fotocheck.cod_periodo_matricula),0,5)'), array($anio,null))
                        ->where('generica.tipo', 'TIPESTFOTOCHECK')
                        ->where('pedidos_fotocheck.id', $pedido->id)
                        ->get();
            }
        } else {
            if ($pedido->estado == '0') {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.cod_periodo_matricula', 'estudiantes_fotocheck.nivel', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                        ->where(DB::raw('substring(trim(estudiantes_fotocheck.cod_periodo_matricula),0,5)'), $anio)
                        ->where('estudiantes_fotocheck.cod_estado_sinu', 1)
                        ->where('estudiantes_fotocheck.nivel', '<>', null)
                        ->whereIn(DB::raw('upper(estudiantes_fotocheck.codprograma) '), $programas)
                        ->where('estudiantes_fotocheck.estado', null)
                        ->orWhere('estudiantes_fotocheck.estado', 0)
                        ->where('estudiantes_fotocheck.user_update', Auth::User()->username)
                        ->where('estudiantes_fotocheck.duplicado', null)
                        ->get();
            } else {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.cod_periodo_matricula', 'estudiantes_fotocheck.nivel', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                        ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                        ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                        ->join('generica', 'pedidos_fotocheck_detalle.estado', '=', 'generica.cod')
                        ->where('generica.tipo', 'TIPESTFOTOCHECK')
                        ->where('pedidos_fotocheck.id', $pedido->id)
                        ->get();
            }
        }


        $carpeta = storage_path('app/public') . '/fotochecks/fotos';
        return Datatables::of($EstudiantesFotocheck, $request)
                        ->editColumn('nrodocumento', function ($data) use ( $carpeta, $permisos ) {
                            return $data->nrodocumento;
                        })
                        ->editColumn('fec_vencimiento', function ($data) use ($pedido, $carpeta, $permisos ) {
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            if ($pedido->estado == '0') {
                                if (file_exists($carpeta)) {
                                    return 'Ult.Mat: ' . $data->cod_periodo_matricula . '<br>' . 'Nivel: ' . $data->nivel . '<br><br>' . $data->fec_vencimiento . ' <br><br><a class="btn btn-xs btn-primary" href="' . Route('fotocheckgestionarpedido.estudiantefotocheck.editar', ['id' => $data->id]) . '"  data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                                } else {
                                    return 'No tiene foto';
                                }
                            } else {
                                return 'Ult.Mat: ' . $data->cod_periodo_matricula . '<br>' . 'Nivel: ' . $data->nivel . '<br><br>' . $data->fec_vencimiento;
                            }
                        })
                        ->editColumn('nomprograma', function ($data) {
                            return $data->abreviaturafacultad . '<br>' . $data->codprograma . '<br>' . $data->nomprograma;
                        })
                        ->editColumn('estado', function ($data) use ( $pedido, $carpeta, $permisos ) {
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            if ($pedido->estado == '0') {
                                if ($data->estado == '0') {
                                    $valor = '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstado(' . $data->id . ')" checked />';
                                    if (file_exists($carpeta)) {
                                        return $valor . '<span class="bold" id="nameestado' . $data->id . '">SI</span><br><span id="validadopor' . $data->id . '" style="font-size:10px;">Validado por usuario:' . $data->user_update . '</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                } else {
                                    if (file_exists($carpeta)) {
                                        return '<input type="checkbox" id="' . $data->id . '" name="' . $data->id . '" onclick="cambiarEstado(' . $data->id . ')" /> <span class="bold" id="nameestado' . $data->id . '">NO</span>';
                                    } else {
                                        return 'No tiene foto';
                                    }
                                }
                            } else {
                                return $data->estado_nombre;
                            }
                        })
                        ->editColumn('foto', function ($data) use ($carpeta, $permisos ) {

                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');
                            if (file_exists($carpeta)) {
                                $url = Storage::url('fotochecks/fotos/' . $data->nrodocumento . '.jpg');
                                return '<img src="' . asset($url) . '" width="120" height="144" >';
                            } else {
                                return '<img src="' . asset($fotoNoEncontrada) . '" width="120" height="144" ><br> <a onclick="agregarEmail(\'' . $data->email . '\')" style="font-size:11px;">Seleccionar email</a>';
                            }
                        })
                        ->rawColumns(['foto', 'estado', 'nrodocumento', 'nomprograma', 'fec_vencimiento'])
                        ->make(true);

        /* FIN: Verificar si el PEDIDO esta cerrado */
    }

    public function editarEstudianteFotocheck($id) {


        $EstudianteFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select(DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id as id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                ->where('estudiantes_fotocheck.id', $id)
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->where('user_update', Auth::User()->username)
                ->first();

        return view('backEnd.fotocheckgestionar.edit_fec_vencimiento', compact('EstudianteFotocheck'));
    }

    public function updateEstudianteFotocheck(Request $request) {
        if (!is_null($request->chk_all_programas)) {
            DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->where('estado', '0')
                    ->where('user_update', Auth::User()->username)
                    ->where('codprograma', $request->estudiante_fotocheck_codprograma)
                    ->update(['fec_vencimiento' => $request->estudiante_fotocheck_fec_vencimiento]);
        } else {
            $estudiante_fotocheck_id = $request->estudiante_fotocheck_id;

            $EstudianteFotocheck = EstudianteFotocheck::findOrFail($estudiante_fotocheck_id);
            $EstudianteFotocheck->fec_vencimiento = $request->estudiante_fotocheck_fec_vencimiento;
            $EstudianteFotocheck->save();
        }
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => ''], 200);
    }

    public function datatableSincronizarEstudiantesFotocheck(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);

        $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select(DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad', 'estudiantes_fotocheck.estado_sinu')
                ->get();

        $carpeta = storage_path('app/public') . '/fotochecks/fotos';
        return Datatables::of($EstudiantesFotocheck, $request)
                        ->editColumn('nrodocumento', function ($data) use ( $carpeta, $permisos ) {
                            return $data->nrodocumento;
                        })
                        ->editColumn('nomprograma', function ($data) {
                            return $data->abreviaturafacultad . '<br>' . $data->codprograma . '<br>' . $data->nomprograma;
                        })
                        ->editColumn('estado', function ($data) use ( $carpeta, $permisos ) {
                            return '<button type="button" class="btn btn-sm btn-primary" onclick="sincronizarFotosEstudiantes(\'' . $data->nrodocumento . '\')"  ><i class="fa fa-refresh"></i> Sincronizar</button>';
                        })
                        ->editColumn('foto', function ($data) use ($carpeta, $permisos ) {
                            $carpeta = $carpeta . '/' . $data->nrodocumento . '.jpg';
                            $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');
                            if (file_exists($carpeta)) {
                                $url = Storage::url('fotochecks/fotos/' . $data->nrodocumento . '.jpg?' . rand(1, 1000));
                                return '<img src="' . asset($url) . '" width="120" height="144" >';
                            } else {
                                return '<img src="' . asset($fotoNoEncontrada) . '" width="120" height="144" ><br> <a onclick="agregarEmail(\'' . $data->email . '\')" style="font-size:11px;">Seleccionar email</a>';
                            }
                        })
                        ->rawColumns(['foto', 'estado', 'nrodocumento', 'nomprograma', 'fec_vencimiento'])
                        ->make(true);

        /* FIN: Verificar si el PEDIDO esta cerrado */
    }

    /*     * **************************FIN FOTOCHECK*************************************************************** */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buscarPersona($tipodocumento, $nrodocumento) {



        $persona = DB::connection('oracle_prod')->table('bas_tercero')
                ->select(DB::raw("decode(tip_identificacion, 'C','1', 'P','7', 'E','4') as cod_tipodocumento"), DB::raw("decode(tip_identificacion, 'C','DNI', 'P','PASAPORTE', 'E','CARNET DE EXTRANJERIA') as tipodocumento"), 'bas_tercero.num_identificacion as nrodocumento', 'bas_tercero.dir_email as email', 'bas_tercero.tel_cecular as movil', 'bas_tercero.nom_largo', 'bas_tercero.fec_nacimiento'
                )->where('bas_tercero.num_identificacion', $nrodocumento)
                ->where('bas_tercero.tip_identificacion', $tipodocumento)
                ->first();

        $data = $persona;
        if (empty($persona)) {
            $persona = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->select("tipodocumento as cod_tipodocumento", DB::raw("case when tipodocumento='1' then 'DNI' WHEN tipodocumento='7' then 'PASAPORTE' when tipodocumento='4' THEN 'CARNET DE EXTRANJERIA' ELSE 'no existe' END as tipodocumento"), 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.email', DB::raw("NULL AS movil"), DB::raw("apepaterno || ' ' || apematerno || ' ' || nombre as nom_largo"), 'estudiantes_fotocheck.fec_nac as fec_nacimiento'
                    )->where('estudiantes_fotocheck.nrodocumento', $nrodocumento)
                    ->where('estudiantes_fotocheck.tipodocumento', $tipodocumento)
                    ->first();
        }


        if (is_null($persona)) {
            return view('mensajes.registro_no_encontrado');
        } else {
            $nrodocumento = $persona->nrodocumento;
            $tipo_documento = $persona->cod_tipodocumento;
        }

        $carnes_universitarios = DB::connection('pgsql_syscarnes')->table('pedidos')
                ->select('pedidos.id as pedido_id', 'pedidos.nombre as pedido', 'pedidos.estado', 'estudiantes_pedido.codalumno', 'estudiantes_pedido.apepaterno', 'estudiantes_pedido.apematerno', 'estudiantes_pedido.nombre', 'estudiantes_pedido.codprograma_sinu', 'estudiantes_pedido.nomfacultad_sinu', 'estudiantes_pedido.nomprograma_sinu', 'estudiantes_pedido.modalidad_desc', 'estudiantes_pedido.formacion')
                ->join('estudiantes_pedido', 'pedidos.id', '=', 'estudiantes_pedido.pedido_id')
                ->where('estudiantes_pedido.nrodocumento', $nrodocumento)
                ->where('estudiantes_pedido.tipodocumento', $tipo_documento)
                ->where('estudiantes_pedido.estado', 1)
                ->get();

        $fotochecks = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select('estudiantes_fotocheck.codprograma', 'estudiantes_fotocheck.nomprograma', 'pedidos_fotocheck.id', 'pedidos_fotocheck_detalle.estado as cod_estado', 'generica.nombre as estado', DB::raw("to_char(pedidos_fotocheck_detalle.fec_solicita,'dd/MM/YYYY') as  fec_solicita"), DB::raw("to_char(pedidos_fotocheck_detalle.fec_genera,'dd/MM/YYYY') as  fec_genera"), DB::raw("to_char(pedidos_fotocheck_detalle.fec_entrega_facultad,'dd/MM/YYYY') as  fec_entrega_facultad"), DB::raw("to_char(pedidos_fotocheck_detalle.fec_entrega_estudiante,'dd/MM/YYYY') as  fec_entrega_estudiante"), DB::raw("to_char(pedidos_fotocheck_detalle.fec_devolucion,'dd/MM/YYYY') as  fec_devolucion"), 'pedidos_fotocheck.duplicado', 'pedidos_fotocheck_detalle.devolucion', 'pedidos_fotocheck_detalle.motivo_devolucion'
                )
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('pedidos_fotocheck', 'pedidos_fotocheck.id', '=', 'pedidos_fotocheck_detalle.pedido_fotocheck_id')
                ->join('generica', 'generica.cod', '=', 'pedidos_fotocheck_detalle.estado')
                ->where('estudiantes_fotocheck.nrodocumento', $nrodocumento)
                ->where('estudiantes_fotocheck.tipodocumento', $tipo_documento)
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->orderBy('pedidos_fotocheck.id', 'DESC')
                ->get();

        $fotoNoEncontrada = Storage::url('fotos/fotos_DEFAULT/foto_noencoentrado.jpg');

        if (empty($data)) {

            $carpeta = storage_path('app/public') . '/fotochecks/fotos';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            $fotoCarpeta = $carpeta . "/" . $nrodocumento . ".jpg";

            if (file_exists($fotoCarpeta)) {
                $foto = Storage::url('fotochecks/fotos/' . $nrodocumento . '.jpg');
                $foto = asset($foto);
            } else {
                $foto = asset($fotoNoEncontrada);
            }
        } else {

            $carpeta = storage_path('app/public') . '/personas/fotos';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }

            $fotosCarpeta = glob($carpeta . "/" . $nrodocumento . ".jpg");
            array_map('unlink', $fotosCarpeta);


            $fotoCarpeta = $carpeta . "/" . $nrodocumento . ".jpg";
            $fotoRepositorio = RepositorioFotosPersonas . $nrodocumento . ".jpg";

            if (file_exists($fotoRepositorio)) {
                if (copy($fotoRepositorio, $fotoCarpeta)) {
                    $imagick = new \Imagick(realpath($fotoCarpeta));
                    $imagick->stripImage();
                    $imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                    $imagick->setImageResolution(300, 300);
                    $imagick->cropThumbnailImage(240, 288);
                    $imagick->setImageCompression(imagick::COMPRESSION_JPEG);
                    $imagick->setImageCompressionQuality(20);
                    $imagick->writeImage(realpath($fotoCarpeta));
                }

                if (file_exists($fotoCarpeta)) {
                    $foto = Storage::url('personas/fotos/' . $nrodocumento . '.jpg');
                    $foto = asset($foto);
                } else {
                    $foto = asset($fotoNoEncontrada);
                }
            } else {
                $foto = asset($fotoNoEncontrada);
            }
        }


        return view('backEnd.persona.view', compact('carnes_universitarios', 'fotochecks', 'persona', 'foto'));
    }

}
