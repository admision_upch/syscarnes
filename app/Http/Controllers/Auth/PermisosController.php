<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opcion;
use App\Acceso;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;


class PermisosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function validarPermisos($request) {

        $url = explode('/', $request->path());
        $link = '/' . $url[0];

        $opcion = Opcion::where([
                    ['enlace', '=', $link]
                ])->first();


        $permissions = Acceso::where([
                    ['perfil_id', '=', Session::get('perfil')],
                    ['opcion_id', '=', $opcion->id],
                    ['estado', '=', '1'],
                ])->first();
        return $permissions;
    }


}
