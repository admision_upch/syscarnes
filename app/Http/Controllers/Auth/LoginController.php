<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Adldap\Laravel\Facades\Adldap;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\User_Perfil;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function username() {
        return 'username';
    }

    public function login(Request $request) {


        $username = $request->username;
        $password = $request->password;
        $credentiales = array("username" => $username, "password" => $password);
        try {
            if (Adldap::auth()->attempt($username, $password, true)) {
                //if (Auth::attempt($credentiales)) {
                $user = User::where([
                            ['username', '=', $username],
                            ['estado', '=', '1'],
                            ['eliminado', '=', null]
                        ])->first();
                if (empty($user)) {

                    $this->guard()->logout();
                    $request->session()->flush();
                    $request->session()->regenerate();
                    Session::flash('message', 'El usuario no existe o se encuentra deshabilitado');
                    return redirect('login');
                }

                $url = explode('/', $request->fullUrl());
                $linkSistema = $url[3];


                $login = DB::connection('pgsql_sysseguridad_seguridad')->select("
                select p.id as perfil_id, u.id as user_id,  s.name ||' '||s.version as nomsistema_version,s.name as nomsistema, s.id as sistema_id from seguridad.perfiles p inner join seguridad.sistemas s on s.id=p.sistema_id
                 inner join seguridad.users_perfiles up on p.id=up.perfil_id
                 inner join seguridad.users u on u.id=up.user_id
                 where s.url='$linkSistema' and u.username='$username' and u.eliminado is null and up.eliminado is null and up.estado='1'
                            ");
                if (empty($login)) {
                    $this->guard()->logout();
                    $request->session()->flush();
                    $request->session()->regenerate();
                    Session::flash('message', 'No tiene acceso al sistema');
                    return redirect('login');
                }
                $user_perfil = User_Perfil::where([
                            ['user_id', '=', $login[0]->user_id],
                            ['perfil_id', '=', $login[0]->perfil_id],
                            ['estado', '=', '1'],
                            ['eliminado', '=', null]
                        ])->first();

                //dd($user_perfil);
                Session::put('perfil', $user_perfil->perfil_id);
                Session::put('sistema', $login[0]->sistema_id);
                Session::put('app.name.sistema', $login[0]->nomsistema);
                Session::put('app.name.sisversion', $login[0]->nomsistema_version);


                $programas = DB::connection('pgsql_sysseguridad_seguridad')->select("
                select upper( programa_cod) as programa_cod from seguridad.users_perfiles_programas upp inner join seguridad.users_perfiles up on up.id=upp.userperfil_id
                where upp.userperfil_id=" . $user_perfil->id . " and upp.eliminado is null and up.eliminado is null
                            ");


                $programasString = '';
                $programasArray[] = 0;
                $i = 1;
                foreach ($programas as $value) {
                    if ($i < count($programas)) {
                        $programasString .= "'" . $value->programa_cod . "',";
                    } else {
                        $programasString .= "'" . $value->programa_cod . "'";
                    }
                    array_push($programasArray, $value->programa_cod);
                    $i++;
                }
                Auth::login($user);


                Session::put('programasArray', $programasArray);
                Session::put('programasString', $programasString);

                return redirect()->to('home');
                //}
            } else {
                if ($request->ajax()) {
                    return response()->json(['status' => 'warning', 'message' => 'Sesion cerrada, vuelva a logearse.', 'estado' => '0'], 200);
                }
                Session::flash('message', 'Usuario o Contraseñas son incorrectos');
                return redirect('login');
            }
        } catch (\Exception $e) {
                Session::flash('message', 'Error de conexión');
                return redirect('login');
        }
    }

    public function logout(Request $request) {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('login');
    }

}
