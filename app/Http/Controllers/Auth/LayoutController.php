<?php


namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Acceso;
use Illuminate\Support\Facades\DB;

class LayoutController extends Controller {

    public function menu(Request $request) {

        $opciones = DB::connection('pgsql_sysseguridad_seguridad')->select("    
                select case when o.padre=0 then cast(o.orden as character varying) else (select concat( opciones.orden,o.orden) from seguridad.opciones opciones where opciones.id=o.padre ) end ordeng, o.enlace, o.name as opcion, o.imagen from seguridad.opciones o inner join seguridad.accesos a on o.id=a.opcion_id where 
                a.perfil_id in (" . Session::get('perfil') . ") and visiblemenu=true and (a.estado='1' or (select sum(case when oo.padre=o.id then 1 else 0 end) as submenu from seguridad.opciones oo)>1)
            and (o.eliminado is null or o.eliminado='0')
            and (a.eliminado is null or a.eliminado='0') order by  case when o.padre=0 then cast(o.orden as character varying) else (select concat( opciones.orden,o.orden) from seguridad.opciones opciones where opciones.id=o.padre ) end");

        
        return view('layouts.sidebar', compact('opciones'));
    }



}
