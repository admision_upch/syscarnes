<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User_Perfil;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class Users_PerfilesController extends Controller {

    public function asignarPerfil(Request $request) {

        try {
            $user_perfil = User_Perfil::where([
                        ['user_id', '=', $request->user_id],
                        ['perfil_id', '=', $request->perfil_id],
                        ['eliminado', '=', null]
                    ])->get();
            if (count($user_perfil) > 0) {
                return response()->json(['status' => 'warning', 'message' => 'Error: Usuario ya se encuentra asignado', 'data' => ''], 200);
            }
            $this->validate($request, ['perfil_id' => 'required', 'user_id' => 'required',]);
            User_Perfil::create($request->all());
            return response()->json(['status' => 'success', 'message' => 'Registro ejecutado exitosamente', 'data' => ''], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Creacion no ejecutada', 'id' => $request->perfil_id], 500);
        }
    }

    public function datatablePerfilesAsignados($id) {
        $user = User::findOrFail($id);
        $perfilesAsignados = DB::connection('pgsql_sysseguridad_seguridad')->select("select up.id, s.name as sistema, p.name as perfil from seguridad.users_perfiles up 
                                                                                inner join seguridad.perfiles p on up.perfil_id=p.id
                                                                                inner join seguridad.users u on up.user_id=u.id
                                                                                inner join seguridad.sistemas s on p.sistema_id=s.id where u.id=" . $user->id  ." and (up.eliminado is null or up.eliminado='0')");


        return Datatables::of($perfilesAsignados)
                        ->addColumn('action', function ($userperfil) {
                            return '<a onclick="verProgramasAsignados(' . $userperfil->id . ')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> Programas Asignados</a><a onclick="eliminar(' . $userperfil->id . ')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>';
                        })
                        ->make(true);
    }

    public function asignarperfil_delete(Request $request) {
        try {

            $user_perfil = User_Perfil::findOrFail($request->id);

            if (count($user_perfil) < 1) {
                return response()->json(['status' => 'warning', 'message' => 'Error: Registro No existe', 'data' => ''], 200);
            }
    

            $user_perfil->eliminado = '1';
            $user_perfil->save();
            return response()->json(['status' => 'success', 'message' => 'Eliminación ejecutada exitosamente', 'data' => ''], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Eliminación no ejecutada', 'id' => ''], 500);
        }
    }

}
