<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PedidoFotocheck;
use App\PedidoCerrado;
use App\SolicitudSunedu;
use App\EstudianteSunedu;
use App\EstudianteSinu;
use App\Estudiante_Pedido;
use App\EstudianteFotocheck;
use App\FacultadSunedu;
use App\ProgramaSunedu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use Yajra\Datatables\Datatables;
use Excel;
use Imagick;
use ImagickPixel;
use Barryvdh\DomPDF\Facade as PDF;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

class Fotocheck_Pedidos_GestionarController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {

        $permisos = $this->seguridad->validarPermisos($request);
        if ($permisos->permission_create) {
            $pedidos = DB::connection('pgsql_syscarnes')->table('vst_pedidos_fotocheck_cx_seguridad')
                    ->select('vst_pedidos_fotocheck_cx_seguridad.duplicado', 'vst_pedidos_fotocheck_cx_seguridad.estado as estado_id', 'vst_pedidos_fotocheck_cx_seguridad.name as user_create', 'vst_pedidos_fotocheck_cx_seguridad.id', DB::raw("to_char(vst_pedidos_fotocheck_cx_seguridad.fec_create,'dd/MM/YYYY') as  fec_creado"), DB::raw("to_char(vst_pedidos_fotocheck_cx_seguridad.fec_solicita,'dd/MM/YYYY') as  fec_solicita"), DB::raw("to_char(vst_pedidos_fotocheck_cx_seguridad.fec_genera,'dd/MM/YYYY') as  fec_genera"), DB::raw("to_char(vst_pedidos_fotocheck_cx_seguridad.fec_entrega,'dd/MM/YYYY') as  fec_entrega"), DB::raw('count(pedidos_fotocheck_detalle.estudiante_fotocheck_id) as cantidad'), 'generica.nombre as estado')
                    ->Leftjoin('pedidos_fotocheck_detalle', 'vst_pedidos_fotocheck_cx_seguridad.id', '=', 'pedidos_fotocheck_detalle.pedido_fotocheck_id')
                    ->join('generica', 'vst_pedidos_fotocheck_cx_seguridad.estado', '=', 'generica.cod')
                    ->where('vst_pedidos_fotocheck_cx_seguridad.estado', '<>', '0')
                    ->where('generica.tipo', 'TIPESTPEDIDOFOTOCHECK')
                    ->orWhere('vst_pedidos_fotocheck_cx_seguridad.user_create', Auth::User()->username)
                    ->where('generica.tipo', 'TIPESTPEDIDOFOTOCHECK')
                    ->groupBy('vst_pedidos_fotocheck_cx_seguridad.duplicado', 'vst_pedidos_fotocheck_cx_seguridad.estado', 'vst_pedidos_fotocheck_cx_seguridad.name', 'vst_pedidos_fotocheck_cx_seguridad.id', 'vst_pedidos_fotocheck_cx_seguridad.fec_create', 'vst_pedidos_fotocheck_cx_seguridad.fec_solicita', 'vst_pedidos_fotocheck_cx_seguridad.fec_genera', 'vst_pedidos_fotocheck_cx_seguridad.fec_entrega', 'generica.nombre')
                    ->get();
        } else {

            $pedidos = DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')
                    ->select('pedidos_fotocheck.duplicado', 'pedidos_fotocheck.estado as estado_id', 'pedidos_fotocheck.user_create', 'pedidos_fotocheck.id', DB::raw("to_char(pedidos_fotocheck.fec_create,'dd/MM/YYYY') as  fec_creado"), DB::raw("to_char(pedidos_fotocheck.fec_solicita,'dd/MM/YYYY') as  fec_solicita"), DB::raw("to_char(pedidos_fotocheck.fec_genera,'dd/MM/YYYY') as  fec_genera"), DB::raw("to_char(pedidos_fotocheck.fec_entrega,'dd/MM/YYYY') as  fec_entrega"), DB::raw('(select count(id) from pedidos_fotocheck_detalle where pedido_fotocheck_id=pedidos_fotocheck.id ) as cantidad'), 'generica.nombre as estado')
                    //   ->join('pedidos_fotocheck_detalle', 'pedidos_fotocheck.id', '=', 'pedidos_fotocheck_detalle.pedido_fotocheck_id')
                    ->join('generica', 'pedidos_fotocheck.estado', '=', 'generica.cod')
                    ->where('pedidos_fotocheck.user_create', Auth::User()->username)
                    ->where('generica.tipo', 'TIPESTPEDIDOFOTOCHECK')
                    // ->groupBy('pedidos_fotocheck.id', 'pedidos_fotocheck.fec_create', 'pedidos_fotocheck.fec_solicita', 'pedidos_fotocheck.fec_genera', 'pedidos_fotocheck.fec_entrega', 'generica.nombre')
                    ->get();
        }


        return view('backEnd.fotocheckgestionar.index', compact('pedidos', 'permisos'));
    }

    public function detalle(Request $request, $pedido_id) {


        try {

            $permisos = $this->seguridad->validarPermisos($request);

            $pedido = DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')->distinct()
                    ->select('pedidos_fotocheck.duplicado', 'pedidos_fotocheck.estado as codestado', 'pedidos_fotocheck.id as id', DB::raw("to_char(pedidos_fotocheck.fec_create,'dd/MM/YYYY') as  fec_creado"), DB::raw("to_char(pedidos_fotocheck.fec_solicita,'dd/MM/YYYY') as  fec_solicita"), DB::raw("to_char(pedidos_fotocheck.fec_genera,'dd/MM/YYYY') as  fec_genera"), DB::raw("to_char(pedidos_fotocheck.fec_entrega,'dd/MM/YYYY') as  fec_entrega"), 'generica.nombre as estado')
                    //->join('pedidos_fotocheck_detalle', 'pedidos_fotocheck.id', '=', 'pedidos_fotocheck_detalle.pedido_fotocheck_id')
                    ->join('generica', 'pedidos_fotocheck.estado', '=', 'generica.cod')
                    ->where('pedidos_fotocheck.id', $pedido_id)
                    //->where('pedidos_fotocheck.user_create', Auth::User()->username)
                    ->where('generica.tipo', 'TIPESTPEDIDOFOTOCHECK')
                    ->first();

            if (!$pedido) {
                return redirect('fotocheckgestionarpedido');
            }


            $programasSession = Session::get('programasArray');
            if (empty($programasSession)) {
                $programasSession = array('NO EXISTE');
            }
            $facultades = DB::connection('pgsql_syscarnes')->table('vst_facultades_programas_fotocheck')->distinct()
                    ->select('vst_facultades_programas_fotocheck.codfacultad as cod', 'vst_facultades_programas_fotocheck.nomfacultad as name')
                    ->whereIn('vst_facultades_programas_fotocheck.codprograma', $programasSession)
                    ->get();
            $programas = DB::connection('pgsql_syscarnes')->table('vst_programas_fotocheck')
                    ->select('vst_programas_fotocheck.codprograma as cod', 'vst_programas_fotocheck.nomprograma as name')
                    ->whereIn('vst_programas_fotocheck.codprograma', $programasSession)
                    ->get();
            $formularios = new FormulariosController();
            $selectFacultades = $formularios->selectSimpleNoLabelDefaultTodos($facultades, 'facultad_id', '0');
            $selectProgramas = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id', '0');
            $selectFacultades2 = $formularios->selectSimpleNoLabelDefaultTodos($facultades, 'facultad_id2', '0');
            $selectProgramas2 = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id2', '0');
        } catch (Exception $ex) {

            return redirect('fotocheckgestionarpedido');
        }

        return view('backEnd.fotocheckgestionar.detallepedido', compact('pedido', 'selectFacultades', 'selectProgramas', 'selectFacultades2', 'selectProgramas2', 'permisos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request, $duplicado) {
        if ($duplicado == 1) {
            $count_pedidos_creados = PedidoFotocheck::where([
                        ['user_create', '=', Auth::User()->username],
                        ['estado', '=', 0],
                        ['duplicado', '=', 1]
                    ])->first();
            if (count($count_pedidos_creados) > 0) {
                
                return redirect('fotocheckgestionarpedido/detalle/' . $count_pedidos_creados->id);
            }
            $pedido = PedidoFotocheck::create(['user_create' => Auth::User()->username, 'estado' => 0, 'duplicado' => 1]);
        } else {
            $count_pedidos_creados = PedidoFotocheck::where([
                        ['user_create', '=', Auth::User()->username],
                        ['estado', '=', 0],
                        ['duplicado', '=', null]
                    ])->first();
            if (count($count_pedidos_creados) > 0) {
                //return redirect('fotocheckgestionarpedido');
                return redirect('fotocheckgestionarpedido/detalle/' . $count_pedidos_creados->id);
            }
            $pedido = PedidoFotocheck::create(['user_create' => Auth::User()->username, 'estado' => 0]);
        }
        return redirect('fotocheckgestionarpedido/detalle/' . $pedido->id);
    }

    public function cambiarEstado(Request $request) {

        /* INICIO: Obtener Parametros */
        $id = $request->id;
        $estado = $request->estado;
        $pedido_id = $request->pedido_id;
        /* FIN: Obtener Parametros */

        /* INICIO: Verificar si usuario ha cerrado el Pedido */
        $pedido = PedidoFotocheck::findOrFail($pedido_id);

        if ($pedido->estado != 0) {
            return response()->json(['status' => 'danger', 'message' => 'No puede cambiar el estado, el pedido ya se encuentra cerrado', 'data' => 'Error', 'nameestado' => 'Error: Pedido ya cerrado'], 200);
        }
        /* FIN: Verificar si usuario ha cerrado el Pedido */


        $EstudianteFotocheck = EstudianteFotocheck::findOrFail($id);

        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');
        /* INICIO: Verificar si el estado enviado al hacer check es SI */
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {

            if ($estado == '1') {
                $estado=0;
                $duplicado=null;
                if ($pedido->duplicado==1){
                    $duplicado=1;
                }
                /* INICIO: Verificar si al hacer check ya otra persona ha validado al estudiante */
                if ($EstudianteFotocheck->estado == '0') {
                    return response()->json(['status' => 'success', 'message' => 'Registrado ya se encuentra reservado', 'data' => 'Registrado ya se encuentra reservado', 'nameestado' => 'No puede validar, Registrado ya se encuentra reservado', 'code' => 0], 200);
                }
                /* FIN: Verificar si al hacer check ya otra persona ha validado al estudiante */
                $NameEstado = 'SI';
                DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $id)
                        //->where('estado', null)
                        ->update(['estado' => $estado, 'duplicado'=>$duplicado, 'user_update' => Auth::User()->username]);

                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')->insert([
                    ['pedido_fotocheck_id' => $pedido_id, 'estudiante_fotocheck_id' => $EstudianteFotocheck->id, 'user_reserva' => Auth::User()->username, 'fec_reserva' => $fechaActual]
                ]);
                
            } else {
                $estado=null;
                if ($pedido->duplicado==1){
                    $duplicado=null;
                    $estado=4;
                }
                $NameEstado = 'NO';
                DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $id)
                        //->where('estado', 0)
                        ->update(['estado' => $estado,'duplicado'=>null,  'user_update' => null]);

                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                        ->where('pedido_fotocheck_id', $pedido_id)
                        ->where('estudiante_fotocheck_id', $EstudianteFotocheck->id)->delete();
            }
            /* FIN: Verificar si el estado enviado al hacer check es SI */
        } catch (Exception $ex) {
            DB::connection('pgsql_syscarnes')->rollBack();
        }
        DB::connection('pgsql_syscarnes')->commit();

        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => 'Datos guardados exitosamente', 'nameestado' => $NameEstado], 200);
    }

    public function procesarSolicitudMasiva(Request $request) {
        ini_set('max_execution_time', 600);
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {
            $now = new \DateTime();
            $anio = $now->format('Y');
            $pedido_id = $request->pedido_id;
            $codprograma = $request->programa_cod;
            $fec_vencimiento = $request->fec_vencimiento;
            $chk_fec_vencimiento_reemplazo=$request->chk_fec_vencimiento_reemplazo;
            $pedido = PedidoFotocheck::where([
                        ['id', '=', $pedido_id]
                    ])->first();
            if ($pedido->estado != 0) {
                return response()->json(['status' => 'warning', 'message' => 'El pedido de encuentra cerrado'], 200);
            }
            if ($codprograma == 'all') {
                return response()->json(['status' => 'warning', 'message' => 'Debe de elegir un programa'], 200);
            }
            if (is_null($fec_vencimiento)) {
                return response()->json(['status' => 'warning', 'message' => 'Debe de elegir una fecha de vencimiento'], 200);
            }


            $estudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->select('estudiantes_fotocheck.cod_periodo_matricula', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.nrodocumento')
                    ->where(DB::raw('substring(trim(estudiantes_fotocheck.cod_periodo_matricula),0,5)'), $anio)
                    ->where('estudiantes_fotocheck.codprograma', $codprograma)
                    ->where('estudiantes_fotocheck.estado', null)
                    ->where('estudiantes_fotocheck.nivel', '<>', null)
                    ->where('estudiantes_fotocheck.cod_estado_sinu', 1)
                    ->get();




            $totalRegistros = count($estudiantesFotocheck);
            $data = json_decode(json_encode($estudiantesFotocheck), true);


            $fechaActual = $now->format('Y-m-d H:i:s');

            $carpeta = storage_path('app/public') . '/fotochecks/fotos';
            $i = 0;
            foreach ($data as $index => $dato) {
                $fotoCarpeta = $carpeta . "/" . $dato['nrodocumento'] . ".jpg";
                if (file_exists($fotoCarpeta)) {

                    if ($chk_fec_vencimiento_reemplazo==1){
                         DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $dato['id'])
                            ->where('estado', null)
                            ->where('cod_estado_sinu', 1)
                            ->update(['estado' => '0', 'user_update' => Auth::User()->username, 'fec_vencimiento' => $fec_vencimiento]);
                    }else{
                         DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $dato['id'])
                            ->where('estado', null)
                            ->where('cod_estado_sinu', 1)
                            ->update(['estado' => '0', 'user_update' => Auth::User()->username]);
                    
                          DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $dato['id'])
                            ->where('fec_vencimiento', null)
                            ->where('cod_estado_sinu', 1)
                            ->update(['estado' => '0', 'user_update' => Auth::User()->username, 'fec_vencimiento' => $fec_vencimiento]);
                    }
                    

                    DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')->insert([
                        ['pedido_fotocheck_id' => $pedido_id, 'estudiante_fotocheck_id' => $dato['id'], 'user_solicita' => Auth::User()->username, 'fec_solicita' => $fechaActual, 'estado' => '0']
                    ]);
                    $i++;
                }
            }
        } catch (Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'success', 'message' => 'Error al ejecutar procedimiento de filtración'], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();

        return response()->json(['status' => 'success', 'message' => 'Se solicitaron ' . $i . ' de ' . $totalRegistros . ' carnés universitarios para el programa ' . $codprograma], 200);
    }

    public function solicitarPedido(Request $request) {

        $pedido_id = $request->pedido_id;
        $CantidadEstudiantesFotocheckSinFechaVencimiento = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->where('fec_vencimiento', null)
                ->where('pedidos_fotocheck.id', $pedido_id)
                ->count();

        $EstudiantesFotocheckSinFechaVencimiento = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select('estudiantes_fotocheck.nrodocumento')
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->where('fec_vencimiento', null)
                ->where('pedidos_fotocheck.id', $pedido_id)
                ->get();


        $nrodocumentos_falta_fec_vencimiento = '';
        $i = 1;
        foreach ($EstudiantesFotocheckSinFechaVencimiento as $value) {
            if ($i < count($EstudiantesFotocheckSinFechaVencimiento)) {
                $nrodocumentos_falta_fec_vencimiento .= $value->nrodocumento . ", ";
            } else {
                $nrodocumentos_falta_fec_vencimiento .= $value->nrodocumento;
            }
            $i++;
        }


        if ($CantidadEstudiantesFotocheckSinFechaVencimiento > 0) {
            return response()->json(['status' => 'warning', 'message' => 'Error: Falta asignar fecha de vencimiento a los fotochecks de  ' . $CantidadEstudiantesFotocheckSinFechaVencimiento . ' personas: ' . $nrodocumentos_falta_fec_vencimiento, 'error' => 1], 200);
        }


        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {
            $now = new \DateTime();
            $fechaActual = $now->format('Y-m-d H:i:s');
            $pedido = PedidoFotocheck::where([
                        ['id', '=', $pedido_id],
                        ['user_create', '=', Auth::User()->username],
                        ['estado', '=', '0'],
                    ])->first();
            $retorno = 0;
            if (count($pedido) == 0) {
                $retorno = 1;
                return response()->json(['status' => 'success', 'message' => 'El pedido no existe o ya se encuentra solicitado.', 'data' => $retorno], 200);
            } else {
                $retorno = 2;
                DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->where('estado', 0)
                        ->where('user_update', Auth::User()->username)
                        ->update(['estado' => 1, 'devolucion' => null]);
                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')
                        ->where('id', $pedido_id)
                        ->where('user_create', Auth::User()->username)
                        ->update(['estado' => 1, 'fec_solicita' => $fechaActual]);
                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                        ->where('pedido_fotocheck_id', $pedido_id)
                        ->update(['user_solicita' => Auth::User()->username, 'estado' => 1, 'fec_solicita' => $fechaActual]);
            }
        } catch (Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'warning', 'message' => 'Error al solicitar el pedido de Fotochecks', 'error' => 0], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return response()->json(['status' => 'success', 'message' => 'Pedido solicitado exitosamente', 'data' => $retorno, 'error' => 0], 200);
    }

    public function imprimirPedido(Request $request) {

        try {
            ini_set('max_execution_time', 1600);
            ini_set('memory_limit', '512M');
            $cantidad_x_documento = 50;
            $pedido_id = $request->pedido_id;
            $pedido = PedidoFotocheck::where([
                        ['id', '=', $pedido_id]
                    ])->first();

            $etm = DB::connection('pgsql_syscarnes')->table('facultades_fotocheck')
                    ->select('facultades_fotocheck.codfacultad', 'facultades_fotocheck.nomfacultad')
                    ->where('facultades_fotocheck.codfacultad', '999')
                    ->first();

            $estudiantesfotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->select('pedidos_fotocheck.duplicado', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.niv_formacion', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', DB::raw("case when estudiantes_fotocheck.cod_escuela='" . $etm->codfacultad . "' then '" . $etm->nomfacultad . "' else estudiantes_fotocheck.nomfacultad end nomfacultad "), DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                    ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                    ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                    ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                    ->where('generica.tipo', 'TIPESTFOTOCHECK')
                    ->where('pedidos_fotocheck.id', $pedido_id)
                    ->get();



            $estudiantesfotocheck = json_decode(json_encode($estudiantesfotocheck), true);
            $dataoriginal = $estudiantesfotocheck;

            $k = 0;

            if (count($estudiantesfotocheck) > $cantidad_x_documento) {
                $hojas = intval(count($estudiantesfotocheck) / $cantidad_x_documento);
            } else {
                $hojas = 0;
            }
            for ($ii = 1; $ii <= $hojas + 1; ++$ii) {
                $estudiantesfotocheck = $dataoriginal;
                $estudiantesfotocheck = array_slice($estudiantesfotocheck, $k, $cantidad_x_documento);
                $d = new DNS1D();
                $carpeta = storage_path('app/public') . '/fotochecks/codigobarras/';
                $d->setStorPath($carpeta);

                foreach ($estudiantesfotocheck as $index => $dato) {
                    $d->getBarcodePNGPath($dato['nrodocumento'], "C39", 1, 33);
                }
                $codigo_barras = storage_path('app/public') . '/fotochecks/codigobarras/' . strtolower($dato['nrodocumento']) . '.png';
                if (file_exists($codigo_barras)) {
                    rename($codigo_barras, $carpeta . strtoupper($dato['nrodocumento']) . '.png');
                }

                $dompdf = PDF::loadView('backEnd.fotocheckgestionar.imprimir', compact('pedido', 'estudiantesfotocheck'));
                $carpeta = storage_path('app/public') . '/fotochecks/pedidos/pedido_' . $pedido_id;
                if (!file_exists($carpeta)) {
                    mkdir($carpeta, 0777, true);
                }
                \Storage::disk('public')->put('fotochecks/pedidos/pedido_' . $pedido_id . '/' . str_pad($ii, 3, "0", STR_PAD_LEFT) . '.pdf', $dompdf->output());
                $k = $k + $cantidad_x_documento;
            }
        } catch (Exception $e) {

            return response()->json(['status' => 'warning', 'message' => 'Error al solicitar el pedido de Fotochecks', 'error' => 0], 200);
        }

        return response()->json(['status' => 'success', 'message' => 'Generación de archivos ejecutada exitosamente', 'data' => 'todo bien'], 200);
    }

    public function viewArchivosGenerados(Request $request) {
        $pedido_id = $request->pedido_id;
        $pedido = PedidoFotocheck::findOrFail($pedido_id);


        $carpeta = storage_path('app/public') . '/fotochecks/pedidos/pedido_' . $pedido_id;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $urlReportes = \Storage::url('fotochecks/pedidos/pedido_' . $pedido_id);
        $directorio = opendir($carpeta); //ruta actual
        $listadoArchivosGenerados = array();

        //  if($listadoArchivosGenerados){


        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (!is_dir($archivo)) {//verificamos si es o no un directorio
                //$listadoArchivosGenerados1 = array('name' => 'Archivo-' . $archivo, 'link' => $urlReportes . '/' . $archivo);
                $listadoArchivosGenerados[] = array('name' => 'Archivo-' . $archivo, 'link' => $urlReportes . '/' . $archivo);
                // array_push($listadoArchivosGenerados, $listadoArchivosGenerados1);
            }
        }
        if ($listadoArchivosGenerados) {
            $sortArray = array();
            foreach ($listadoArchivosGenerados as $person) {
                foreach ($person as $key => $value) {
                    if (!isset($sortArray[$key])) {
                        $sortArray[$key] = array();
                    }
                    $sortArray[$key][] = $value;
                }
            }
            $orderby = "name";
            array_multisort($sortArray[$orderby], SORT_ASC, $listadoArchivosGenerados);
        }
        return view('backEnd.fotocheckgestionar.view_archivosgenerados', compact('pedido', 'listadoArchivosGenerados'));
    }

    public function cambiarEstadoGenerado(Request $request) {

        $pedido_id = $request->pedido_id;
        $pedido = PedidoFotocheck::findOrFail($pedido_id);

        if ($pedido->estado != 1) {
            return response()->json(['status' => 'danger', 'message' => 'No puede cambiar el estado, el pedido ya se encuentra cerrado', 'data' => 'Error', 'nameestado' => 'Error: Pedido ya cerrado'], 200);
        }

        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');

        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {

            /*   $fotocheckEstudiante = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
              ->select('estudiantes_fotocheck.id as id')
              ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
              ->where('estudiantes_fotocheck.estado', 1)
              ->where('pedidos_fotocheck_detalle.estado', 1)
              ->where('pedidos_fotocheck_detalle.pedido_fotocheck_id', $pedido_id)->get();

              foreach ($fotocheckEstudiante as $index => $dato) {
              DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
              ->where('estudiantes_fotocheck.id', $dato->id)
              ->update(['estado' => 2]);
              }
             */

            $estudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->select('estudiantes_fotocheck.id', 'estudiantes_fotocheck.estado', 'pedidos_fotocheck_detalle.id as detalle_id')
                    ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                    ->where('estudiantes_fotocheck.estado', 1)
                    ->where('pedidos_fotocheck_detalle.estado', 1)
                    ->where('pedidos_fotocheck_detalle.pedido_fotocheck_id', $pedido_id)
                    ->get();
            
            $data = json_decode(json_encode($estudiantesFotocheck), true);
            
            foreach ($data as $index => $dato) {
                    DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where('id', $dato['id'])
                            ->where('estado', 1)
                            ->update(['estado' => 2,'ultimo_id_detalle_fotocheck'=> $dato['detalle_id']]);
            }

            DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                    ->where('pedidos_fotocheck_detalle.pedido_fotocheck_id', $pedido_id)
                    ->update(['estado' => 2, 'user_genera' => Auth::User()->username, 'fec_genera' => $fechaActual]);

            DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')->where('id', $pedido_id)
                    ->where('estado', 1)
                    ->update(['estado' => 2, 'user_genera' => Auth::User()->username, 'fec_genera' => $fechaActual]);
        } catch (Exception $ex) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'warning', 'message' => 'Cambio de estado fallo', 'data' => 'Cambio de estado fallo'], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return response()->json(['status' => 'success', 'message' => 'Cambio de estado satisfactoriamente', 'data' => 'Cambio de estado satisfactoriamente'], 200);
    }

    public function createPersonaExterna($pedido_id) {
        $facultades = DB::connection('pgsql_syscarnes')->table('facultades_fotocheck')
                ->select('facultades_fotocheck.codfacultad as cod', DB::raw("facultades_fotocheck.abreviaturafacultad|| ' (' ||facultades_fotocheck.nomfacultad|| ')' as  name"))
                ->where('facultades_fotocheck.facultad_externos', '1')
                ->get();

        $tipodocumento = DB::connection('pgsql_syscarnes')->table('generica')
                ->select('generica.cod as cod', 'generica.nombre as name')
                ->where('generica.tipo', 'TIPDOCUMENTO')
                ->get();

        $formularios = new FormulariosController();
        $selectFacultades = $formularios->selectSimpleNoLabel($facultades, 'facultad_id', '0');
        $selectTipodocumento = $formularios->selectSimpleNoLabel($tipodocumento, 'tipodocumento', '0');


        return view('backEnd.fotocheckgestionar.createpersonaexterna', compact('pedido_id', 'selectFacultades', 'selectTipodocumento'));
    }

    public function storePersonaExterna(Request $request) {


        /* INICIO: Validaciones */
        if (empty($request->tipodocumento)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Debe seleccionar un tipo de documento</span>', 'validacion' => 1], 200);
        }
        if (empty($request->nrodocumento)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Debe seleccionar un n° de documento</span>', 'validacion' => 1], 200);
        }
        if (empty($request->apepaterno)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Ingrese apellido paterno</span>', 'validacion' => 1], 200);
        }
        if (empty($request->nombre)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Ingrese nombres</span>', 'validacion' => 1], 200);
        }
        if (empty($request->facultad_id)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Debe seleccionar una facultad</span>', 'validacion' => 1], 200);
        }
        if (empty($request->nomprograma)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Ingrese un nombre de programa</span>', 'validacion' => 1], 200);
        }
        if (empty($request->fec_vencimiento)) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Ingrese una fecha de caducidad para el fotocheck</span>', 'validacion' => 1], 200);
        }
        if (empty($request->file('file_foto'))) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Debe seleccionar un archivo</span>', 'validacion' => 1], 200);
        }

        /* FIN:  Validaciones */


        /* INICIO: Obtener Parametros */
        $pedido_id = $request->pedido_id;
        //$pedido_id = 36;
        /* FIN: Obtener Parametros */

        /* INICIO: Verificar si usuario ha cerrado el Pedido */
        $pedido = PedidoFotocheck::findOrFail($pedido_id);

        if ($pedido->estado != 0) {
            return response()->json(['status' => 'danger', 'message' => 'No puede cambiar el estado, el pedido ya se encuentra cerrado', 'validacion' => 1], 200);
        }
        /* FIN: Verificar si usuario ha cerrado el Pedido */



        $EstudianteFotocheck = EstudianteFotocheck::where([
                    ['nrodocumento', '=', $request->nrodocumento]
                ])->first();

        if ($EstudianteFotocheck) {
            return response()->json(['status' => 'warning', 'message' => '<span class="alert alert-danger" ><i class="fa fa-info-circle" ></i> Error: El N° documento ya se encuentra registrado.</span>', 'validacion' => 1], 200);
        }


        $facultad = DB::connection('pgsql_syscarnes')->table('facultades_fotocheck')->where('codfacultad', $request->facultad_id)->first();



        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');
        /* INICIO: Verificar si el estado enviado al hacer check es SI */
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {
            /* Inicio: Insertar Estudiante */

            $estudianteFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->insertGetId(
                    ['tipodocumento' => $request->tipodocumento,
                        'nrodocumento' => strtoupper($request->nrodocumento),
                        'apepaterno' => strtoupper($request->apepaterno),
                        'apematerno' => strtoupper($request->apematerno),
                        'nombre' => strtoupper($request->nombre),
                        'codfacultad' => $facultad->codfacultad,
                        'nomfacultad' => $facultad->nomfacultad,
                        'abreviaturafacultad' => strtoupper($facultad->abreviaturafacultad),
                        'cod_escuela' => $facultad->codfacultad,
                        'abreviatura_escuela' => strtoupper($facultad->abreviaturafacultad),
                        'nomprograma' => strtoupper($request->nomprograma),
                        'fec_vencimiento' => $request->fec_vencimiento,
                        'user_update' => Auth::User()->username,
                        'nivel' => 0,
                        'cod_estado_sinu' => 1,
                        'estado' => 0]
            );


            /* Fin:Insertar Estudiante */

            DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')->insert([
                ['pedido_fotocheck_id' => $pedido_id, 'estudiante_fotocheck_id' => $estudianteFotocheck, 'user_reserva' => Auth::User()->username, 'fec_reserva' => $fechaActual]
            ]);



            try {
                $file = $request->file('file_foto');
                $extension = $file->getClientOriginalExtension();
                $cargarArchivo = \Storage::disk('public')->put('fotochecks/fotos/' . $request->nrodocumento . '.jpg', \File::get($file));
                if ($cargarArchivo) {
                    DB::connection('pgsql_syscarnes')->commit();
                    return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'validacion' => 0], 200);
                }
            } catch (\Exception $e) {
                DB::connection('pgsql_syscarnes')->rollBack();
                return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al cargar el archivo.'], 200);
            }
        } catch (Exception $ex) {
            DB::connection('pgsql_syscarnes')->rollBack();
        }
        DB::connection('pgsql_syscarnes')->commit();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'validacion' => 0], 200);
    }

}
