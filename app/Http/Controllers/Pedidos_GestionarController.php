<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Pedido;
use App\PedidoCerrado;
use App\SolicitudSunedu;
use App\EstudianteSunedu;
use App\EstudianteSinu;
use App\Estudiante_Pedido;
use App\FacultadSunedu;
use App\ProgramaSunedu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use Yajra\Datatables\Datatables;
use Excel;
use Imagick;
use ImagickPixel;

class Pedidos_GestionarController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $permisos = $this->seguridad->validarPermisos($request);
        $pedidos = DB::connection('pgsql_syscarnes')->select("select
            case 
            when p.estado='0'  then 'Creado' 
            when p.estado='1'  then 'Abierto'
            when p.estado='2' then 'Cerrado'
            when p.estado='3' then 'Prueba'
            else 'Sin estado' end as estado,
            p.id,to_char(p.fec_inicio,'dd/MM/YYYY') as fec_inicio,
            to_char(p.fec_fin,'dd/MM/YYYY') as fec_fin,
            p.nombre,to_char(p.fec_importacion_estudiantes,'dd/MM/YYYY') as  fec_importacion_estudiantes, 
            to_char(p.fec_filtracion_estudiantes,'dd/MM/YYYY') as  fec_filtracion_estudiantes, 
            to_char(p.fec_cierre,'dd/MM/YYYY') as fec_cierre from pedidos
            p    where p.eliminado is null or p.eliminado='0' ");

        

        
        return view('backEnd.pedidosgestionar.index', compact('pedidos', 'permisos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request) {

        $permisos = $this->seguridad->validarPermisos($request);
        if ($permisos->permission_create == true) {
            return view('backEnd.pedidosgestionar.create');
        } else {
            return view('mensajes.permisos');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        Pedido::create($request->all());
        Session::flash('message', 'Pedido added!');
        Session::flash('status', 'success');
        return redirect('pedidosgestionar');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $pedido = Pedido::findOrFail($id);
        return view('backEnd.pedidosgestionar.edit', compact('pedido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {

        $pedido = Pedido::findOrFail($id);
        $pedido->update($request->all());

        Session::flash('message', 'Pedido updated!');
        Session::flash('status', 'success');

        return redirect('pedidosgestionar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $pedido = Pedido::findOrFail($id);

        $pedido->eliminado = '1';
        $pedido->save();

        Session::flash('message', 'Pedido deleted!');
        Session::flash('status', 'success');

        return redirect('pedidos');
    }

    public function open(Request $request, $pedido_id) {
        $pedido = Pedido::findOrFail($pedido_id);
         return view('backEnd.pedidosgestionar.open', compact('pedido'));
    }

    public function openPaso01(Request $request) {
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::findOrFail($pedido_id);
        return view('backEnd.pedidosgestionar.paso01', compact('pedido'));
    }

    public function openPaso01GenerarCodigosAlumnosSinu(Request $request) {
        ini_set('max_execution_time', 600);
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::findOrFail($pedido_id);
        $anio = $pedido->fec_inicio->format('Y');
		
        DB::connection('oracle_prod')->beginTransaction();
        try {
			$dataAlumnosNoCorrespondeCodalumno = DB::connection('oracle_prod')->select($this->querys('query_codalumno_no_corresponde', $anio));
			$totalAlumnosNoCorrespondeCodalumno = count($dataAlumnosNoCorrespondeCodalumno);

			$j=0;
		    foreach ($dataAlumnosNoCorrespondeCodalumno as $index => $dato) {
                $ejecutar = DB::connection('oracle_prod')->table('src_alum_programa')->where('id_alum_programa', $dato->id_alum_programa)
                        ->where('cod_alumno', $dato->cod_alumno)
                        ->update(['cod_alumno' => null]);
                if ($ejecutar) {
                    $j++;
                }
            }
		
		    $data = DB::connection('oracle_prod')->select($this->querys('query_listar_update_codalumno', $anio));
            $totalRegistrosActualizar = count($data);
		
            $i = 0;
            foreach ($data as $index => $dato) {
                $ejecutar = DB::connection('oracle_prod')->table('src_alum_programa')->where('id_alum_programa', $dato->id_alum_programa)
                        ->where('cod_alumno', null)
                        ->update(['cod_alumno' => $dato->codalumno]);
                if ($ejecutar) {
                    $i++;
                }
            }
        } catch (\Exception $e) {
            DB::connection('oracle_prod')->rollBack();
            return response()->json(['status' => 'warning', 'message' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>'], 200);
        }
        DB::connection('oracle_prod')->commit();

        /*  $i = 0;
          $totalRegistrosActualizar = 0; */
		  
        return response()->json(['status' => 'success', 'message' => '<br><div class="alert alert-success"><strong>Correcto!</strong> Se han limpiado ' . $j . ' codigos de alumno que no correspondian con el programa de un total de  ' . $totalAlumnosNoCorrespondeCodalumno . ' registros </div><br><div class="alert alert-success"><strong>Correcto!</strong> Se han generado ' . $i . ' codigos de alumno de un total de ' . $totalRegistrosActualizar . ' registros </div>'], 200);
    }

    public function openPaso01InsertarAlumnosSinu(Request $request) {
        ini_set('max_execution_time', 600);
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::findOrFail($pedido_id);
        $anio = $pedido->fec_inicio->format('Y');

        $data = DB::connection('oracle_prod')->select($this->querys('query_listar_alumnos', $anio));
        $totalRegistrosInsertar = count($data);

        DB::connection('pgsql_syscarnes')->beginTransaction();

        $estudiantesSinu = DB::connection('pgsql_syscarnes')->table('estudiantes_sinu')->where([
                    ['pedido_id', '=', $pedido_id]
                ])->get();
        $totalRegistrosExistentes = count($estudiantesSinu);
       /* if ($totalRegistrosInsertar == $totalRegistrosExistentes) {
			return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-warning"><strong>Correcto!</strong> Anteriormente ya se han insertado ' . $totalRegistrosExistentes . ' registros </div>','messageFinalizado' => 'Proceso Finalizado!', 'messageFinalizadoHtml' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>', 'info' => '0'], 200);
        }*/
        try {
            $i = 0;
            foreach ($data as $index => $dato) {

                $validar = DB::connection('pgsql_syscarnes')->table('estudiantes_sinu')->where([
                            ['pedido_id', '=', $pedido_id],
							['estado', '=', $dato->estado],
                            ['codalumno', '=', $dato->codalumno]
                        ])->first();
                if (count($validar) <= 0) {

                    $ejecutar = DB::connection('pgsql_syscarnes')->table('estudiantes_sinu')->insert(
                            array(
                                'codalumno' => $dato->codalumno,
                                'apepaterno' => $dato->apepaterno,
                                'apematerno' => $dato->apematerno,
                                'nombre' => $dato->nombre,
                                'tipodocumento' => $dato->tipodocumento,
                                'nrodocumento' => $dato->nrodocumento,
                                'ano_ing' => $dato->ano_ing,
                                'per_ing' => $dato->per_ing,
                                'id_alum_programa' => $dato->id_alum_programa,
                                'codfacultad' => $dato->codfacultad,
                                'nomfacultad' => $dato->nomfacultad,
                                'codprograma' => $dato->codprograma,
                                'nomprograma' => $dato->nomprograma,
                                'codmodalidad' => $dato->codmodalidad,
                                'formacion' => $dato->formacion,
                                'estado' => $dato->estado,
                                'abreviaturafacultad' => $dato->abreviaturafacultad,
                                'modalidad_desc' => $dato->modalidad_desc,
                                'niv_formacion' => $dato->niv_formacion,
                                'ano_ing_completo' => $dato->ano_ing_completo,
                                'nivel' => $dato->nivel,
                                'email' => $dato->email,
                                'genero' => $dato->genero,
                                'fec_nac' => $dato->fec_nac,
                                'departamento' => $dato->departamento,
                                'distrito' => $dato->distrito,
                                'codpensum'=> $dato->cod_pensum,
                                'cod_escuela'=> $dato->cod_escuela,
                                'abreviatura_escuela'=> $dato->abreviatura_escuela,
                                'pedido_id' => $pedido_id)
                    );
                    if ($ejecutar) {
                        $i++;
                    }
                }
            }

            $now = new \DateTime();
            $fechaActual = $now->format('Y-m-d H:i:s');
            $pedido = Pedido::findOrFail($pedido_id);
            $pedido->fec_importacion_estudiantes = $fechaActual;
            $pedido->save();
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage(),  'info' => '0'], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();

        return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros. </div>', 'message' => 'Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros.', 'messageFinalizado' => 'Proceso Finalizado!', 'messageFinalizadoHtml' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>', 'messageFechaActualizacion' => $fechaActual,  'info' => '1'], 200);
    }

    public function openPaso02(Request $request) {
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::findOrFail($pedido_id);
        return view('backEnd.pedidosgestionar.paso02', compact('pedido'));
    }

    public function datatableSolicitudesSunedu() {
        $solicitudes = DB::connection('pgsql_syscarnes')->select("select ss.id,  ss.nrosolicitud, count(es.solicitud_id) as totalestudiantes from solicitudes_sunedu ss left join 
                                                                    estudiantes_sunedu es on ss.id=es.solicitud_id where ss.estado='1'  group by ss.id,ss.nrosolicitud order by  ss.nrosolicitud    ");
        return Datatables::of($solicitudes)
                        ->addColumn('action', function ($data) {
                            return '<form class="form-inline" role="form" method="POST" id="formuploadajax' . $data->id . '" name="formuploadajax' . $data->id . '" accept-charset="UTF-8" enctype="multipart/form-data" >
                                        <div  class="form-group"><button type="button" class="btn btn-xs btn-primary " onclick="verEstudiantesSunedu(' . $data->id . ');" >Ver Estudiantes SUNEDU</button></div>
                                        <div class="form-group"><input type="file" class="btn btn-xs btn-primary"  id="file_' . $data->id . '" name="file_' . $data->id . '"  /></div>
                                        <button type="button" onclick="cargarArchivoEstudiantesSunedu(' . $data->id . ');" class="btn btn-xs btn-primary"><i class="fa fa-upload"></i> Cargar Archivo</button>
                                        <button type="button" onclick="deleteSolicitud(' . $data->id . ');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
                                            
                                    </form>';
                        })
                        ->make(true);
    }

    public function createSolicitud() {
        return view('backEnd.pedidosgestionar.create_solicitud');
    }

    public function storeSolicitud(Request $request) {
        try {
            $solicitudSunedu = SolicitudSunedu::where([
                        ['nrosolicitud', '=', $request->nrosolicitud],
                        ['estado', '=', '1'],
                    ])->first();
            if (count($solicitudSunedu) > 0) {
                return response()->json(['status' => 'warning', 'message' => 'Ya existe una solicitud con ese Número'], 200);
            }
            SolicitudSunedu::create($request->all());
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al crear.'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Se ha creado satisfactoriamente'], 200);
    }

    public function destroySolicitud(Request $request) {
        try {
            $solicitud_id = $request->solicitud_id;
            $solicitudSunedu = SolicitudSunedu::findOrFail($solicitud_id);
            if (count($solicitudSunedu) == 0) {
                return response()->json(['status' => 'warning', 'message' => 'No existe una solicitud con ese Número'], 200);
            }
            SolicitudSunedu::destroy($solicitudSunedu->id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al eliminar.'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Se ha eliminado satisfactoriamente'], 200);
    }

    public function subirArchivoEstudiantesSunedu(Request $request) {
        try {
            if (empty($request->file('file_' . $request->solicitud_id))) {
                return response()->json(['status' => 'warning', 'message' => 'Debe seleccionar un archivo'], 200);
            }
            $solicitud = SolicitudSunedu::findOrFail($request->solicitud_id);
            $file = $request->file('file_' . $request->solicitud_id);
            $extension = $file->getClientOriginalExtension();
            $cargarArchivo = \Storage::disk('local')->put('ReportEstudiantesSUNEDU_' . $solicitud->id . '_' . $solicitud->nrosolicitud . '.' . $extension, \File::get($file));
            if ($cargarArchivo) {
                $this->importarEstudiantesSunedu($solicitud->id);
                /* if ($procesar == true) {
                  return response()->json(['status' => 'success', 'message' => 'El archivo se ha procesado correctamente.'], 200);
                  } else {
                  return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al procesar el archivo.'], 200);
                  } */
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al cargar el archivo.'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'El archivo se ha procesado correctamente.'], 200);
    }

    public function importarEstudiantesSunedu($solicitud_id) {
        ini_set('max_execution_time', 600);
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {
            $solicitud = SolicitudSunedu::findOrFail($solicitud_id);
            $estudianteSunedu = EstudianteSunedu::where([
                        ['solicitud_id', '=', $solicitud_id],
            ]);
            if ($estudianteSunedu) {
                $estudianteSunedu->delete();
            }
            $url = storage_path('app') . '/ReportEstudiantesSUNEDU_' . $solicitud->id . '_' . $solicitud->nrosolicitud . '.xls';
            $returnImportacion = Excel::load($url, function($reader ) use ($solicitud_id) {
                        $reader->get();
                        $reader->each(function($row) use($solicitud_id) {
                            if ($row->codigo_alumno) {
                                $estudianteSunedu = new EstudianteSunedu();
                                $estudianteSunedu->solicitud_id = $solicitud_id;
                                $estudianteSunedu->codalumno = $row->codigo_alumno;
                                $estudianteSunedu->carrera = $row->carrera;
                                $estudianteSunedu->tipodocumento = $row->tipo_documento;
                                $estudianteSunedu->nrodocumento = $row->n0_documento;
                                $estudianteSunedu->apepaterno = $row['ap._paterno'];
                                $estudianteSunedu->apematerno = $row['ap._materno'];
                                $estudianteSunedu->nombre = $row->nombre;
                                $estudianteSunedu->estado = $row->estado;
                                $estudianteSunedu->save();
                            }
                        });
                    })->get();
            $totalReturnImportacion = count($returnImportacion) - 1;
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return false;
            //return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage()], 500);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return true;
        //return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado ' . $totalReturnImportacion . ' estudiantes.</div>', 'message' => 'Se han insertado ' . $totalReturnImportacion . ' estudiantes. ', 'messageFinalizado' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>'], 200);
    }

    public function verEstudiantesSunedu(Request $request) {

        $solicitud_id = $request->solicitud_id;
        $solicitud = SolicitudSunedu::findOrFail($solicitud_id);
        return view('backEnd.pedidosgestionar.estudiantessunedu', compact('solicitud'));
    }

    public function openPaso03(Request $request) {
        return view('backEnd.pedidosgestionar.paso03');
    }

    public function subirArchivoSunedu(Request $request) {
        try {
            if (empty($request->file('file_archivosunedu'))) {
                return response()->json(['status' => 'warning', 'message' => 'Debe seleccionar un archivo'], 200);
            }

            $file = $request->file('file_archivosunedu');
            $extension = $file->getClientOriginalExtension();
            $cargarArchivo = \Storage::disk('local')->put('ArchivoSUNEDU.' . $extension, \File::get($file));
            if ($cargarArchivo) {
                $this->importarFacultadesSunedu();
                $this->importarProgramasSunedu();
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Ha ocurrido un error al cargar el archivo.'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'El archivo se ha procesado correctamente.'], 200);
    }

    public function importarFacultadesSunedu() {
        ini_set('max_execution_time', 600);
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {

            $facultadesSunedu = FacultadSunedu::where([
                        ['nomfacultad', '<>', null],
            ]);

            if ($facultadesSunedu) {
                $facultadesSunedu->delete();
            }

            Excel::selectSheets('Facultad')->load(storage_path('app') . '/ArchivoSUNEDU.xls', function($reader ) {
                $reader->get();
                $reader->each(function($row) {
                    if ($row->codigo_facultad) {
                        $facultadSunedu = new FacultadSunedu();
                        $facultadSunedu->codfacultad = $row->codigo_facultad;
                        $facultadSunedu->nomfacultad = $row->facultad_escuela_de_posgrado;
                        $facultadSunedu->save();
                    }
                });
            });
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return 0;
            //   return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage()], 500);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return 1;
        //return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado facultades correctamente.</div>', 'message' => 'Se han insertado facultades correctamente. ', 'messageFinalizado' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>'], 200);
    }

    public function importarProgramasSunedu() {

        ini_set('max_execution_time', 600);
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {

            $programasSunedu = ProgramaSunedu::where([
                        ['nomprograma', '<>', null],
            ]);

            if ($programasSunedu) {
                $programasSunedu->delete();
            }

            Excel::selectSheets('Carrera')->load(storage_path('app') . '/ArchivoSUNEDU.xls', function($reader ) {
                $reader->get();

                $reader->each(function($row) {
                    if ($row->codigo_carrera) {
                        $programaSunedu = new ProgramaSunedu();
                        $programaSunedu->codfacultad = $row->codigo_facultad;
                        $programaSunedu->codprograma = $row->codigo_carrera;
                        $programaSunedu->nomprograma = $row->carrera_escuela_especialidad_de_posgrado;
                        $programaSunedu->abreviatura = $row->abreviatura;
                        $programaSunedu->save();
                    }
                });
            });
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return 0;
            // return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage()], 500);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return 1;
        //return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado programas correctamente.</div>', 'message' => 'Se han insertado programas correctamente. ', 'messageFinalizado' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>'], 200);
    }

    public function openPaso04(Request $request) {
        $pedido = Pedido::findOrFail($request->pedido_id);

        return view('backEnd.pedidosgestionar.paso04', compact('pedido'));
    }

    public function filtrarEstudiantesPedido(Request $request) {
        ini_set('max_execution_time', 600);
        try {
            $pedido_id = $request->pedido_id;
            $sql = "select func_filtrarestudiantespedido(?)";
            $ejecutar = DB::connection('pgsql_syscarnes')->select($sql, array($pedido_id));
            if ($ejecutar == 0) {
                return response()->json(['status' => 'warning', 'message' => 'Error al ejecutar procedimiento de filtración', 'data' => ''], 200);
            }

            $this->cargarImagenes($pedido_id);

            $now = new \DateTime();
            $fechaActual = $now->format('Y-m-d H:i:s');
            $pedido = Pedido::findOrFail($pedido_id);

            $pedido->fec_filtracion_estudiantes = $fechaActual;
            $pedido->save();

            $pedido = Pedido::findOrFail($pedido_id);
        } catch (Exception $e) {
            return response()->json(['status' => 'success', 'message' => 'Error al ejecutar procedimiento de filtración'], 200);
        }
        return response()->json(['status' => 'success', 'messageFechaFiltracion' => $pedido->fec_filtracion_estudiantes->format('d/m/Y H:i:s'), 'message' => 'Proceso ejecutado exitosamente'], 200);
    }

    public function cargarImagenes($pedido_id) {
        ini_set('max_execution_time', 600);
        if (empty($pedido_id)) {
            return 0;
        }

        try {
            $carpeta = storage_path('app/public') . '/pedido_' . $pedido_id.'/gestion/fotospedido';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            $estudiantesPedido = Estudiante_Pedido::where([
                        ['pedido_id', '=', $pedido_id]
                    ])->get();

            $data = json_decode(json_encode($estudiantesPedido), true);

            $fotosCarpeta = glob($carpeta . "/*.jpg");
            array_map('unlink', $fotosCarpeta);


            foreach ($data as $index => $dato) {
                $fotoRepositorio = RepositorioFotosPersonas . $dato['nrodocumento'] . ".jpg";
                $fotoCarpeta = $carpeta . "/" . $dato['nrodocumento'] . ".jpg";
                if (file_exists($fotoRepositorio)) {
                    if (copy($fotoRepositorio, $fotoCarpeta)) {
                        $imagick = new \Imagick(realpath($fotoCarpeta));
                        $imagick->stripImage();
                        $imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                        $imagick->setImageResolution(300, 300);
                        $imagick->cropThumbnailImage(240, 288);
                        $imagick->setImageCompression(imagick::COMPRESSION_JPEG);
                        $imagick->setImageCompressionQuality(20);
                        $imagick->writeImage(realpath($fotoCarpeta));
                    }
                }
            }
        } catch (\Exception $e) {
            return 0;
        }
        return 1;
    }

    public function selectPedidoEstado(Request $request) {
        $pedido_id=$request->pedido_id;
        //$pedido_estado=$request->pedido_estado;
        $pedido = Pedido::findOrFail($pedido_id);
        
        if ($pedido->estado == 0) {
            $selectPedidoEstadosOpcion= '<option value="0" selected="selected"  >   Estado : Creado </option>';
            $claseSelect='btn-default';
        } else {
            $selectPedidoEstadosOpcion= '<option value="0" >   Estado : Creado </option>';
        }

        if ($pedido->estado == 1) {
            $claseSelect='btn-success';
            $selectPedidoEstadosOpcion .= '<option value="1" selected="selected"  >   Estado : Abierto </option>';
        } else {
            $selectPedidoEstadosOpcion .= '<option value="1" >   Estado : Abierto </option>';
        }

        if ($pedido->estado == 2) {
            $claseSelect='btn-danger';
            $selectPedidoEstadosOpcion .= '<option value="2" selected="selected"  >   Estado : Cerrado </option>';
        } else {
            $selectPedidoEstadosOpcion .= '<option value="2" >   Estado : Cerrado </option>';
        }

        if ($pedido->estado == 3) {
            $claseSelect='btn-primary';
            $selectPedidoEstadosOpcion .= '<option value="3" selected="selected"  >   Estado : Prueba </option>';
        } else {
            $selectPedidoEstadosOpcion .= '<option value="3" >   Estado : Prueba </option>';
        }

        $selectPedidoEstados = '<select onchange="cambiarEstadoPedido();" id="pedido_estado" class="btn btn-lg '.$claseSelect.'">';

         $selectPedidoEstados.=$selectPedidoEstadosOpcion;
        
        $selectPedidoEstados .= "</select>";

        return $selectPedidoEstados;
    }
    
    public function cambioEstadoPedido(Request $request) {
        try {
            $pedido_id=$request->pedido_id;
            $pedido_estado=$request->pedido_estado;
            
            if ($pedido_estado==1 || $pedido_estado==3 ){
                    $estudiantesSinu = EstudianteSinu::where([
                        ['pedido_id', '=', $pedido_id]
                    ])->get();
                    if (count($estudiantesSinu)==0){
                          return response()->json(['status' => 'warning', 'message' => 'No existen estudiantes SINU, por favor ejecutar el Paso 01'], 200);
                    }
                    $estudiantesPedido = Estudiante_Pedido::where([
                        ['pedido_id', '=', $pedido_id]
                    ])->get();
                    if (count($estudiantesPedido)==0){
                          return response()->json(['status' => 'warning', 'message' => 'No existen estudiantes para el PEDIDO, por favor ejecutar el Paso 04'], 200);
                    }
            }
            
            $pedido = Pedido::findOrFail($pedido_id);
            if ($pedido_estado==2 ){
                 $now = new \DateTime();
                 $fechaActual = $now->format('Y-m-d H:i:s');
                 $pedido->fec_cierre = $fechaActual;
            }
            $pedido->estado = $request->pedido_estado;
            $pedido->save();
 
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => 'Error al cambiar el estado'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Cambio de estado satisfactoriamente'], 200);
    }

    public function querys($query, $anio) {


        switch ($query) {
            case 'query_listar_update_codalumno':
                return "select  COD_ALUMNOV2 as codalumno,id_alum_programa from (
SELECT 
id_alum_programa,
 LPAD(TO_CHAR((SELECT MAX(DISTINCT SUBSTR(  src_alum_programa.COD_ALUMNO,1,6)) FROM SRC_ALUM_PROGRAMA)+rownum   ),6,'0')   ||SUBSTR(  codprograma,2,5) 
COD_ALUMNOV2
 FROM (
SELECT 
    src_alum_programa.cod_alumno as COD_ALUMNO,
    pri_apellido AS apepat, 
    seg_apellido AS APEMAT, 
    nom_tercero ||' '|| seg_nombre AS nombre,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') docu_tip,
    num_identificacion as docu_num, 
    SUBSTR(src_alum_programa.cod_periodo,1,5) as ano_ing_completo,
    SUBSTR(src_alum_programa.cod_periodo,1,4) as ano_ing,
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01') as per_ing,
    src_alum_programa.id_alum_programa AS id_alum_programa,
    bas_dependencia.id_dependencia AS idfacultad, 
    bas_dependencia.nom_dependencia AS nomfacultad, 
    bas_dependencia.COD_dependencia AS abreviaturafacultad, 
    src_uni_academica.cod_unidad AS codprograma, 
    src_uni_academica.nom_unidad as nomprograma,
    src_uni_academica.cod_modalidad AS codmodalidad, 
    (select nom_tabla from src_generica where tip_tabla='TIPMOD' and cod_tabla=src_uni_academica.COD_MODALIDAD)modalidad_desc,
    src_uni_academica.NIV_FORMACION,
    decode(src_uni_academica.niv_formacion,'4','UNIVERSITARIA', '5','ESPECIALIDAD', '6', 'MAESTRIA', '7','DOCTORADO', '9','DIPLOMADO') AS formacion,
    (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) estado
FROM src_enc_matricula
LEFT JOIN src_enc_liquidacion
    ON src_enc_liquidacion.cod_periodo       = src_enc_matricula.cod_periodo
    AND src_enc_liquidacion.id_alum_programa = src_enc_matricula.id_alum_programa
INNER JOIN src_alum_programa
    ON src_enc_matricula.id_alum_programa = src_alum_programa.id_alum_programa
inner join src_mat_pensum
    on src_mat_pensum.cod_unidad = src_alum_programa.cod_unidad
    and src_mat_pensum.cod_pensum = src_alum_programa.cod_pensum
    and src_mat_pensum.cod_materia = src_enc_matricula.cod_materia
inner join src_uni_academica
    on src_uni_academica.cod_unidad = src_alum_programa.cod_unidad
inner join bas_dependencia
    on bas_dependencia.id_dependencia = src_uni_academica.id_dependencia
INNER JOIN bas_tercero
    on bas_tercero.id_tercero = src_alum_programa.id_tercero
where 
    src_mat_pensum.cod_modalidad IN (1,2) 
        and src_enc_liquidacion.est_liquidacion=2
    and src_enc_matricula.cod_periodo like '" . $anio . "%' 
    and src_enc_matricula.cod_periodo not in ('" . $anio . "V','" . $anio . "C','" . $anio . "D')
    and bas_dependencia.cod_dependencia NOT IN ('DURIN','DUBU','DURS','UGBN') 
    and src_alum_programa.EST_ALUMNO not in (2)
    
GROUP BY 
    src_alum_programa.cod_alumno ,
    pri_apellido , 
    seg_apellido , 
    nom_tercero ||' '|| seg_nombre ,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') ,
    num_identificacion , 
    SUBSTR(src_alum_programa.cod_periodo,1,5) ,
    SUBSTR(src_alum_programa.cod_periodo,1,4),
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01'),
    src_alum_programa.id_alum_programa,
    '313', 
    bas_dependencia.num_apartado,
    bas_dependencia.id_dependencia, 
    bas_dependencia.nom_dependencia,
    bas_dependencia.COD_dependencia,
    src_uni_academica.cod_anterior ,
    src_uni_academica.cod_unidad, 
    src_uni_academica.nom_unidad, 
    src_uni_academica.cod_modalidad, 
    src_uni_academica.niv_formacion,
    src_alum_programa.est_alumno 
  
  ) A where COD_ALUMNO is null
  )  ";
  
  
  case 'query_codalumno_no_corresponde':
  return "    SELECT 
        id_alum_programa, 
        SUBSTR(  src_alum_programa.COD_UNIDAD,2,4) AS UNIDAD,
        SUBSTR(  src_alum_programa.COD_ALUMNO,7,4) AS UNIALUMNO,
        COD_ALUMNO 
    FROM  src_alum_programa
    WHERE  SUBSTR(  src_alum_programa.COD_UNIDAD,2,4)<>SUBSTR(  src_alum_programa.COD_ALUMNO,7,4)     
    ";

  //colocar esto para limitar los registro en la linea arriba desde del parentesis where rownum <10
  
            case 'query_listar_alumnos':
                return "

SELECT 

CASE WHEN  BG2.NOM_DIV_GEOPOLITICA ='Perú' THEN  BG.NOM_DIV_GEOPOLITICA ELSE  BG2.NOM_DIV_GEOPOLITICA END AS  DEPARTAMENTO ,

BG.NOM_DIV_GEOPOLITICA as DISTRITO,
to_char(bas_tercero.fec_nacimiento,'dd/MM/YYYY') as FEC_NAC, 
case when upper(bas_tercero.gen_tercero)='M' THEN 1 ELSE 2 END as GENERO,
upper(bas_tercero.dir_email) as email,
3 as pedido_id,
    TABLA_NIVEL_ACTUAL.NIVEL AS NIVEL,
    src_alum_programa.cod_alumno as CODALUMNO,
    pri_apellido AS apepaterno, 
    seg_apellido AS apematerno, 
    nom_tercero ||' '|| seg_nombre AS nombre,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') tipodocumento,
    num_identificacion as nrodocumento, 
    SUBSTR(src_alum_programa.cod_periodo,1,5) as ano_ing_completo,
    SUBSTR(src_alum_programa.cod_periodo,1,4) as ano_ing,
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01') as per_ing,
    src_alum_programa.id_alum_programa AS id_alum_programa,
    bas_dependencia.id_dependencia AS codfacultad, 
    bas_dependencia.nom_dependencia AS nomfacultad, 
    bas_dependencia.COD_dependencia AS abreviaturafacultad, 
    src_uni_academica.cod_unidad AS codprograma, 
    src_uni_academica.nom_unidad as nomprograma,
    src_uni_academica.cod_modalidad AS codmodalidad, 
    (select nom_tabla from src_generica where tip_tabla='TIPMOD' and cod_tabla=src_uni_academica.COD_MODALIDAD)modalidad_desc,
    src_uni_academica.NIV_FORMACION,
	(select  UPPER(nom_tabla)  from src_generica where tip_tabla='NIVFOR'  and cod_tabla=src_uni_academica.NIV_FORMACION) AS formacion,
    (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) estado,
   upper( src_alum_programa.cod_pensum) cod_pensum,
   CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001' ) then 999 else bas_dependencia.id_dependencia end AS cod_escuela,
    CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001'  ) then 'ETM' else bas_dependencia.COD_dependencia end AS abreviatura_escuela
FROM src_enc_matricula
LEFT JOIN src_enc_liquidacion
    ON src_enc_liquidacion.cod_periodo       = src_enc_matricula.cod_periodo
    AND src_enc_liquidacion.id_alum_programa = src_enc_matricula.id_alum_programa
INNER JOIN src_alum_programa
    ON src_enc_matricula.id_alum_programa = src_alum_programa.id_alum_programa
inner join src_mat_pensum
    on src_mat_pensum.cod_unidad = src_alum_programa.cod_unidad
    and src_mat_pensum.cod_pensum = src_alum_programa.cod_pensum
    and src_mat_pensum.cod_materia = src_enc_matricula.cod_materia
inner join src_uni_academica
    on src_uni_academica.cod_unidad = src_alum_programa.cod_unidad
inner join bas_dependencia
    on bas_dependencia.id_dependencia = src_uni_academica.id_dependencia
INNER JOIN bas_tercero
    on bas_tercero.id_tercero = src_alum_programa.id_tercero

LEFT join BAS_GEOPOLITICA BG
on bas_tercero.ID_UBI_NAC=BG.id_geopolitica 
LEFT join BAS_GEOPOLITICA BG2
on BG.ID_PADRE=BG2.id_geopolitica

LEFT JOIN (
SELECT 
     id_alum_programa,
     min(num_nivel) nivel 
FROM (  
      select
            resumen.id_alum_programa,
            resumen.cod_unidad,
            resumen.cod_pensum,
            resumen.cod_materia,
            src_mat_pensum.num_nivel
      from (
             select 
                   src_alum_programa.id_alum_programa,
                   src_mat_pensum.cod_unidad,
                   src_mat_pensum.cod_pensum,
                   src_mat_pensum.COD_MATERIA
             from src_alum_programa
             inner join src_mat_pensum on src_mat_pensum.COD_UNIDAD = src_alum_programa.COD_UNIDAD
             and src_mat_pensum.COD_PENSUM = src_alum_programa.COD_PENSUM
             and src_mat_pensum.IND_ELECTIVA=0
             where src_alum_programa.COD_UNIDAD in (select cod_unidad from src_uni_academica where cod_modalidad IN (1,2)) 
             minus
             SELECT
                   src_alum_programa.id_alum_programa,
                   src_alum_programa.cod_unidad,
                   src_alum_programa.cod_pensum,
                   src_his_academica.COD_MATERIA
             from src_alum_programa
             inner join src_his_academica on src_alum_programa.id_alum_programa=src_his_academica.id_alum_programa
             and (src_his_academica.IND_APROBADA=1 or src_his_academica.IND_APROBADA_ALFA=1)
             where src_alum_programa.COD_UNIDAD in (select cod_unidad from src_uni_academica where cod_modalidad IN (1,2)) 

            )resumen
      inner join src_mat_pensum on resumen.cod_unidad=src_mat_pensum.cod_unidad
      and resumen.cod_pensum=src_mat_pensum.cod_pensum
      and resumen.cod_materia=src_mat_pensum.cod_materia
     )
GROUP BY ID_ALUM_PROGRAMA
)    TABLA_NIVEL_ACTUAL ON TABLA_NIVEL_ACTUAL.ID_ALUM_PROGRAMA= src_alum_programa.id_alum_programa
where 
    src_mat_pensum.cod_modalidad IN (1,2) 
        and src_enc_liquidacion.est_liquidacion=2
    and src_enc_matricula.cod_periodo like '" . $anio . "%' 
    and src_enc_matricula.cod_periodo not in ('" . $anio . "V','" . $anio . "C','" . $anio . "D')
    and bas_dependencia.cod_dependencia NOT IN ('DURIN','DUBU','DURS','UGBN') 
    and src_alum_programa.EST_ALUMNO not in (2)
	and  src_alum_programa.cod_alumno is not null
	
	
GROUP BY 
BG2.NOM_DIV_GEOPOLITICA,


BG.NOM_DIV_GEOPOLITICA,
bas_tercero.dir_email,
 upper(bas_tercero.gen_tercero),
 bas_tercero.fec_nacimiento,
    src_alum_programa.cod_alumno ,
    pri_apellido , 
    seg_apellido , 
    nom_tercero ||' '|| seg_nombre ,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') ,
    num_identificacion , 
    SUBSTR(src_alum_programa.cod_periodo,1,5) ,
    SUBSTR(src_alum_programa.cod_periodo,1,4),
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01'),
    src_alum_programa.id_alum_programa,
    bas_dependencia.num_apartado,
    bas_dependencia.id_dependencia, 
    bas_dependencia.nom_dependencia,
    bas_dependencia.COD_dependencia,
    src_uni_academica.cod_anterior ,
    src_uni_academica.cod_unidad, 
    src_uni_academica.nom_unidad, 
    src_uni_academica.cod_modalidad, 
    src_uni_academica.niv_formacion,
    src_alum_programa.est_alumno,
    src_alum_programa.cod_pensum,
     TABLA_NIVEL_ACTUAL.NIVEL
order by 
    src_alum_programa.cod_alumno desc, (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) desc";

            case 'ssdsdsd':
                echo "i es igual a 2";
                break;
        }
    }

}
