<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PedidoFotocheck;
use App\PedidoCerrado;
use App\SolicitudSunedu;
use App\EstudianteSunedu;
use App\EstudianteSinu;
use App\Estudiante_Pedido;
use App\EstudianteFotocheck;
use App\FacultadSunedu;
use App\ProgramaSunedu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use Yajra\Datatables\Datatables;
use Excel;
use Imagick;
use ImagickPixel;
use Barryvdh\DomPDF\Facade as PDF;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

class Fotocheck_SincronizarController extends Controller {

    protected $seguridad;
    protected $anio;

    public function __construct() {
        $this->seguridad = new PermisosController();
        $this->anio = date("Y");
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        return view('backEnd.fotochecksincronizar.index');
    }

    public function sincronizarImagenes(Request $request) {
        $nrodocumento = $request->nrodocumento;
        $anio = $this->anio;
        ini_set('max_execution_time', -1);
        try {
            $carpeta = storage_path('app/public') . '/fotochecks/fotos';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }

            if (!is_null($nrodocumento)) {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.codalumno')
                        ->where('estudiantes_fotocheck.nrodocumento', $nrodocumento)
                        ->get();
                $data = json_decode(json_encode($EstudiantesFotocheck), true);
                $fotosCarpeta = glob($carpeta . "/" . $nrodocumento . ".jpg");
                array_map('unlink', $fotosCarpeta);
                \Artisan::call('cache:clear');
            } else {
                $EstudiantesFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.nrodocumento')
                        ->get();
                $data = json_decode(json_encode($EstudiantesFotocheck), true);
            }


            if (!is_null($nrodocumento)) {
                foreach ($data as $index => $dato) {
                    $fotoRepositorio = RepositorioFotosPersonas . $dato['nrodocumento'] . ".jpg";
                    $fotoCarpeta = $carpeta . "/" . $dato['nrodocumento'] . ".jpg";


                    /* Incio:Actualizar Informacion */
                    $data = DB::connection('oracle_prod')->select($this->querys('query_listar_alumno', $anio, $dato['codalumno']));

                    foreach ($data as $index => $dato) {


                        $foto = '0';
                        if (file_exists($fotoRepositorio)) {
                            $foto = '1';
                        }

                        DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                                ->where('codalumno', $dato->codalumno)
                                ->update(
                                        [
                                            'foto' => $foto,
                                            'apepaterno' => $dato->apepaterno,
                                            'apematerno' => $dato->apematerno,
                                            'nombre' => $dato->nombre,
                                            'tipodocumento' => $dato->tipodocumento,
                                            'nrodocumento' => $dato->nrodocumento,
                                            'ano_ing' => $dato->ano_ing,
                                            'per_ing' => $dato->per_ing,
                                            'id_alum_programa' => $dato->id_alum_programa,
                                            'codfacultad' => $dato->codfacultad,
                                            'nomfacultad' => $dato->nomfacultad,
                                            'codprograma' => $dato->codprograma,
                                            'nomprograma' => $dato->nomprograma,
                                            'codmodalidad' => $dato->codmodalidad,
                                            'formacion' => $dato->formacion,
                                            'abreviaturafacultad' => $dato->abreviaturafacultad,
                                            'modalidad_desc' => $dato->modalidad_desc,
                                            'niv_formacion' => $dato->niv_formacion,
                                            'ano_ing_completo' => $dato->ano_ing_completo,
                                            'nivel' => $dato->nivel,
                                            'ultimo_nivel_cursado' => $dato->ultimo_nivel_cursado,
                                            'email' => $dato->email,
                                            'genero' => $dato->genero,
                                            'fec_nac' => $dato->fec_nac,
                                            'departamento' => $dato->departamento,
                                            'distrito' => $dato->distrito,
                                            'codpensum' => $dato->cod_pensum,
                                            'val_periodicidad' => $dato->val_periodicidad,
                                            'dur_programa' => $dato->dur_programa,
                                            'cod_periodo_matricula' => $dato->cod_periodo_matricula,
                                            'cod_escuela' => $dato->cod_escuela,
                                            'abreviatura_escuela' => $dato->abreviatura_escuela,
                                            'cod_estado_sinu' => $dato->cod_estado,
                                            'estado_sinu' => $dato->estado
                                        ]
                        );
                    }

                    /* Fin: Actualizar Informacion */



                    if (file_exists($fotoRepositorio)) {
                        if (copy($fotoRepositorio, $fotoCarpeta)) {
                            $imagick = new \Imagick(realpath($fotoCarpeta));
                            $imagick->stripImage();
                            $imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                            $imagick->setImageResolution(300, 300);
                            $imagick->cropThumbnailImage(240, 288);
                            $imagick->setImageCompression(imagick::COMPRESSION_JPEG);
                            $imagick->setImageCompressionQuality(20);
                            $imagick->writeImage(realpath($fotoCarpeta));
                        }
                    } else {
                        return response()->json(['status' => 'success', 'message' => 'No se encontro foto en repositorio', 'valor' => 0], 200);
                    }
                }
            } else {
                foreach ($data as $index => $dato) {
                    $fotoRepositorio = RepositorioFotosPersonas . $dato['nrodocumento'] . ".jpg";
                    $fotoCarpeta = $carpeta . "/" . $dato['nrodocumento'] . ".jpg";
                    if (file_exists($fotoRepositorio)) {
                        if (copy($fotoRepositorio, $fotoCarpeta)) {
                            $imagick = new \Imagick(realpath($fotoCarpeta));
                            $imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                            $imagick->setImageResolution(300, 300);
                            $imagick->cropThumbnailImage(240, 288);
                            $imagick->setImageCompression(imagick::COMPRESSION_JPEG);
                            $imagick->setImageCompressionQuality(20);
                            $imagick->writeImage(realpath($fotoCarpeta));
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'warning', 'message' => $e, 'data' => 'Error al guardar'], 200);
        }
        return response()->json(['status' => 'success', 'message' => 'Sincronización realizada exitosamente', 'valor' => 1], 200);
    }

    public function generarCodigosEstudiantesSinu(Request $request) {
        ini_set('max_execution_time', 600);
        $now = new \DateTime();
        $anio = $this->anio;


        DB::connection('oracle_prod')->beginTransaction();
        try {
            $dataAlumnosNoCorrespondeCodalumno = DB::connection('oracle_prod')->select($this->querys('query_codalumno_no_corresponde', $anio, null));
            $totalAlumnosNoCorrespondeCodalumno = count($dataAlumnosNoCorrespondeCodalumno);

            $j = 0;
            foreach ($dataAlumnosNoCorrespondeCodalumno as $index => $dato) {
                $ejecutar = DB::connection('oracle_prod')->table('src_alum_programa')->where('id_alum_programa', $dato->id_alum_programa)
                        ->where('cod_alumno', $dato->cod_alumno)
                        ->update(['cod_alumno' => null]);
                if ($ejecutar) {
                    $j++;
                }
            }

            $data = DB::connection('oracle_prod')->select($this->querys('query_listar_update_codalumno', $anio, null));
            $totalRegistrosActualizar = count($data);

            $i = 0;
            foreach ($data as $index => $dato) {
                $ejecutar = DB::connection('oracle_prod')->table('src_alum_programa')->where('id_alum_programa', $dato->id_alum_programa)
                        ->where('cod_alumno', null)
                        ->update(['cod_alumno' => $dato->codalumno]);
                if ($ejecutar) {
                    $i++;
                }
            }
        } catch (\Exception $e) {
            DB::connection('oracle_prod')->rollBack();
            return response()->json(['status' => 'warning', 'message' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>'], 200);
        }
        DB::connection('oracle_prod')->commit();

        /*  $i = 0;
          $totalRegistrosActualizar = 0; */

        return response()->json(['status' => 'success', 'message' => '<br><div class="alert alert-success"><strong>Correcto!</strong> Se han limpiado ' . $j . ' codigos de alumno que no correspondian con el programa de un total de  ' . $totalAlumnosNoCorrespondeCodalumno . ' registros </div><br><div class="alert alert-success"><strong>Correcto!</strong> Se han generado ' . $i . ' codigos de alumno de un total de ' . $totalRegistrosActualizar . ' registros </div>'], 200);
    }

    public function sincronizarEstudiantes() {
        //return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros. </div>', 'message' => 'Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros.', 'messageFinalizado' => 'Proceso Finalizado!', 'messageFinalizadoHtml' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>', 'info' => '1'], 200);
        ini_set('max_execution_time', 600);
        // $anio = date("Y");
        $now = new \DateTime();
        $anio = $this->anio;


        try {
            $data = DB::connection('oracle_prod')->select($this->querys('query_listar_alumnos', $anio, null));
            $totalRegistrosInsertar = count($data);

            DB::connection('pgsql_syscarnes')->beginTransaction();

            $totalRegistrosExistentes = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->count();



            $i = 0;
            foreach ($data as $index => $dato) {

                $validar = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->where([
                            ['codalumno', '=', $dato->codalumno]
                        ])->first();

                $fotoRepositorio = RepositorioFotosPersonas . $dato->nrodocumento . ".jpg";
                $foto = '0';
                if (file_exists($fotoRepositorio)) {
                    $foto = '1';
                }

                if (count($validar) == 0) {

                    $ejecutar = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')->insert(
                            array(
                                'foto' => $foto,
                                'codalumno' => strtoupper($dato->codalumno),
                                'apepaterno' => $dato->apepaterno,
                                'apematerno' => $dato->apematerno,
                                'nombre' => $dato->nombre,
                                'tipodocumento' => $dato->tipodocumento,
                                'nrodocumento' => $dato->nrodocumento,
                                'ano_ing' => $dato->ano_ing,
                                'per_ing' => $dato->per_ing,
                                'id_alum_programa' => $dato->id_alum_programa,
                                'codfacultad' => $dato->codfacultad,
                                'nomfacultad' => $dato->nomfacultad,
                                'codprograma' => strtoupper($dato->codprograma),
                                'nomprograma' => strtoupper($dato->nomprograma),
                                'codmodalidad' => $dato->codmodalidad,
                                'formacion' => $dato->formacion,
                                'abreviaturafacultad' => $dato->abreviaturafacultad,
                                'modalidad_desc' => $dato->modalidad_desc,
                                'niv_formacion' => $dato->niv_formacion,
                                'ano_ing_completo' => $dato->ano_ing_completo,
                                'nivel' => $dato->nivel,
                                'ultimo_nivel_cursado' => $dato->ultimo_nivel_cursado,
                                'email' => $dato->email,
                                'genero' => $dato->genero,
                                'fec_nac' => $dato->fec_nac,
                                'departamento' => $dato->departamento,
                                'distrito' => $dato->distrito,
                                'codpensum' => $dato->cod_pensum,
                                'val_periodicidad' => $dato->val_periodicidad,
                                'dur_programa' => $dato->dur_programa,
                                'cod_periodo_matricula' => $dato->cod_periodo_matricula,
                                'cod_escuela' => $dato->cod_escuela,
                                'abreviatura_escuela' => $dato->abreviatura_escuela,
                                'cod_estado_sinu' => $dato->cod_estado,
                                'estado_sinu' => $dato->estado
                            )
                    );
                    if ($ejecutar) {
                        $i++;
                    }
                } else {

                    $ejecutar = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                            ->where('codalumno', $dato->codalumno)
                            ->update(
                            [
                                'foto' => $foto,
                                'apepaterno' => $dato->apepaterno,
                                'apematerno' => $dato->apematerno,
                                'nombre' => $dato->nombre,
                                'tipodocumento' => $dato->tipodocumento,
                                'nrodocumento' => $dato->nrodocumento,
                                'ano_ing' => $dato->ano_ing,
                                'per_ing' => $dato->per_ing,
                                'id_alum_programa' => $dato->id_alum_programa,
                                'codfacultad' => $dato->codfacultad,
                                'nomfacultad' => $dato->nomfacultad,
                                'codprograma' => strtoupper($dato->codprograma),
                                'nomprograma' => strtoupper($dato->nomprograma),
                                'codmodalidad' => $dato->codmodalidad,
                                'formacion' => $dato->formacion,
                                'abreviaturafacultad' => $dato->abreviaturafacultad,
                                'modalidad_desc' => $dato->modalidad_desc,
                                'niv_formacion' => $dato->niv_formacion,
                                'ano_ing_completo' => $dato->ano_ing_completo,
                                'nivel' => $dato->nivel,
                                'ultimo_nivel_cursado' => $dato->ultimo_nivel_cursado,
                                'email' => $dato->email,
                                'genero' => $dato->genero,
                                'fec_nac' => $dato->fec_nac,
                                'departamento' => $dato->departamento,
                                'distrito' => $dato->distrito,
                                'codpensum' => $dato->cod_pensum,
                                'val_periodicidad' => $dato->val_periodicidad,
                                'dur_programa' => $dato->dur_programa,
                                'cod_periodo_matricula' => $dato->cod_periodo_matricula,
                                'cod_escuela' => $dato->cod_escuela,
                                'abreviatura_escuela' => $dato->abreviatura_escuela,
                                'cod_estado_sinu' => $dato->cod_estado,
                                'estado_sinu' => $dato->estado
                            ]
                    );
                }
            }


            /* INICIO:Actualizacion de estado cuando se retiro de programa pero en programa nivelacion aun permanece en ACTIVO */
            DB::connection('pgsql_syscarnes')->select("update estudiantes_fotocheck SET cod_estado_sinu = 0, estado_sinu='Inactivo' from (
                        select codalumno from estudiantes_fotocheck where codprograma='PA001' and cod_estado_sinu =1 and nrodocumento  in 
                        (
                        select nrodocumento from estudiantes_fotocheck where codprograma<>'PA001' and nrodocumento in (
                        select  nrodocumento from estudiantes_fotocheck where  cod_periodo_matricula like '$anio%' 
                         group by nrodocumento having count(nrodocumento)>1 
                         )
                         )
                         ) A where estudiantes_fotocheck.codalumno = A.codalumno ");

            /* FIN:Actualizacion de estado cuando se retiro de programa pero en programa nivelacion aun permanece en ACTIVO */

            /* INICIO:Actualizar la fecha de caducidad */
            DB::connection('pgsql_syscarnes')->select("update estudiantes_fotocheck set fec_vencimiento=A.fec_cadu_propuesta from (
                            select codalumno,  fec_cadu_propuesta from vst_estudiantes_fotocheck_fecha_caducidad) A
                            where estudiantes_fotocheck.codalumno=A.codalumno  ");

            /* FIN:Actualizar la fecha de caducidad */
        } catch (\Exception $e) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'warning', 'messageHtml' => '<div class="alert alert-danger">Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage() . '</div>', 'message' => 'Ha ocurrido un error con el codigo ' . $e->getCode() . ' nombre: ' . $e->getMessage(), 'info' => '0'], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();

        return response()->json(['status' => 'success', 'messageHtml' => '<div class="alert alert-success"><strong>Correcto!</strong> Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros. </div>', 'message' => 'Se han insertado ' . $i . ' de un total de ' . $totalRegistrosInsertar . ' estudiantes. La ultima vez se insertaron ' . $totalRegistrosExistentes . ' registros.', 'messageFinalizado' => 'Proceso Finalizado!', 'messageFinalizadoHtml' => '<div class="alert alert-success"><strong>Proceso Finalizado!</strong></div>', 'info' => '1'], 200);
    }

    public function querys($query, $anio, $codalumno) {


        switch ($query) {
            case 'query_listar_update_codalumno':
                return "select  COD_ALUMNOV2 as codalumno,id_alum_programa from (
SELECT 
id_alum_programa,
 LPAD(TO_CHAR((SELECT MAX(DISTINCT SUBSTR(  src_alum_programa.COD_ALUMNO,1,6)) FROM SRC_ALUM_PROGRAMA)+rownum   ),6,'0')   ||SUBSTR(  codprograma,2,5) 
COD_ALUMNOV2
 FROM (
SELECT 
   upper(src_alum_programa.cod_alumno) as COD_ALUMNO,
    pri_apellido AS apepat, 
    seg_apellido AS APEMAT, 
    nom_tercero ||' '|| seg_nombre AS nombre,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') docu_tip,
    num_identificacion as docu_num, 
    SUBSTR(src_alum_programa.cod_periodo,1,5) as ano_ing_completo,
    SUBSTR(src_alum_programa.cod_periodo,1,4) as ano_ing,
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01') as per_ing,
    src_alum_programa.id_alum_programa AS id_alum_programa,
    bas_dependencia.id_dependencia AS idfacultad, 
    bas_dependencia.nom_dependencia AS nomfacultad, 
    bas_dependencia.COD_dependencia AS abreviaturafacultad, 
    upper(src_uni_academica.cod_unidad) AS codprograma, 
    upper(src_uni_academica.nom_unidad) as nomprograma,
    src_uni_academica.cod_modalidad AS codmodalidad, 
    (select nom_tabla from src_generica where tip_tabla='TIPMOD' and cod_tabla=src_uni_academica.COD_MODALIDAD)modalidad_desc,
    src_uni_academica.NIV_FORMACION,
    (select nom_tabla from src_generica where tip_tabla='NIVFOR' and cod_tabla=src_uni_academica.niv_formacion) as formacion,
    (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) estado
FROM src_enc_matricula
LEFT JOIN src_enc_liquidacion
    ON src_enc_liquidacion.cod_periodo       = src_enc_matricula.cod_periodo
    AND src_enc_liquidacion.id_alum_programa = src_enc_matricula.id_alum_programa
INNER JOIN src_alum_programa
    ON src_enc_matricula.id_alum_programa = src_alum_programa.id_alum_programa
inner join src_mat_pensum
    on src_mat_pensum.cod_unidad = src_alum_programa.cod_unidad
    and src_mat_pensum.cod_pensum = src_alum_programa.cod_pensum
    and src_mat_pensum.cod_materia = src_enc_matricula.cod_materia
inner join src_uni_academica
    on src_uni_academica.cod_unidad = src_alum_programa.cod_unidad
inner join bas_dependencia
    on bas_dependencia.id_dependencia = src_uni_academica.id_dependencia
INNER JOIN bas_tercero
    on bas_tercero.id_tercero = src_alum_programa.id_tercero
where 
    
    src_enc_matricula.cod_periodo like '" . $anio . "%' 
          and src_enc_liquidacion.est_liquidacion=2
    and src_enc_matricula.cod_periodo not in ('" . $anio . "V','" . $anio . "C','" . $anio . "D')
     and src_alum_programa.EST_ALUMNO  in (1)
    
GROUP BY 
    src_alum_programa.cod_alumno ,
    pri_apellido , 
    seg_apellido , 
    nom_tercero ||' '|| seg_nombre ,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') ,
    num_identificacion , 
    SUBSTR(src_alum_programa.cod_periodo,1,5) ,
    SUBSTR(src_alum_programa.cod_periodo,1,4),
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01'),
    src_alum_programa.id_alum_programa,
    '313', 
    bas_dependencia.num_apartado,
    bas_dependencia.id_dependencia, 
    bas_dependencia.nom_dependencia,
    bas_dependencia.COD_dependencia,
    src_uni_academica.cod_anterior ,
    src_uni_academica.cod_unidad, 
    src_uni_academica.nom_unidad, 
    src_uni_academica.cod_modalidad, 
    src_uni_academica.niv_formacion,
    src_alum_programa.est_alumno 
  
  ) A where COD_ALUMNO is null
  )  ";


            case 'query_codalumno_no_corresponde':
                return "    SELECT 
        id_alum_programa, 
        SUBSTR(  src_alum_programa.COD_UNIDAD,2,4) AS UNIDAD,
        SUBSTR(  src_alum_programa.COD_ALUMNO,7,4) AS UNIALUMNO,
        COD_ALUMNO 
    FROM  src_alum_programa
    WHERE  SUBSTR(  src_alum_programa.COD_UNIDAD,2,4)<>SUBSTR(  src_alum_programa.COD_ALUMNO,7,4)     
    ";

            //colocar esto para limitar los registro en la linea arriba desde del parentesis where rownum <10

            case 'query_listar_alumnos':
                return "
SELECT 
max(src_enc_matricula.cod_periodo) as cod_periodo_matricula,
src_uni_academica.VAL_PERIODICIDAD,
src_uni_academica.DUR_PROGRAMA,
CASE WHEN  BG2.NOM_DIV_GEOPOLITICA ='Perú' THEN  BG.NOM_DIV_GEOPOLITICA ELSE  BG2.NOM_DIV_GEOPOLITICA END AS  DEPARTAMENTO ,

BG.NOM_DIV_GEOPOLITICA as DISTRITO,
to_char(bas_tercero.fec_nacimiento,'dd/MM/YYYY') as FEC_NAC, 
case when upper(bas_tercero.gen_tercero)='M' THEN 1 ELSE 2 END as GENERO,
upper(bas_tercero.dir_email) as email,
    TABLA_NIVEL_ACTUAL.NIVEL   AS NIVEL,
    CASE WHEN TABLA_NIVEL_ACTUAL.NIVEL IS NULL THEN TABLA_ULTIMO_NIVEL_CURSADO.NIVEL ELSE TABLA_NIVEL_ACTUAL.NIVEL END  ULTIMO_NIVEL_CURSADO,
    upper(src_alum_programa.cod_alumno) as CODALUMNO,
    pri_apellido AS apepaterno, 
    seg_apellido AS apematerno, 
    nom_tercero ||' '|| seg_nombre AS nombre,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') tipodocumento,
    num_identificacion as nrodocumento, 
    SUBSTR(src_alum_programa.cod_periodo,1,5) as ano_ing_completo,
    SUBSTR(src_alum_programa.cod_periodo,1,4) as ano_ing,
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01') as per_ing,
    src_alum_programa.id_alum_programa AS id_alum_programa,
    bas_dependencia.id_dependencia AS codfacultad, 
    bas_dependencia.nom_dependencia AS nomfacultad, 
    bas_dependencia.COD_dependencia AS abreviaturafacultad, 
    upper(src_uni_academica.cod_unidad) AS codprograma, 
    upper(src_uni_academica.nom_unidad) as nomprograma,
    src_uni_academica.cod_modalidad AS codmodalidad, 
    (select nom_tabla from src_generica where tip_tabla='TIPMOD' and cod_tabla=src_uni_academica.COD_MODALIDAD)modalidad_desc,
    src_uni_academica.NIV_FORMACION,
	(select  UPPER(nom_tabla)  from src_generica where tip_tabla='NIVFOR'  and cod_tabla=src_uni_academica.NIV_FORMACION) AS formacion,
    (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) estado,
    src_alum_programa.est_alumno cod_estado,
   upper( src_alum_programa.cod_pensum) cod_pensum,
   CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001'  ) then 999 else bas_dependencia.id_dependencia end AS cod_escuela,
    CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001'  ) then 'ETM' else bas_dependencia.COD_dependencia end AS abreviatura_escuela
    
FROM src_enc_matricula
LEFT JOIN src_enc_liquidacion
    ON src_enc_liquidacion.cod_periodo       = src_enc_matricula.cod_periodo
    AND src_enc_liquidacion.id_alum_programa = src_enc_matricula.id_alum_programa
INNER JOIN src_alum_programa
    ON src_enc_matricula.id_alum_programa = src_alum_programa.id_alum_programa
inner join src_mat_pensum
    on src_mat_pensum.cod_unidad = src_alum_programa.cod_unidad
    and src_mat_pensum.cod_pensum = src_alum_programa.cod_pensum
    and src_mat_pensum.cod_materia = src_enc_matricula.cod_materia
inner join src_uni_academica
    on src_uni_academica.cod_unidad = src_alum_programa.cod_unidad
inner join bas_dependencia
    on bas_dependencia.id_dependencia = src_uni_academica.id_dependencia
INNER JOIN bas_tercero
    on bas_tercero.id_tercero = src_alum_programa.id_tercero

LEFT join BAS_GEOPOLITICA BG
on bas_tercero.ID_UBI_NAC=BG.id_geopolitica 
LEFT join BAS_GEOPOLITICA BG2
on BG.ID_PADRE=BG2.id_geopolitica

LEFT JOIN 
OAMRA_NIV_ACTUAL_MIN   TABLA_NIVEL_ACTUAL ON TABLA_NIVEL_ACTUAL.ID_ALUM_PROGRAMA= src_alum_programa.id_alum_programa
LEFT JOIN 
OAMRA_NIV_ACTUAL_MAX   TABLA_ULTIMO_NIVEL_CURSADO ON TABLA_ULTIMO_NIVEL_CURSADO.ID_ALUM_PROGRAMA= src_alum_programa.id_alum_programa
where 
    
     src_enc_matricula.cod_periodo like '" . $anio . "%' 
           and src_enc_liquidacion.est_liquidacion=2
    and src_enc_matricula.cod_periodo not in ('" . $anio . "V','" . $anio . "C','" . $anio . "D')
    
    
	and  src_alum_programa.cod_alumno is not null
	
	
GROUP BY 

src_uni_academica.VAL_PERIODICIDAD,
src_uni_academica.DUR_PROGRAMA,
BG2.NOM_DIV_GEOPOLITICA,


BG.NOM_DIV_GEOPOLITICA,
bas_tercero.dir_email,
 upper(bas_tercero.gen_tercero),
 bas_tercero.fec_nacimiento,
    src_alum_programa.cod_alumno ,
    pri_apellido , 
    seg_apellido , 
    nom_tercero ||' '|| seg_nombre ,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') ,
    num_identificacion , 
    SUBSTR(src_alum_programa.cod_periodo,1,5) ,
    SUBSTR(src_alum_programa.cod_periodo,1,4),
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01'),
    src_alum_programa.id_alum_programa,
    bas_dependencia.num_apartado,
    bas_dependencia.id_dependencia, 
    bas_dependencia.nom_dependencia,
    bas_dependencia.COD_dependencia,
    src_uni_academica.cod_anterior ,
    src_uni_academica.cod_unidad, 
    src_uni_academica.nom_unidad, 
    src_uni_academica.cod_modalidad, 
    src_uni_academica.niv_formacion,
    src_alum_programa.est_alumno,
    src_alum_programa.cod_pensum,
          TABLA_NIVEL_ACTUAL.NIVEL,
     TABLA_ULTIMO_NIVEL_CURSADO.NIVEL
order by src_alum_programa.id_alum_programa asc, 
    src_alum_programa.cod_alumno desc, (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) desc";


            case 'query_listar_alumno':
                return "
SELECT 
max(src_enc_matricula.cod_periodo) as cod_periodo_matricula,
src_uni_academica.VAL_PERIODICIDAD,
src_uni_academica.DUR_PROGRAMA,
CASE WHEN  BG2.NOM_DIV_GEOPOLITICA ='Perú' THEN  BG.NOM_DIV_GEOPOLITICA ELSE  BG2.NOM_DIV_GEOPOLITICA END AS  DEPARTAMENTO ,

BG.NOM_DIV_GEOPOLITICA as DISTRITO,
to_char(bas_tercero.fec_nacimiento,'dd/MM/YYYY') as FEC_NAC, 
case when upper(bas_tercero.gen_tercero)='M' THEN 1 ELSE 2 END as GENERO,
upper(bas_tercero.dir_email) as email,
    TABLA_NIVEL_ACTUAL.NIVEL   AS NIVEL,
    CASE WHEN TABLA_NIVEL_ACTUAL.NIVEL IS NULL THEN TABLA_ULTIMO_NIVEL_CURSADO.NIVEL ELSE TABLA_NIVEL_ACTUAL.NIVEL END  ULTIMO_NIVEL_CURSADO,
    upper(src_alum_programa.cod_alumno) as CODALUMNO,
    pri_apellido AS apepaterno, 
    seg_apellido AS apematerno, 
    nom_tercero ||' '|| seg_nombre AS nombre,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') tipodocumento,
    num_identificacion as nrodocumento, 
    SUBSTR(src_alum_programa.cod_periodo,1,5) as ano_ing_completo,
    SUBSTR(src_alum_programa.cod_periodo,1,4) as ano_ing,
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01') as per_ing,
    src_alum_programa.id_alum_programa AS id_alum_programa,
    bas_dependencia.id_dependencia AS codfacultad, 
    bas_dependencia.nom_dependencia AS nomfacultad, 
    bas_dependencia.COD_dependencia AS abreviaturafacultad, 
    src_uni_academica.cod_unidad AS codprograma, 
    src_uni_academica.nom_unidad as nomprograma,
    src_uni_academica.cod_modalidad AS codmodalidad, 
    (select nom_tabla from src_generica where tip_tabla='TIPMOD' and cod_tabla=src_uni_academica.COD_MODALIDAD)modalidad_desc,
    src_uni_academica.NIV_FORMACION,
	(select  UPPER(nom_tabla)  from src_generica where tip_tabla='NIVFOR'  and cod_tabla=src_uni_academica.NIV_FORMACION) AS formacion,
    (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) estado,
    src_alum_programa.est_alumno cod_estado,
   upper( src_alum_programa.cod_pensum) cod_pensum,
   CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001'  ) then 999 else bas_dependencia.id_dependencia end AS cod_escuela,
    CASE WHEN UPPER(src_uni_academica.cod_unidad) in  (select DISTINCT UPPER(COD_UNIDAD) from src_mat_pensum where  UPPER(SUBSTR(src_mat_pensum.cod_materia,1,1))='T' and UPPER(COD_UNIDAD)<>'PX001'  ) then 'ETM' else bas_dependencia.COD_dependencia end AS abreviatura_escuela
FROM src_enc_matricula
LEFT JOIN src_enc_liquidacion
    ON src_enc_liquidacion.cod_periodo       = src_enc_matricula.cod_periodo
    AND src_enc_liquidacion.id_alum_programa = src_enc_matricula.id_alum_programa
INNER JOIN src_alum_programa
    ON src_enc_matricula.id_alum_programa = src_alum_programa.id_alum_programa
inner join src_mat_pensum
    on src_mat_pensum.cod_unidad = src_alum_programa.cod_unidad
    and src_mat_pensum.cod_pensum = src_alum_programa.cod_pensum
    and src_mat_pensum.cod_materia = src_enc_matricula.cod_materia
inner join src_uni_academica
    on src_uni_academica.cod_unidad = src_alum_programa.cod_unidad
inner join bas_dependencia
    on bas_dependencia.id_dependencia = src_uni_academica.id_dependencia
INNER JOIN bas_tercero
    on bas_tercero.id_tercero = src_alum_programa.id_tercero

LEFT join BAS_GEOPOLITICA BG
on bas_tercero.ID_UBI_NAC=BG.id_geopolitica 
LEFT join BAS_GEOPOLITICA BG2
on BG.ID_PADRE=BG2.id_geopolitica

LEFT JOIN 
OAMRA_NIV_ACTUAL_MIN   TABLA_NIVEL_ACTUAL ON TABLA_NIVEL_ACTUAL.ID_ALUM_PROGRAMA= src_alum_programa.id_alum_programa
LEFT JOIN 
OAMRA_NIV_ACTUAL_MAX   TABLA_ULTIMO_NIVEL_CURSADO ON TABLA_ULTIMO_NIVEL_CURSADO.ID_ALUM_PROGRAMA= src_alum_programa.id_alum_programa
where 
    
      src_enc_matricula.cod_periodo like '" . $anio . "%' 
           and src_enc_liquidacion.est_liquidacion=2
    and src_enc_matricula.cod_periodo not in ('" . $anio . "V','" . $anio . "C','" . $anio . "D')
    
    and 
	
 upper(src_alum_programa.cod_alumno)='" . $codalumno . "'
	
	
GROUP BY 

src_uni_academica.VAL_PERIODICIDAD,
src_uni_academica.DUR_PROGRAMA,
BG2.NOM_DIV_GEOPOLITICA,


BG.NOM_DIV_GEOPOLITICA,
bas_tercero.dir_email,
 upper(bas_tercero.gen_tercero),
 bas_tercero.fec_nacimiento,
    src_alum_programa.cod_alumno ,
    pri_apellido , 
    seg_apellido , 
    nom_tercero ||' '|| seg_nombre ,
    decode(tip_identificacion, 'C','1', 'P','7', 'E','4') ,
    num_identificacion , 
    SUBSTR(src_alum_programa.cod_periodo,1,5) ,
    SUBSTR(src_alum_programa.cod_periodo,1,4),
    decode(SUBSTR(src_alum_programa.cod_periodo,5,1),'1','01','2','02','3','03','6','02','7','02','4','02','5','02','8','02','9','02','A','01','B','01','C','01','D','01','E','01','F','01','T','01'),
    src_alum_programa.id_alum_programa,
    bas_dependencia.num_apartado,
    bas_dependencia.id_dependencia, 
    bas_dependencia.nom_dependencia,
    bas_dependencia.COD_dependencia,
    src_uni_academica.cod_anterior ,
    src_uni_academica.cod_unidad, 
    src_uni_academica.nom_unidad, 
    src_uni_academica.cod_modalidad, 
    src_uni_academica.niv_formacion,
    src_alum_programa.est_alumno,
    src_alum_programa.cod_pensum,
     TABLA_NIVEL_ACTUAL.NIVEL,
     TABLA_ULTIMO_NIVEL_CURSADO.NIVEL
order by src_alum_programa.id_alum_programa asc, 
    src_alum_programa.cod_alumno desc, (select nom_tabla from src_generica where tip_tabla='ESTALU' and src_generica.cod_tabla like src_alum_programa.est_alumno) desc";

            case 'ssdsdsd':
                echo "i es igual a 2";
                break;
        }
    }

}
