<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Pedido;
use App\PedidoCerrado;
use App\SolicitudSunedu;
use App\EstudianteSunedu;
use App\EstudianteSinu;
use App\Estudiante_Pedido;
use App\FacultadSunedu;
use App\EstudianteFotocheck;
use App\PedidoFotocheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Http\Controllers\Auth\PermisosController;
use Yajra\Datatables\Datatables;
use Excel;
use Imagick;
use ImagickPixel;
use Barryvdh\DomPDF\Facade as PDF;

class Fotocheck_EntregarController extends Controller {

    protected $seguridad;

    public function __construct() {
        $this->seguridad = new PermisosController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $pedido_id = null) {
        $permisos = $this->seguridad->validarPermisos($request);
        return view('backEnd.fotocheckentregar.index');
    }

    public function verEstadoFotocheck(Request $request) {
        $nrodocumento = $request->nrodocumento;
        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');
        $fotocheckEstudiante = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->select('estudiantes_fotocheck.devolucion', 'estudiantes_fotocheck.id as id', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.estado')
                        ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                        ->where('generica.tipo', 'TIPESTFOTOCHECK')
                        ->where('estudiantes_fotocheck.cod_estado_sinu', 1)
                        ->where('estudiantes_fotocheck.nrodocumento', $nrodocumento)->get();
        
        $data = json_decode(json_encode($fotocheckEstudiante), true);
        return response()->json(['status' => 'success', 'message' => 'Cambio de estado satisfactoriamente', 'data' => $data, 'error' => 0], 200);
    }

    public function entregarFotocheck(Request $request) {

        $id = $request->id;
        if (is_null($id)) {
            return response()->json(['status' => 'warning', 'message' => 'Error: No existe ID', 'data' => 'cambio de estado fallo', 'error' => 1], 200);
        }
        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');
        DB::connection('pgsql_syscarnes')->beginTransaction();
        try {
            DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->whereIn('estudiantes_fotocheck.estado', array(2, 3))
                    ->where('estudiantes_fotocheck.id', $id)
                    ->where('estudiantes_fotocheck.cod_estado_sinu', 1)
                    ->update(['estado' => 4, 'fec_entrega' => $fechaActual]);


            DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                    ->join('estudiantes_fotocheck', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                    ->where('pedidos_fotocheck_detalle.estudiante_fotocheck_id', $id)
                    ->whereIn('pedidos_fotocheck_detalle.estado', array(2, 3))
                    ->update(['estado' => 4, 'user_entrega_estudiante' => Auth::User()->username, 'fec_entrega_estudiante' => $fechaActual]);


            $estudianteFotocheck = EstudianteFotocheck::findOrFail($id);

            $pedido_id = DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                    ->select('pedidos_fotocheck_detalle.pedido_fotocheck_id as id')
                    ->where('pedidos_fotocheck_detalle.id', $estudianteFotocheck->ultimo_id_detalle_fotocheck)
                    ->first();

            $count_pedido_detalle = DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                    ->where('pedidos_fotocheck_detalle.pedido_fotocheck_id', $pedido_id->id)
                    ->whereIn('pedidos_fotocheck_detalle.estado', array(1, 2))
                    ->count();

            if ($count_pedido_detalle == 0) {
                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')->where('id', $pedido_id->id)
                        ->update(['estado' => 3]);
            }
        } catch (Exception $ex) {
            DB::connection('pgsql_syscarnes')->rollBack();
            return response()->json(['status' => 'warning', 'message' => 'Cambio de estado fallo', 'data' => 'Cambio de estado fallo', 'error' => 1], 200);
        }
        DB::connection('pgsql_syscarnes')->commit();
        return response()->json(['status' => 'success', 'message' => 'Cambio de estado satisfactoriamente', 'data' => '', 'error' => 0], 200);
    }

    public function imprimirEntregaPedido(Request $request) {
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        try {
            $pedido_id = $request->pedido_id;
            $usuario_recepciona = $request->nrodocumento_recepciona;
            //$pedido_id = 157;
            //$usuario_recepciona = '45006478';

            if (is_null($usuario_recepciona)) {
                return response()->json(['status' => 'warning', 'message' => 'Ingrese n° documento que recepciona el pedido', 'data' => '', 'error' => 1], 200);
            }


            $now = new \DateTime();
            $fechaActual_format1 = $now->format('d/m/Y H:i:s');
            $fechaActual = $now->format('Y-m-d H:i:s');
            $pedido = PedidoFotocheck::where([
                        ['id', '=', $pedido_id]
                    ])->first();

            $estudiantesfotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->select('pedidos_fotocheck.duplicado', DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.niv_formacion', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                    ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                    ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                    ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                    ->where('generica.tipo', 'TIPESTFOTOCHECK')
                    ->where('pedidos_fotocheck.id', $pedido_id)
                    ->where('pedidos_fotocheck_detalle.estado', 2)
                    ->get();

            if (count($estudiantesfotocheck) == 0) {
                return response()->json(['status' => 'warning', 'message' => 'No existen registros para procesar', 'data' => 'No existen registros para procesar', 'error' => 1], 200);
            }

            $estudiantesfotocheck = json_decode(json_encode($estudiantesfotocheck), true);
            $pdf = new \Dompdf\Dompdf();

            $html = '<style> 
            body{
            text-align:center;
                        }
            table {
               width: 100%;
               border: 1px solid #000;
               font-size:8px;
            }
            th, td {
               width: 25%;
               text-align: left;
               vertical-align: top;
               border: 1px solid #000;
               border-collapse: collapse;
               padding: 0.3em;
            }
            caption {
               padding: 0.3em;
            }
            </style>';
            $html .= '<h1 >Relación de Entrega del Pedido N° ' . $pedido_id . '</h1>';
            $html .= '<table style="width: 100%;  border: 1px solid #000;" cellspacing="0" cellpadding="0">
            <thead style="font-size:11px;text-align: center;">
                <tr>
                    <th style="width:20px;text-align: center;" >N°</th>
                    <th style="width:60px;text-align: center;" >N° Documento</th>
                    <th style="width:80px;text-align: center;">Apellido Paterno</th>
                    <th style="width:80px;text-align: center;">Apellido Materno</th>
                    <th style="width:100px;text-align: center;">Nombre</th>
                    <th style="width:100px;text-align: center;">Facultad</th>
                    <th style="width:180px;text-align: center;">Programa</th>
                    <th style="width:20px;text-align: center;">Caduca</th>
                </tr>
            </thead>
            <tbody>';

            $i = 0;
            foreach ($estudiantesfotocheck as $index => $dato) {
                $html .= '<tr>';
                $html .= '<td style="text-align:center;">' . ($index + 1) . '</td>';
                $html .= '<td style="text-align:center;">' . $dato['nrodocumento'] . '</td>';
                $html .= '<td>' . $dato['apepaterno'] . '</td>';
                $html .= '<td>' . $dato['apematerno'] . '</td>';
                $html .= '<td>' . $dato['nombre'] . '</td>';
                $html .= '<td>' . $dato['nomfacultad'] . '</td>';
                $html .= '<td>' . $dato['nomprograma'] . '</td>';
                $html .= '<td>' . $dato['fec_vencimiento'] . '</td>';

                $html .= '</tr>';
                $i++;

                if ($i >= 2500) {
                    break;
                }
            }
            $html .= '</tbody>
        </table>';


            $html .= '<br><br><br><br><br><br>';
            $html .= '<span>' . count($estudiantesfotocheck) . '</span><br>';
            $html .= '<span><i>Número de Fotochecks Entregados</i></span>';
            $html .= '<br><br><br><br><br><br>';
            $html .= '<span>N° Doc. :' . $usuario_recepciona . '</span><br>';
            $html .= '<span><i>Usuario Recepciona</i></span>';
            $pdf->set_paper("A4");

            $pdf->loadHtml($html);

            $pdf->render();
            // return $pdf->stream('nombre.pdf');
            $canvas = $pdf->get_canvas();
            $canvas->page_text(200, 810, "Usuario: " . Auth::User()->username, '', 8, array(0, 0, 0));
            $canvas->page_text(320, 810, "Usuario Recepciona: " . $usuario_recepciona, '', 8, array(0, 0, 0));
            $canvas->page_text(35, 810, "Fecha: " . $fechaActual_format1, '', 8, array(0, 0, 0));
            $canvas->page_text(520, 810, "Página: {PAGE_NUM} de {PAGE_COUNT}", '', 8, array(0, 0, 0));


            $carpeta = storage_path('app/public') . '/fotochecks/pedidos/pedido_' . $pedido_id;
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }

            \Storage::disk('public')->put('fotochecks/pedidos/pedido_' . $pedido_id . '/pedido_' . $pedido_id . '_entrega.pdf', $pdf->output());


            /* Inicio: Actualización a estado entregado */
            DB::connection('pgsql_syscarnes')->beginTransaction();
            try {

                DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                        ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                        ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                        ->where('pedidos_fotocheck.id', $pedido_id)
                        ->where('estudiantes_fotocheck.estado', 2)
                        ->update(['estado' => 3, 'user_update' => Auth::User()->username]);

                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                        ->join('estudiantes_fotocheck', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                        ->join('pedidos_fotocheck', 'pedidos_fotocheck_detalle.pedido_fotocheck_id', '=', 'pedidos_fotocheck.id')
                        ->where('pedidos_fotocheck.id', $pedido_id)
                        ->where('pedidos_fotocheck_detalle.estado', 2)
                        ->update(['estado' => 3, 'user_entrega_facultad' => Auth::User()->username, 'user_recoge_facultad' => $usuario_recepciona, 'fec_entrega_facultad' => $fechaActual]);

                DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck')
                        ->where('pedidos_fotocheck.id', $pedido_id)
                        ->where('pedidos_fotocheck.estado', 2)
                        ->update(['estado' => 3, 'fec_entrega' => $fechaActual]);
            } catch (Exception $ex) {
                DB::connection('pgsql_syscarnes')->rollBack();
                return response()->json(['status' => 'warning', 'message' => 'Cambio de estado fallo', 'data' => 'Cambio de estado fallo', 'error' => 1], 200);
            }
            DB::connection('pgsql_syscarnes')->commit();
            /* Fin: Actualización a estado entregado */
        } catch (Exception $e) {

            return response()->json(['status' => 'warning', 'message' => $e, 'error' => 0], 200);
        }

        return response()->json(['status' => 'success', 'message' => 'Generación de archivos ejecutada exitosamente', 'data' => 'todo bien', 'error' => 0], 200);
    }

    public function devolverEdit($id) {
        $EstudianteFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select(DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id as id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->where('estudiantes_fotocheck.id', $id)
                ->whereIn('estudiantes_fotocheck.estado', array(3, 4))
                ->where('estudiantes_fotocheck.devolucion', null)
                ->first();
        return view('backEnd.fotocheckentregar.devolucion', compact('EstudianteFotocheck'));
    }

    public function devolver(Request $request) {

        $id = $request->id;

        if (is_null($request->motivo_devolucion)) {
            return response()->json(['status' => 'warning', 'message' => 'Ingrese motivo de devolución', 'data' => '', 'error' => 1], 200);
        }

        $now = new \DateTime();
        $fechaActual = $now->format('Y-m-d H:i:s');

        $motivo_devolucion = $request->motivo_devolucion;
        $count_estudianteFotocheck = DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                ->select(DB::raw("to_char(estudiantes_fotocheck.fec_vencimiento,'dd/MM/YYYY') as  fec_vencimiento"), 'estudiantes_fotocheck.abreviaturafacultad', 'estudiantes_fotocheck.id as id', 'estudiantes_fotocheck.user_update', 'generica.nombre as estado_nombre', 'estudiantes_fotocheck.codalumno as codalumno', 'estudiantes_fotocheck.tipodocumento', 'estudiantes_fotocheck.nrodocumento', 'estudiantes_fotocheck.apepaterno', 'estudiantes_fotocheck.apematerno', 'estudiantes_fotocheck.nombre', 'estudiantes_fotocheck.nomfacultad', DB::raw('upper(estudiantes_fotocheck.nomprograma) as nomprograma '), 'estudiantes_fotocheck.ano_ing', 'estudiantes_fotocheck.per_ing', 'estudiantes_fotocheck.estado', 'estudiantes_fotocheck.codmodalidad', 'estudiantes_fotocheck.foto', DB::raw('upper(estudiantes_fotocheck.codprograma) as codprograma '), 'estudiantes_fotocheck.email', 'estudiantes_fotocheck.codfacultad')
                ->join('pedidos_fotocheck_detalle', 'estudiantes_fotocheck.id', '=', 'pedidos_fotocheck_detalle.estudiante_fotocheck_id')
                ->join('generica', 'estudiantes_fotocheck.estado', '=', 'generica.cod')
                ->where('generica.tipo', 'TIPESTFOTOCHECK')
                ->where('estudiantes_fotocheck.id', $id)
                ->whereIn('estudiantes_fotocheck.estado', array(3, 4))
                ->where('estudiantes_fotocheck.devolucion', null)
                ->count();

        if ($count_estudianteFotocheck > 0) {

            DB::connection('pgsql_syscarnes')->table('estudiantes_fotocheck')
                    ->where('estudiantes_fotocheck.id', $id)
                    ->whereIn('estudiantes_fotocheck.estado', array(3, 4))
                    ->where('estudiantes_fotocheck.devolucion', null)
                    ->update(['devolucion' => 1]);


            $estudianteFotocheck = EstudianteFotocheck::findOrFail($id);

            DB::connection('pgsql_syscarnes')->table('pedidos_fotocheck_detalle')
                    ->where('pedidos_fotocheck_detalle.id', $estudianteFotocheck->ultimo_id_detalle_fotocheck)
                    ->where('pedidos_fotocheck_detalle.estudiante_fotocheck_id', $id)
                    ->whereIn('pedidos_fotocheck_detalle.estado', array(3, 4))
                    ->where('pedidos_fotocheck_detalle.devolucion', null)
                    ->update(['devolucion' => 1, 'motivo_devolucion' => $motivo_devolucion, 'user_devolucion' => Auth::User()->username, 'fec_devolucion' => $fechaActual]);
        }

        return response()->json(['status' => 'success', 'message' => 'Devolución satisfactoria', 'data' => 'todo bien', 'error' => 0], 200);
    }

}
