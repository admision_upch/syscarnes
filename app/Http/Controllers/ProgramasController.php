<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ProgramaSinu;
use App\ProgramaSunedu;
use App\FacultadSinu;
use App\FacultadSunedu;
use App\Pedido;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\componentes\FormulariosController;
use Illuminate\Support\Facades\Session;
use App\PedidoCerrado;
use Illuminate\Support\Facades\Auth;

class ProgramasController extends Controller {

    public function datatableProgramasSinuSunedu(Request $request) {
        $pedido_id = $request->pedido_id;

        $programasSinuSunedu = DB::connection('pgsql_syscarnes')->select(" select * from vst_programas_sinu where codprograma in (select codprograma_sinu from estudiantes_pedido where pedido_id=" . $pedido_id . ") ");
        return Datatables::of($programasSinuSunedu)
                        ->addColumn('action', function ($data) {
                            if (!$data->codprograma_sunedu) {
                                return '<a class="btn btn-xs btn-success" href="programasinu/edit/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            } else {
                                return '<a class="btn btn-xs btn-primary" href="programasinu/edit/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            }
                        })
                        ->make(true);
    }

    public function datatableProgramasSinu(Request $request) {
        $pedido_id = $request->pedido_id;
        $programasSession = Session::get('programasString');

        if (empty($programasSession)) {
            $programasSession = "'NO EXISTE'";
        }

//      $programasSinu = DB::connection('pgsql_syscarnes')->select(" select * from vst_programas_sinu where   codprograma in  (" . $programasSession . ")");
        $programasSinu = DB::connection('pgsql_syscarnes')->select("select * from vst_programas_sinu where   codprograma in  (select codprograma_sinu from estudiantes_pedido where pedido_id=" . $pedido_id . "  and codprograma_sinu in (" . $programasSession . "))");

        return Datatables::of($programasSinu)
                        ->addColumn('action', function ($data) use ($pedido_id) {

                            $pedido = Pedido::where([
                                        ['id', '=', $pedido_id],
                                        ['eliminado', '=', null],
                                    ])->first();


                            if ($pedido->estado == '2') {
                                return '<span>Pedido Cerrado</span>';
                            }

                            $pedidocerrado = PedidoCerrado::where([
                                        ['pedido_id', '=', $pedido_id],
                                        ['user_update', '=', Auth::User()->username],
                                        ['estado', '=', '1'],
                                    ])->first();

                            if (count($pedidocerrado) > 0) {
                                return '<span>Pedido Cerrado</span>';
                            } else {
                                if (!$data->codprograma_sunedu) {
                                    return '<a class="btn btn-xs btn-success" href="programasinu/edit/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                                } else {
                                    return '<a class="btn btn-xs btn-primary" href="programasinu/edit/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                                }
                            }
                        })
                        ->make(true);
    }

    public function selectProgramasByFacultad(Request $request) {
        $pedido_id = $request->pedido_id;
        $facultad_id = $request->facultad_id;
        $pedido = Pedido::findOrFail($pedido_id);
        $programasSession = Session::get('programasString');
        //  dd($programasSession);
        //$programas = DB::connection('pgsql_syscarnes')->select("select distinct codprograma as cod, nomprograma as name from vst_programas_sinu where  codfacultad='" . $facultad_id . "' and codprograma in  (" . $programasSession . ")");
        $programas = DB::connection('pgsql_syscarnes')->select("select distinct codprograma as cod, nomprograma as name from vst_programas_sinu where  codfacultad='" . $facultad_id . "'");

        $formularios = new FormulariosController();
        $selectProgramas = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id', '0');
        return $selectProgramas;
    }

    public function fotocheckSelectProgramas(Request $request) {
        $facultad_id = $request->facultad_id;
        $programasSession = Session::get('programasString');
        $programas = DB::connection('pgsql_syscarnes')->select("select distinct codprograma as cod, nomprograma as name from vst_facultades_programas_fotocheck where  codfacultad='" . $facultad_id . "'");
        $formularios = new FormulariosController();
        $selectProgramas = $formularios->selectSimpleNoLabelDefaultTodos($programas, 'programa_id', '0');
        return $selectProgramas;
    }

    public function editProgramaSinu($codfacultad, $codprograma) {
        $programaSinu = ProgramaSinu::where([
                    ['codfacultad', '=', $codfacultad],
                    ['codprograma', '=', $codprograma],
                ])->first();


        $facultadSinu = FacultadSinu::where([
                    ['codfacultad', '=', $codfacultad],
                    ['codmodalidad', '=', $programaSinu->codmodalidad],
                    ['niv_formacion', '=', $programaSinu->niv_formacion],
                ])->first();


        $facultadSunedu = FacultadSunedu::where([
                    ['codfacultad', '=', $facultadSinu->codfacultad_sunedu]
                ])->first();
        if (!$facultadSunedu) {
            $mensaje = "Por favor primero relacionar Facultad SINU con Facultad SUNEDU";
            $tipoalerta = "alert-warning";
            return view('layouts.mensaje', compact('mensaje', 'tipoalerta'));
        }

        $programaSunedu = DB::connection('pgsql_syscarnes')->select("select codprograma as cod, nomprograma as name from programas_sunedu where codfacultad in ('" . $facultadSinu->codfacultad_sunedu . "') ");

        $formularios = new FormulariosController();


        //CAMBIARSUNEDU


        $selectProgramaSunedu = $formularios->selectSimple($programaSunedu, 'Programa SUNEDU', 'select_programasunedu', $programaSinu->codprograma_sunedu);

        return view('backEnd.pedidos.edit_programasinu', compact('programaSinu', 'selectProgramaSunedu', 'facultadSunedu'));
    }

    public function updateProgramaSinu(Request $request) {

        $programaSinu = ProgramaSinu::where([
                    ['codfacultad', '=', $request->codfacultad],
                    ['codprograma', '=', $request->codprograma],
                ])->first();


        if (!$programaSinu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra una Programa con ese código.'])], 404);
        }

        $programaSunedu = ProgramaSunedu::where([
                    ['codprograma', '=', $request->select_programasunedu],
                ])->first();


        $codProgramaSunedu = $request->select_programasunedu;
        $nomProgramaSunedu = $programaSunedu->nomprograma;

        /* if ($request->method() === 'PATCH') {
          // Creamos una bandera para controlar si se ha modificado algún dato en el método PATCH.
          $bandera = false;
          // Actualización parcial de campos.
          if ($codProgramaSunedu) {
          $programaSinu->select_Programasunedu = $codProgramaSunedu;
          $bandera = true;
          }
          if ($bandera) {
          // Almacenamos en la base de datos el registro.
          $programaSinu->save();
          return response()->json(['status' => 'ok', 'data' => $programaSinu], 200);
          } else {
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 304 Not Modified – [No Modificada] Usado cuando el cacheo de encabezados HTTP está activo
          // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
          return response()->json(['errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato.'])], 304);
          }
          } */

        if (!$codProgramaSunedu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }

        $programaSinu->codprograma_sunedu = $codProgramaSunedu;
        $programaSinu->nomprograma_sunedu = $nomProgramaSunedu;

        $programaSinu->save();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => $programaSinu], 200);
    }

    public function datatableProgramasSunedu() {
        $programasSunedu = ProgramaSunedu::all();
        return Datatables::of($programasSunedu)->make(true);
    }

}
