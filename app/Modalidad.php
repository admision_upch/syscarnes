<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modalidad extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modalidades';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function facultades() {
        return $this->hasMany('App\Facultad');
    }
    public function programas() {
        return $this->hasMany('App\Programa');
    }
}
