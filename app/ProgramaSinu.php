<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaSinu extends Model {

    protected $connection = 'pgsql_syscarnes';
    protected $table = 'programas_sinu';
 //   protected $primaryKey = 'codprograma';
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array('codprograma_sunedu', 'nomprograma_sunedu');

}
