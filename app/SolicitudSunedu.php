<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudSunedu extends Model
{
  protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'solicitudes_sunedu';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nrosolicitud'];

   /* use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function opciones() {
        return $this->hasMany('App\Opcion');
    }

    public function perfiles() {
        return $this->hasMany('App\Perfile');
    }*/
}
