<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante_Pedido extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'pgsql_syscarnes';
        
    protected $table = 'estudiantes_pedido';
       

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['estado'];

}
