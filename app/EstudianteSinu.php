<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteSinu extends Model
{
  protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'estudiantes_sinu';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pedido_id','codalumno','carrera','tipodocumento','nrodocumento','apepaterno','apematerno','nombre','estado'];
}
