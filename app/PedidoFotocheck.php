<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoFotocheck extends Model {

    protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pedidos_fotocheck';
    protected $dates = ['fec_create','fec_solicita','fec_genera','fec_entrega'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_create','estado','fec_create','fec_solicita','fec_genera','fec_entrega','duplicado'];

   /* use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function opciones() {
        return $this->hasMany('App\Opcion');
    }

    public function perfiles() {
        return $this->hasMany('App\Perfile');
    }*/

}
