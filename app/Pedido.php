<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model {

    protected $connection = 'pgsql_syscarnes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pedidos';
    protected $dates = ['fec_inicio','fec_fin','fec_importacion_estudiantes','fec_filtracion_estudiantes'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'fec_inicio', 'fec_fin','fec_importacion_estudiantes', 'fec_solicitud','fec_filtracion_estudiantes'];

   /* use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function opciones() {
        return $this->hasMany('App\Opcion');
    }

    public function perfiles() {
        return $this->hasMany('App\Perfile');
    }*/

}
