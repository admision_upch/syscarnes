<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */




Route::get('/', function () {
    return redirect('/login');
})->middleware('auth');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('layout', 'Auth\LayoutController@menu')->middleware('auth')->name('menu');

Auth::routes();





Route::group(['middleware' => ['auth']], function () {

Route::get('autocomplete_persona',array('as'=>'search','uses'=>'HomeController@search'));
Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'HomeController@autocomplete'));

Route::get('buscarpersona/{tipodocumento}/{nrodocumento}','EstudiantesController@buscarPersona')->name('buscarpersona');

});



Route::group(['middleware' => ['auth', 'accesocontrolador']], function () {




    Route::get('/pedidos/facultad/{codfacultad}', 'PedidosController@indexFacultades');
    Route::get('/pedidos/facultadsinu/edit/{id}', 'FacultadesController@editFacultadSinu');
    Route::get('/pedidosgestionar/open/facultadsinu/edit/{id}', 'FacultadesController@editFacultadSinu');
    Route::put('/pedidos/facultadsinu/update', 'FacultadesController@updateFacultadSinu')->name('pedidos.facultad.update');
    Route::get('/pedidos/programasinu/edit/{codfacultad}/{codprograma}', 'ProgramasController@editProgramaSinu');
    Route::get('/pedidosgestionar/open/programasinu/edit/{codfacultad}/{codprograma}', 'ProgramasController@editProgramaSinu');
    Route::put('/pedidos/programasinu/update', 'ProgramasController@updateProgramaSinu')->name('pedidos.programa.update');
    Route::get('/pedidos/estudiantes/programasunedu/edit/{estudiante_pedido_id}', 'EstudiantesController@editProgramaSuneduEstudiantes');
    Route::put('/pedidos/estudiantes/programasunedu/update', 'EstudiantesController@updateProgramaSuneduEstudiantes')->name('pedidos.estudiante.programa.update');
    Route::post('/pedidos/procesar/solicitudmasiva', 'PedidosController@procesarSolicitudMasiva')->name('pedidos.procesar.solicitudmasiva');





    Route::post('/pedidos/datatable/resumenpedido', 'PedidosController@datatableResumenPedido')->name('datatable.resumenpedido');
    Route::post('/pedidos/datatable/estudiantesentregacarne/', 'EstudiantesController@datatableEstudiantesEntregaCarne')->name('datatable.estudiantes.entregacarne');
    Route::post('/pedidos/estudiante/cambiarestadoentregado', 'EstudiantesController@cambiarEstadoEntregado')->name('cambiarestado.entregado');
    Route::get('/pedidos/datatable/solicitudessunedu/', 'PedidosController@datatableSolicitudesSuneduVisualiza')->name('datatable.solicitudes.sunedu.visualiza');
    Route::post('/pedidos/ver/estudiantessunedu/', 'PedidosController@verEstudiantesSunedu')->name('pedidos.open.paso02.ver.estudiantessunedu');
    Route::post('/pedidos/open/paso02/datatable/estudiantessunedu/', 'EstudiantesController@datatableEstudiantesSunedu')->name('pedidos.datatable.estudiantes.sunedu');

    /* Inicio: Pedidos Gestionar */
    Route::get('/pedidosgestionar/', 'Pedidos_GestionarController@index')->name('pedidosgestionar');
    Route::get('/pedidosgestionar/create', 'Pedidos_GestionarController@create')->name('pedidosgestionar.create');
    Route::post('/pedidosgestionar', 'Pedidos_GestionarController@store')->name('pedidosgestionar.store');
    Route::get('/pedidosgestionar/edit/{pedido_id}', 'Pedidos_GestionarController@edit')->name('pedidosgestionar.edit');
    Route::patch('/pedidosgestionar/update/{id}', 'Pedidos_GestionarController@update')->name('pedidosgestionar.update');
    Route::get('/pedidosgestionar/open/{id}', 'Pedidos_GestionarController@open')->name('pedidosgestionar.open');
    Route::post('/pedidosgestionar/cambioestadopedido', 'Pedidos_GestionarController@cambioEstadoPedido')->name('cambio.estado.pedido');
    Route::post('/pedidosgestionar/selectestadopedido', 'Pedidos_GestionarController@selectPedidoEstado')->name('select.estado.pedido');


    Route::post('/pedidosgestionar/open/paso01', 'Pedidos_GestionarController@openPaso01')->name('pedidosgestionar.open.paso01');
    Route::post('/pedidosgestionar/open/paso01/datatable/estudiantes/', 'EstudiantesController@datatableEstudiantesSinu')->name('datatable.estudiantes.sinu');
    Route::post('/pedidosgestionar/open/paso01/generarcodalumnosinu', 'Pedidos_GestionarController@openPaso01GenerarCodigosAlumnosSinu')->name('pedidosgestionar.open.paso01.generarcodalumnosinu');
    Route::post('/pedidosgestionar/open/paso01/insertaralumnossinu', 'Pedidos_GestionarController@openPaso01InsertarAlumnosSinu')->name('pedidosgestionar.open.paso01.insertaralumnossinu');

    Route::post('/pedidosgestionar/open/paso02', 'Pedidos_GestionarController@openPaso02')->name('pedidosgestionar.open.paso02');

    Route::post('/pedidosgestionar/open/paso02/ver/estudiantessunedu/', 'Pedidos_GestionarController@verEstudiantesSunedu')->name('pedidosgestionar.open.paso02.ver.estudiantessunedu');
    Route::post('/pedidosgestionar/open/paso02/datatable/estudiantessunedu/', 'EstudiantesController@datatableEstudiantesSunedu')->name('datatable.estudiantes.sunedu');
    Route::get('/pedidosgestionar/open/paso02/datatable/solicitudessunedu/', 'Pedidos_GestionarController@datatableSolicitudesSunedu')->name('datatable.solicitudes.sunedu');
    Route::get('/pedidosgestionar/open/paso02/create/solicitud', 'Pedidos_GestionarController@createSolicitud')->name('pedidosgestionar.open.paso02.create.solicitud');
    Route::post('/pedidosgestionar/open/paso02/store/solicitud', 'Pedidos_GestionarController@storeSolicitud')->name('pedidosgestionar.open.paso02.store.solicitud');
    Route::post('/pedidosgestionar/open/paso02/destroy/solicitud', 'Pedidos_GestionarController@destroySolicitud')->name('pedidosgestionar.open.paso02.destroy.solicitud');

    Route::post('/pedidosgestionar/open/paso02/subirarchivo/estudiantessunedu', 'Pedidos_GestionarController@subirarchivoEstudiantesSunedu')->name('pedidosgestionar.open.paso02.subirarchivo.estudiantessunedu');
    Route::get('/pedidosgestionar/open/paso02/importar/estudiantessunedu', 'Pedidos_GestionarController@importarEstudiantesSunedu')->name('pedidosgestionar.open.paso02.importar.estudiantessunedu');
    Route::get('/pedidosgestionar/open/paso02/importar/facultadessunedu', 'Pedidos_GestionarController@importarFacultadesSunedu')->name('pedidosgestionar.open.paso02.importar.facultadessunedu');
    Route::get('/pedidosgestionar/open/paso02/importar/programassunedu', 'Pedidos_GestionarController@importarProgramasSunedu')->name('pedidosgestionar.open.paso02.importar.programassunedu');

    Route::post('/pedidosgestionar/open/paso03', 'Pedidos_GestionarController@openPaso03')->name('pedidosgestionar.open.paso03');
    Route::post('/pedidosgestionar/open/paso03/subirarchivo/archivosunedu', 'Pedidos_GestionarController@subirArchivoSunedu')->name('pedidosgestionar.open.paso02.subirarchivo.archivosunedu');
    Route::get('/pedidosgestionar/open/paso03/datatable/facultadessunedu/', 'FacultadesController@datatableFacultadesSunedu')->name('datatable.facultades.sunedu');
    Route::get('/pedidosgestionar/open/paso03/datatable/programassunedu/', 'ProgramasController@datatableProgramasSunedu')->name('datatable.programas.sunedu');

    Route::post('/pedidosgestionar/open/paso04', 'Pedidos_GestionarController@openPaso04')->name('pedidosgestionar.open.paso04');
    Route::post('/pedidosgestionar/open/paso04/datatable/estudiantespedido/', 'EstudiantesController@datatableEstudiantesPedido')->name('datatable.estudiantes.pedido');

    Route::post('/pedidosgestionar/open/paso04/filtrarestudiantespedido', 'Pedidos_GestionarController@filtrarEstudiantesPedido')->name('pedidosgestionar.open.paso04.filtrarestudiantespedido');
    Route::post('/pedidosgestionar/open/paso04/datatable/facultadessinusunedu', 'FacultadesController@datatableFacultadesSinuSunedu')->name('datatable.facultadessinusunedu');
    Route::post('/pedidosgestionar/open/paso04/datatable/programassinusunedu', 'ProgramasController@datatableProgramasSinuSunedu')->name('datatable.programassinusunedu');

    Route::get('/pedidosgestionar/filtrar/pedido/{pedido_id}/imagenes', 'Pedidos_GestionarController@cargarImagenes');
    /* Fin: Pedidos Gestionar */




    Route::get('/home/', 'HomeController@index')->name('inicio');
    Route::post('/home/detalle', 'HomeController@detalle')->name('home.detalle.carnes.universitarios');


    // Route::resource('pedidos', 'PedidosController');




    Route::resource('sistemas', 'SistemasController');
    Route::resource('facultades', 'FacultadesController');
    //Route::resource('programas', 'ProgramasController');
    Route::resource('perfiles', 'PerfilesController');
    Route::resource('modalidades', 'ModalidadesController');
    Route::resource('formaciones', 'FormacionesController');


    Route::post('facultades/facultadesselect', 'FacultadesController@selectByModalidadAndFormacion')->name('facultades_select');

    Route::get('sistemas/{id}/opciones', 'SistemasController@opciones')->name('opciones');
    Route::get('sistemas/{id}/opciones_tree', 'SistemasController@opciones_tree')->name('opciones_tree');
    Route::post('sistemas/opciones_tree_crud_create_node', 'SistemasController@opciones_tree_crud_create_node')->name('opciones_tree_crud_create_node');
    Route::post('sistemas/opciones_tree_crud_rename_node', 'SistemasController@opciones_tree_crud_rename_node')->name('opciones_tree_crud_rename_node');
    Route::post('sistemas/opciones_tree_crud_delete_node', 'SistemasController@opciones_tree_crud_delete_node')->name('opciones_tree_crud_delete_node');

    Route::resource('users', 'UsersController');
    Route::get('users/{id}/perfiles', 'UsersController@perfiles');

    Route::get('perfiles/{id}/accesos', 'PerfilesController@accesos')->name('accesos');
    Route::get('perfiles/{id}/accesos_tree', 'PerfilesController@accesos_tree')->name('accesos_tree');
    Route::post('perfiles/accesos_tree_crud_change_node', 'PerfilesController@accesos_tree_crud_change_node')->name('accesos_tree_crud_change_node');
    Route::post('perfiles/asignarperfil', 'Users_PerfilesController@asignarPerfil')->name('asignarperfil');
    Route::post('perfiles/perfilesselect', 'PerfilesController@selectPerfilesFindSistemaId')->name('perfiles_select');
    Route::get('perfiles/datatableperfilesasignados/{user_id}', 'Users_PerfilesController@datatablePerfilesAsignados')->name('datatable.perfilesasignados');
    Route::post('perfiles/asignarperfil_delete', 'Users_PerfilesController@asignarperfil_delete')->name('asignarperfil_delete');



    Route::get('/pedidos', 'PedidosController@index');
    Route::get('/pedidos/{id}', 'PedidosController@detalle')->name('detallepedido');



    Route::get('/pedidosprocesar', 'Pedidos_ProcesarController@index');








    /* INICIO: Rutas para Pedidos */
    Route::post('/pedidos/facultadessinu/datatable', 'FacultadesController@datatableFacultadesSinu')->name('datatable.facultadessinu');
    Route::post('/pedidos/programassinu/datatable', 'ProgramasController@datatableProgramasSinu')->name('datatable.programassinu');
    Route::post('/pedidos/estudiantes/datatable/', 'EstudiantesController@datatableEstudiantes')->name('datatable.estudiantes');
    Route::post('/pedidos/cerrarpedido/', 'PedidosController@cerrarPedido')->name('cerrarpedido');
    Route::get('/pedidos/downloadExcelConfirmados/{pedido_id}', 'PedidosController@downloadExcelConfirmados')->name('downloadExcelConfirmados');
    Route::post('/pedidos/programassinu/programasselect', 'ProgramasController@selectProgramasByFacultad')->name('programas_select');
    Route::post('/pedidos/estudiante/cambiarestado', 'EstudiantesController@cambiarEstado')->name('cambiarestado');
    /* FIN: Rutas para Pedidos */






    Route::get('viewArchivosGenerados', 'PedidosController@viewArchivosGenerados');




    Route::post('/pedidosprocesar/validarpedidos', 'Pedidos_ProcesarController@validarPedidos')->name('procesarpedido');
    Route::post('/pedidosprocesar/verarchivosgenerados', 'Pedidos_ProcesarController@viewArchivosGenerados')->name('procesarpedido.verarchivosgenerados');
    Route::get('/pedidosprocesar/datatable/', 'Pedidos_ProcesarController@datatablePedidos')->name('datatable.pedidos');
    Route::get('/pedidosprocesar/extranjeros/{pedido_id}', 'Pedidos_ProcesarController@extranjeros')->name('extranjeros');
    Route::get('/pedidosprocesar/extranjeros/edit/{pedido_id}', 'Pedidos_ProcesarController@extranjerosEdit')->name('extranjeros.edit');
    Route::get('/pedidosprocesar/extranjeros/datatable/{pedido_id}', 'Pedidos_ProcesarController@datatableExtranjeros')->name('datatable.extranjeros');
    Route::put('/pedidosprocesar/extranjeros/update', 'Pedidos_ProcesarController@extranjerosUpdate')->name('extranjeros.update');

    Route::get('/pedidosprocesar/reportebyusuarios/{pedido_id}', 'Pedidos_ProcesarController@reporteByUsuarios');

    Route::get('/pedidosprocesar/download/extranjeros/{pedido_id}', 'Pedidos_ProcesarController@downloadEstudiantesExtranjeros')->name('download.extranjeros');
    Route::post('/pedidosprocesar/subirarchivo/extranjeros', 'Pedidos_ProcesarController@subirArchivoEstudiantesExtranjeros')->name('pedidosprocesar.subirarchivo.extranjeros');
    Route::get('/pedidosprocesar/actualizar/extranjeros', 'Pedidos_ProcesarController@actualizarEstudiantesExtranjeros');





    //  Fotocheck


    Route::get('/fotocheckgestionarpedido', 'Fotocheck_Pedidos_GestionarController@index')->name('fotocheckgestionar.index');
    Route::get('/fotocheckgestionarpedido/detalle/{pedido_id}', 'Fotocheck_Pedidos_GestionarController@detalle')->name('fotocheckgestionarpedido.detalle');
    Route::post('/fotocheckgestionarpedido/datatable/estudiantesfotocheck', 'EstudiantesController@datatableEstudiantesFotocheck')->name('datatable.estudiantes.fotocheck');
    Route::get('/fotocheckgestionarpedido/create/{duplicado}', 'Fotocheck_Pedidos_GestionarController@create')->name('fotocheckgestionarpedido.create');
    Route::post('/fotocheckgestionarpedido/cambiarestado', 'Fotocheck_Pedidos_GestionarController@cambiarEstado')->name('fotocheckgestionarpedido.cambiarestado');
    Route::get('/fotocheckgestionarpedido/estudiantesfotocheck/edit/{id}', 'EstudiantesController@editarEstudianteFotocheck')->name('fotocheckgestionarpedido.estudiantefotocheck.editar');
    Route::put('/fotocheckgestionarpedido/estudiantesfotocheck/update', 'EstudiantesController@updateEstudianteFotocheck')->name('fotocheckgestionarpedido.estudiantefotocheck.update');
    Route::post('/fotocheckgestionarpedido/procesar/solicitudmasiva', 'Fotocheck_Pedidos_GestionarController@procesarSolicitudMasiva')->name('fotocheckgestionarpedido.procesar.solicitudmasiva');
    Route::post('/fotocheckgestionarpedido/solicitar/pedido', 'Fotocheck_Pedidos_GestionarController@solicitarPedido')->name('fotocheckgestionarpedido.solicitar.pedido');
    Route::post('/fotocheckgestionarpedido/imprimir/pedido', 'Fotocheck_Pedidos_GestionarController@imprimirPedido')->name('fotocheckgestionarpedido.imprimir.pedido');
    Route::post('/fotocheckgestionarpedido/verarchivosgenerados', 'Fotocheck_Pedidos_GestionarController@viewArchivosGenerados')->name('fotocheckgestionarpedido.verarchivosgenerados');
    Route::post('/fotocheckgestionarpedido/programasselect', 'ProgramasController@fotocheckSelectProgramas')->name('fotocheck.programas_select');
    Route::post('/fotocheckgestionarpedido/cambiar/estado/generado', 'Fotocheck_Pedidos_GestionarController@cambiarEstadoGenerado')->name('fotocheckgestionarpedido.cambiar.estado.generado');
//    Route::post('/fotocheckgestionarpedido/cambiar/estado/entregadofacultad', 'Fotocheck_Pedidos_GestionarController@cambiarEstadoEntregadoFacultad')->name('fotocheckgestionarpedido.cambiar.estado.entregadofacultad');
    Route::get('/fotocheckgestionarpedido/personaexterna/create/{pedido_id}', 'Fotocheck_Pedidos_GestionarController@createPersonaExterna')->name('fotocheckgestionarpedido.create.personaexterna');
    Route::post('/fotocheckgestionarpedido/personaexterna/store', 'Fotocheck_Pedidos_GestionarController@storePersonaExterna')->name('fotocheckgestionarpedido.store.personaexterna');


    Route::get('/fotochecksincronizar', 'Fotocheck_SincronizarController@index')->name('fotochecksincronizar.index');
    Route::post('/fotochecksincronizar/imagenes', 'Fotocheck_SincronizarController@sincronizarImagenes')->name('fotochecksincronizar.imagenes');
    Route::post('/fotochecksincronizar/generarcodigossinu', 'Fotocheck_SincronizarController@generarCodigosEstudiantesSinu')->name('fotochecksincronizar.generarcodigossinu');
    Route::post('/fotochecksincronizar/estudiantes', 'Fotocheck_SincronizarController@sincronizarEstudiantes')->name('fotochecksincronizar.estudiantes');
    Route::post('/fotochecksincronizar/datatable/estudiantes/', 'EstudiantesController@datatableSincronizarEstudiantesFotocheck')->name('fotochecksincronizar.datatable.estudiantes');



    Route::post('/fotocheckentregar/ver/estado', 'Fotocheck_EntregarController@verEstadoFotocheck')->name('fotocheckentregar.ver.estado');
    Route::post('/fotocheckentregar/entregar', 'Fotocheck_EntregarController@entregarFotocheck')->name('fotocheckentregar.entregar');
    Route::get('/fotocheckentregar', 'Fotocheck_EntregarController@index')->name('fotocheckentregar.index');

    Route::get('/fotocheckentregar/devolver/edit/{id}', 'Fotocheck_EntregarController@devolverEdit')->name('fotocheckentregar.devolver.edit');
    Route::post('/fotocheckentregar/devolver/update', 'Fotocheck_EntregarController@devolver')->name('fotocheckentregar.devolver.update');

    Route::post('/fotocheckentregar/imprimir/pedido', 'Fotocheck_EntregarController@imprimirEntregaPedido')->name('fotocheckentregar.imprimir.pedido');
    
    
});


