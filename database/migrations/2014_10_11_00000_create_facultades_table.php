<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultadesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('facultades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dependencia');
            $table->string('cod_dependencia');
            $table->string('name');
            $table->char('estado', 1)->nullable()->default('1');
            $table->char('eliminado', 1)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('modalidad_id')->unsigned();
            $table->foreign('modalidad_id')->references('id')->on('modalidades');
            $table->integer('formacion_id')->unsigned();
            $table->foreign('formacion_id')->references('id')->on('formaciones');
           // $table->primary(['id_dependencia','modalidad_id','formacion_id']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('facultades');
    }

}
