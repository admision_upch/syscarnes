<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('opciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('enlace')->nullable();
            $table->string('destino')->nullable();
            $table->string('nivel')->nullable();
            $table->string('tipoenlace')->nullable();
            $table->integer('padre')->nullable();
            $table->integer('orden')->nullable();
            $table->string('imagen')->nullable();
            $table->boolean('visiblemenu')->default(true);
            
            
            $table->char('estado', 1)->nullable()->default('1');
            $table->char('eliminado', 1)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('sistema_id')->unsigned();
            $table->foreign('sistema_id')->references('id')->on('sistemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('opciones');
    }

}
